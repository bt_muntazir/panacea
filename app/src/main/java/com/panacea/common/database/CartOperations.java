package com.panacea.common.database;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.panacea.R;
import com.panacea.common.interfaces.CartUpdate;

/**
 * Created by Braintech on 20-12-2017.
 */

public class CartOperations {

    public static String TYPE_ADD_ITEM = "add";
    public static String TYPE_SUB_ITEM = "sub";
    public static String TYPE_ADD_MULTIPLE_PROUCT = "add_multiple_product";

    static CartItem cartItems = null;

    static DBHelper dbHelper = null;

    static int qty = 0;


    public static void updateDataBase(Context context, CartItem products, boolean isForAddOnly, String type, int maxQty, CartUpdate cartUpdate) {


        dbHelper = new DBHelper(context);
        qty = 0;
        try {
            qty = dbHelper.countObject(products.getProductId());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }


        if (type.equals(TYPE_ADD_ITEM)) {

            if (qty < 99) {
                cartItems = getData(qty + 1, /*variants,*/ products);

                insertUpdate(context, isForAddOnly,/* qty,*/ cartItems, dbHelper);

                cartUpdate.onCartUpdate(TYPE_ADD_ITEM, qty + 1);
            }


        } else if (type.equals(TYPE_ADD_MULTIPLE_PROUCT)) {
            if (qty < 99) {
                products.setProduct_quantity(products.getProduct_quantity());
                insertUpdate(context, isForAddOnly, products, dbHelper);

                cartUpdate.onCartUpdate(TYPE_ADD_ITEM, products.getProduct_quantity());
            }
        } else {//decrease qty
            if (qty == 1) {
                //remove item
                openDialog(products, isForAddOnly, cartUpdate, context);

            } else {

                cartItems = getData(qty - 1,/* variants,*/ products);
                insertUpdate(context, isForAddOnly,/* qty,*/ cartItems, dbHelper);
                cartUpdate.onCartUpdate(TYPE_SUB_ITEM, qty - 1);
                // }
            }
        }
    }

    private static void openDialog(final CartItem products, final boolean isForAddOnly, final CartUpdate cartUpdate, final Context context) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        // Setting Dialog Message
        alertDialog.setMessage(context.getString(R.string.confirmation_remove_cart_item));
        alertDialog.setCancelable(true);

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

                //remove item
                cartItems = getData(qty - 1,/* variants,*/ products);
                insertUpdate(context, isForAddOnly,/* qty,*/ cartItems, dbHelper);
                cartUpdate.onCartUpdate(TYPE_SUB_ITEM, qty - 1);
            }
        });
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    public static CartItem getData(int quantity, /*Variants variants,*/ CartItem products) {
        CartItem cartItems = null;

        cartItems = new CartItem();

        cartItems.setId(products.getId());
        cartItems.setProductId(products.getProductId());
        cartItems.setProductImage(products.getProductImage());
        cartItems.setProductName(products.getProductName());
        cartItems.setProduct_quantity(quantity);
        cartItems.setProductmodel(products.getProductmodel());
        cartItems.setProductprice(products.getProductprice());
        cartItems.setProductTax(products.getProductTax());
        cartItems.setProduct_discount(products.getProduct_discount());

        return cartItems;
    }


    private static void insertUpdate(Context context, boolean isForAddOnly, CartItem cartItems, DBHelper dbHelper) {
        // Adding data to database
        boolean isProductAvailable = dbHelper.isProductAvailable(cartItems.getProductId());

        Log.e("Product Available: ", "" + isProductAvailable);
        if (!isProductAvailable) {
            dbHelper.insertToCart(cartItems);

           dbHelper.extractDatabase();
            // Toast.makeText(context, context.getString(R.string.insert_product_message), Toast.LENGTH_SHORT).show();
        } else {

            dbHelper.updateCart(cartItems.getProduct_quantity(),cartItems.getProductId());

            dbHelper.extractDatabase();
            /*if (cartItems.getProduct_quantity() == 0) {//delete when qty ==0
                dbHelper.deleteCartItem(cartItems.getProductId());
            } else {
                if (!isForAddOnly)

            }*/
        }
    }
}
