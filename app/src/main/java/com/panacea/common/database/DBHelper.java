package com.panacea.common.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

/**
 * Created by Braintech on 20-12-2017.
 */

public class DBHelper extends SQLiteOpenHelper {

    public static String DATABASE_NAME = "panacea_db2";
    private static final int DATABASE_VERSION = 2;

    public static String TABLE_CART_ITEM = "table_cart";
    public static String TABLE_CART_ITEM_QUANTITY = "table_cart_qty";


    //Column Table cart item
    public static String COLUMN_ID = "id";

    public static String COLUMN_CART_PRODUCT_ID = "pId";
    public static String COLUMN_CART_PRODUCT_IMAGE = "image";
    public static String COLUMN_CART_PRODUCT_NAME = "pname";
    public static String COLUMN_CART_PRODUCT_MODEL = "pmodel";
    public static String COLUMN_CART_PRODUCT_PRICE = "pprice";
    public static String COLUMN_CART_PRODUCT_TAX = "ptax";
    public static String COLUMN_CART_PRODUCT_QUANTITY = "pquantity";
    public static String COLUMN_CART_PRODUCT_DISCOUNT = "pdiscount";


    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(CREATE_TABLE_CART_ITEM);
        sqLiteDatabase.execSQL(CREATE_TABLE_CART_PRODUCT_QTY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXIST " + TABLE_CART_ITEM);
        sqLiteDatabase.execSQL("DROP TABLE IF EXIST " + CREATE_TABLE_CART_PRODUCT_QTY);
        onCreate(sqLiteDatabase);
    }


    public String CREATE_TABLE_CART_ITEM = "create table " + TABLE_CART_ITEM + " (" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_CART_PRODUCT_ID + "  TEXT,  " +
            COLUMN_CART_PRODUCT_NAME + " TEXT " +
            " ) ";

    public String CREATE_TABLE_CART_PRODUCT_QTY = "create table " + TABLE_CART_ITEM_QUANTITY + " (" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_CART_PRODUCT_ID + "  TEXT,  " +
            COLUMN_CART_PRODUCT_QUANTITY + " INTEGER " + ")";


    public int getCartCount() {
        int count = 0;

        String countQuery = "SELECT  * FROM " + TABLE_CART_ITEM;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        count = cursor.getCount();
        cursor.close();

        // return count
        return count;
    }

    //------------------------------------------Cart Item list----------------------------


    public void insertToCart(CartItem cartItems) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(COLUMN_CART_PRODUCT_ID, cartItems.getProductId());
        //  values.put(COLUMN_CART_PRODUCT_IMAGE, cartItems.getProductImage());
        values.put(COLUMN_CART_PRODUCT_NAME, cartItems.getProductName());
        //  values.put(COLUMN_CART_PRODUCT_MODEL, cartItems.getProductmodel());
        //   values.put(COLUMN_CART_PRODUCT_TAX, cartItems.getProductTax());

        sqLiteDatabase.insert(TABLE_CART_ITEM, null, values);

        Log.e("Insert to cart", " Muntazir");

        insertDataInCartQty(cartItems);
    }

    //--------------------------------Table operation cart product qty------------------------
    public void insertDataInCartQty(CartItem cartItems) {

        // cartItems.setProduct_discount(0.00);

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(COLUMN_CART_PRODUCT_ID, cartItems.getProductId());
        //values.put(COLUMN_CART_PRODUCT_PRICE, cartItems.getProductprice());
        Log.e("Insert to cart quantity", ""+cartItems.getProduct_quantity());
        values.put(COLUMN_CART_PRODUCT_QUANTITY, cartItems.getProduct_quantity());
        //values.put(COLUMN_CART_PRODUCT_DISCOUNT, cartItems.getProduct_discount());

        Log.e("Insert to cart quantity", " Muntazir");

        sqLiteDatabase.insert(TABLE_CART_ITEM_QUANTITY, null, values);
    }

    private String getProductTotalPrice(Integer itemQuantity, Double itemDiscountPrice) {
        String totalPrice;

        totalPrice = String.valueOf(itemQuantity * itemDiscountPrice);

        return totalPrice;
    }

    public int countObject(String pId/*, int vId*/) {
        SQLiteDatabase db = getWritableDatabase();
        String selectString = "SELECT " + COLUMN_CART_PRODUCT_QUANTITY + " FROM " + TABLE_CART_ITEM_QUANTITY + " WHERE " + COLUMN_CART_PRODUCT_ID + " =? ";

        // Add the String you are searching by here.
        // Put it in an array to avoid an unrecognized token error
        Cursor cursor = db.rawQuery(selectString, new String[]{pId/*, String.valueOf(vId)*/});
        int count = 0;
        // boolean hasObject = false;
        if (cursor.moveToFirst()) {
            //hasObject = true;

            //region if you had multiple records to check for, use this region.
            count = cursor.getInt(cursor.getColumnIndex(COLUMN_CART_PRODUCT_QUANTITY));

            while (cursor.moveToNext()) {

            }
        }

        cursor.close();          // Dont forget to close your cursor
        db.close();              //AND your Database!
        return count;
    }

    public ArrayList<CartItem> getCartItem() {
        ArrayList<CartItem> listCartItem = new ArrayList<>();

        SQLiteDatabase db = this.getReadableDatabase();

        String query = "SELECT  * FROM " + TABLE_CART_ITEM + " tc, "
                + TABLE_CART_ITEM_QUANTITY + " tcq WHERE tc." + COLUMN_CART_PRODUCT_ID
                + " = " + "tcq." + COLUMN_CART_PRODUCT_ID;


        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()) {
            do {
                CartItem products = new CartItem();

                products.setId(cursor.getInt(cursor.getColumnIndex(COLUMN_ID)));
                products.setProductId(cursor.getString(cursor.getColumnIndex(COLUMN_CART_PRODUCT_ID)));
                // products.setProductImage(cursor.getString(cursor.getColumnIndex(COLUMN_CART_PRODUCT_IMAGE)));
                products.setProduct_quantity(cursor.getInt(cursor.getColumnIndex(COLUMN_CART_PRODUCT_QUANTITY)));
                products.setProductName(cursor.getString(cursor.getColumnIndex(COLUMN_CART_PRODUCT_NAME)));
                //  products.setProductmodel(cursor.getString(cursor.getColumnIndex(COLUMN_CART_PRODUCT_MODEL)));
                //     products.setProductprice(cursor.getString(cursor.getColumnIndex(COLUMN_CART_PRODUCT_PRICE)));
                //     products.setProductTax(cursor.getString(cursor.getColumnIndex(COLUMN_CART_PRODUCT_TAX)));
                //     products.setProduct_discount(cursor.getDouble(cursor.getColumnIndex(COLUMN_CART_PRODUCT_DISCOUNT)));
                listCartItem.add(products);

            } while (cursor.moveToNext());
        }

        Log.e("listCartItem Count", listCartItem.size() + "");
        return listCartItem;
    }

    public void updateCart(CartItem cartItems) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        // LogUtil.e("Update Quantity " + cartItems.getQuantity());
        values.put(COLUMN_CART_PRODUCT_QUANTITY, cartItems.getProduct_quantity());
        sqLiteDatabase.update(TABLE_CART_ITEM_QUANTITY, values, COLUMN_CART_PRODUCT_ID + " =? ", new String[]{cartItems.getProductId()});
    }

    public void updateCart(int qty, String productId) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues values = new ContentValues();
       Log.e("Qunatity hahahahaaaa",String.valueOf(qty));
        values.put(COLUMN_CART_PRODUCT_QUANTITY, qty);
        sqLiteDatabase.update(TABLE_CART_ITEM_QUANTITY, values, COLUMN_CART_PRODUCT_ID + " =? ", new String[]{productId});
    }

    public void deleteCartItem(String prodID) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.delete(TABLE_CART_ITEM, COLUMN_CART_PRODUCT_ID + " =? "
                , new String[]{prodID});

        sqLiteDatabase.delete(TABLE_CART_ITEM_QUANTITY, COLUMN_CART_PRODUCT_ID + " =? "
                , new String[]{prodID});


        // Log.d("inside", "delete");
    }

    public void deleteAllCartItem() {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.execSQL("delete from " + TABLE_CART_ITEM);
        sqLiteDatabase.execSQL("delete from " + TABLE_CART_ITEM_QUANTITY);
    }

    public boolean isProductAvailable(String pId) {
        SQLiteDatabase db = getWritableDatabase();
        String selectString = "SELECT " + COLUMN_CART_PRODUCT_NAME + " FROM " + TABLE_CART_ITEM + " WHERE " + COLUMN_CART_PRODUCT_ID + " =? ";

        // Add the String you are searching by here.
        // Put it in an array to avoid an unrecognized token error
        Cursor cursor = db.rawQuery(selectString, new String[]{String.valueOf(pId)});
        boolean hasObject = false;

        if (cursor.moveToFirst()) {

            hasObject = true;
            while (cursor.moveToNext()) {

            }
        }
        cursor.close();          // Dont forget to close your cursor
        db.close();              //AND your Database!
        return hasObject;
    }

    public void extractDatabase() {
        try {
            File sd = Environment.getExternalStorageDirectory();
            File data = Environment.getDataDirectory();

            if (sd.canWrite()) {
               // String currentDBPath = "/data/data/" + "com.panacea" + "/databases/" + DATABASE_NAME;
                String currentDBPath = data.getAbsolutePath() +"panacea/"+ DATABASE_NAME;
                String backupDBPath = DATABASE_NAME + ".db";
                File currentDB = new File(currentDBPath);
                File backupDB = new File(sd, backupDBPath);

                Log.e("currentDBPath", currentDBPath);

                if (currentDB.exists()) {
                    FileChannel src = new FileInputStream(currentDB).getChannel();
                    FileChannel dst = new FileOutputStream(backupDB).getChannel();
                    dst.transferFrom(src, 0, src.size());
                    src.close();
                    dst.close();
                }
            }
        } catch (Exception e) {

        }

    }

}
