package com.panacea.common.helpers;

import com.google.gson.Gson;
import com.panacea.common.application.PanaceaApplication;
import com.panacea.common.requestresponse.ConstSession;
import com.panacea.common.utility.PrefUtil;
import com.panacea.mvp.login.model.LoginResponseModel;
import com.panacea.mvp.splash.model.SplashModel;

/**
 * Created by Braintech on 06-12-2017.
 */

public class LoginManager {

    public static String KEY_USER_DATA = "user_data";
    public static String KEY_USER_DETAIL = "user_detail";
    public static String KEY_CONSTANT_DETAIL = "constant_detail";
    public static String IS_LOGIN = "is_login";
    public static String USER_PASSWORD = "user_pass";


    public static String KEY_BILLING_ADDRESS_ID = "billing_address_id";
    public static String KEY_DELIVERY_ADDRESS_ID = "delivery_address_id";
    public static String KEY_PAYMENT_VALUE = "payment_value";
    public static String KEY_DELIVERY_VALUE = "delivery_value";
    public static String KEY_CHKOUT_COURIER = "chkout_courier";
    public static String KEY_CHKOUT_COMMENT = "chkout_comment";
    public static String KEY_ORDER_ID = "order_id";

    private static volatile LoginManager instance;
    //  private static Context ctx;


    /* public static synchronized void initialize(Context applicationContext) {
         ctx = applicationContext;
     }
 */
    public static LoginManager getInstance() {
        if (instance == null) {
            synchronized (LoginManager.class) {
                if (instance == null) {
                    instance = new LoginManager();
                }
            }
        }
        return instance;
    }

    public void logoutUser() {
        // Clearing all data from Shared Preferences
        //PrefUtil.clear(ctx);
        PrefUtil.remove(PanaceaApplication.getInstance(), KEY_USER_DATA);
        PrefUtil.remove(PanaceaApplication.getInstance(), IS_LOGIN);
        PrefUtil.remove(PanaceaApplication.getInstance(), KEY_USER_DETAIL);
        PrefUtil.remove(PanaceaApplication.getInstance(), ConstSession.PREF_NICKNAME);

        PrefUtil.remove(PanaceaApplication.getInstance(), KEY_BILLING_ADDRESS_ID);
        PrefUtil.remove(PanaceaApplication.getInstance(), KEY_DELIVERY_ADDRESS_ID);
        PrefUtil.remove(PanaceaApplication.getInstance(), KEY_PAYMENT_VALUE);
        PrefUtil.remove(PanaceaApplication.getInstance(), KEY_DELIVERY_VALUE);
        PrefUtil.remove(PanaceaApplication.getInstance(), KEY_CHKOUT_COURIER);
        PrefUtil.remove(PanaceaApplication.getInstance(), KEY_CHKOUT_COMMENT);
        PrefUtil.remove(PanaceaApplication.getInstance(), KEY_ORDER_ID);
    }

    public void clearCheckOutSession() {
        // Clearing all data from Shared Preferences
        //PrefUtil.clear(ctx);
        PrefUtil.remove(PanaceaApplication.getInstance(), KEY_BILLING_ADDRESS_ID);
        PrefUtil.remove(PanaceaApplication.getInstance(), KEY_DELIVERY_ADDRESS_ID);
        PrefUtil.remove(PanaceaApplication.getInstance(), KEY_PAYMENT_VALUE);
        PrefUtil.remove(PanaceaApplication.getInstance(), KEY_DELIVERY_VALUE);
        PrefUtil.remove(PanaceaApplication.getInstance(), KEY_CHKOUT_COURIER);
        PrefUtil.remove(PanaceaApplication.getInstance(), KEY_CHKOUT_COMMENT);
        PrefUtil.remove(PanaceaApplication.getInstance(), KEY_ORDER_ID);
    }

    public boolean isLoggedIn() {
        return PrefUtil.getBoolean(PanaceaApplication.getInstance(), IS_LOGIN, false);
    }

   /* public void createLoginSession(LoginModel data) {
        createLoginSession("", data);
    }
*/

    public LoginResponseModel.Data getUserDetailModel() {
        LoginResponseModel.Data data = new Gson().fromJson(PrefUtil.getString(PanaceaApplication.getInstance(), KEY_USER_DETAIL, ""), LoginResponseModel.Data.class);
        return data;
    }

    public void SaveUserDetail(LoginResponseModel.Data data) {
        PrefUtil.putString(PanaceaApplication.getInstance(), KEY_USER_DETAIL, new Gson().toJson(data));
    }

    public void SaveConstantDetail(SplashModel.Data splashModel) {
        PrefUtil.putString(PanaceaApplication.getInstance(), KEY_CONSTANT_DETAIL, new Gson().toJson(splashModel));
    }

    public LoginResponseModel.Data getUserData() {
        LoginResponseModel.Data data = new Gson().fromJson(PrefUtil.getString(PanaceaApplication.getInstance(), KEY_USER_DATA, ""), LoginResponseModel.Data.class);
        return data;
    }

    public SplashModel.Data getConstantData() {
        SplashModel.Data data = new Gson().fromJson(PrefUtil.getString(PanaceaApplication.getInstance(), KEY_CONSTANT_DETAIL, ""), SplashModel.Data.class);
        return data;
    }


    public void createLoginSession(LoginResponseModel.Data data) {
        PrefUtil.putBoolean(PanaceaApplication.getInstance(), IS_LOGIN, true);
        PrefUtil.putString(PanaceaApplication.getInstance(), KEY_USER_DATA, new Gson().toJson(data));

        // PrefUtil.putString(ctx, AppConstant.KEY_MERCHANT_CODE, data.getMerchantCode());
    }

    public String getUserPassword() {
        return PrefUtil.getString(PanaceaApplication.getInstance(), USER_PASSWORD, "");
    }

/*Check out session*/

    public void saveBillingAddressId(String addressId) {
        PrefUtil.putString(PanaceaApplication.getInstance(), KEY_BILLING_ADDRESS_ID, addressId);
    }

    public String getBillingAddressId() {
        return PrefUtil.getString(PanaceaApplication.getInstance(), KEY_BILLING_ADDRESS_ID, "");
    }

    public void saveDeliveryAddressId(String addressId) {
        PrefUtil.putString(PanaceaApplication.getInstance(), KEY_DELIVERY_ADDRESS_ID, addressId);
    }

    public String getDeliveryAddressId() {
        return PrefUtil.getString(PanaceaApplication.getInstance(), KEY_DELIVERY_ADDRESS_ID, "");
    }

    public void savePaymentMethod(String payment) {
        PrefUtil.putString(PanaceaApplication.getInstance(), KEY_PAYMENT_VALUE, payment);
    }

    public String getPaymentMethod() {
        return PrefUtil.getString(PanaceaApplication.getInstance(), KEY_PAYMENT_VALUE, "");
    }

    public void saveDeliveryMethod(String deliveryValue) {
        PrefUtil.putString(PanaceaApplication.getInstance(), KEY_DELIVERY_VALUE, deliveryValue);
    }

    public String getDeliveryMethod() {
        return PrefUtil.getString(PanaceaApplication.getInstance(), KEY_DELIVERY_VALUE, "");
    }

    public void saveCourierInfo(String deliveryValue) {
        PrefUtil.putString(PanaceaApplication.getInstance(), KEY_CHKOUT_COURIER, deliveryValue);
    }

    public String getCourierInfo() {
        return PrefUtil.getString(PanaceaApplication.getInstance(), KEY_CHKOUT_COURIER, "");
    }

    public void saveChkOutComment(String deliveryValue) {
        PrefUtil.putString(PanaceaApplication.getInstance(), KEY_CHKOUT_COMMENT, deliveryValue);
    }

    public String getChkOutComment() {
        return PrefUtil.getString(PanaceaApplication.getInstance(), KEY_CHKOUT_COMMENT, "");
    }

    public void saveOrderId(String orderId) {
        PrefUtil.putString(PanaceaApplication.getInstance(), KEY_ORDER_ID, orderId);
    }

    public String getOrderId() {
        return PrefUtil.getString(PanaceaApplication.getInstance(), KEY_ORDER_ID, "");
    }

}
