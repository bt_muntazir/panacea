package com.panacea.common.helpers;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;

import com.panacea.R;
import com.panacea.common.database.DBHelper;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.login.LoginActivity;
import com.panacea.mvp.navigation.NavigationActivity;

/**
 * Created by Braintech on 30-11-2017.
 */

public class AlertDialogHelper {

    DBHelper dbHelper;

    public static void showMessageToLogout(final Context context, final DBHelper dbHelper) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context, android.R.style.Theme_DeviceDefault_Dialog_Alert);
        // Setting Dialog Message
        alertDialog.setMessage(context.getString(R.string.logout_message));
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            public void onClick(DialogInterface dialog, int which) {

                LoginManager.getInstance().logoutUser();

                //empty the cart
                dbHelper.deleteAllCartItem();

                context.startActivity(new Intent(context, LoginActivity.class));
                // ((Activity) context).finishAffinity();

                dialog.dismiss();


            }
        });

        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public static void showMessage(Context context, String msg) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        // Setting Dialog Message
        alertDialog.setMessage(msg);
        alertDialog.setCancelable(false);
        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();

            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

}
