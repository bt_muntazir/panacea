package com.panacea.common.requestresponse;

/**
 * Created by Braintech on 30-11-2017.
 */

public class Const {

    public static final String Base_URL = "http://www.easychinese.biz/";

    public static final String APP_KEY = "Appkey";
    public static final String APP_KEY_VALUE = "panaceaco@2018Nav";

    //Hashmao key
    public static String KEY_ID = "id";
    public static String KEY_NAME = "name";


    /*Common const*/
    public static final String PARAM_SUCCESS = "1";
    public static final String PARAM_PAGE = "page";
    public static final String PARAM_ADDRESS_ID = "address_id";
    public static final String PARAM_DEFAULT = "default";

    public static final String PARAM_COMPLETE = "Complete";
    public static final String PARAM_CANCELED = "Canceled";





    /* login*/

    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_PASSWORD = "password";
    public static final String PARAM_QTY = "qty";


    /*Registration*/

    public static final String PARAM_FIRSTNAME = "firstname";
    public static final String PARAM_LASTNAME = "lastname";
    public static final String PARAM_TELEPHONE = "telephone";
    public static final String PARAM_ACCOUNT = "account";
    public static final String PARAM_FAX = "fax";
    public static final String PARAM_COMPANY = "company";
    public static final String PARAM_ADDRESS_1 = "address_1";
    public static final String PARAM_ADDRESS_2 = "address_2";
    public static final String PARAM_CITY = "city";
    public static final String PARAM_POSTCODE = "postcode";
    public static final String PARAM_COUNTRY_ID = "country_id";
    public static final String PARAM_ZONE_ID = "zone_id";
    public static final String PARAM_CONFIRMPASSWORD = "confirm";


    /*Change Password*/
    public static final String PARAM_CUSTOMER_ID = "customer_id";
    public static final String PARAM_ORDER_ID = "order_id";
    public static final String PARAM_COUPON = "coupon";
    public static final String PARAM_OLD_PASSWORD = "old_password";
    public static final String PARAM_CONFIRMPASS = "confirm";


    /*Confirm Order Activity*/
    public static final String PARAM_BILLING_ADD_ID = "billing_address_id";
    public static final String PARAM_DELIVERY_ADD_ID = "delivery_address_id";
    public static final String PARAM_SHIPPING_METHOD = "shipping_method";
    public static final String PARAM_PAYMENT_METHOD = "payment_method";
    public static final String PARAM_COURIER_INFO = "courier_info";
    public static final String PARAM_ORDER_COMENTS = "order_comment";
    public static final String PARAM_PRODUCT_DETAIL = "product_detail";

    public static final String PARAM_CC_NUMBER = "cc_number";
    public static final String PARAM_CC_EXP_MONTH = "cc_expire_date_month";
    public static final String PARAM_CC_EXP_YEAR = "cc_expire_date_year";
    public static final String PARAM_CC_CVV = "cc_cvv2";




    /*Multi Parts Search Fragment*/

    public static final String PARAM_SEARCH = "search";
    public static final String PARAM_SEARCH1 = "search1";
    public static final String PARAM_SEARCH2 = "search2";
    public static final String PARAM_SEARCH3 = "search3";
    public static final String PARAM_SEARCH4 = "search4";
    public static final String PARAM_SEARCH5 = "search5";
    public static final String PARAM_SEARCH6 = "search6";

    public static final String PARAM_QTY1 = "qty1";
    public static final String PARAM_QTY2 = "qty2";
    public static final String PARAM_QTY3 = "qty3";
    public static final String PARAM_QTY4 = "qty4";
    public static final String PARAM_QTY5 = "qty5";
    public static final String PARAM_QTY6 = "qty6";
    public static final String PARAM_FILTER_NAME = "filter_name";

    public static final String PARAM_PRODUCT_ID = "product_id";


    /*Contact Us Send Message*/
    public static final String PARAM_NAME = "name";
    public static final String PARAM_COMPANY_NAME = "company_name";
    public static final String PARAM_ZIP = "zip";
    public static final String PARAM_ADDRESS = "address";
    public static final String PARAM_PHONE = "phone";
    public static final String PARAM_ENQUIRY = "enquiry";
    public static final String PARAM_CONTACT = "contact";


    /*Paco Credit Application*/

    public static final String PARAM_COMP_ADDRESS = "comp_addr";
    public static final String PARAM_COMP_STATE = "comp_state";
    public static final String PARAM_COMP_CITY = "comp_city";
    public static final String PARAM_COMP_ZIP = "comp_zip";
    public static final String PARAM_LONG_CURRENT_ADDR = "long_current_addr";
    public static final String PARAM_WEB_ADDR = "web_addr";
    public static final String PARAM_DATA_BUIS_COMM = "date_business_commen";
    public static final String PARAM_BANK_NAME = "bank_name";
    public static final String PARAM_BANK_ADDR = "bank_addr";
    public static final String PARAM_BANK_STATE = "bank_state";
    public static final String PARAM_BANK_CITY = "bank_city";
    public static final String PARAM_BANK_ZIP = "bank_zip";
    public static final String PARAM_BANK_PHONE = "bank_phone";
    public static final String PARAM_BANK_FAX = "bank_fax";
    public static final String PARAM_BANK_ACC_NO= "bank_acc_no";

    public static final String PARAM_SOLE_PROP = "sole_propritorship";
    public static final String PARAM_PARTNERSHIP = "partnership";
    public static final String PARAM_CORPORATION = "corporation";

    public static final String PARAM_NAME1 = "comp_name1";
    public static final String PARAM_COMPANY_ADD1 = "comp_addr1";
    public static final String PARAM_STATE1 = "comp_state1";
    public static final String PARAM_CITY1 = "comp_city1";
    public static final String PARAM_ZIP1 = "comp_zip1";
    public static final String PARAM_PHONE1 = "comp_phone1";
    public static final String PARAM_FAX1 = "comp_fax1";
    public static final String PARAM_EMAIL1 = "comp_email1";
    public static final String PARAM_CURRENT_BALANCE1 = "comp_current_outstanding_balence1";

    public static final String PARAM_NAME2 = "comp_name2";
    public static final String PARAM_COMPANY_ADD2 = "comp_addr2";
    public static final String PARAM_STATE2 = "comp_state2";
    public static final String PARAM_CITY2 = "comp_city2";
    public static final String PARAM_ZIP2 = "comp_zip2";
    public static final String PARAM_PHONE2 = "comp_phone2";
    public static final String PARAM_FAX2 = "comp_fax2";
    public static final String PARAM_EMAIL2 = "comp_email2";
    public static final String PARAM_CURRENT_BALANCE2 = "comp_current_outstanding_balence2";

    public static final String PARAM_NAME3 = "comp_name3";
    public static final String PARAM_COMPANY_ADD3 = "comp_addr3";
    public static final String PARAM_STATE3 = "comp_state3";
    public static final String PARAM_CITY3 = "comp_city3";
    public static final String PARAM_ZIP3 = "comp_zip3";
    public static final String PARAM_PHONE3 = "comp_phone3";
    public static final String PARAM_FAX3 = "comp_fax3";
    public static final String PARAM_EMAIL3 = "comp_email3";
    public static final String PARAM_CURRENT_BALANCE3 = "comp_current_outstanding_balence3";


  /*  public static final String PARAM_NAME = "name";
    public static final String PARAM_COMPANY_NAME = "company_name";
    public static final String PARAM_ZIP = "zip";
    public static final String PARAM_ADDRESS = "address";
    public static final String PARAM_PHONE = "phone";
    public static final String PARAM_ENQUIRY = "enquiry";
    public static final String PARAM_CONTACT = "contact";*/

}
