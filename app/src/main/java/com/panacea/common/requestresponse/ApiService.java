package com.panacea.common.requestresponse;

import com.panacea.common.adapter.model.CountrySpinnerModel;
import com.panacea.common.adapter.model.StateSpinnerModel;
import com.panacea.mvp.add_edit_address.model.AddAddressModel;
import com.panacea.mvp.add_edit_address.model.EditAddressModel;
import com.panacea.mvp.address.model.AddressModel;
import com.panacea.mvp.cart.model.ShoppingCartModel;
import com.panacea.mvp.checkout.confirm_order.model.ConfirmOrderModel;
import com.panacea.mvp.checkout.delivery_method.model.DeliveryMethodModel;
import com.panacea.mvp.checkout.payment_method.model.CreateOrderIdModel;
import com.panacea.mvp.checkout.payment_method.model.PaymentMethodModel;
import com.panacea.mvp.commonMvp.model.CommonModel;
import com.panacea.mvp.dashboard.model.CameraModel;
import com.panacea.mvp.dashboard.model.HomeProductsModel;
import com.panacea.mvp.dashboard.model.PartsModel;
import com.panacea.mvp.dashboard.model.RadiatorModel;
import com.panacea.mvp.edit_information.model.EditInfoModel;
import com.panacea.mvp.halo.model.HaloModel;
import com.panacea.mvp.halo.model.HaloSuccessModel;
import com.panacea.mvp.login.model.LoginResponseModel;
import com.panacea.mvp.order_history.model.OrderHistoryModel;
import com.panacea.mvp.order_information.model.OrderInformationModel;
import com.panacea.mvp.productdetail.model.ProductDetailModel;
import com.panacea.mvp.splash.model.SplashModel;
import com.panacea.mvp.success_order.model.OrderSuccessModel;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

/**
 * Created by Braintech on 30-11-2017.
 */

public interface ApiService {

    //Constant Api
    @POST("/index.php?route=app/constant")
    Call<SplashModel> getConstant(@Header("Content-Type")
                                          String contentType,
                                  @Header("Cache-Control")
                                          String cache,
                                  @Body RequestBody params);


    //login Api
    @POST("index.php?route=app/account/login")
    Call<LoginResponseModel> login(@Header("Content-Type")
                                           String contentType,
                                   @Header("Cache-Control")
                                           String cache,
                                   @Body RequestBody params);

    //paco credit Api
    @POST("index.php?route=app/information/pacocredit")
    Call<CommonModel> pacocredit(@Header("Content-Type")
                                           String contentType,
                                   @Header("Cache-Control")
                                           String cache,
                                   @Body RequestBody params);



    //change password Api
    @POST("index.php?route=app/account/changepassword")
    Call<CommonModel> changePassword(@Header("Content-Type")
                                           String contentType,
                                   @Header("Cache-Control")
                                           String cache,
                                   @Body RequestBody params);

    //register Api
    @POST("index.php?route=app/account/registration")
    Call<LoginResponseModel> register(@Header("Content-Type")
                                              String contentType,
                               @Header("Cache-Control")
                                              String cache,
                               @Body RequestBody params);

    //country list Api
    @GET("index.php?route=app/account/countries")
    Call<CountrySpinnerModel> getCountry();

    //state list Api
    @POST("index.php?route=app/account/zone")
    Call<StateSpinnerModel> getState(@Header("Content-Type")
                                             String contentType,
                                     @Header("Cache-Control")
                                             String cache,
                                     @Body RequestBody params);


    //dashboard home products Api
    @GET("index.php?route=app/products/homeproducts")
    Call<HomeProductsModel> getHomeProducts();

    //get radiators list Api
    @POST("index.php?route=app/products/radiators")
    Call<RadiatorModel> getRadiators(@Header("Content-Type")
                                             String contentType,
                                     @Header("Cache-Control")
                                             String cache,
                                     @Body RequestBody params);

    //get led list Api
    @POST("index.php?route=app/products/ledlighting")
    Call<RadiatorModel> getLedLightning(@Header("Content-Type")
                                                String contentType,
                                        @Header("Cache-Control")
                                                String cache,
                                        @Body RequestBody params);


    //get contacts us list Api
    @POST("index.php?route=app/information/contact")
    Call<CommonModel> getContacts(@Header("Content-Type")
                                           String contentType,
                                   @Header("Cache-Control")
                                           String cache,
                                   @Body RequestBody params);

    //get contacts us list Api
    @POST("index.php?route=app/products/search")
    Call<PartsModel> getSearch(@Header("Content-Type")
                                           String contentType,
                                        @Header("Cache-Control")
                                           String cache,
                                        @Body RequestBody params);

    //get contacts us list Api
    @POST("index.php?route=app/products/details")
    Call<ProductDetailModel> productDetail(@Header("Content-Type")
                                       String contentType,
                                           @Header("Cache-Control")
                                       String cache,
                                           @Body RequestBody params);

    //camera products Api
    @GET("index.php?route=app/information/camera")
    Call<CameraModel> getCamera();

    //order history Api
    @POST("index.php?route=app/account/orderlist")
    Call<OrderHistoryModel> orderhistory(@Header("Content-Type")
                                           String contentType,
                                         @Header("Cache-Control")
                                           String cache,
                                         @Body RequestBody params);

    //order information detail Api
    @POST("index.php?route=app/account/orderinfo")
    Call<OrderInformationModel> orderInformation(@Header("Content-Type")
                                           String contentType,
                                                 @Header("Cache-Control")
                                           String cache,
                                                 @Body RequestBody params);

    //get detail ofedit info Api
    @POST("index.php?route=app/account/getaccountdetail")
    Call<EditInfoModel> getUserDetail(@Header("Content-Type")
                                           String contentType,
                                      @Header("Cache-Control")
                                           String cache,
                                      @Body RequestBody params);

    //save account detail of edit info Api
    @POST("index.php?route=app/account/saveaccountdetail")
    Call<CommonModel> saveUserDetail(@Header("Content-Type")
                                              String contentType,
                                      @Header("Cache-Control")
                                              String cache,
                                      @Body RequestBody params);

    //get address list Api
    @POST("index.php?route=app/account/addresslist")
    Call<AddressModel> getAddressList(@Header("Content-Type")
                                           String contentType,
                                      @Header("Cache-Control")
                                           String cache,
                                      @Body RequestBody params);

    //add address Api
    @POST("index.php?route=app/account/addaddress")
    Call<AddAddressModel> addAddress(@Header("Content-Type")
                                              String contentType,
                                     @Header("Cache-Control")
                                              String cache,
                                     @Body RequestBody params);

    //save edit address Api
    @POST("index.php?route=app/account/saveeditaddress")
    Call<CommonModel> saveEditList(@Header("Content-Type")
                                              String contentType,
                                      @Header("Cache-Control")
                                              String cache,
                                      @Body RequestBody params);

    //get edit address Api
    @POST("index.php?route=app/account/geteditaddress")
    Call<EditAddressModel> getEditAddress(@Header("Content-Type")
                                              String contentType,
                                          @Header("Cache-Control")
                                              String cache,
                                          @Body RequestBody params);

    //delete address Api
    @POST("index.php?route=app/account/deleteddress")
    Call<CommonModel> deleteAddress(@Header("Content-Type")
                                                  String contentType,
                                          @Header("Cache-Control")
                                                  String cache,
                                          @Body RequestBody params);


    /*Halo API*/

    //get halo detail Api
    @GET("index.php?route=app/information/gethalo")
    Call<HaloModel> getHalo();

    //delete address Api
    @POST("index.php?route=app/information/posthalo")
    Call<CommonModel> saveHaloData(@Header("Content-Type")
                                            String contentType,
                                    @Header("Cache-Control")
                                            String cache,
                                    @Body RequestBody params);

    //get halo detail Api
    @GET("index.php?route=app/information/gethalosuccess")
    Call<HaloSuccessModel> getHaloSuccess();

    //get cart item Api
    @POST("index.php?route=app/checkout/getcart")
    Call<ShoppingCartModel> cartItems(@Header("Content-Type")
                                                 String contentType,
                                      @Header("Cache-Control")
                                                 String cache,
                                      @Body RequestBody params);

    //shipping data Api
    @GET("index.php?route=app/checkout/shippingdata")
    Call<DeliveryMethodModel> shippingData();

    //payment data Api
    @GET("index.php?route=app/checkout/paymentdata")
    Call<PaymentMethodModel> paymentData();

    //payment data Api
    @POST("index.php?route=app/checkout/paymentdata")
    Call<PaymentMethodModel> paymentData(@Header("Content-Type")
                                                   String contentType,
                                           @Header("Cache-Control")
                                                   String cache,
                                           @Body RequestBody params);

    //create order id Api
    @POST("index.php?route=app/checkout/createorderid")
    Call<CreateOrderIdModel> createOrderId(@Header("Content-Type")
                                              String contentType,
                                           @Header("Cache-Control")
                                              String cache,
                                           @Body RequestBody params);

    //get Order Detail info Api
    @POST("index.php?route=app/checkout/getorderbyid")
    Call<ConfirmOrderModel> orderDetail(@Header("Content-Type")
                                          String contentType,
                                        @Header("Cache-Control")
                                          String cache,
                                        @Body RequestBody params);

    //get Pay and Confirm order Detail info Api
    @POST("index.php?route=app/checkout/payandconfirmorder")
    Call<CommonModel> payAndConfirm(@Header("Content-Type")
                                                String contentType,
                                        @Header("Cache-Control")
                                                String cache,
                                        @Body RequestBody params);

    //get order success Api
    @POST("index.php?route=app/checkout/ordersuccess")
    Call<OrderSuccessModel> orderSuccess(@Header("Content-Type")
                                                   String contentType,
                                         @Header("Cache-Control")
                                                   String cache,
                                         @Body RequestBody params);

    //get Order Detail info Api
    @POST("index.php?route=app/checkout/applycoupon")
    Call<CommonModel> applyCoupon(@Header("Content-Type")
                                                String contentType,
                                        @Header("Cache-Control")
                                                String cache,
                                        @Body RequestBody params);



}



