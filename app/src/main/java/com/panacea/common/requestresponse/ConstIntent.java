package com.panacea.common.requestresponse;

/**
 * Created by Braintech on 30-11-2017.
 */

public class ConstIntent {

    public static String KEY_USER_ID = "userId";

    public static String KEY_USERNAME = "username";
    public static String KEY_EMAIL = "email";
    public static String KEY_IS_LOGGED_IN = "is_logged_in";

    public static String KEY_CONSTANT_SPLASH = "splash_model";

    public static String KEY_LAT = "latitude";
    public static String KEY_LON = "longitude";

    public static String KEY_SEARCH = "search";

    public static String KEY_PRODUCT_ID = "product_id";

    public static String KEY_INSTAGRAM_YOUTUBE_URL = "instagram_youtube_url";

    public static String KEY_INSTAGRAM_YOUTUBE_CONST = "instagram_youtube_const";

    public static String KEY_ADD_EDIT_ADDRESS = "add_edit_address";

    public static int KEY_ADD = 1;
    public static int KEY_EDIT = 2;
    public static int KEY_ADD_CHECKOUT_BILLING = 3;
    public static int KEY_ADD_CHECKOUT_DELIVERY = 4;

    public static String KEY_ORDER_ID = "orderId";

    public static String KEY_REGISTER = "register";
    public static String KEY_FROM_REGISTER = "from_register";
    public static String KEY_FROM_CHECKOUT = "from_checkout";


    public static String KEY_ADDRESS_ID = "addressId";

}


