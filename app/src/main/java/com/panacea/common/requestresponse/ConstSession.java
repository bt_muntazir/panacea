package com.panacea.common.requestresponse;

/**
 * Created by Braintech on 30-11-2017.
 */

public class ConstSession {

    public static String PREF_LOGIN = "isLogin";
    public static String PREF_NICKNAME = "nickname";
}
