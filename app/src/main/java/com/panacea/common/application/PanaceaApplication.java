package com.panacea.common.application;

import android.app.Application;
import android.content.Context;

import com.panacea.R;

import org.acra.ACRA;
import org.acra.ReportField;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

/**
 * Created by Braintech on 06-12-2017.
 */

@ReportsCrashes(mailTo = "muntazir@braintechnosys.com", customReportContent = {
        ReportField.APP_VERSION_CODE, ReportField.APP_VERSION_NAME,
        ReportField.ANDROID_VERSION, ReportField.PHONE_MODEL,
        ReportField.CUSTOM_DATA, ReportField.STACK_TRACE, ReportField.LOGCAT},
        mode = ReportingInteractionMode.TOAST, resToastText = R.string.crash_toast_text)

public class PanaceaApplication extends Application {

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();

        context=this;

        //TypeFaceUtil.setDefaultFont(this, "MONOSPACE", "fonts/proxima-nova.otf");

          ACRA.init(this);
    }


    public static Context getInstance() {
        return context;
    }




    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }
}
