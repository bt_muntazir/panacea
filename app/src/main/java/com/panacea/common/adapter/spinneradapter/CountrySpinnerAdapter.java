package com.panacea.common.adapter.spinneradapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.adapter.model.CountrySpinnerModel;

import java.util.ArrayList;

/**
 * Created by Braintech on 07-12-2017.
 */

public class CountrySpinnerAdapter  extends BaseAdapter {

    ArrayList<CountrySpinnerModel.Datum> list;
    Context context;

    public CountrySpinnerAdapter(Context context, ArrayList<CountrySpinnerModel.Datum> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView = (TextView) View.inflate(context, R.layout.spinner_selected_view, null);


        CountrySpinnerModel.Datum events = list.get(position);
        textView.setText(events.getName());
        // FontHelper.applyFont(context, textView, FontHelper.FontType.FONTREGULAR);
        return textView;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView textView = (TextView) View.inflate(context,
                R.layout.spinner_item, null);

        // textView.setTextColor(ContextCompat.getColor(context, android.R.color.black));

        CountrySpinnerModel.Datum events = list.get(position);
        textView.setText(events.getName());

        return textView;
    }
}
