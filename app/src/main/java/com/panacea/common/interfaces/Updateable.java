package com.panacea.common.interfaces;

import org.json.JSONObject;

public interface Updateable {
   public void update(JSONObject jsonObject);
}