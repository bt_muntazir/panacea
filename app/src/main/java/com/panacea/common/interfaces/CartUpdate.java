package com.panacea.common.interfaces;

/**
 * Created by Braintech on 20-12-2017.
 */

public interface CartUpdate {
    public void onCartUpdate(String type, int qty);
}
