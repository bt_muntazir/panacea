package com.panacea.common.interfaces;

/**
 * Created by Braintech on 30-11-2017.
 */

public interface OnClickInterface {
    public void onClick();
}
