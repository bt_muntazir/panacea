package com.panacea.common.utility;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by Braintech on 30-11-2017.
 */

public class Utils {

    // for keyboard down
    public static void hideKeyboardIfOpen(Activity activity) {


        View view = activity.getCurrentFocus();

        if (view != null) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

    }

    public static boolean isValidEmail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }


    public static Boolean isEmptyOrNull(String value) {
        if (value == null || value.isEmpty())
            return true;
        else
            return false;
    }

}
