package com.panacea.common.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;


import com.panacea.R;
import com.panacea.common.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;

import okhttp3.OkHttpClient;
import okhttp3.Protocol;

/**
 * Created by Braintech on 22-Jul-16.
 */
public class AlertDialogManager {

    public static void sAlertFinish(final Context context, String msg) {


        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage(msg);
        alertDialogBuilder.setCancelable(false);


        alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                ((Activity) context).finish();
                arg0.dismiss();
            }
        });

        final AlertDialog alertDialog = alertDialogBuilder.create();

        alertDialog.show();
    }

    public static void alertShowImage(final Context mContext, String imgUrl) {

        try {

            // final View dialogView = View.inflate(mContext,R.layout.alert_show_image,null);

            final Dialog customDialog = new Dialog(mContext, R.style.CustomDialog);

            customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            customDialog.setContentView(R.layout.alert_show_image);
            //customDialog.setContentView(dialogView);
            customDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            customDialog.setCancelable(true);

            PhotoView imageView = (PhotoView) customDialog.findViewById(R.id.imageView);


            // set animation
            ((ViewGroup) customDialog.getWindow().getDecorView().findViewById(android.R.id.content))
                    .getChildAt(0).startAnimation(AnimationUtils.loadAnimation(
                    mContext, R.anim.dialog_scale_animation));


            if (imgUrl.length() > 0)
                Picasso.with(mContext).load(imgUrl).error(R.mipmap.ic_launcher).placeholder(R.mipmap.ic_launcher).into(imageView);

            customDialog.show();

        } catch (RuntimeException e) {
            e.printStackTrace();
        }

        /*try {

            final Dialog customDialog = new Dialog(mContext);

            customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            customDialog.setContentView(R.layout.alert_show_image);
            customDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
            customDialog.setCancelable(true);

            ImageView imageView = (ImageView) customDialog.findViewById(R.id.imageView);

            // set animation
            *//*((ViewGroup) customDialog.getWindow().getDecorView().findViewById(android.R.id.content))
                    .getChildAt(0).startAnimation(AnimationUtils.loadAnimation(
                    mContext, R.anim.dialog_scale_animation));*//*
            //OkHttpClient client = new OkHttpClient()
            *//*        ;
            final OkHttpClient client = new OkHttpClient.Builder()
                    .protocols(Collections.singletonList(Protocol.HTTP_1_1))
                    .build();

            Picasso picasso = new Picasso.Builder(mContext)
                    .downloader(new OkHttp3Downloader(client))
                    .build();
*//*
            Picasso.with(mContext).load(imgUrl).into(imageView);

            customDialog.show();


        } catch (RuntimeException e) {
        }*/
    }

    public static void alertShowSdCardImage(final Context mContext, String imgUri) {

        try {

            final Dialog customDialog = new Dialog(mContext, R.style.CustomDialog);

            customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            customDialog.setContentView(R.layout.alert_show_image);
            //customDialog.setContentView(dialogView);
            customDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            customDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            customDialog.setCancelable(true);

            PhotoView imageView = (PhotoView) customDialog.findViewById(R.id.imageView);

            // set animation
            ((ViewGroup) customDialog.getWindow().getDecorView().findViewById(android.R.id.content))
                    .getChildAt(0).startAnimation(AnimationUtils.loadAnimation(
                    mContext, R.anim.dialog_scale_animation));


            Uri uri = Uri.fromFile(new File(imgUri));

            if (imgUri.length() > 0)
                Picasso.with(mContext).load(uri).error(R.mipmap.ic_launcher).placeholder(R.mipmap.ic_launcher).into(imageView);

            customDialog.show();

        } catch (RuntimeException e) {

            e.printStackTrace();
        }

    }
}
