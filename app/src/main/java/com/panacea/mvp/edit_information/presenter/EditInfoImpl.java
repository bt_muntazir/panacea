package com.panacea.mvp.edit_information.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.commonMvp.model.CommonModel;
import com.panacea.mvp.edit_information.model.EditInfoModel;
import com.panacea.mvp.login.model.LoginResponseModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 20-12-2017.
 */

public class EditInfoImpl implements EditInfoContractor.Presenter {

    EditInfoContractor.EditInfoView editInfoView;
    Activity activity;
    JSONObject jsonObject;

    public EditInfoImpl(EditInfoContractor.EditInfoView editInfoView, Activity activity) {
        this.editInfoView = editInfoView;
        this.activity = activity;
    }

    @Override
    public void getEditInfo(String customerId) {
        try {
            ApiAdapter.getInstance(activity);
            gettingEditInfoData(customerId);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            editInfoView.onInternetError();
        }
    }

    @Override
    public void getSaveEditInfo(JSONObject jsonObject) {
        try {
            ApiAdapter.getInstance(activity);
            saveEditInfoData(jsonObject);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            editInfoView.saveInfoInternetError();
        }
    }

    private  void  gettingEditInfoData(String customerId){
        Progress.start(activity);

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_CUSTOMER_ID, customerId);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<EditInfoModel> getEditInfoDataOutput = ApiAdapter.getApiService().getUserDetail("application/json", "no-cache", body);

        getEditInfoDataOutput.enqueue(new Callback<EditInfoModel>() {
            @Override
            public void onResponse(Call<EditInfoModel> call, Response<EditInfoModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    EditInfoModel editInfoModel = response.body();

                    if (editInfoModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        editInfoView.getEditInfoSuccess(editInfoModel.getData());
                    } else {
                        editInfoView.apiUnsuccess(activity.getString(R.string.error_username_pass));
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    editInfoView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<EditInfoModel> call, Throwable t) {
                Progress.stop();
                editInfoView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }

    private void saveEditInfoData(JSONObject jsonObject){
        Progress.start(activity);

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CommonModel> getOutput = ApiAdapter.getApiService().saveUserDetail("application/json", "no-cache", body);

        getOutput.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    CommonModel commonModel = response.body();

                    if (commonModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        editInfoView.getSaveInfoSuccess(commonModel.getMessage());
                    } else {
                        editInfoView.getSaveInfoUnSucess(commonModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    editInfoView.getSaveInfoUnSucess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                Progress.stop();
                editInfoView.getSaveInfoUnSucess(activity.getString(R.string.error_server));
            }
        });
    }
}
