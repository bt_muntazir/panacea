package com.panacea.mvp.edit_information;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.FontHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.requestresponse.Const;
import com.panacea.common.utility.SnackNotify;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.edit_information.model.EditInfoModel;
import com.panacea.mvp.edit_information.presenter.EditInfoContractor;
import com.panacea.mvp.edit_information.presenter.EditInfoImpl;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditInformationActivity extends AppCompatActivity implements EditInfoContractor.EditInfoView {

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.txtViewPersonalDetail)
    AppCompatTextView txtViewPersonalDetail;

    @BindView(R.id.txtViewFirstName)
    AppCompatTextView txtViewFirstname;

    @BindView(R.id.txtViewLastName)
    AppCompatTextView txtViewLastName;

    @BindView(R.id.txtViewEmail)
    AppCompatTextView txtViewEmail;

    @BindView(R.id.txtViewAccountNo)
    AppCompatTextView txtViewAccountNo;

    @BindView(R.id.txtViewTelephone)
    AppCompatTextView txtViewTelephone;

    @BindView(R.id.txtViewFax)
    AppCompatTextView txtViewFax;

    @BindView(R.id.edtTextFirstName)
    AppCompatEditText edtTextFirstName;

    @BindView(R.id.edtTextLastName)
    AppCompatEditText edtTextLastName;

    @BindView(R.id.edtTextEmail)
    AppCompatEditText edtTextEmail;

    @BindView(R.id.edtTextAccountNo)
    AppCompatEditText edtTextAccountNo;

    @BindView(R.id.edtTextTelephone)
    AppCompatEditText edtTextTelephone;

    @BindView(R.id.edtTextFax)
    AppCompatEditText edtTextFax;

    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.btnContinue)
    Button btnContinue;

    EditInfoImpl editInfo;

    JSONObject jsonObject;

    String customerId;

    DBHelper dbHelper;

    EditInfoModel.Data editInfoData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_information);

        ButterKnife.bind(this);
        dbHelper=new DBHelper(this);

        customerId=  LoginManager.getInstance().getUserData().getCustomerId();
        setTitle();
        setFont();
        editInfo = new EditInfoImpl(this, this);
        getEditInfoDataApi();
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateCartQty();
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onInternetError() {
        SnackNotify.checkConnection(onretryEditInfoApi, coordinateLayout);
    }

    OnClickInterface onretryEditInfoApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getEditInfoDataApi();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);


    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, coordinateLayout);


    }

    @Override
    public void getEditInfoSuccess(EditInfoModel.Data editInfoModel) {

        editInfoData = editInfoModel;
        setTheFields();
    }

    @Override
    public void getSaveInfoSuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);

        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            public void run() {
                finish();
            }
        }, 1000);
    }

    @Override
    public void getSaveInfoUnSucess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void saveInfoInternetError() {
        SnackNotify.checkConnection(onretrySaveInfoApi, coordinateLayout);
    }

    OnClickInterface onretrySaveInfoApi = new OnClickInterface() {
        @Override
        public void onClick() {
            saveUserDataApi();
        }
    };

    @Override
    public void saveInfoServerError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    /*-----------------------------private method-----------------------------------------*/

    private void setTitle() {
        txtViewTitle.setText(getString(R.string.title_edit_info));
    }

    private void setFont() {

        FontHelper.setFontFace(txtViewPersonalDetail, FontHelper.FontType.FONT_THIN, this);

        FontHelper.setFontFace(txtViewFirstname, FontHelper.FontType.FONT_SEMIBOLD, this);
        FontHelper.setFontFace(txtViewLastName, FontHelper.FontType.FONT_SEMIBOLD, this);
        FontHelper.setFontFace(txtViewEmail, FontHelper.FontType.FONT_SEMIBOLD, this);
        FontHelper.setFontFace(txtViewFax, FontHelper.FontType.FONT_SEMIBOLD, this);
        FontHelper.setFontFace(txtViewAccountNo, FontHelper.FontType.FONT_SEMIBOLD, this);
        FontHelper.setFontFace(edtTextTelephone, FontHelper.FontType.FONT_SEMIBOLD, this);
    }

    private void getEditInfoDataApi() {

        editInfo.getEditInfo(customerId);
    }

    private void setTheFields() {

        edtTextFirstName.setText(editInfoData.getFirstname());
        edtTextLastName.setText(editInfoData.getLastname());
        edtTextEmail.setText(editInfoData.getEmail());
        edtTextFax.setText(editInfoData.getFax());
        edtTextTelephone.setText(editInfoData.getTelephone());
        edtTextAccountNo.setText(editInfoData.getAccount());
    }

    private void saveUserDataApi() {
        String firstName = String.valueOf(edtTextFirstName.getText());
        String lastName = String.valueOf(edtTextLastName.getText());
        String email = String.valueOf(edtTextEmail.getText());
        String accountNo = String.valueOf(edtTextAccountNo.getText());
        String telephone = String.valueOf(edtTextTelephone.getText());
        String fax = String.valueOf(edtTextFax.getText());

        //get customerid from session
        String customerId = LoginManager.getInstance().getUserData().getCustomerId();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_CUSTOMER_ID, customerId);
            jsonObject.put(Const.PARAM_FIRSTNAME, firstName);
            jsonObject.put(Const.PARAM_LASTNAME, lastName);
            jsonObject.put(Const.PARAM_EMAIL, email);
            jsonObject.put(Const.PARAM_TELEPHONE, telephone);
            jsonObject.put(Const.PARAM_ACCOUNT, accountNo);
            jsonObject.put(Const.PARAM_FAX, fax);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        editInfo.getSaveEditInfo(jsonObject);
    }
    /*-----------------------------public method-----------------------------------------*/


    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }

    /*-----------------------------on click method-----------------------------------------*/

    @OnClick(R.id.imgBackButton)
    public void backPressed() {
        onBackPressed();
    }

    @OnClick(R.id.btnContinue)
    public void btnContinueClick() {

        saveUserDataApi();

    }
    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (dbHelper.getCartCount() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            }
        }
    }



}
