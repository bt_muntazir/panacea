package com.panacea.mvp.edit_information.presenter;

import com.panacea.mvp.commonMvp.BaseView;
import com.panacea.mvp.edit_information.model.EditInfoModel;
import com.panacea.mvp.login.model.LoginResponseModel;

import org.json.JSONObject;

/**
 * Created by Braintech on 20-12-2017.
 */

public interface EditInfoContractor {
    public interface EditInfoView extends BaseView {

        void getEditInfoSuccess(EditInfoModel.Data editInfoModel);

        void getSaveInfoSuccess(String message);

        void getSaveInfoUnSucess(String message);

        void saveInfoInternetError();

        void saveInfoServerError(String message);
    }


    public interface Presenter {
        void getEditInfo(String customerId);
        void getSaveEditInfo(JSONObject jsonObject);
    }
}
