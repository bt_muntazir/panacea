package com.panacea.mvp.cart.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.panacea.R;
import com.panacea.common.database.DBHelper;
import com.panacea.common.requestresponse.Const;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.cart.model.ShoppingCartModel;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.order_history.adapter.OrderHistoryAdapter;
import com.panacea.mvp.order_history.model.OrderHistoryModel;
import com.panacea.mvp.order_information.OrderInformationActivity;
import com.panacea.mvp.productdetail.ProductDetailActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 29-12-2017.
 */

public class ShoppingCartAdapter extends RecyclerView.Adapter {

    Activity activity;
    ArrayList<ShoppingCartModel.Data> raDataArrayListResult;

    DBHelper dbHelper;

    public ShoppingCartAdapter(Activity activity, ArrayList<ShoppingCartModel.Data> raDataArrayList) {
        this.activity = activity;
        this.raDataArrayListResult = raDataArrayList;
        dbHelper = new DBHelper(activity);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_shopping_cart, parent, false);
        return new ShoppingCartAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        final ShoppingCartModel.Data cartData = raDataArrayListResult.get(position);

        Picasso.with(activity).load(cartData.getThumb()).into(((ShoppingCartAdapter.ViewHolder) holder).imgProduct);
        ((ShoppingCartAdapter.ViewHolder) holder).txtViewHeadingTitle.setText(cartData.getName().toString());
        ((ShoppingCartAdapter.ViewHolder) holder).txtViewPartNo.setText(cartData.getModel().toString());
        ((ShoppingCartAdapter.ViewHolder) holder).txtViewPrice.setText(cartData.getTotalPrice().toString());
        ((ShoppingCartAdapter.ViewHolder) holder).txtViewQuantity.setText(cartData.getQty().toString());
        if (cartData.getAvailabilityStatus() == 1) {
            ((ShoppingCartAdapter.ViewHolder) holder).txtViewQtyNotAvailable.setVisibility(View.GONE);
            ((ShoppingCartActivity) activity).setBitForCheckOut(true);
        } else {
            ((ShoppingCartAdapter.ViewHolder) holder).txtViewQtyNotAvailable.setVisibility(View.VISIBLE);

            ((ShoppingCartActivity) activity).setBitForCheckOut(false);
        }


       /* ((ShoppingCartAdapter.ViewHolder) holder).imgProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, ProductDetailActivity.class);
                intent.putExtra(ConstIntent.KEY_PRODUCT_ID, cartData.getProductId());
                activity.startActivity(intent);
            }
        });*/


        ((ShoppingCartAdapter.ViewHolder) holder).imgProducdPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //update the database
                dbHelper.updateCart(cartData.getQty() + 1, cartData.getProductId());

                ((ShoppingCartActivity) activity).getShoppingCartDataApi();

            }
        });


        ((ShoppingCartAdapter.ViewHolder) holder).imgProductMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cartData.getQty() > 1) {
                    //update the database
                    dbHelper.updateCart(cartData.getQty() - 1, cartData.getProductId());
                    ((ShoppingCartActivity) activity).getShoppingCartDataApi();
                    Log.e("Minus", "- - - - - - ");
                } else {

                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);

                    // Setting Dialog Message
                    alertDialog.setMessage(activity.getString(R.string.confirmation_remove_cart_item));
                    alertDialog.setCancelable(true);

                    // Setting Positive "Yes" Button
                    alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            dbHelper.deleteCartItem(cartData.getProductId());

                            if (raDataArrayListResult.size() > 1) {
                                ((ShoppingCartActivity) activity).getShoppingCartDataApi();
                            } else {
                                raDataArrayListResult.remove(position);
                                notifyDataSetChanged();
                                activity.finish();
                            }
                        }
                    });
                    alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();

                        }
                    });

                    // Showing Alert Message
                    alertDialog.show();
                }
            }
        });


        ((ViewHolder) holder).imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PopupMenu popup = new PopupMenu(activity, v);
                popup.getMenuInflater().inflate(R.menu.popup_menu, popup.getMenu());

                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {

                        AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);

                        // Setting Dialog Message
                        alertDialog.setMessage(activity.getString(R.string.confirmation_remove_cart_item));
                        alertDialog.setCancelable(true);

                        // Setting Positive "Yes" Button
                        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                                dbHelper.deleteCartItem(cartData.getProductId());

                                if (raDataArrayListResult.size() > 1) {
                                    ((ShoppingCartActivity) activity).getShoppingCartDataApi();
                                } else {
                                    raDataArrayListResult.remove(position);
                                    notifyDataSetChanged();
                                    activity.finish();
                                }
                            }
                        });
                        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();

                            }
                        });

                        // Showing Alert Message
                        alertDialog.show();
                        return true;
                    }
                });
                popup.show();//showing popup menu


            }
        });

       /* ((ShoppingCartAdapter.ViewHolder) holder).relLayToolTip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((ViewHolder) holder).relLayToolTip.setVisibility(View.GONE);

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(activity);

                // Setting Dialog Message
                alertDialog.setMessage(activity.getString(R.string.confirmation_remove_cart_item));
                alertDialog.setCancelable(true);

                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        dbHelper.deleteCartItem(cartData.getProductId());

                        if (raDataArrayListResult.size() > 1) {
                            ((ShoppingCartActivity) activity).getShoppingCartDataApi();
                        } else {
                            raDataArrayListResult.remove(position);
                            notifyDataSetChanged();
                            activity.finish();
                        }
                    }
                });
                alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                    }
                });

                // Showing Alert Message
                alertDialog.show();
            }
        });


        ((ViewHolder) holder).imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((ViewHolder) holder).relLayToolTip.setVisibility(View.VISIBLE);

            }
        });*/
    }


    @Override
    public int getItemCount() {
        return raDataArrayListResult.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgProduct)
        ImageView imgProduct;

        @BindView(R.id.txtViewHeadingTitle)
        AppCompatTextView txtViewHeadingTitle;

        @BindView(R.id.txtViewPartNo)
        AppCompatTextView txtViewPartNo;

        @BindView(R.id.txtViewPrice)
        AppCompatTextView txtViewPrice;

        @BindView(R.id.txtViewQuantity)
        AppCompatTextView txtViewQuantity;

        @BindView(R.id.imgProducdPlus)
        ImageView imgProducdPlus;

        @BindView(R.id.imgProductMinus)
        ImageView imgProductMinus;

        @BindView(R.id.imgDelete)
        ImageView imgDelete;

        @BindView(R.id.relLayToolTip)
        RelativeLayout relLayToolTip;

        @BindView(R.id.txtViewQtyNotAvailable)
        AppCompatTextView txtViewQtyNotAvailable;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
