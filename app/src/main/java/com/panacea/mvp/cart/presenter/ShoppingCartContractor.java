package com.panacea.mvp.cart.presenter;

import com.panacea.mvp.cart.model.ShoppingCartModel;
import com.panacea.mvp.commonMvp.BaseView;
import com.panacea.mvp.order_history.model.OrderHistoryModel;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Braintech on 29-12-2017.
 */

public interface ShoppingCartContractor {
     interface ShoppingCartView extends BaseView {

        void getShoppingCartSuccess(ArrayList<ShoppingCartModel.Data> arraylistCartPrdts);
    }

     interface Presenter {
        void getShoppingCart(String jsonArrayRequest);
    }
}
