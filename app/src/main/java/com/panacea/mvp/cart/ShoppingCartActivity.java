package com.panacea.mvp.cart;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.support.annotation.StyleableRes;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.database.CartItem;
import com.panacea.common.database.CartOperations;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.AlertDialogHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.interfaces.CartUpdate;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.requestresponse.Const;
import com.panacea.common.utility.SnackNotify;
import com.panacea.mvp.cart.adapter.ShoppingCartAdapter;
import com.panacea.mvp.cart.model.ShoppingCartModel;
import com.panacea.mvp.cart.presenter.ShoppingCartContractor;
import com.panacea.mvp.cart.presenter.ShoppingCartPresenterImpl;
import com.panacea.mvp.checkout.billing_detail.BillingDetailActivity;
import com.panacea.mvp.checkout.checkout_option.CheckoutOptionActivity;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.dashboard.adapter.HomeProductAdapter;
import com.panacea.mvp.dashboard.adapter.LedRadiatorAdapter;
import com.panacea.mvp.dashboard.model.RadiatorModel;
import com.panacea.mvp.order_history.model.OrderHistoryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ShoppingCartActivity extends AppCompatActivity implements ShoppingCartContractor.ShoppingCartView {

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.recyclerViewProducts)
    RecyclerView recyclerViewProducts;

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.txtViewCheckOut)
    AppCompatTextView txtViewCheckOut;

    ShoppingCartPresenterImpl shoppingCartPresenter;
    ShoppingCartAdapter shoppingCartAdapter;

    JSONObject jsonObject;
    DBHelper dbHelper;
    ArrayList<ShoppingCartModel.Data> arrayListCartProducts;
    boolean isCheckOutClicked=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping_cart);
        ButterKnife.bind(this);
        dbHelper = new DBHelper(this);
        setTitle();
        arrayListCartProducts = new ArrayList<>();
        shoppingCartPresenter = new ShoppingCartPresenterImpl(this, this);
        shoppingCartAdapter = new ShoppingCartAdapter(this, arrayListCartProducts);
        getShoppingCartDataApi();
    }

    @Override
    protected void onResume() {
        super.onResume();

        frameLayCart.setVisibility(View.GONE);
        // updateCartQty();
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onInternetError() {

        SnackNotify.checkConnection(onretryShoppingCartApi, coordinateLayout);
    }

    OnClickInterface onretryShoppingCartApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getShoppingCartDataApi();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getShoppingCartSuccess(ArrayList<ShoppingCartModel.Data> arraylistCartPrdts) {

        arrayListCartProducts = arraylistCartPrdts;

        if (arrayListCartProducts.size() > 0) {
            // imgViewNoRecordFound.setVisibility(View.GONE);
            recyclerViewProducts.setVisibility(View.VISIBLE);
            setLayoutManager();
        } else {
            // imgViewNoRecordFound.setVisibility(View.VISIBLE);
            recyclerViewProducts.setVisibility(View.GONE);
        }
    }

    /*---------------------------------private method-------------------------------------------*/

    private void setTitle() {
        txtViewTitle.setText(getString(R.string.title_shopping_cart));
    }

    private void setLayoutManager() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewProducts.setLayoutManager(linearLayoutManager);
        recyclerViewProducts.setItemAnimator(new DefaultItemAnimator());
        recyclerViewProducts.setHasFixedSize(true);
        recyclerViewProducts.setNestedScrollingEnabled(false);
        setAdapter();
    }

    private void setAdapter() {
        shoppingCartAdapter = new ShoppingCartAdapter(this, arrayListCartProducts);
        recyclerViewProducts.setAdapter(shoppingCartAdapter);
    }

    private JSONArray getDataFromDB() {

        JSONArray jsonArray = new JSONArray();
        ArrayList<CartItem> getCartItems = dbHelper.getCartItem();

        try {
            for (int i = 0; i < getCartItems.size(); i++) {
                jsonObject = new JSONObject();
                jsonObject.put(Const.PARAM_PRODUCT_ID, getCartItems.get(i).getProductId());
                jsonObject.put(Const.PARAM_QTY, getCartItems.get(i).getProduct_quantity());
                jsonArray.put(jsonObject);
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        return jsonArray;
    }

    /*---------------------------------public method-------------------------------------------*/

    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }

    public void getShoppingCartDataApi() {
        JSONArray jsonArray = getDataFromDB();
        shoppingCartPresenter.getShoppingCart(String.valueOf(jsonArray));
    }

    public void setBitForCheckOut(boolean isClicked) {
      isCheckOutClicked=isClicked;

      if (isCheckOutClicked){
          txtViewCheckOut.setAlpha(1);
      }
      else
      {
          txtViewCheckOut.setAlpha(.5f);
      }
    }

    /*---------------------------------On Click method-------------------------------------------*/

    @OnClick(R.id.imgBackButton)
    public void backPressed() {
        onBackPressed();
    }

    @OnClick(R.id.txtViewCheckOut)
    public void txtViewCheckOutClick() {

        if (isCheckOutClicked) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, BillingDetailActivity.class));
            } else {
                startActivity(new Intent(this, CheckoutOptionActivity.class));
            }
        }
        else
        {
            AlertDialogHelper.showMessage(this, getString(R.string.title_change_product_qty));
        }
    }
}


