package com.panacea.mvp.cart.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.cart.model.ShoppingCartModel;
import com.panacea.mvp.order_history.model.OrderHistoryModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 29-12-2017.
 */

public class ShoppingCartPresenterImpl implements ShoppingCartContractor.Presenter {

    ShoppingCartContractor.ShoppingCartView shoppingCartView;
    Activity activity;
    JSONObject jsonObject;

    public ShoppingCartPresenterImpl(ShoppingCartContractor.ShoppingCartView shoppingCartView, Activity activity) {
        this.shoppingCartView = shoppingCartView;
        this.activity = activity;
    }

    @Override
    public void getShoppingCart(String jsonArrayRequest) {
        try {
            ApiAdapter.getInstance(activity);
            ShoppingCartData(jsonArrayRequest);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            shoppingCartView.onInternetError();
        }
    }

    private void ShoppingCartData(String jsonArrayRequest) {
        Progress.start(activity);

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonArrayRequest));

        Call<ShoppingCartModel> getCartOutput = ApiAdapter.getApiService().cartItems("application/json", "no-cache", body);

        getCartOutput.enqueue(new Callback<ShoppingCartModel>() {
            @Override
            public void onResponse(Call<ShoppingCartModel> call, Response<ShoppingCartModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    ShoppingCartModel shoppingCartModel = response.body();

                    if (shoppingCartModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        shoppingCartView.getShoppingCartSuccess(shoppingCartModel.getData());
                    } else {
                        shoppingCartView.apiUnsuccess(activity.getString(R.string.error_username_pass));
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    shoppingCartView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ShoppingCartModel> call, Throwable t) {
                Progress.stop();
                shoppingCartView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}
