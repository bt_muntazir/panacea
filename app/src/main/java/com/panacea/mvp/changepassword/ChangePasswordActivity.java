package com.panacea.mvp.changepassword;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.utility.SnackNotify;
import com.panacea.common.utility.Utils;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.changepassword.presenter.ChangePasswordContractor;
import com.panacea.mvp.changepassword.presenter.ChangePasswordImpl;
import com.panacea.mvp.register.RegisterActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChangePasswordActivity extends AppCompatActivity implements ChangePasswordContractor.ChangePasswordView {

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.edtTextCurrentPassword)
    AppCompatEditText edtTextCurrentPassword;

    @BindView(R.id.edtTextNewPassword)
    AppCompatEditText edtTextNewPassword;

    @BindView(R.id.edtTextConfirmPassword)
    AppCompatEditText edtTextConfirmPassword;

    @BindView(R.id.btnContinue)
    Button btnContinue;

    DBHelper dbHelper;


    ChangePasswordImpl changePassword;
    String custumerId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        dbHelper = new DBHelper(this);
        changePassword = new ChangePasswordImpl(this, this);

        //set title
        setTitle();
    }

    @Override
    protected void onResume() {
        super.onResume();

        frameLayCart.setVisibility(View.GONE);
        updateCartQty();
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onInternetError() {
        SnackNotify.checkConnection(onretryChangePassApi, coordinateLayout);
    }

    OnClickInterface onretryChangePassApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getChangePasswordApi();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getChangePasswordSuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);

        finish();
    }

        /*-----------------------------private method---------------------------------------*/

    private void getChangePasswordApi() {

        if (LoginManager.getInstance().isLoggedIn()) {

            custumerId = LoginManager.getInstance().getUserData().getCustomerId();
        }

        if (custumerId != null) {

            String oldPass = edtTextCurrentPassword.getText().toString();
            String newPass = edtTextNewPassword.getText().toString();
            String confirmPass = edtTextConfirmPassword.getText().toString();

            if (validation(oldPass, newPass, confirmPass)) {

                changePassword.getChangePass(custumerId, oldPass, newPass, confirmPass);
            }
        }
    }

    private void setTitle() {
        txtViewTitle.setText(getString(R.string.title_change_password));
    }

    private boolean validation(String oldPass, String newPass, String conPass) {

        if ((Utils.isEmptyOrNull(oldPass)) && (Utils.isEmptyOrNull(newPass)) && (Utils.isEmptyOrNull(conPass))) {

            SnackNotify.showMessage(getString(R.string.empty_all_fields), coordinateLayout);
            return false;
        }
        return true;
    }

     /*-----------------------------public method---------------------------------------*/

    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }
    /*-----------------------------On Click method---------------------------------------*/

    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (dbHelper.getCartCount() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            }
        }
    }

    @OnClick(R.id.imgBackButton)
    public void onBackClick() {

        onBackPressed();
    }

    @OnClick(R.id.btnContinue)
    public void btnContinueClick() {

        getChangePasswordApi();
    }
}
