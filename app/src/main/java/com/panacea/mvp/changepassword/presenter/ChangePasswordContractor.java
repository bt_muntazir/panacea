package com.panacea.mvp.changepassword.presenter;

import com.panacea.mvp.commonMvp.BaseView;
import com.panacea.mvp.login.model.LoginResponseModel;

/**
 * Created by Braintech on 18-12-2017.
 */

public class ChangePasswordContractor {
    public interface ChangePasswordView extends BaseView {

        void getChangePasswordSuccess(String message);
    }

    public interface Presenter {
        void getChangePass(String custumerId, String oldPass, String newPass, String conPass);
    }
}
