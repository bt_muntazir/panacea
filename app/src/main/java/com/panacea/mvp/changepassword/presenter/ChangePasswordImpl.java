package com.panacea.mvp.changepassword.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.commonMvp.model.CommonModel;
import com.panacea.mvp.login.model.LoginResponseModel;
import com.panacea.mvp.login.presenter.LoginContractor;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 18-12-2017.
 */

public class ChangePasswordImpl implements ChangePasswordContractor.Presenter {

    ChangePasswordContractor.ChangePasswordView changePasswordView;
    Activity activity;
    JSONObject jsonObject;

    public ChangePasswordImpl(ChangePasswordContractor.ChangePasswordView changePasswordView, Activity activity) {
        this.changePasswordView = changePasswordView;
        this.activity = activity;
    }

    @Override
    public void getChangePass(String custumerId, String oldPass, String newPass, String conPass) {
        try {
            ApiAdapter.getInstance(activity);
            changePassword(custumerId, oldPass, newPass, conPass);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            changePasswordView.onInternetError();
        }
    }

    private void changePassword(String custumerId, String oldPass, String newPass, String conPass) {

        Progress.start(activity);

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_CUSTOMER_ID, custumerId);
            jsonObject.put(Const.PARAM_PASSWORD, newPass);
            jsonObject.put(Const.PARAM_OLD_PASSWORD, oldPass);
            jsonObject.put(Const.PARAM_CONFIRMPASS, conPass);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CommonModel> getChangePasswordOutput = ApiAdapter.getApiService().changePassword("application/json", "no-cache", body);

        getChangePasswordOutput.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    CommonModel commonModel = response.body();
                    String message = "Hello";

                    if (commonModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        changePasswordView.getChangePasswordSuccess(commonModel.getMessage());
                    } else {
                        changePasswordView.apiUnsuccess(commonModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    changePasswordView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                Progress.stop();
                changePasswordView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}
