package com.panacea.mvp.checkout.payment_method.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.checkout.delivery_method.model.DeliveryMethodModel;
import com.panacea.mvp.checkout.payment_method.model.CreateOrderIdModel;
import com.panacea.mvp.checkout.payment_method.model.PaymentMethodModel;
import com.panacea.mvp.login.model.LoginResponseModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 03-01-2018.
 */

public class PaymentMethodPresenterImpl implements PaymentMethodContractor.Presenter{

    PaymentMethodContractor.PaymentMethodView paymentMethodView;
    Activity activity;
    JSONObject jsonObject;


    public PaymentMethodPresenterImpl(PaymentMethodContractor.PaymentMethodView paymentMethodView, Activity activity) {

        this.paymentMethodView = paymentMethodView;
        this.activity = activity;
    }

    @Override
    public void getPaymentMethod() {
        try {
            ApiAdapter.getInstance(activity);
            getPaymentData();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            paymentMethodView.onInternetError();
        }
    }

    @Override
    public void getCreateOrderIdMethod(JSONObject jsonObject) {
        try {
            ApiAdapter.getInstance(activity);
            getCreateOrderId(jsonObject);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            paymentMethodView.createOrderIdInternetError();
        }
    }

    private void getPaymentData(){
        Progress.start(activity);

        String customerId= LoginManager.getInstance().getUserData().getCustomerId();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_CUSTOMER_ID, customerId);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<PaymentMethodModel> getOutput = ApiAdapter.getApiService().paymentData("application/json", "no-cache", body);

        getOutput.enqueue(new Callback<PaymentMethodModel>() {
            @Override
            public void onResponse(Call<PaymentMethodModel> call, Response<PaymentMethodModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    PaymentMethodModel deliveryMethodModel = response.body();
                    String message = "Hello";

                    if (deliveryMethodModel.getStatus().equals("1")) {
                        paymentMethodView.getPaymentMethodSuccess(deliveryMethodModel.getData());
                    } else {
                        paymentMethodView.apiUnsuccess("");
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    paymentMethodView.serverNotResponding(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<PaymentMethodModel> call, Throwable t) {
                Progress.stop();
                paymentMethodView.serverNotResponding(activity.getString(R.string.error_server));
            }
        });
    }

    private void getCreateOrderId(JSONObject jsonObject){

        Progress.start(activity);

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CreateOrderIdModel> getOutput = ApiAdapter.getApiService().createOrderId("application/json", "no-cache", body);


        getOutput.enqueue(new Callback<CreateOrderIdModel>() {
            @Override
            public void onResponse(Call<CreateOrderIdModel> call, Response<CreateOrderIdModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    CreateOrderIdModel createOrderIdModel = response.body();
                    String message = "Hello";

                    if (createOrderIdModel.getStatus().equals("1")) {
                        paymentMethodView.createOrderIdSuccess(createOrderIdModel);
                    } else {
                        paymentMethodView.createOrderIdUnsuccess(createOrderIdModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    paymentMethodView.createOrderIdServerNotResponding(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CreateOrderIdModel> call, Throwable t) {
                Progress.stop();
                paymentMethodView.createOrderIdServerNotResponding(activity.getString(R.string.error_server));
            }
        });
    }
}
