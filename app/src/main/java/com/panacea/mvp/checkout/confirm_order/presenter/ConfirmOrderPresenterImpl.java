package com.panacea.mvp.checkout.confirm_order.presenter;

import android.app.Activity;
import android.util.Log;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.checkout.confirm_order.model.ConfirmOrderModel;
import com.panacea.mvp.commonMvp.model.CommonModel;
import com.panacea.mvp.contactus_sendmessage.presenter.ContactUsContractor;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 05-01-2018.
 */

public class ConfirmOrderPresenterImpl implements ConfirmOrderContractor.Presenter {

    ConfirmOrderContractor.ConfirmOrderView confirmOrderView;
    Activity activity;


    public ConfirmOrderPresenterImpl(ConfirmOrderContractor.ConfirmOrderView confirmOrderView, Activity activity) {
        this.confirmOrderView = confirmOrderView;
        this.activity = activity;
    }

    @Override
    public void getConfirmorderData(JSONObject jsonObject) {
        try {
            ApiAdapter.getInstance(activity);
            getConfirmOrder(jsonObject);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            confirmOrderView.onInternetError();
        }
    }

    @Override
    public void getPayAndConfirmData(JSONObject jsonObject) {
        try {
            ApiAdapter.getInstance(activity);
            getPayAndConfirmOrder(jsonObject);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            confirmOrderView.onInternetError();
        }
    }

    @Override
    public void getApplyCouponData(JSONObject jsonObject) {
        try {
            ApiAdapter.getInstance(activity);
            getApplyCoupon(jsonObject);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            confirmOrderView.applyCouponInternetError();
        }
    }

    private void getConfirmOrder(JSONObject jsonObject) {
        Progress.start(activity);


        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<ConfirmOrderModel> getOutput = ApiAdapter.getApiService().orderDetail("application/json", "no-cache", body);

        getOutput.enqueue(new Callback<ConfirmOrderModel>() {
            @Override
            public void onResponse(Call<ConfirmOrderModel> call, Response<ConfirmOrderModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    ConfirmOrderModel confirmOrderModel = response.body();

                    if (confirmOrderModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        confirmOrderView.getConfirmOrderSuccess(confirmOrderModel.getData());
                    } else {
                        confirmOrderView.apiUnsuccess(confirmOrderModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    confirmOrderView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ConfirmOrderModel> call, Throwable t) {
                Progress.stop();
                confirmOrderView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }

    private void getPayAndConfirmOrder(JSONObject jsonObject) {
        Progress.start(activity);

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CommonModel> getOutput = ApiAdapter.getApiService().payAndConfirm("application/json", "no-cache", body);

        getOutput.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    CommonModel commonModel = response.body();

                    if (commonModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        confirmOrderView.getPayAndConfirmSuccess(commonModel.getMessage());
                    } else {
                        confirmOrderView.getPayAndConfirmUnsuccess(commonModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    confirmOrderView.getPayAndConfirmServerNotResponding(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                Progress.stop();
                confirmOrderView.getPayAndConfirmServerNotResponding(activity.getString(R.string.error_server));
            }
        });
    }

    private void getApplyCoupon(JSONObject jsonObject) {
        Progress.start(activity);


        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CommonModel> getOutput = ApiAdapter.getApiService().applyCoupon("application/json", "no-cache", body);

        getOutput.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    CommonModel commonModel = response.body();

                    if (commonModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        confirmOrderView.getApplyCouponSuccess(commonModel.getMessage());
                    } else {
                        confirmOrderView.getApplyCouponUnSuccess(commonModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    confirmOrderView.applyCouponServerNotResponding(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                Progress.stop();
                confirmOrderView.applyCouponServerNotResponding(activity.getString(R.string.error_server));
            }
        });
    }

}
