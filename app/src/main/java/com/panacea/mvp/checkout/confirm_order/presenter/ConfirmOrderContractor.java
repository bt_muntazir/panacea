package com.panacea.mvp.checkout.confirm_order.presenter;

import com.panacea.mvp.checkout.confirm_order.model.ConfirmOrderModel;
import com.panacea.mvp.commonMvp.BaseView;

import org.json.JSONObject;

/**
 * Created by Braintech on 05-01-2018.
 */

public interface ConfirmOrderContractor {

    public interface ConfirmOrderView extends BaseView{

        void getConfirmOrderSuccess(ConfirmOrderModel.Data confirmOrderModel);

        void getPayAndConfirmSuccess(String message);
        void  getPayAndConfirmUnsuccess(String message);
        void getPayAndConfirmInternetError();
        void getPayAndConfirmServerNotResponding(String message);

        void getApplyCouponSuccess(String message);
        void  getApplyCouponUnSuccess(String message);
        void applyCouponInternetError();
        void applyCouponServerNotResponding(String message);
    }

    public interface Presenter{
        void getConfirmorderData(JSONObject jsonObject);
        void getPayAndConfirmData(JSONObject jsonObject);
        void getApplyCouponData(JSONObject jsonObject);

    }
}
