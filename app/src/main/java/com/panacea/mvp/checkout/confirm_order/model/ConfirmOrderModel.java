package com.panacea.mvp.checkout.confirm_order.model;

/**
 * Created by Braintech on 05-01-2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class ConfirmOrderModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Data data;

    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("page_title")
        @Expose
        private String pageTitle;
        @SerializedName("invoice_no")
        @Expose
        private String invoiceNo;
        @SerializedName("order_id")
        @Expose
        private String orderId;
        @SerializedName("date_added")
        @Expose
        private String dateAdded;
        @SerializedName("payment_address")
        @Expose
        private String paymentAddress;
        @SerializedName("payment_method")
        @Expose
        private String paymentMethod;
        @SerializedName("payment_code")
        @Expose
        private String paymentCode;
        @SerializedName("shipping_address")
        @Expose
        private String shippingAddress;
        @SerializedName("shipping_method")
        @Expose
        private String shippingMethod;
        @SerializedName("products")
        @Expose
        private ArrayList<Product> products = null;
        @SerializedName("vouchers")
        @Expose
        private ArrayList<Object> vouchers = null;
        @SerializedName("totals")
        @Expose
        private ArrayList<Total> totals = null;
        @SerializedName("comment")
        @Expose
        private String comment;
        @SerializedName("histories")
        @Expose
        private ArrayList<Object> histories = null;

        public String getPageTitle() {
            return pageTitle;
        }

        public void setPageTitle(String pageTitle) {
            this.pageTitle = pageTitle;
        }

        public String getInvoiceNo() {
            return invoiceNo;
        }

        public void setInvoiceNo(String invoiceNo) {
            this.invoiceNo = invoiceNo;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getDateAdded() {
            return dateAdded;
        }

        public void setDateAdded(String dateAdded) {
            this.dateAdded = dateAdded;
        }

        public String getPaymentAddress() {
            return paymentAddress;
        }

        public void setPaymentAddress(String paymentAddress) {
            this.paymentAddress = paymentAddress;
        }

        public String getPaymentMethod() {
            return paymentMethod;
        }

        public void setPaymentMethod(String paymentMethod) {
            this.paymentMethod = paymentMethod;
        }

        public String getPaymentCode() {
            return paymentCode;
        }

        public void setPaymentCode(String paymentCode) {
            this.paymentCode = paymentCode;
        }

        public String getShippingAddress() {
            return shippingAddress;
        }

        public void setShippingAddress(String shippingAddress) {
            this.shippingAddress = shippingAddress;
        }

        public String getShippingMethod() {
            return shippingMethod;
        }

        public void setShippingMethod(String shippingMethod) {
            this.shippingMethod = shippingMethod;
        }

        public ArrayList<Product> getProducts() {
            return products;
        }

        public void setProducts(ArrayList<Product> products) {
            this.products = products;
        }

        public ArrayList<Object> getVouchers() {
            return vouchers;
        }

        public void setVouchers(ArrayList<Object> vouchers) {
            this.vouchers = vouchers;
        }

        public ArrayList<Total> getTotals() {
            return totals;
        }

        public void setTotals(ArrayList<Total> totals) {
            this.totals = totals;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public ArrayList<Object> getHistories() {
            return histories;
        }

        public void setHistories(ArrayList<Object> histories) {
            this.histories = histories;
        }


        public class Product {

            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("model")
            @Expose
            private String model;
            @SerializedName("option")
            @Expose
            private ArrayList<Object> option = null;
            @SerializedName("quantity")
            @Expose
            private String quantity;
            @SerializedName("image")
            @Expose
            private String image;
            @SerializedName("price")
            @Expose
            private String price;
            @SerializedName("total")
            @Expose
            private String total;
            @SerializedName("reorder")
            @Expose
            private String reorder;
            @SerializedName("return")
            @Expose
            private String _return;

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getModel() {
                return model;
            }

            public void setModel(String model) {
                this.model = model;
            }

            public ArrayList<Object> getOption() {
                return option;
            }

            public void setOption(ArrayList<Object> option) {
                this.option = option;
            }

            public String getQuantity() {
                return quantity;
            }

            public void setQuantity(String quantity) {
                this.quantity = quantity;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getTotal() {
                return total;
            }

            public void setTotal(String total) {
                this.total = total;
            }

            public String getReorder() {
                return reorder;
            }

            public void setReorder(String reorder) {
                this.reorder = reorder;
            }

            public String getReturn() {
                return _return;
            }

            public void setReturn(String _return) {
                this._return = _return;
            }

        }

        public class Total {

            @SerializedName("title")
            @Expose
            private String title;
            @SerializedName("text")
            @Expose
            private String text;

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getText() {
                return text;
            }

            public void setText(String text) {
                this.text = text;
            }

        }
    }
}
