package com.panacea.mvp.checkout.checkout_option.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import com.panacea.R;
import com.panacea.common.helpers.FontHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.utility.SnackNotify;
import com.panacea.common.utility.Utils;
import com.panacea.mvp.checkout.checkout_option.CheckoutOptionActivity;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.login.LoginActivity;
import com.panacea.mvp.login.model.LoginResponseModel;
import com.panacea.mvp.login.presenter.LoginContractor;
import com.panacea.mvp.login.presenter.LoginPresenterImpl;
import com.panacea.mvp.register.RegisterActivity;
import com.panacea.mvp.register.fragment.AddressFragment;
import com.panacea.mvp.register.presenter.CountyrStatePresenterImpl;
import com.panacea.mvp.register.presenter.RegisterPresenterImpl;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Braintech on 03-01-2018.
 */

public class ReturningCustomerFragment  extends Fragment implements LoginContractor.loginView {

    @BindView(R.id.edtTextUserName)
    AppCompatEditText edtTextUserName;

    @BindView(R.id.edtTextPassword)
    AppCompatEditText edtTextPassword;

    @BindView(R.id.btnContinue)
    Button btnContinue;

    @BindView(R.id.txtViewDetail)
    AppCompatTextView txtViewDetail;


    @BindView(R.id.frameLayout)
    FrameLayout frameLayout;

    LoginPresenterImpl loginPresenter;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types and number of parameters
    public static ReturningCustomerFragment newInstance(int param1) {
        ReturningCustomerFragment fragment = new ReturningCustomerFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_returning_customer, container, false);
        ButterKnife.bind(this, view);
        return view;


    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        loginPresenter = new LoginPresenterImpl(this,getActivity());
        setFont();


    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, frameLayout);
    }

    @Override
    public void onInternetError() {
        SnackNotify.checkConnection(onretryLoginApi, frameLayout);
    }

    OnClickInterface onretryLoginApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, frameLayout);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, frameLayout);
    }

    @Override
    public void getLoginSuccess(LoginResponseModel.Data loginModel) {

        LoginManager.getInstance().createLoginSession(loginModel);

      //  SnackNotify.showMessage("Login Successfull", frameLayout);

        ((CheckoutOptionActivity) getActivity()).finishCurrentActivity();

      /*  new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ((CheckoutOptionActivity) getActivity()).finishCurrentActivity();
            }
        }, 2000);*/
    }

    private void setFont() {

        FontHelper.setFontFace(txtViewDetail, FontHelper.FontType.FONT_THIN, getActivity());
        FontHelper.setFontFace(btnContinue, FontHelper.FontType.FONT_SEMIBOLD, getActivity());
    }


    private void getData() {
        // hide keyboard
        Utils.hideKeyboardIfOpen(getActivity());

        String userName = String.valueOf(edtTextUserName.getText());
        String password = String.valueOf(edtTextPassword.getText());

        if (validation(userName, password)) {

            // call login api
            loginPresenter.getLogin(userName, password);
        }
    }

    private boolean validation(String email, String password) {

        if (Utils.isEmptyOrNull(email)) {
            SnackNotify.showMessage(getString(R.string.empty_username),frameLayout);
            //edtTextUserName.setError(getString(R.string.empty_email));
            return false;
//        } else if (!Utils.isValidEmail(email)) {
//            edtTextUserName.setError(getString(R.string.error_email));
//            return false;
        } else if (Utils.isEmptyOrNull(password)) {
            SnackNotify.showMessage(getString(R.string.empty_password),frameLayout);
            //edtTextPassword.setError(getString(R.string.empty_password));
            return false;
        }
        return true;
    }

    @OnClick(R.id.btnContinue)
    public void btnContinueClick() {

        getData();
    }

}
