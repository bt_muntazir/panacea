package com.panacea.mvp.checkout.checkout_option.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;

import com.panacea.R;
import com.panacea.common.helpers.FontHelper;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.mvp.add_edit_address.AddEditAddressActivity;
import com.panacea.mvp.address.AddressActivity;
import com.panacea.mvp.checkout.billing_detail.BillingDetailActivity;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.productdetail.ProductDetailActivity;
import com.panacea.mvp.register.RegisterActivity;
import com.panacea.mvp.register.fragment.AddressFragment;
import com.panacea.mvp.register.presenter.CountyrStatePresenterImpl;
import com.panacea.mvp.register.presenter.RegisterPresenterImpl;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * Created by Braintech on 03-01-2018.
 */

public class NewCustomerFragment extends Fragment {


    @BindView(R.id.radioRegister)
    RadioButton radioRegister;

    @BindView(R.id.radioGuest)
    RadioButton radioGuest;

    @BindView(R.id.btnContinue)
    Button btnContinue;

    @BindView(R.id.txtViewDetail)
    AppCompatTextView txtViewDetail;

    /*for REGISTER selectedRadioButtonId=1 and for GUEST selectedRadioButtonId=0*/
    int selectedRadioButtonId = 1;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types and number of parameters
    public static NewCustomerFragment newInstance(int param1) {
        NewCustomerFragment fragment = new NewCustomerFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_customer, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setFont();
    }

    private void setFont() {

        FontHelper.setFontFace(txtViewDetail, FontHelper.FontType.FONT_THIN, getActivity());
        FontHelper.setFontFace(btnContinue, FontHelper.FontType.FONT_SEMIBOLD, getActivity());
    }


    @OnCheckedChanged({R.id.radioRegister, R.id.radioGuest})
    public void onRadioButtonCheckChanged(CompoundButton button, boolean checked) {
        if (checked) {
            switch (button.getId()) {
                case R.id.radioRegister:
                    selectedRadioButtonId = 1;
                    break;
                case R.id.radioGuest:
                    selectedRadioButtonId = 2;
                    break;
            }
        }
    }

    @OnClick(R.id.btnContinue)
    public void btnContinueClick() {

        if (selectedRadioButtonId == 1) {
            Log.e("selectedRadioButtonId", "Register");

            Intent intent = new Intent(getActivity(), RegisterActivity.class);
            intent.putExtra(ConstIntent.KEY_REGISTER, ConstIntent.KEY_FROM_CHECKOUT);
            startActivity(intent);

           /* Intent intent = new Intent(getActivity(), AddEditAddressActivity.class);
            intent.putExtra(ConstIntent.KEY_ADD_EDIT_ADDRESS, ConstIntent.KEY_ADD_CHECKOUT_ADD);
            startActivity(intent);*/
        } else if (selectedRadioButtonId == 2) {
          /*  Intent intent = new Intent(getActivity(), BillingDetailActivity.class);
            startActivity(intent);*/
        }
    }
}
