package com.panacea.mvp.checkout.payment_method.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.panacea.R;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.mvp.add_edit_address.AddEditAddressActivity;
import com.panacea.mvp.address.adapter.AddressAdapter;
import com.panacea.mvp.address.model.AddressModel;
import com.panacea.mvp.checkout.delivery_method.DeliveryMethodActivity;
import com.panacea.mvp.checkout.payment_method.model.PaymentMethodModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 05-01-2018.
 */

public class PaymentMethodAdapter extends RecyclerView.Adapter {

    Activity activity;
    ArrayList<PaymentMethodModel.Data> raDataArrayListResult;

    public PaymentMethodAdapter(Activity activity, ArrayList<PaymentMethodModel.Data> raDataArrayList) {
        this.activity = activity;
        this.raDataArrayListResult = raDataArrayList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_payment_method, parent, false);
        return new PaymentMethodAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        String payMentMethodSession = LoginManager.getInstance().getPaymentMethod();

        ((PaymentMethodAdapter.ViewHolder) holder).radioPayment.setTag(position);

        PaymentMethodModel.Data paymentdata = raDataArrayListResult.get(position);

        // ((PaymentMethodAdapter.ViewHolder) holder).radioPayment.setTag(position);

        ((PaymentMethodAdapter.ViewHolder) holder).radioPayment.setText(paymentdata.getText());

        //check is selected
       /* if (paymentdata.getIsSelected())
            ((PaymentMethodAdapter.ViewHolder) holder).radioPayment.setChecked(true);
        else
            ((PaymentMethodAdapter.ViewHolder) holder).radioPayment.setChecked(false);
*/
        //Check the session and set the radio button
        if (paymentdata.getValue().equals(payMentMethodSession)) {

            ((PaymentMethodAdapter.ViewHolder) holder).radioPayment.setChecked(true);
        } else {
            ((PaymentMethodAdapter.ViewHolder) holder).radioPayment.setChecked(false);
        }


        ((PaymentMethodAdapter.ViewHolder) holder).radioPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tag = (Integer) v.getTag();
                Log.e("Muntazir", raDataArrayListResult.get(tag).getValue());
                //save in session
                LoginManager.getInstance().savePaymentMethod(String.valueOf(raDataArrayListResult.get(tag).getValue()));

             /*   if ( raDataArrayListResult.get(tag).getValue().equals("cod")) {


                }
                else
                {
                    //save in session
                    LoginManager.getInstance().savePaymentMethod(String.valueOf( raDataArrayListResult.get(tag).getValue()));
                }
*/


                Log.e("Muntazir", String.valueOf(tag));

               /* for (int i = 0; i < raDataArrayListResult.size(); i++) {

                    if (i == tag) {
                        raDataArrayListResult.get(i).setSelected(true);
                    } else {
                        raDataArrayListResult.get(i).setSelected(false);
                    }
                }*/
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return raDataArrayListResult.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.radioPayment)
        RadioButton radioPayment;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
