package com.panacea.mvp.checkout.payment_method;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.database.CartItem;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.FontHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.requestresponse.Const;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.common.utility.SnackNotify;
import com.panacea.mvp.address.adapter.AddressAdapter;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.checkout.confirm_order.ConfirmOrderActivity;
import com.panacea.mvp.checkout.payment_method.adapter.PaymentMethodAdapter;
import com.panacea.mvp.checkout.payment_method.model.CreateOrderIdModel;
import com.panacea.mvp.checkout.payment_method.model.PaymentMethodModel;
import com.panacea.mvp.checkout.payment_method.presenter.PaymentMethodContractor;
import com.panacea.mvp.checkout.payment_method.presenter.PaymentMethodPresenterImpl;
import com.panacea.mvp.dashboard.DashboardActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class PaymentMethodActivity extends AppCompatActivity implements PaymentMethodContractor.PaymentMethodView {

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.imgViewHome)
    ImageView imgViewHome;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

   /* @BindView(R.id.radioPayment)
    RadioButton radioPayment;*/

    @BindView(R.id.edtTextComments)
    AppCompatEditText edtTextComments;

    @BindView(R.id.txtViewHeading)
    AppCompatTextView txtViewHeading;

    @BindView(R.id.txtViewPrefShipping)
    AppCompatTextView txtViewPrefShipping;

    @BindView(R.id.recyclerViewProducts)
    RecyclerView recyclerViewProducts;

    @BindView(R.id.txtViewStep)
    AppCompatTextView txtViewStep;

    @BindView(R.id.btnContinue)
    Button btnContinue;
    PaymentMethodPresenterImpl paymentMethodPresenter;
    PaymentMethodAdapter paymentMethodAdapter;
    LinearLayoutManager linearLayoutManager;

    ArrayList<PaymentMethodModel.Data> arrayListPaymentMethod;

    int selectedRadioButtonId = 1;
    String selectedRadioText;
    String chkOutComment;
    DBHelper dbHelper;
    ArrayList<CartItem> cartItems;

    JSONObject jsonObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);
        ButterKnife.bind(this);
        setFont();
        getSessionData();
        dbHelper = new DBHelper(this);
        arrayListPaymentMethod = new ArrayList<>();
        cartItems = dbHelper.getCartItem();
        setTitle();
        paymentMethodPresenter = new PaymentMethodPresenterImpl(this, this);
        paymentMethodAdapter = new PaymentMethodAdapter(this, arrayListPaymentMethod);
        getPaymentTypeDataApi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        frameLayCart.setVisibility(View.GONE);
        imgViewHome.setVisibility(View.VISIBLE);
        updateCartQty();
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onInternetError() {

        SnackNotify.checkConnection(onretryGetPaymentTypeApi, coordinateLayout);
    }

    OnClickInterface onretryGetPaymentTypeApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getPaymentTypeDataApi();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getPaymentMethodSuccess(ArrayList<PaymentMethodModel.Data> arrayListPaymentMethod) {

        this.arrayListPaymentMethod = arrayListPaymentMethod;

        setLayoutManager();

        setTheField();
    }


    @Override
    public void createOrderIdSuccess(CreateOrderIdModel orderIdModel) {
        Log.e("Order ID", String.valueOf(orderIdModel.getOrderId()));

        //save data to session
        LoginManager.getInstance().saveOrderId(String.valueOf(orderIdModel.getOrderId()));

        //go to the next activity
        Intent intent = new Intent(this, ConfirmOrderActivity.class);
        startActivity(intent);
    }

    @Override
    public void createOrderIdUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void createOrderIdInternetError() {
        SnackNotify.checkConnection(onretryOrderIdApi, coordinateLayout);
    }

    OnClickInterface onretryOrderIdApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getCrateOrderIdDataApi();
        }
    };

    @Override
    public void createOrderIdServerNotResponding(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    /*-------------------------------------private method-------------------------------------*/

    private void getSessionData() {

        selectedRadioText = LoginManager.getInstance().getPaymentMethod();
        chkOutComment = LoginManager.getInstance().getChkOutComment();

    }


    private void setFont() {

        //FontHelper.setFontFace(radioPayment, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(edtTextComments, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(btnContinue, FontHelper.FontType.FONT_SEMIBOLD, this);
        FontHelper.setFontFace(txtViewHeading, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewPrefShipping, FontHelper.FontType.FONT_SEMIBOLD, this);
        FontHelper.setFontFace(txtViewStep, FontHelper.FontType.FONT_THIN, this);
    }

    private void setLayoutManager() {
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewProducts.setLayoutManager(linearLayoutManager);
        recyclerViewProducts.setItemAnimator(new DefaultItemAnimator());
        recyclerViewProducts.setHasFixedSize(true);
        recyclerViewProducts.setNestedScrollingEnabled(false);
        setAdapter();
    }

    private void setAdapter() {
        paymentMethodAdapter = new PaymentMethodAdapter(this, arrayListPaymentMethod);
        recyclerViewProducts.setAdapter(paymentMethodAdapter);
    }


    private void setTitle() {
        txtViewTitle.setText(getString(R.string.title_checkout));
    }

    private void getPaymentTypeDataApi() {
        paymentMethodPresenter.getPaymentMethod();
    }

    private void setTheField() {

        edtTextComments.setText(chkOutComment);

      /*  if (arrayListPaymentMethod != null) {

            for (int i = 0; i < arrayListPaymentMethod.size(); i++) {

                if (i == 0) {
                    radioPayment.setText(arrayListPaymentMethod.get(i).getText());
                    selectedRadioText = (arrayListPaymentMethod.get(i).getValue());

                    //save addressId in session
                    LoginManager.getInstance().savePaymentMethod(String.valueOf(selectedRadioText));

                }

            }
        }*/
    }


    private void getCrateOrderIdDataApi() {
        getJsonData();

        Log.e("Json data", jsonObject.toString());

        paymentMethodPresenter.getCreateOrderIdMethod(jsonObject);
    }

    private void getJsonData() {

        String customerId = LoginManager.getInstance().getUserData().getCustomerId();
        String BillingAddId = LoginManager.getInstance().getBillingAddressId();
        String deliveryAddId = LoginManager.getInstance().getDeliveryAddressId();
        String shippingMethod = LoginManager.getInstance().getDeliveryMethod();
        String paymentMethod = LoginManager.getInstance().getPaymentMethod();
        String courierData = LoginManager.getInstance().getCourierInfo();
        String commentData = LoginManager.getInstance().getChkOutComment();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_CUSTOMER_ID, customerId);
            jsonObject.put(Const.PARAM_BILLING_ADD_ID, BillingAddId);
            jsonObject.put(Const.PARAM_DELIVERY_ADD_ID, deliveryAddId);
            jsonObject.put(Const.PARAM_SHIPPING_METHOD, shippingMethod);
            jsonObject.put(Const.PARAM_PAYMENT_METHOD, paymentMethod);
            jsonObject.put(Const.PARAM_COURIER_INFO, courierData);
            jsonObject.put(Const.PARAM_ORDER_COMENTS, commentData);
            jsonObject.put(Const.PARAM_PRODUCT_DETAIL, getCartItemJsonArray());

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
    }

    private JSONArray getCartItemJsonArray() {
        JSONObject jsonObject;
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < cartItems.size(); i++) {
                jsonObject = new JSONObject();
                jsonObject.put(Const.PARAM_PRODUCT_ID, cartItems.get(i).getProductId());
                jsonObject.put(Const.PARAM_QTY, cartItems.get(i).getProduct_quantity());
                jsonArray.put(jsonObject);
            }

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        return jsonArray;
    }

        /*--------------------------------------public method--------------------------------*/

    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }

    /*------------------on click method--------------------------------------------------------*/

    @OnClick(R.id.imgBackButton)
    public void backPressed() {
        onBackPressed();
    }

    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (dbHelper.getCartCount() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
                finish();
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
                finish();
            }
        }
    }

    @OnClick(R.id.btnContinue)
    public void btnContinueClicked() {

        //save in session
        String payMentMethod = LoginManager.getInstance().getPaymentMethod();
        if (payMentMethod.length() > 0) {
            getCrateOrderIdDataApi();
        }
        else
        {
            SnackNotify.showMessage(getString(R.string.error_payment_method), coordinateLayout);
        }
    }

    @OnClick(R.id.imgViewHome)
    public void imgViewHomeClicked(){
        startActivity(new Intent(this, DashboardActivity.class));

        finishAffinity();

    }

}
