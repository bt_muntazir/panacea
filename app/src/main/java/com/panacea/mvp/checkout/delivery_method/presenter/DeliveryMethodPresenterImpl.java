package com.panacea.mvp.checkout.delivery_method.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.mvp.checkout.delivery_method.model.DeliveryMethodModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 03-01-2018.
 */

public class DeliveryMethodPresenterImpl implements DeliveryMethodContractor.presenter {


    DeliveryMethodContractor.DeliveryMethodView deliveryMethodView;
    Activity activity;


    public DeliveryMethodPresenterImpl(DeliveryMethodContractor.DeliveryMethodView deliveryMethodView, Activity activity) {

        this.deliveryMethodView = deliveryMethodView;
        this.activity = activity;
    }


    @Override
    public void getDeliveryMethod() {
        try {
            ApiAdapter.getInstance(activity);
            getShippingData();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            deliveryMethodView.onInternetError();
        }
    }

    private void getShippingData(){
        Progress.start(activity);

        Call<DeliveryMethodModel> getOutput = ApiAdapter.getApiService().shippingData();

        getOutput.enqueue(new Callback<DeliveryMethodModel>() {
            @Override
            public void onResponse(Call<DeliveryMethodModel> call, Response<DeliveryMethodModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    DeliveryMethodModel deliveryMethodModel = response.body();
                    String message = "Hello";

                    if (deliveryMethodModel.getStatus().equals("1")) {
                        deliveryMethodView.getDeliveryMethodSuccess(deliveryMethodModel.getData());
                    } else {
                        deliveryMethodView.apiUnsuccess("");
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    deliveryMethodView.serverNotResponding(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<DeliveryMethodModel> call, Throwable t) {
                Progress.stop();
                deliveryMethodView.serverNotResponding(activity.getString(R.string.error_server));
            }
        });
    }
}
