package com.panacea.mvp.checkout.confirm_order;

import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.adapter.model.CountrySpinnerModel;
import com.panacea.common.database.CartItem;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.AlertDialogHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.requestresponse.Const;
import com.panacea.common.utility.SnackNotify;
import com.panacea.common.utility.Utils;
import com.panacea.mvp.checkout.confirm_order.adapter.ConfirmOrderAdapter;
import com.panacea.mvp.checkout.confirm_order.model.ConfirmOrderModel;
import com.panacea.mvp.checkout.confirm_order.presenter.ConfirmOrderContractor;
import com.panacea.mvp.checkout.confirm_order.presenter.ConfirmOrderPresenterImpl;
import com.panacea.mvp.commonMvp.adapter.AdapterSpinner;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.order_information.adapter.OrderInformationAdapter;
import com.panacea.mvp.order_information.model.OrderInformationModel;
import com.panacea.mvp.productdetail.model.ProductDetailModel;
import com.panacea.mvp.success_order.SuccessOrderActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

public class ConfirmOrderActivity extends AppCompatActivity implements ConfirmOrderContractor.ConfirmOrderView {

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.imgViewCart)
    ImageView imgViewCart;

    @BindView(R.id.imgViewHome)
    ImageView imgViewHome;

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.txtViewHeading)
    AppCompatTextView txtViewHeading;

    @BindView(R.id.txtViewStep)
    AppCompatTextView txtViewStep;

    @BindView(R.id.shippingMethod)
    AppCompatTextView shippingMethod;

    @BindView(R.id.txtViewShippingMethod)
    AppCompatTextView txtViewShippingMethod;

    @BindView(R.id.paymentAddress)
    AppCompatTextView paymentAddress;

    @BindView(R.id.txtViewPaymentAddress)
    TextView txtViewPaymentAddress;

    @BindView(R.id.shippingAddress)
    AppCompatTextView shippingAddress;

    @BindView(R.id.txtViewShippingAddress)
    AppCompatTextView txtViewShippingAddress;

    @BindView(R.id.recyclerViewProducts)
    RecyclerView recyclerViewProducts;

    @BindView(R.id.tableLayout)
    TableLayout tableLayout;

    @BindView(R.id.tableRow)
    TableRow tableRow;

    @BindView(R.id.txtViewCreditCardDetail)
    AppCompatTextView txtViewCreditCardDetail;

    @BindView(R.id.edtTextCardOwner)
    AppCompatEditText edtTextCardOwner;

    @BindView(R.id.edtTextCardNumber)
    AppCompatEditText edtTextCardNumber;

    @BindView(R.id.spinnerMonth)
    AppCompatSpinner spinnerMonth;

    @BindView(R.id.linLayMonth)
    LinearLayout linLayMonth;

    @BindView(R.id.spinnerYear)
    AppCompatSpinner spinnerYear;

    @BindView(R.id.edtTextCVV)
    AppCompatEditText edtTextCVV;

    @BindView(R.id.btnContinue)
    Button btnContinue;

    @BindView(R.id.relLayCreditCard)
    RelativeLayout relLayCreditCard;

    @BindView(R.id.txtViewHaveCouponCode)
    TextView txtViewHaveCouponCode;

    DBHelper dbHelper;
    ArrayList<CartItem> cartItems;

    JSONObject jsonObject;

    ConfirmOrderModel.Data confirmOrderModel;

    ArrayList<ConfirmOrderModel.Data.Product> arrayListProduct;

    ConfirmOrderAdapter confirmOrderAdapter;
    ConfirmOrderPresenterImpl confirmOrderPresenter;
    LinearLayoutManager linearLayoutManager;

    ArrayList<HashMap<String, String>> arrayListMonth;
    ArrayList<HashMap<String, String>> arrayListYear;

    AdapterSpinner adapterSpinnerMonth;
    AdapterSpinner adapterSpinnerYear;
    String cc_exp_year;
    String cc_exp_month;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_order);
        ButterKnife.bind(this);
        dbHelper = new DBHelper(this);
        cartItems = dbHelper.getCartItem();
        arrayListProduct = new ArrayList<>();
        confirmOrderPresenter = new ConfirmOrderPresenterImpl(this, this);
        confirmOrderAdapter = new ConfirmOrderAdapter(this, arrayListProduct);
        getConfirmOrderDataApi();
        setTitle();
        tableLayout.setColumnStretchable(0, true);
        tableLayout.setColumnStretchable(1, true);
        frameLayCart.setVisibility(View.GONE);
        setInitialAdapter();
        getMonths();
        getYear();
    }

    @Override
    protected void onResume() {
        super.onResume();
        frameLayCart.setVisibility(View.GONE);
        imgViewHome.setVisibility(View.VISIBLE);
        //updateCartQty();
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onInternetError() {
        SnackNotify.checkConnection(onretryConfirmOrderApi, coordinateLayout);
    }

    OnClickInterface onretryConfirmOrderApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getConfirmOrderDataApi();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getConfirmOrderSuccess(ConfirmOrderModel.Data confirmOrderModel) {

        this.confirmOrderModel = confirmOrderModel;

        arrayListProduct = confirmOrderModel.getProducts();

        setTheField();
        setLayoutManager();
        setTableLayout();
    }

    @Override
    public void getPayAndConfirmSuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);

        startActivity(new Intent(this, SuccessOrderActivity.class));
    }

    @Override
    public void getPayAndConfirmUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getPayAndConfirmInternetError() {
        SnackNotify.checkConnection(onretryPayAndConfirmApi, coordinateLayout);
    }

    OnClickInterface onretryPayAndConfirmApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getPaymentDataApi();
        }
    };

    @Override
    public void getPayAndConfirmServerNotResponding(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getApplyCouponSuccess(String message) {

        getConfirmOrderDataApi();
    }

    @Override
    public void getApplyCouponUnSuccess(String message) {
        //edtTextCoupenCode.setText("");
        //SnackNotify.showMessage(message, coordinateLayout);
        AlertDialogHelper.showMessage(this, message);
    }

    @Override
    public void applyCouponInternetError() {

        SnackNotify.checkConnection(onretryApplyCouponApi, coordinateLayout);
    }

    OnClickInterface onretryApplyCouponApi = new OnClickInterface() {
        @Override
        public void onClick() {
            //getApplyCouponDataApi();
        }
    };

    @Override
    public void applyCouponServerNotResponding(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }
    /*---------------------------------private method--------------------------------*/

    private void ShowDoalogForCoupon() {

        // Create custom dialog object
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.item_apply_coupon);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.alpha(Color.BLACK)));


// set values for custom dialog components - text, image and button
        final AppCompatEditText edtTextCouponCode = (AppCompatEditText) dialog.findViewById(R.id.edtTextCouponCode);
        ImageView imgViewCross = (ImageView) dialog.findViewById(R.id.imgViewCross);

        dialog.show();

        Button btnApply = (Button) dialog.findViewById(R.id.btnApply);
// if decline button is clicked, close the custom dialog

        imgViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // hide keyboard
                //Utils.hideKeyboardIfOpen(this);
                getApplyCouponDataApi(String.valueOf(edtTextCouponCode.getText()));

                dialog.dismiss();
            }
        });
    }

    private void setTitle() {
        txtViewTitle.setText(getString(R.string.title_checkout));
    }


    private void setInitialAdapter() {
        arrayListMonth = new ArrayList<>();
        HashMap hashMapFirstIndexCountry = new HashMap();
        hashMapFirstIndexCountry.put(Const.KEY_ID, 0);
        hashMapFirstIndexCountry.put(Const.KEY_NAME, getString(R.string.select_month));
        arrayListMonth.add(hashMapFirstIndexCountry);

        arrayListYear = new ArrayList<>();
        HashMap hashMapFirstIndexState = new HashMap();
        hashMapFirstIndexState.put(Const.KEY_ID, 0);
        hashMapFirstIndexState.put(Const.KEY_NAME, getString(R.string.select_year));
        arrayListYear.add(hashMapFirstIndexState);

        setMonthAdapter();
        setYearAdapter();
    }

    private void setTableLayout() {

        tableLayout.removeAllViews();

        ArrayList<ConfirmOrderModel.Data.Total> arrayListTotal = confirmOrderModel.getTotals();

        if (arrayListTotal != null && arrayListTotal.size() > 0) {

            for (int i = 0; i < arrayListTotal.size(); i++) {

                tableRow = new TableRow(this);
                tableRow.setVisibility(View.VISIBLE);

                View view = LayoutInflater.from(this).inflate(R.layout.item_order_info_table_row, null, false);
                AppCompatTextView title = (AppCompatTextView) view.findViewById(R.id.txtViewTitlePrice);
                AppCompatTextView price = (AppCompatTextView) view.findViewById(R.id.txtViewPrice);
                RelativeLayout linLayMain = (RelativeLayout) view.findViewById(R.id.linLayMain);

                if (i + 1 == arrayListTotal.size()) {

                    linLayMain.setBackgroundColor(ContextCompat.getColor(this, R.color.color_gray));
                }

                title.setText(arrayListTotal.get(i).getTitle() + " = ");
                price.setText(arrayListTotal.get(i).getText());

                tableRow.addView(view);

                tableLayout.addView(tableRow);
            }
        }
    }

    private void setLayoutManager() {
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewProducts.setLayoutManager(linearLayoutManager);
        recyclerViewProducts.setItemAnimator(new DefaultItemAnimator());
        recyclerViewProducts.setHasFixedSize(true);
        recyclerViewProducts.setNestedScrollingEnabled(false);
        setAdapter();
    }

    private void setAdapter() {
        confirmOrderAdapter = new ConfirmOrderAdapter(this, arrayListProduct);
        recyclerViewProducts.setAdapter(confirmOrderAdapter);
    }

    private void setTheField() {
        txtViewShippingMethod.setText(confirmOrderModel.getShippingMethod());
        txtViewPaymentAddress.setText(confirmOrderModel.getPaymentAddress());
        txtViewShippingAddress.setText(confirmOrderModel.getShippingAddress());

        if (confirmOrderModel.getPaymentCode().equals("web_payment_software")) {
            txtViewCreditCardDetail.setVisibility(View.VISIBLE);
            relLayCreditCard.setVisibility(View.VISIBLE);
        } else {
            txtViewCreditCardDetail.setVisibility(View.GONE);
            relLayCreditCard.setVisibility(View.GONE);
        }
    }

    private void getConfirmOrderDataApi() {

        if (LoginManager.getInstance().isLoggedIn()) {
            String customerId = LoginManager.getInstance().getUserData().getCustomerId();
            String orderId = LoginManager.getInstance().getOrderId();
            String paymentMethod = LoginManager.getInstance().getPaymentMethod();

            try {
                jsonObject = new JSONObject();
                jsonObject.put(Const.PARAM_CUSTOMER_ID, customerId);
                jsonObject.put(Const.PARAM_ORDER_ID, orderId);
                jsonObject.put(Const.PARAM_PAYMENT_METHOD, paymentMethod);

            } catch (JSONException ex) {
                ex.printStackTrace();
            }

            confirmOrderPresenter.getConfirmorderData(jsonObject);
        }

    }


    private void getMonths() {
        Resources res = getResources();
        String[] months = res.getStringArray(R.array.month);

        for (int i = 0; i < months.length; i++) {
            String monthId = "" + i;
            String monthName = months[i];

            HashMap hashMap = new HashMap();
            hashMap.put(Const.KEY_ID, monthId);
            hashMap.put(Const.KEY_NAME, monthName);
            arrayListMonth.add(hashMap);
            setMonthAdapter();
        }
    }

    private void getYear() {
        Resources res = getResources();
        String[] years = res.getStringArray(R.array.year);

        for (int i = 0; i < years.length; i++) {
            String yearId = "" + i;
            String year = years[i];

            HashMap hashMap = new HashMap();
            hashMap.put(Const.KEY_ID, yearId);
            hashMap.put(Const.KEY_NAME, year);
            arrayListYear.add(hashMap);
            setYearAdapter();
        }
    }

    private void setMonthAdapter() {
        adapterSpinnerMonth = new AdapterSpinner(this, R.layout.spinner_selected_view, arrayListMonth);
        spinnerMonth.setAdapter(adapterSpinnerMonth);
    }

    private void setYearAdapter() {
        adapterSpinnerYear = new AdapterSpinner(this, R.layout.spinner_selected_view, arrayListYear);
        spinnerYear.setAdapter(adapterSpinnerYear);
    }

    private void getJsonDataCreditCard() {

        if (LoginManager.getInstance().isLoggedIn()) {
            String customerId = LoginManager.getInstance().getUserData().getCustomerId();
            String orderId = LoginManager.getInstance().getOrderId();

            String cardNumber = String.valueOf(edtTextCardNumber.getText());
            String cardCVV = String.valueOf(edtTextCVV.getText());

            if (validation(cardNumber, cc_exp_month, cc_exp_year)) {
                try {
                    jsonObject = new JSONObject();
                    jsonObject.put(Const.PARAM_CUSTOMER_ID, customerId);
                    jsonObject.put(Const.PARAM_ORDER_ID,orderId );
                    jsonObject.put(Const.PARAM_CC_NUMBER, cardNumber);
                    jsonObject.put(Const.PARAM_CC_EXP_MONTH, cc_exp_month);
                    jsonObject.put(Const.PARAM_CC_EXP_YEAR, cc_exp_year);
                    jsonObject.put(Const.PARAM_CC_CVV, cardCVV);

                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
                confirmOrderPresenter.getPayAndConfirmData(jsonObject);
            }
        }
    }

    private boolean validation(String cardNO, String month, String year) {

        if (Utils.isEmptyOrNull(cardNO)) {
            SnackNotify.showMessage(getString(R.string.empty_card_no), coordinateLayout);
            return false;
        } else if (cardNO.length() != 16) {
            SnackNotify.showMessage(getString(R.string.error_card_no), coordinateLayout);
            return false;
        } else if (Utils.isEmptyOrNull(month)) {
            SnackNotify.showMessage(getString(R.string.empty_month), coordinateLayout);
            return false;
        } else if (Utils.isEmptyOrNull(year)) {
            SnackNotify.showMessage(getString(R.string.empty_year), coordinateLayout);
            return false;
        }
        return true;
    }

    private void getJsonDataCOD() {

        if (LoginManager.getInstance().isLoggedIn()) {
            String customerId = LoginManager.getInstance().getUserData().getCustomerId();
            String orderId = LoginManager.getInstance().getOrderId();

            try {
                jsonObject = new JSONObject();
                jsonObject.put(Const.PARAM_CUSTOMER_ID, customerId);
                jsonObject.put(Const.PARAM_ORDER_ID, orderId);

            } catch (JSONException ex) {
                ex.printStackTrace();
            }

            confirmOrderPresenter.getPayAndConfirmData(jsonObject);
        }
    }


    private void getPaymentDataApi() {

        if (confirmOrderModel.getPaymentCode().equals("web_payment_software")) {
            getJsonDataCreditCard();
        } else {
            getJsonDataCOD();
        }
    }

    private void getApplyCouponDataApi(String couponCode) {
        //String couponCode = String.valueOf(edtTextCoupenCode.getText());
        if (couponCode.length() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                String customerId = LoginManager.getInstance().getUserData().getCustomerId();
                String orderId = LoginManager.getInstance().getOrderId();

                try {
                    jsonObject = new JSONObject();
                    jsonObject.put(Const.PARAM_CUSTOMER_ID, customerId);
                    jsonObject.put(Const.PARAM_ORDER_ID, orderId);
                    jsonObject.put(Const.PARAM_COUPON, couponCode);

                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
            confirmOrderPresenter.getApplyCouponData(jsonObject);

        } else {
            SnackNotify.showMessage(getString(R.string.empty_coupon_code), coordinateLayout);
        }

    }

    /*---------------------------------on click method--------------------------------*/

    @OnClick(R.id.imgBackButton)
    public void onBackClick() {

        finish();
    }

    @OnItemSelected(R.id.spinnerMonth)
    void spinnerMonthSelected(int position) {
        if (position != 0) {
            cc_exp_month = String.valueOf(arrayListMonth.get(position).get(Const.KEY_NAME));
            Log.e("Month ", cc_exp_month);
        }
    }

    @OnItemSelected(R.id.spinnerYear)
    void spinnerYearSelected(int position) {
        if (position != 0) {
            cc_exp_year = String.valueOf(arrayListYear.get(position).get(Const.KEY_NAME));
            Log.e("Month ", cc_exp_year);
        }
    }

    @OnClick(R.id.btnContinue)
    public void btnContinueClick() {

        getPaymentDataApi();
    }

    @OnClick(R.id.txtViewHaveCouponCode)
    public void txtViewHaveCouponCodeClicked() {

        ShowDoalogForCoupon();
    }

    @OnClick(R.id.imgViewHome)
    public void imgViewHomeClicked() {
        startActivity(new Intent(this, DashboardActivity.class));

        finishAffinity();

    }
}


