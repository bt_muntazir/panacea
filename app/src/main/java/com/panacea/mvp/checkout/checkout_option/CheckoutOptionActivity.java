package com.panacea.mvp.checkout.checkout_option;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.FontHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.checkout.billing_detail.BillingDetailActivity;
import com.panacea.mvp.checkout.checkout_option.adapter.CheckoutOptionPagerAdapter;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.login.LoginActivity;
import com.panacea.mvp.register.RegisterActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CheckoutOptionActivity extends AppCompatActivity {

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.imgViewHome)
    ImageView imgViewHome;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.txtViewHeading)
    AppCompatTextView txtViewHeading;

    @BindView(R.id.txtViewStep)
    AppCompatTextView txtViewStep;


    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_option);

        ButterKnife.bind(this);
        dbHelper = new DBHelper(this);
        setTitle();
        setFont();
        setTab();
    }

    @Override
    protected void onResume() {
        super.onResume();
        imgViewHome.setVisibility(View.VISIBLE);
        frameLayCart.setVisibility(View.GONE);
        updateCartQty();
    }

     /*-------------------------------private method-------------------------------------*/

    private void setFont() {

        FontHelper.setFontFace(txtViewHeading, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewStep, FontHelper.FontType.FONT_REGULAR, this);
    }

    private void setTitle() {
        txtViewTitle.setText(getString(R.string.title_checkout));
    }

    private void setTab() {

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.title_new_customer)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.title_returning_customer)));


        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        final CheckoutOptionPagerAdapter checkoutOptionPagerAdapter = new CheckoutOptionPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), this);
        viewPager.setAdapter(checkoutOptionPagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                // Log.e("pos", "" + tab.getPosition());
                viewPager.setCurrentItem(tab.getPosition());

                if (tab.getPosition() == 0) {
                    tabLayout.setBackgroundResource(R.mipmap.ic_tab1_selected);
                } else if (tab.getPosition() == 1) {
                    tabLayout.setBackgroundResource(R.mipmap.ic_tab2_selected);
                }
                //tabLayout.setBackground();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
     /*-------------------------------public method-------------------------------------*/


    public void finishCurrentActivity() {
        Intent intent = new Intent(CheckoutOptionActivity.this, BillingDetailActivity.class);
        startActivity(intent);
        //SnackNotify.showMessage("Register Successfully", coordinateLayout);
    }


    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }

    /*-------------------------------on click method-------------------------------------*/

    @OnClick(R.id.imgBackButton)
    public void onBackClick() {

        onBackPressed();
    }

    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (dbHelper.getCartCount() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
                finish();
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
                finish();
            }
        }
    }

    @OnClick(R.id.imgViewHome)
    public void imgViewHomeClicked(){
        startActivity(new Intent(this, DashboardActivity.class));

        finishAffinity();

    }
}
