package com.panacea.mvp.checkout.confirm_order.adapter;

import android.app.Activity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.panacea.R;
import com.panacea.mvp.checkout.confirm_order.model.ConfirmOrderModel;
import com.panacea.mvp.order_information.adapter.OrderInformationAdapter;
import com.panacea.mvp.order_information.model.OrderInformationModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 05-01-2018.
 */

public class ConfirmOrderAdapter extends RecyclerView.Adapter {

    Activity activity;
    ArrayList<ConfirmOrderModel.Data.Product> raDataArrayListResult;

    public ConfirmOrderAdapter(Activity activity, ArrayList<ConfirmOrderModel.Data.Product> raDataArrayList) {
        this.activity = activity;
        this.raDataArrayListResult = raDataArrayList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_confirm_order, parent, false);
        return new ConfirmOrderAdapter.ConfirmOrderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final ConfirmOrderModel.Data.Product productData = raDataArrayListResult.get(position);

        String price = productData.getPrice() + " X " + productData.getQuantity() + " = ";

         Picasso.with(activity).load(productData.getImage()).into(((ConfirmOrderAdapter.ConfirmOrderViewHolder) holder).imgProduct);
        ((ConfirmOrderAdapter.ConfirmOrderViewHolder) holder).txtViewHeadingTitle.setText(productData.getName());
        ((ConfirmOrderAdapter.ConfirmOrderViewHolder) holder).txtViewPartNo.setText(productData.getModel().toString());
        ((ConfirmOrderAdapter.ConfirmOrderViewHolder) holder).txtViewPrice.setText(price);
        ((ConfirmOrderViewHolder) holder).txtViewTotalPrice.setText(productData.getTotal());

    }

    @Override
    public int getItemCount() {
        return raDataArrayListResult.size();
    }

    public class ConfirmOrderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imgProduct)
        ImageView imgProduct;

        @BindView(R.id.txtViewHeadingTitle)
        AppCompatTextView txtViewHeadingTitle;

        @BindView(R.id.txtViewPart)
        AppCompatTextView txtViewPart;

        @BindView(R.id.txtViewPartNo)
        AppCompatTextView txtViewPartNo;

        @BindView(R.id.txtViewPrice)
        AppCompatTextView txtViewPrice;

        @BindView(R.id.txtViewTotalPrice)
        AppCompatTextView txtViewTotalPrice;


        public ConfirmOrderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
