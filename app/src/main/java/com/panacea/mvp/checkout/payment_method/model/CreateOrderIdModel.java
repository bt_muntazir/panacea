package com.panacea.mvp.checkout.payment_method.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Braintech on 05-01-2018.
 */


public class CreateOrderIdModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("order_id")
    @Expose
    private Integer orderId;
    @SerializedName("message")
    @Expose
    private String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}