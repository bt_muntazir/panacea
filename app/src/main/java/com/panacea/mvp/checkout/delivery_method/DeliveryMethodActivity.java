package com.panacea.mvp.checkout.delivery_method;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.FontHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.common.utility.SnackNotify;
import com.panacea.mvp.add_edit_address.AddEditAddressActivity;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.checkout.delivery_method.adapter.DeliveryMethodAdapter;
import com.panacea.mvp.checkout.delivery_method.model.DeliveryMethodModel;
import com.panacea.mvp.checkout.delivery_method.presenter.DeliveryMethodContractor;
import com.panacea.mvp.checkout.delivery_method.presenter.DeliveryMethodPresenterImpl;
import com.panacea.mvp.checkout.payment_method.PaymentMethodActivity;
import com.panacea.mvp.checkout.payment_method.adapter.PaymentMethodAdapter;
import com.panacea.mvp.dashboard.DashboardActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class DeliveryMethodActivity extends AppCompatActivity implements DeliveryMethodContractor.DeliveryMethodView {

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.imgViewHome)
    ImageView imgViewHome;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

    @BindView(R.id.txtViewHeading)
    AppCompatTextView txtViewHeading;

    @BindView(R.id.txtViewStep)
    AppCompatTextView txtViewStep;

    @BindView(R.id.txtViewPrefShipping)
    AppCompatTextView txtViewPrefShipping;
/*
    @BindView(R.id.radioPrepay)
    RadioButton radioPrepay;

    @BindView(R.id.radioCourier)
    RadioButton radioCourier;

    @BindView(R.id.txtViewUnderPrepay)
    AppCompatTextView txtViewUnderPrepay;

    @BindView(R.id.txtViewUnderCourier)
    AppCompatTextView txtViewUnderCourier;*/

    @BindView(R.id.edtTextCourierInfo)
    AppCompatEditText edtTextCourierInfo;

    @BindView(R.id.edtTextComments)
    AppCompatEditText edtTextComments;

    @BindView(R.id.recyclerViewProducts)
    RecyclerView recyclerViewProducts;


    @BindView(R.id.btnContinue)
    Button btnContinue;

    DBHelper dbHelper;

    DeliveryMethodPresenterImpl deliveryMethodPresenter;
    DeliveryMethodAdapter deliveryMethodAdapter;
    LinearLayoutManager linearLayoutManager;
    ArrayList<DeliveryMethodModel.Data> arrayListDeliveryMethod;

    String selectedRadioText;
    int selectedRadioButtonId = 0;
    String courierInfo;
    String chkOutComment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_method);
        ButterKnife.bind(this);
        setFont();
        getSessionData();
        arrayListDeliveryMethod = new ArrayList<>();
        dbHelper = new DBHelper(this);
        setTitle();
        deliveryMethodPresenter = new DeliveryMethodPresenterImpl(this, this);
        deliveryMethodAdapter = new DeliveryMethodAdapter(this, arrayListDeliveryMethod);
        getShippingDataApi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        imgViewHome.setVisibility(View.VISIBLE);
        frameLayCart.setVisibility(View.GONE);
        updateCartQty();
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onInternetError() {

        SnackNotify.checkConnection(onretryGetShippingApi, coordinateLayout);
    }

    OnClickInterface onretryGetShippingApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getShippingDataApi();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getDeliveryMethodSuccess(ArrayList<DeliveryMethodModel.Data> arrayListDeliveryMethod) {

        this.arrayListDeliveryMethod = arrayListDeliveryMethod;


        setLayoutManager();
    }

    /*------------------------------private method--------------------------------------*/

    private void setLayoutManager() {
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewProducts.setLayoutManager(linearLayoutManager);
        recyclerViewProducts.setItemAnimator(new DefaultItemAnimator());
        recyclerViewProducts.setHasFixedSize(true);
        recyclerViewProducts.setNestedScrollingEnabled(true);
        setAdapter();
    }

    private void setAdapter() {
        deliveryMethodAdapter = new DeliveryMethodAdapter(this, arrayListDeliveryMethod);
        recyclerViewProducts.setAdapter(deliveryMethodAdapter);
    }


    private void setTitle() {
        txtViewTitle.setText(getString(R.string.title_checkout));
    }

    private void getShippingDataApi() {
        deliveryMethodPresenter.getDeliveryMethod();
    }

    private void getSessionData() {

        selectedRadioText = LoginManager.getInstance().getDeliveryMethod();
        courierInfo = LoginManager.getInstance().getCourierInfo();
        chkOutComment = LoginManager.getInstance().getChkOutComment();

        edtTextCourierInfo.setText(courierInfo);
        edtTextComments.setText(chkOutComment);
    }

    private void setFont() {

        FontHelper.setFontFace(txtViewHeading, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewStep, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewPrefShipping, FontHelper.FontType.FONT_SEMIBOLD, this);
        //  FontHelper.setFontFace(radioPrepay, FontHelper.FontType.FONT_REGULAR, this);
        //   FontHelper.setFontFace(txtViewUnderPrepay, FontHelper.FontType.FONT_THIN, this);
        //  FontHelper.setFontFace(radioCourier, FontHelper.FontType.FONT_REGULAR, this);
        //  FontHelper.setFontFace(txtViewUnderCourier, FontHelper.FontType.FONT_THIN, this);
        FontHelper.setFontFace(edtTextCourierInfo, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(edtTextComments, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(btnContinue, FontHelper.FontType.FONT_SEMIBOLD, this);
    }

    /*----------------------------public method--------------------------------------*/

    public void hideShowTextView(boolean isCourier) {
        if (isCourier) {
            edtTextCourierInfo.setVisibility(View.VISIBLE);
        } else {
            edtTextCourierInfo.setVisibility(View.GONE);
        }

    }

    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }

    public void setFirstRadioButtonChecked(){

        try {
            View view = recyclerViewProducts.getChildAt(0);

            RadioButton radioButton=(RadioButton)view.findViewById(R.id.radioButton);
            radioButton.setChecked(true);


        }catch (ArrayIndexOutOfBoundsException exp){

        }

    }

    /*----------------------------on click method--------------------------------*/

    @OnClick(R.id.imgBackButton)
    public void backPressed() {
        onBackPressed();
    }

    @OnClick(R.id.btnContinue)
    public void btnContinueClicked() {

        //save in session
        String deliveryMethod = LoginManager.getInstance().getDeliveryMethod();

        if (deliveryMethod.length() >0) {

            String edtTextComment = String.valueOf(edtTextComments.getText());
            String edtTextCourier = String.valueOf(edtTextCourierInfo.getText());

            //save data to session
            LoginManager.getInstance().saveChkOutComment(edtTextComment);
            LoginManager.getInstance().saveCourierInfo(edtTextCourier);

            Intent intent = new Intent(this, PaymentMethodActivity.class);
            startActivity(intent);
        }
        else
        {
            SnackNotify.showMessage(getString(R.string.error_shipping_method), coordinateLayout);
        }
    }

    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (dbHelper.getCartCount() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
                finish();
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
                finish();
            }
        }
    }

    @OnClick(R.id.imgViewHome)
    public void imgViewHomeClicked(){
        startActivity(new Intent(this, DashboardActivity.class));

        finishAffinity();

    }

}
