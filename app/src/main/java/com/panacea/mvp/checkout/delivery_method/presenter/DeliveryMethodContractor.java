package com.panacea.mvp.checkout.delivery_method.presenter;

import com.panacea.mvp.checkout.delivery_method.model.DeliveryMethodModel;
import com.panacea.mvp.commonMvp.BaseView;

import java.util.ArrayList;

/**
 * Created by Braintech on 03-01-2018.
 */

public interface DeliveryMethodContractor {

    public interface DeliveryMethodView extends BaseView {

        void getDeliveryMethodSuccess(ArrayList<DeliveryMethodModel.Data> arrayListDeliveryMethod);
    }

    public interface presenter {
        void getDeliveryMethod();
    }
}
