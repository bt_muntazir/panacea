package com.panacea.mvp.checkout.checkout_option.adapter;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.panacea.mvp.checkout.checkout_option.fragment.NewCustomerFragment;
import com.panacea.mvp.checkout.checkout_option.fragment.ReturningCustomerFragment;

/**
 * Created by Braintech on 03-01-2018.
 */

public class CheckoutOptionPagerAdapter  extends FragmentStatePagerAdapter {


    int tabCount;
    Activity activity;

    public CheckoutOptionPagerAdapter(FragmentManager fragmentManager, int tabCount, Activity activity) {
        super(fragmentManager);
        this.activity = activity;
        this.tabCount = tabCount;
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return tabCount;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return NewCustomerFragment.newInstance(position);
            case 1:
                return ReturningCustomerFragment.newInstance(position);
            default:
                return null;
        }
    }
}
