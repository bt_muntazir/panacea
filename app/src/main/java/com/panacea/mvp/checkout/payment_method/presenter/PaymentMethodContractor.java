package com.panacea.mvp.checkout.payment_method.presenter;

import com.panacea.mvp.checkout.delivery_method.model.DeliveryMethodModel;
import com.panacea.mvp.checkout.payment_method.model.CreateOrderIdModel;
import com.panacea.mvp.checkout.payment_method.model.PaymentMethodModel;
import com.panacea.mvp.commonMvp.BaseView;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Braintech on 03-01-2018.
 */

public interface PaymentMethodContractor {
    public interface PaymentMethodView extends BaseView {

        void getPaymentMethodSuccess(ArrayList<PaymentMethodModel.Data> arrayListPaymentMethod);

        void createOrderIdSuccess(CreateOrderIdModel createOrderIdModel);
        void  createOrderIdUnsuccess(String message);
        void createOrderIdInternetError();
        void createOrderIdServerNotResponding(String message);

    }

    public interface Presenter {
        void getPaymentMethod();

        void getCreateOrderIdMethod(JSONObject jsonObject);
    }


}
