package com.panacea.mvp.checkout.delivery_method.adapter;

import android.app.Activity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.panacea.R;
import com.panacea.common.helpers.LoginManager;
import com.panacea.mvp.checkout.delivery_method.DeliveryMethodActivity;
import com.panacea.mvp.checkout.delivery_method.model.DeliveryMethodModel;
import com.panacea.mvp.checkout.payment_method.adapter.PaymentMethodAdapter;
import com.panacea.mvp.checkout.payment_method.model.PaymentMethodModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 05-01-2018.
 */

public class DeliveryMethodAdapter extends RecyclerView.Adapter {

    Activity activity;
    ArrayList<DeliveryMethodModel.Data> raDataArrayListResult;
    String deliverMethodSession;
    boolean flag=false;

    public DeliveryMethodAdapter(Activity activity, ArrayList<DeliveryMethodModel.Data> raDataArrayList) {
        this.activity = activity;
        this.raDataArrayListResult = raDataArrayList;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_delivery_method, parent, false);
        return new DeliveryMethodAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {

        final DeliveryMethodModel.Data deliveryData = raDataArrayListResult.get(position);

        deliverMethodSession = LoginManager.getInstance().getDeliveryMethod();

        ((DeliveryMethodAdapter.ViewHolder) holder).radioButton.setTag(position);

        ((DeliveryMethodAdapter.ViewHolder) holder).radioButton.setText(deliveryData.getShipping());
        ((DeliveryMethodAdapter.ViewHolder) holder).txtViewUnderRadio.setText(deliveryData.getAlert());

       /* //check is selected
        if (deliveryData.getIsSelected())
            ((DeliveryMethodAdapter.ViewHolder) holder).radioButton.setChecked(true);
        else
            ((DeliveryMethodAdapter.ViewHolder) holder).radioButton.setChecked(false);*/


        //Check the session and set the radio button
        if (deliveryData.getShipping().equals(deliverMethodSession)) {

            ((DeliveryMethodActivity) activity).hideShowTextView(true);
            ((DeliveryMethodAdapter.ViewHolder) holder).radioButton.setChecked(true);
        } else {
            ((DeliveryMethodActivity) activity).hideShowTextView(false);
            ((DeliveryMethodAdapter.ViewHolder) holder).radioButton.setChecked(false);
        }



        ((DeliveryMethodAdapter.ViewHolder) holder).radioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (deliveryData.getShipping().equals("USE YOUR COURIER")) {

                    ((DeliveryMethodActivity) activity).hideShowTextView(true);

                    //save in session
                    LoginManager.getInstance().saveDeliveryMethod(String.valueOf(deliveryData.getShipping()));
                } else {
                    ((DeliveryMethodActivity) activity).hideShowTextView(false);

                    //save in session
                    LoginManager.getInstance().saveDeliveryMethod(String.valueOf(deliveryData.getShipping()));
                }

                int tag = (Integer) v.getTag();


                for (int i = 0; i < raDataArrayListResult.size(); i++) {

                    if (i == tag) {
                        raDataArrayListResult.get(i).setSelected(true);
                    } else {
                        raDataArrayListResult.get(i).setSelected(false);
                    }
                }
                notifyDataSetChanged();

            }
        });
    }

    @Override
    public int getItemCount() {
        return raDataArrayListResult.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.radioButton)
        RadioButton radioButton;

        @BindView(R.id.txtViewUnderRadio)
        AppCompatTextView txtViewUnderRadio;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
