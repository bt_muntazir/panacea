package com.panacea.mvp.checkout.delivery_detail;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.FontHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.requestresponse.Const;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.common.utility.SnackNotify;
import com.panacea.mvp.add_edit_address.AddEditAddressActivity;
import com.panacea.mvp.address.model.AddressModel;
import com.panacea.mvp.address.presenter.AddressContractor;
import com.panacea.mvp.address.presenter.AddressPresenterImpl;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.checkout.delivery_method.DeliveryMethodActivity;
import com.panacea.mvp.commonMvp.adapter.AdapterSpinner;
import com.panacea.mvp.dashboard.DashboardActivity;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnItemSelected;

public class DeliveryDetailActivity extends AppCompatActivity implements AddressContractor.AddressView {

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.imgViewHome)
    ImageView imgViewHome;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.txtViewHeading)
    AppCompatTextView txtViewHeading;

    @BindView(R.id.txtViewStep)
    AppCompatTextView txtViewStep;

    @BindView(R.id.radioExistingAddress)
    RadioButton radioExistingAddress;

    @BindView(R.id.radioNewAddress)
    RadioButton radioNewAddress;

    @BindView(R.id.spinnerAddress)
    AppCompatSpinner spinnerAddress;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;


    @BindView(R.id.btnContinue)
    Button btnContinue;

    int selectedRadioButtonId = 1;
    String customerId;
    String addressId;
    String selectedAddressName;

    ArrayList<AddressModel.Data.Address> arrayListAddressList;
    ArrayList<HashMap<String, String>> arrayListAddress;

    DBHelper dbHelper;

    AddressPresenterImpl addressPresenter;
    AdapterSpinner adapterSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_detail);

        ButterKnife.bind(this);
        setFont();
        dbHelper = new DBHelper(this);
        addressPresenter = new AddressPresenterImpl(this, this);
        getSessionData();
        setTitle();
        setInitialAdapter();
        getAddressDataApi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        imgViewHome.setVisibility(View.VISIBLE);
        frameLayCart.setVisibility(View.GONE);
        updateCartQty();
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onInternetError() {

    }

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getAddressSuccess(AddressModel.Data addressModel) {

        arrayListAddressList = addressModel.getAddresses();
        int position = 0;

        for (int i = 0; i < arrayListAddressList.size(); i++) {
            AddressModel.Data.Address datum = arrayListAddressList.get(i);
            String adrsId = datum.getAddressId();
            String address = datum.getAddress();

            HashMap hashMap = new HashMap();
            hashMap.put(Const.KEY_ID, adrsId);
            hashMap.put(Const.KEY_NAME, address);
            arrayListAddress.add(hashMap);

            try {
                if (addressId.equals(adrsId)) {
                    position = i + 1;
                }
            } catch (ArrayIndexOutOfBoundsException ee) {
                ee.printStackTrace();
            }
        }

        setAdapter();
        spinnerAddress.setSelection(position);
    }

    @Override
    public void getDeleteAddressSuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getDeleteAddressUnSucess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void deleteAddressInternetError() {

    }

    @Override
    public void deleteAddressServerError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    /*---------------------------private method---------------------------------------*/

    private void setFont() {

        FontHelper.setFontFace(txtViewHeading, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewStep, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(radioExistingAddress, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(radioNewAddress, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(btnContinue, FontHelper.FontType.FONT_SEMIBOLD, this);
    }


    private void getSessionData() {
        //get customerId from LoginManager
        customerId = LoginManager.getInstance().getUserData().getCustomerId();
        addressId = LoginManager.getInstance().getDeliveryAddressId();
    }


    private void setInitialAdapter() {
        arrayListAddress = new ArrayList<>();
        HashMap hashMapFirstIndexCountry = new HashMap();
        hashMapFirstIndexCountry.put(Const.KEY_ID, 0);
        hashMapFirstIndexCountry.put(Const.KEY_NAME, getString(R.string.select_address));
        arrayListAddress.add(hashMapFirstIndexCountry);

        setAdapter();
    }

    private void setTitle() {
        txtViewTitle.setText(getString(R.string.title_checkout));
    }

    private void getAddressDataApi() {
        addressPresenter.getAddress(customerId);
    }

    private void setAdapter() {
        adapterSpinner = new AdapterSpinner(this, R.layout.spinner_selected_view, arrayListAddress);
        spinnerAddress.setAdapter(adapterSpinner);
    }

    /*----------------------------public method------------------------------------*/

    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }

    /*---------------------------------on click method-------------------------------------*/

    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (dbHelper.getCartCount() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
                finish();
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
                finish();
            }
        }
    }


    @OnClick(R.id.imgBackButton)
    public void backPressed() {
        onBackPressed();
    }


    @OnCheckedChanged({R.id.radioExistingAddress, R.id.radioNewAddress})
    public void onCheckChanged(CompoundButton button, boolean checked) {
        if (checked) {
            switch (button.getId()) {
                case R.id.radioExistingAddress:
                    selectedRadioButtonId = 1;
                    break;
                case R.id.radioNewAddress:
                    selectedRadioButtonId = 2;
                    break;
            }
        }
    }


    @OnClick(R.id.btnContinue)
    public void btnContinueClicked() {

        String sessionAddressId=LoginManager.getInstance().getBillingAddressId();

        if (selectedRadioButtonId == 1) {
            if (sessionAddressId.length() > 0) {
                Intent intent = new Intent(this, DeliveryMethodActivity.class);
                startActivity(intent);
            } else {
                SnackNotify.showMessage(getString(R.string.error_select_address), coordinateLayout);
            }
        } else if (selectedRadioButtonId == 2) {

            Intent intent = new Intent(this, AddEditAddressActivity.class);
            intent.putExtra(ConstIntent.KEY_ADD_EDIT_ADDRESS, ConstIntent.KEY_ADD_CHECKOUT_DELIVERY);
            startActivity(intent);
        }
    }

    @OnItemSelected(R.id.spinnerAddress)
    void onItemAddressSelected(int position) {
        if (position != 0) {
            addressId = String.valueOf(arrayListAddress.get(position).get(Const.KEY_ID));
            selectedAddressName = String.valueOf(arrayListAddress.get(position).get(Const.KEY_NAME));
            spinnerAddress.setSelection(arrayListAddress.indexOf(addressId));
            // selectedAddressName = String.valueOf(arrayListAddress.indexOf(addressId));

            //save addressId in session
            LoginManager.getInstance().saveDeliveryAddressId(addressId);
        }
        else{
            //save addressId in session
            LoginManager.getInstance().saveDeliveryAddressId("");
        }
    }

    @OnClick(R.id.imgViewHome)
    public void imgViewHomeClicked() {
        startActivity(new Intent(this, DashboardActivity.class));

        finishAffinity();

    }
}
