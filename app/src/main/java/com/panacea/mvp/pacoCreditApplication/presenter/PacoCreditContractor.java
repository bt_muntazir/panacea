package com.panacea.mvp.pacoCreditApplication.presenter;

import com.panacea.mvp.commonMvp.BaseView;
import com.panacea.mvp.login.model.LoginResponseModel;

import org.json.JSONObject;

/**
 * Created by Braintech on 18-12-2017.
 */

public interface PacoCreditContractor {

    public interface PacoCreditView extends BaseView {

        void getPacoCreditSuccess(String message);
    }

    public interface Presenter {
        void getPacoCredit(JSONObject jsonObject);
    }


}

