package com.panacea.mvp.pacoCreditApplication.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;

import com.panacea.R;
import com.panacea.common.requestresponse.Const;
import com.panacea.common.utility.SnackNotify;
import com.panacea.common.utility.Utils;
import com.panacea.mvp.login.LoginActivity;
import com.panacea.mvp.pacoCreditApplication.PacoCreditActivity;
import com.panacea.mvp.register.RegisterActivity;
import com.panacea.mvp.register.fragment.AddressFragment;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

/**
 * Created by Braintech on 05-12-2017.
 */

public class PacoInformationFragment extends Fragment {

    @BindView(R.id.mainContainer)
    FrameLayout mainContainer;

    @BindView(R.id.edtTextName)
    AppCompatEditText edtTextName;

    @BindView(R.id.edtTextCompanyName)
    AppCompatEditText edtTextCompanyName;

    @BindView(R.id.edtTextCompanyAddress)
    AppCompatEditText edtTextCompanyAddress;

    @BindView(R.id.edtTextCity)
    AppCompatEditText edtTextCity;

    @BindView(R.id.edtTextState)
    AppCompatEditText edtTextState;

    @BindView(R.id.edtTextPostalCode)
    AppCompatEditText edtTextPostalCode;

    @BindView(R.id.edtTextCurrentAdd)
    AppCompatEditText edtTextCurrentAdd;

    @BindView(R.id.edtTextEmail)
    AppCompatEditText edtTextEmail;

    @BindView(R.id.edtTextPhone)
    AppCompatEditText edtTextPhone;

    @BindView(R.id.edtTextFax)
    AppCompatEditText edtTextFax;

    @BindView(R.id.edtTextWebsiteAdd)
    AppCompatEditText edtTextWebsiteAdd;

    @BindView(R.id.edtTextDataBuisness)
    AppCompatEditText edtTextDataBuisness;

    @BindView(R.id.edtTextBankName)
    AppCompatEditText edtTextBankName;

    @BindView(R.id.edtTextBankAddress)
    AppCompatEditText edtTextBankAddress;

    @BindView(R.id.edtTextBankState)
    AppCompatEditText edtTextBankState;

    @BindView(R.id.edtTextBankCity)
    AppCompatEditText edtTextBankCity;

    @BindView(R.id.edtTextBankPostalCode)
    AppCompatEditText edtTextBankPostalCode;

    @BindView(R.id.edtTextBankPhone)
    AppCompatEditText edtTextBankPhone;

    @BindView(R.id.edtTextBankFax)
    AppCompatEditText edtTextBankFax;

    @BindView(R.id.edtTextAccountNo)
    AppCompatEditText edtTextAccountNo;

    @BindView(R.id.checkboxCorporation)
    AppCompatCheckBox checkboxCorporation;

    @BindView(R.id.checkboxSoleProp)
    AppCompatCheckBox checkboxSoleProp;

    @BindView(R.id.checkboxPartenrship)
    AppCompatCheckBox checkboxPartenrship;

    @BindView(R.id.btnContinue)
    Button btnContinue;

    String solePropiership;
    String corporation;
    String partnership;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types and number of parameters
    public static PacoInformationFragment newInstance(int param1) {
        PacoInformationFragment fragment = new PacoInformationFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_paco_information, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    private void getData() {
        // hide keyboard
        Utils.hideKeyboardIfOpen(getActivity());

        String name = edtTextName.getText().toString();
        String companyName = edtTextCompanyName.getText().toString();
        String companyAddress = edtTextCompanyAddress.getText().toString();
        String state = edtTextState.getText().toString();
        String city = edtTextCity.getText().toString();
        String postalCode = edtTextPostalCode.getText().toString();
        String currentAddress = edtTextCurrentAdd.getText().toString();
        String email = edtTextEmail.getText().toString();
        String phone = edtTextPhone.getText().toString();
        String fax = edtTextFax.getText().toString();
        String webSiteAddress = edtTextWebsiteAdd.getText().toString();
        String dataBuisness = edtTextDataBuisness.getText().toString();
        String bankName = edtTextBankName.getText().toString();
        String bankAddress = edtTextBankAddress.getText().toString();
        String bankState = edtTextBankState.getText().toString();
        String bankCity = edtTextBankCity.getText().toString();
        String bankPostalCode = edtTextBankPostalCode.getText().toString();
        String bankPhone = edtTextBankPhone.getText().toString();
        String bankFax = edtTextBankFax.getText().toString();
        String accountno = edtTextAccountNo.getText().toString();

        //  if (validation(company, firstname, lastname, email, phone, fax, password, confirmpassword)) {

        ((PacoCreditActivity) getActivity()).changeViewToRegistration();

        JSONObject obj = new JSONObject();

        try {
            obj = new JSONObject();
            obj.put(Const.PARAM_NAME, name);
            obj.put(Const.PARAM_COMPANY, companyName);
            obj.put(Const.PARAM_COMP_ADDRESS, companyAddress);
            obj.put(Const.PARAM_COMP_STATE, state);
            obj.put(Const.PARAM_COMP_CITY, city);
            obj.put(Const.PARAM_PHONE, phone);
            obj.put(Const.PARAM_COMP_ZIP, postalCode);
            obj.put(Const.PARAM_LONG_CURRENT_ADDR, currentAddress);


            obj.put(Const.PARAM_EMAIL, email);
            obj.put(Const.PARAM_FAX, fax);
            obj.put(Const.PARAM_WEB_ADDR, webSiteAddress);
            obj.put(Const.PARAM_DATA_BUIS_COMM, dataBuisness);
            obj.put(Const.PARAM_BANK_NAME, bankName);

            obj.put(Const.PARAM_BANK_ADDR, bankAddress);
            obj.put(Const.PARAM_BANK_STATE, bankState);
            obj.put(Const.PARAM_BANK_CITY, bankCity);
            obj.put(Const.PARAM_BANK_ZIP, bankPostalCode);
            obj.put(Const.PARAM_BANK_PHONE, bankPhone);
            obj.put(Const.PARAM_BANK_FAX, bankFax);
            obj.put(Const.PARAM_BANK_ACC_NO, accountno);

            obj.put(Const.PARAM_SOLE_PROP, solePropiership);
            obj.put(Const.PARAM_PARTNERSHIP, partnership);
            obj.put(Const.PARAM_CORPORATION, corporation);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        if (getActivity() instanceof PacoCreditActivity) {
            ((PacoCreditActivity) getActivity()).setJsonObjectPersonalDetail(obj);
        }
        //}
    }

    private boolean validation(String company, String firstname, String lastname, String email, String phone, String fax, String password, String confirmpassword) {

        if ((Utils.isEmptyOrNull(company)) && (Utils.isEmptyOrNull(lastname)) && (Utils.isEmptyOrNull(firstname)) &&
                (Utils.isEmptyOrNull(email)) && (Utils.isEmptyOrNull(phone))
                && (Utils.isEmptyOrNull(fax)) && (Utils.isEmptyOrNull(password)) && (Utils.isEmptyOrNull(confirmpassword))) {

            SnackNotify.showMessage(getString(R.string.empty_all_fields), mainContainer);
            return false;
        }
        return true;
    }


    @OnCheckedChanged({R.id.checkboxPartenrship, R.id.checkboxSoleProp, R.id.checkboxCorporation})
    public void onRadioButtonCheckChanged(CompoundButton button, boolean checked) {

        switch (button.getId()) {
            case R.id.checkboxPartenrship:
                if (checked) {
                    partnership = getString(R.string.title_partnership);
                } else {
                    partnership = "";
                }
                break;
            case R.id.checkboxSoleProp:
                if (checked) {
                    solePropiership = getString(R.string.title_sole_prop);
                } else {
                    solePropiership = "";
                }
                break;
            case R.id.checkboxCorporation:
                if (checked) {
                    corporation=getString(R.string.title_corporation);
                } else {
                    corporation="";
                }
                break;
        }
    }


    @OnClick(R.id.btnContinue)
    public void btnContinueClick(View v) {

        getData();
    }

}
