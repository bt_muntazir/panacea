package com.panacea.mvp.pacoCreditApplication.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.commonMvp.model.CommonModel;
import com.panacea.mvp.login.model.LoginResponseModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 18-12-2017.
 */

public class PacoCreditImpl implements PacoCreditContractor.Presenter {

    PacoCreditContractor.PacoCreditView pacoCreditView;
    Activity activity;
    JSONObject jsonObject;

    public PacoCreditImpl( PacoCreditContractor.PacoCreditView pacoCreditView, Activity activity) {
        this.pacoCreditView = pacoCreditView;
        this.activity = activity;
    }

    @Override
    public void getPacoCredit(JSONObject jsonObject) {
        try {
            ApiAdapter.getInstance(activity);
            callPacoCredit(jsonObject);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            pacoCreditView.onInternetError();
        }
    }

    private void callPacoCredit(JSONObject jsonObject) {
        Progress.start(activity);

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CommonModel> getOutput = ApiAdapter.getApiService().pacocredit("application/json", "no-cache", body);

        getOutput.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    CommonModel commonModel = response.body();

                    if (commonModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        pacoCreditView.getPacoCreditSuccess(commonModel.getMessage());
                    } else {
                        pacoCreditView.apiUnsuccess(commonModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    pacoCreditView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                Progress.stop();
                pacoCreditView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}


