package com.panacea.mvp.pacoCreditApplication.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.panacea.R;
import com.panacea.common.interfaces.Updateable;
import com.panacea.common.requestresponse.Const;
import com.panacea.common.utility.SnackNotify;
import com.panacea.common.utility.Utils;
import com.panacea.mvp.pacoCreditApplication.PacoCreditActivity;
import com.panacea.mvp.pacoCreditApplication.presenter.PacoCreditContractor;
import com.panacea.mvp.pacoCreditApplication.presenter.PacoCreditImpl;
import com.panacea.mvp.register.RegisterActivity;
import com.panacea.mvp.register.fragment.AddressFragment;
import com.panacea.mvp.register.presenter.CountyrStatePresenterImpl;
import com.panacea.mvp.register.presenter.RegisterPresenterImpl;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Braintech on 05-12-2017.
 */

public class PacoAddressFragment extends Fragment implements PacoCreditContractor.PacoCreditView, Updateable {

    @BindView(R.id.mainContainer)
    FrameLayout mainContainer;

    @BindView(R.id.edtTextRef1CompanyName)
    AppCompatEditText edtTextRef1CompanyName;

    @BindView(R.id.edtTextRef1Address)
    AppCompatEditText edtTextRef1Address;

    @BindView(R.id.edtTextRef1State)
    AppCompatEditText edtTextRef1State;

    @BindView(R.id.edtTextRef1City)
    AppCompatEditText edtTextRef1City;

    @BindView(R.id.edtTextRef1PostalCode)
    AppCompatEditText edtTextRef1PostalCode;

    @BindView(R.id.edtTextRef1Phone)
    AppCompatEditText edtTextRef1Phone;

    @BindView(R.id.edtTextRef1Fax)
    AppCompatEditText edtTextRef1Fax;

    @BindView(R.id.edtTextRef1Email)
    AppCompatEditText edtTextRef1Email;

    @BindView(R.id.edtTextRef1CurtOutBal)
    AppCompatEditText edtTextRef1CurtOutBal;

    @BindView(R.id.edtTextRef2CompanyName)
    AppCompatEditText edtTextRef2CompanyName;

    @BindView(R.id.edtTextRef2Address)
    AppCompatEditText edtTextRef2Address;

    @BindView(R.id.edtTextRef2State)
    AppCompatEditText edtTextRef2State;

    @BindView(R.id.edtTextRef2City)
    AppCompatEditText edtTextRef2City;

    @BindView(R.id.edtTextRef2PostalCode)
    AppCompatEditText edtTextRef2PostalCode;

    @BindView(R.id.edtTextRef2Phone)
    AppCompatEditText edtTextRef2Phone;

    @BindView(R.id.edtTextRef2Fax)
    AppCompatEditText edtTextRef2Fax;

    @BindView(R.id.edtTextRef2Email)
    AppCompatEditText edtTextRef2Email;

    @BindView(R.id.edtTextRef2CurtOutBal)
    AppCompatEditText edtTextRef2CurtOutBal;

    @BindView(R.id.edtTextRef3CompanyName)
    AppCompatEditText edtTextRef3CompanyName;

    @BindView(R.id.edtTextRef3Address)
    AppCompatEditText edtTextRef3Address;

    @BindView(R.id.edtTextRef3State)
    AppCompatEditText edtTextRef3State;

    @BindView(R.id.edtTextRef3City)
    AppCompatEditText edtTextRef3City;

    @BindView(R.id.edtTextRef3PostalCode)
    AppCompatEditText edtTextRef3PostalCode;

    @BindView(R.id.edtTextRef3Phone)
    AppCompatEditText edtTextRef3Phone;

    @BindView(R.id.edtTextRef3Fax)
    AppCompatEditText edtTextRef3Fax;

    @BindView(R.id.edtTextRef3Email)
    AppCompatEditText edtTextRef3Email;

    @BindView(R.id.edtTextRef3CurtOutBal)
    AppCompatEditText edtTextRef3CurtOutBal;

    @BindView(R.id.btnContinue)
    Button btnContinue;

    PacoCreditImpl pacoCredit;

    JSONObject jsonObject;

    String name;
    String companyName;
    String companyAddress;
    String state;
    String city;
    String phone;
    String postalCode;
    String currentAddress;
    String email;
    String fax;
    String webSiteAddress;
    String dataBuisness;
    String bankName;
    String bankAddress;
    String bankState;
    String bankCity;
    String bankPostalCode;
    String bankPhone;
    String bankFax;
    String accountno;
    String corporation;
    String solePropiership;
    String partnership;

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types and number of parameters
    public static PacoAddressFragment newInstance(int param1) {
        PacoAddressFragment fragment = new PacoAddressFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_paco_address, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        pacoCredit = new PacoCreditImpl(this, getActivity());
    }

    @OnClick(R.id.btnContinue)
    public void btnContinueOnClick(View v) {

        getData();
    }

    @Override
    public void update(JSONObject jsonObject) {
        getLastFragmentData(jsonObject);
    }

    public void getLastFragmentData(JSONObject obj) {
        if (obj != null) {

            try {

                name = obj.getString(Const.PARAM_NAME);
                companyName = obj.getString(Const.PARAM_COMPANY);
                companyAddress = obj.getString(Const.PARAM_COMP_ADDRESS);
                state = obj.getString(Const.PARAM_COMP_STATE);
                city = obj.getString(Const.PARAM_COMP_CITY);
                phone = obj.getString(Const.PARAM_PHONE);
                postalCode = obj.getString(Const.PARAM_COMP_ZIP);
                currentAddress = obj.getString(Const.PARAM_LONG_CURRENT_ADDR);
                email = obj.getString(Const.PARAM_EMAIL);
                fax = obj.getString(Const.PARAM_FAX);
                webSiteAddress = obj.getString(Const.PARAM_WEB_ADDR);
                dataBuisness = obj.getString(Const.PARAM_DATA_BUIS_COMM);
                bankName = obj.getString(Const.PARAM_BANK_NAME);
                bankAddress = obj.getString(Const.PARAM_BANK_ADDR);
                bankState = obj.getString(Const.PARAM_BANK_STATE);
                bankCity = obj.getString(Const.PARAM_BANK_CITY);
                bankPostalCode = obj.getString(Const.PARAM_BANK_ZIP);
                bankPhone = obj.getString(Const.PARAM_BANK_PHONE);
                bankFax = obj.getString(Const.PARAM_BANK_FAX);
                accountno = obj.getString(Const.PARAM_BANK_ACC_NO);

                solePropiership=obj.getString(Const.PARAM_SOLE_PROP);
                partnership=obj.getString(Const.PARAM_PARTNERSHIP);
                corporation=  obj.getString(Const.PARAM_CORPORATION);

            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
    }

    private void getData() {
        // hide keyboard
        Utils.hideKeyboardIfOpen(getActivity());

        String companyNameRef1 = edtTextRef1CompanyName.getText().toString();
        String addressRef1 = edtTextRef1Address.getText().toString();
        String stateRef1 = edtTextRef1State.getText().toString();
        String cityRef1 = edtTextRef1City.getText().toString();
        String postalCodeRef1 = edtTextRef1PostalCode.getText().toString();
        String phoneRef1 = edtTextRef1Phone.getText().toString();
        String faxRef1 = edtTextRef1Fax.getText().toString();
        String emailRef1 = edtTextRef1Email.getText().toString();
        String currentOutbalRef1 = edtTextRef1CurtOutBal.getText().toString();

        String companyNameRef2 = edtTextRef2CompanyName.getText().toString();
        String addressRef2 = edtTextRef2Address.getText().toString();
        String stateRef2 = edtTextRef2State.getText().toString();
        String cityRef2 = edtTextRef2City.getText().toString();
        String postalCodeRef2 = edtTextRef2PostalCode.getText().toString();
        String phoneRef2 = edtTextRef2Phone.getText().toString();
        String faxRef2 = edtTextRef2Fax.getText().toString();
        String emailRef2 = edtTextRef2Email.getText().toString();
        String currentOutbalRef2 = edtTextRef2CurtOutBal.getText().toString();

        String companyNameRef3 = edtTextRef3CompanyName.getText().toString();
        String addressRef3 = edtTextRef3Address.getText().toString();
        String stateRef3 = edtTextRef3State.getText().toString();
        String cityRef3 = edtTextRef3City.getText().toString();
        String postalCodeRef3 = edtTextRef3PostalCode.getText().toString();
        String phoneRef3 = edtTextRef3Phone.getText().toString();
        String faxRef3 = edtTextRef3Fax.getText().toString();
        String emailRef3 = edtTextRef3Email.getText().toString();
        String currentOutbalRef3 = edtTextRef3CurtOutBal.getText().toString();

        if (getActivity() instanceof PacoCreditActivity)
            getLastFragmentData(((PacoCreditActivity) getActivity()).getJsonObjectPersonalDetail());

       /* if (validation(address1, address2, city, postcode, selectedCountryName, selectedStateName)) {*/

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_NAME, name);
            jsonObject.put(Const.PARAM_COMPANY, companyName);
            jsonObject.put(Const.PARAM_COMP_ADDRESS, companyAddress);
            jsonObject.put(Const.PARAM_COMP_STATE, state);
            jsonObject.put(Const.PARAM_COMP_CITY, city);
            jsonObject.put(Const.PARAM_PHONE, phone);
            jsonObject.put(Const.PARAM_COMP_ZIP, postalCode);
            jsonObject.put(Const.PARAM_LONG_CURRENT_ADDR, currentAddress);
            jsonObject.put(Const.PARAM_EMAIL, email);
            jsonObject.put(Const.PARAM_FAX, fax);
            jsonObject.put(Const.PARAM_WEB_ADDR, webSiteAddress);
            jsonObject.put(Const.PARAM_DATA_BUIS_COMM, dataBuisness);
            jsonObject.put(Const.PARAM_BANK_NAME, bankName);
            jsonObject.put(Const.PARAM_BANK_ADDR, bankAddress);
            jsonObject.put(Const.PARAM_BANK_STATE, bankState);
            jsonObject.put(Const.PARAM_BANK_CITY, bankCity);
            jsonObject.put(Const.PARAM_BANK_ZIP, bankPostalCode);
            jsonObject.put(Const.PARAM_BANK_PHONE, bankPhone);
            jsonObject.put(Const.PARAM_BANK_FAX, bankFax);
            jsonObject.put(Const.PARAM_BANK_ACC_NO, accountno);
            jsonObject.put(Const.PARAM_SOLE_PROP, solePropiership);
            jsonObject.put(Const.PARAM_PARTNERSHIP, partnership);
            jsonObject.put(Const.PARAM_CORPORATION, corporation);

            jsonObject.put(Const.PARAM_NAME1, companyNameRef1);
            jsonObject.put(Const.PARAM_COMPANY_ADD1, addressRef1);
            jsonObject.put(Const.PARAM_STATE1, stateRef1);
            jsonObject.put(Const.PARAM_CITY1, cityRef1);
            jsonObject.put(Const.PARAM_ZIP1, postalCodeRef1);
            jsonObject.put(Const.PARAM_PHONE1, phoneRef1);
            jsonObject.put(Const.PARAM_FAX1, faxRef1);
            jsonObject.put(Const.PARAM_EMAIL1, emailRef1);
            jsonObject.put(Const.PARAM_CURRENT_BALANCE1, currentOutbalRef1);

            jsonObject.put(Const.PARAM_NAME2, companyNameRef2);
            jsonObject.put(Const.PARAM_COMPANY_ADD2, addressRef2);
            jsonObject.put(Const.PARAM_STATE2, stateRef2);
            jsonObject.put(Const.PARAM_CITY2, cityRef2);
            jsonObject.put(Const.PARAM_ZIP2, postalCodeRef2);
            jsonObject.put(Const.PARAM_PHONE2, phoneRef2);
            jsonObject.put(Const.PARAM_FAX2, faxRef2);
            jsonObject.put(Const.PARAM_EMAIL2, emailRef2);
            jsonObject.put(Const.PARAM_CURRENT_BALANCE2, currentOutbalRef2);

            jsonObject.put(Const.PARAM_NAME3, companyNameRef3);
            jsonObject.put(Const.PARAM_COMPANY_ADD3, addressRef3);
            jsonObject.put(Const.PARAM_STATE3, stateRef3);
            jsonObject.put(Const.PARAM_CITY3, cityRef3);
            jsonObject.put(Const.PARAM_ZIP3, postalCodeRef3);
            jsonObject.put(Const.PARAM_PHONE3, phoneRef3);
            jsonObject.put(Const.PARAM_FAX3, faxRef3);
            jsonObject.put(Const.PARAM_EMAIL3, emailRef3);
            jsonObject.put(Const.PARAM_CURRENT_BALANCE3, currentOutbalRef3);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        // call login api
        pacoCredit.getPacoCredit(jsonObject);
        //}
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, mainContainer);
    }

    @Override
    public void onInternetError() {

    }

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, mainContainer);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, mainContainer);
    }

    @Override
    public void getPacoCreditSuccess(String message) {
        SnackNotify.showMessage(message, mainContainer);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ((PacoCreditActivity) getActivity()).finishCurrentActivity();
            }
        }, 2000);

    }
}
