package com.panacea.mvp.pacoCreditApplication.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.panacea.mvp.pacoCreditApplication.fragment.PacoAddressFragment;
import com.panacea.mvp.pacoCreditApplication.fragment.PacoInformationFragment;
import com.panacea.mvp.register.fragment.AddressFragment;
import com.panacea.mvp.register.fragment.PersonalDetailFragment;

/**
 * Created by Braintech on 05-12-2017.
 */

public class PacoCreditPagerAdapter extends FragmentStatePagerAdapter {

    int tabCount;

    public PacoCreditPagerAdapter(FragmentManager fragmentManager, int tabCount) {
        super(fragmentManager);

        this.tabCount = tabCount;
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return tabCount;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return PacoInformationFragment.newInstance(position);
            case 1:
                return PacoAddressFragment.newInstance(position);
            default:
                return null;
        }


    }

}

