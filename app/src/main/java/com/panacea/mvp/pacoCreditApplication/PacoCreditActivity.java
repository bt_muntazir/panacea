package com.panacea.mvp.pacoCreditApplication;

import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.login.LoginActivity;
import com.panacea.mvp.pacoCreditApplication.adapter.PacoCreditPagerAdapter;
import com.panacea.mvp.register.RegisterActivity;
import com.panacea.mvp.register.adapter.RegisterPagerAdapter;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PacoCreditActivity extends AppCompatActivity {

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    DBHelper dbHelper;
    JSONObject jsonObjectPersonalDetail;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paco_credit);

        ButterKnife.bind(this);

        dbHelper=new DBHelper(this);
        setTitle();

        setTab();
    }

    @Override
    protected void onResume() {
        super.onResume();

        frameLayCart.setVisibility(View.GONE);
        updateCartQty();
    }

    private void setTab() {

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.title_personal_detail)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.title_address)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        final PacoCreditPagerAdapter pacoCreditPagerAdapter = new PacoCreditPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(pacoCreditPagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {

            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                // Log.e("pos", "" + tab.getPosition());
                viewPager.setCurrentItem(tab.getPosition());

                if(tab.getPosition()==0){
                    tabLayout.setBackgroundResource(R.mipmap.ic_tab1_selected);
                }else if(tab.getPosition()==1){
                    tabLayout.setBackgroundResource(R.mipmap.ic_tab2_selected);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public JSONObject getJsonObjectPersonalDetail() {
        return jsonObjectPersonalDetail;
    }

    public void setJsonObjectPersonalDetail(JSONObject jsonObjectPersonalDetail) {
        this.jsonObjectPersonalDetail = jsonObjectPersonalDetail;
    }

    public void changeViewToRegistration() {

        viewPager.setCurrentItem(1);
    }


    private void setTitle(){
        txtViewTitle.setText(getString(R.string.title_paco_credit_app));
    }

    @OnClick(R.id.imgBackButton)
    public void backPressed() {
        onBackPressed();
    }

    public void yourPublicMethod(){

    }

    public void finishCurrentActivity() {
        Intent intent = new Intent(PacoCreditActivity.this, DashboardActivity.class);
        startActivity(intent);
        //SnackNotify.showMessage("Register Successfully", coordinateLayout);

        finish();
    }

    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }

    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (dbHelper.getCartCount() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            }
        }
    }
}
