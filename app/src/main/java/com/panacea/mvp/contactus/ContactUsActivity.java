package com.panacea.mvp.contactus;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.common.utility.SnackNotify;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.contactus_sendmessage.ContactUsSendMessageActivity;
import com.panacea.mvp.direction.GetDirectionActivity;
import com.panacea.mvp.splash.model.SplashModel;
import com.panacea.mvp.splash.presenter.SplashContractor;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContactUsActivity extends AppCompatActivity {

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    /*Dirtection*/
    @BindView(R.id.txtViewDirection1)
    TextView txtViewDirection1;

    @BindView(R.id.txtViewDirection2)
    TextView txtViewDirection2;

    /*Address*/
    @BindView(R.id.txtViewAddress1)
    TextView txtViewAddress1;

    @BindView(R.id.txtViewAddress2)
    TextView txtViewAddress2;

    /*Address*/
    @BindView(R.id.txtViewPhone)
    TextView txtViewPhone;

   /* @BindView(R.id.txtViewFax)
    TextView txtViewFax;*/

    @BindView(R.id.txtViewEmail)
    TextView txtViewEmail;

    @BindView(R.id.relLayPhone)
    RelativeLayout relLayPhone;

   /* @BindView(R.id.relLayFax)
    RelativeLayout relLayFax;*/

    @BindView(R.id.relLayEmail)
    RelativeLayout relLayEmail;

    /*toolbar*/
    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.btnSendUsMessage)
    Button btnSendUsMessage;


    SplashModel.Data constantData;

    // session value
    String phone;
    String fax;
    String email;
    String address1;
    String address2;
    String addr1Lat;
    String addr1Lon;
    String addr2Lat;
    String addr2Lon;

    DBHelper dbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);

        ButterKnife.bind(this);

        dbHelper = new DBHelper(this);

        setToolbar();
        // set value
        setData();
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateCartQty();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 2909: {
                int permissionCheckCall = ContextCompat.checkSelfPermission(this,
                        Manifest.permission.CALL_PHONE);

                if ((permissionCheckCall == 0)) {
                    call();
                } else {

                    SnackNotify.showMessage("Please provide security permission from app setting.", coordinateLayout);
                }
                return;
            }
        }
    }

    /*---------------------------------private method--------------------------------------*/
    private void setData() {

        // get constant data from session
        constantData = LoginManager.getInstance().getConstantData();

        if (constantData != null) {

            // retrive session data
            phone = constantData.getContactPhone();
            fax = constantData.getContactFax();
            email = constantData.getContactEmail();
            address1 = constantData.getAddress1();
            address2 = constantData.getAddress2();
            addr1Lat = constantData.getLat1();
            addr1Lon = constantData.getLon2();
            addr2Lat = constantData.getLat2();
            addr2Lon = constantData.getLon2();

            if (phone != null)
                txtViewPhone.setText(phone);
            else
                txtViewPhone.setText("--NA--");

          /*  if (fax != null)
                txtViewFax.setText(fax);
            else
                txtViewFax.setText("--NA--");*/

            if (email != null)
                txtViewEmail.setText(email);
            else
                txtViewEmail.setText("--NA--");

            if (address1 != null)
                txtViewAddress1.setText(address1);
            else
                txtViewAddress1.setText("--NA--");

            if (address2 != null)
                txtViewAddress2.setText(address2);
            else
                txtViewAddress2.setText("--NA--");

        } else {

            txtViewPhone.setText("--NA--");
            //txtViewFax.setText("--NA--");
            txtViewEmail.setText("--NA--");
            txtViewAddress1.setText("--NA--");
            txtViewAddress2.setText("--NA--");
        }

        // set underline textview
        txtViewDirection1.setPaintFlags(txtViewDirection1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtViewDirection2.setPaintFlags(txtViewDirection2.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

    }

    private void setToolbar() {

        txtViewTitle.setText(getString(R.string.title_contact_us));
    }


    private void call() {

        Intent callIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + phone));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        startActivity(callIntent);
    }

    /*----------------------------------------public method------------------------------------*/

    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }


    /*----------------------------on click method------------------------------------*/

    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (dbHelper.getCartCount() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            }
        }
    }

    @OnClick(R.id.imgBackButton)
    public void onBackClick() {

        onBackPressed();
    }


    @OnClick(R.id.txtViewDirection1)
    public void onAddr1DirectionClick() {

        Intent intent = new Intent(this, GetDirectionActivity.class);
        intent.putExtra(ConstIntent.KEY_LAT, addr1Lat);
        intent.putExtra(ConstIntent.KEY_LON, addr1Lon);
        startActivity(intent);
    }

    @OnClick(R.id.txtViewDirection2)
    public void onAddr2DirectionClick() {

        Intent intent = new Intent(this, GetDirectionActivity.class);
        intent.putExtra(ConstIntent.KEY_LAT, addr2Lat);
        intent.putExtra(ConstIntent.KEY_LON, addr2Lon);
        startActivity(intent);
    }

    @OnClick(R.id.btnSendUsMessage)
    public void btnSendUsMessageClick() {

        Intent intent = new Intent(this, ContactUsSendMessageActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.relLayEmail)
    public void relLayEmailClick() {

        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        String[] recipients = {email};
        emailIntent.putExtra(Intent.EXTRA_EMAIL, recipients);
        emailIntent.setType("message/rfc822");
        startActivity(emailIntent);
    }

    @OnClick(R.id.relLayPhone)
    public void relLayPhoneClick() {

        int permissionCheckCamera = ContextCompat.checkSelfPermission(this,
                Manifest.permission.CALL_PHONE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if ((permissionCheckCamera == -1)) {

                if (!Settings.System.canWrite(this)) {
                    requestPermissions(new String[]{Manifest.permission.CALL_PHONE
                    }, 2909);
                } else {
                    call();
                }
            } else {
                call();
            }
        } else {
            call();
        }
    }
}
