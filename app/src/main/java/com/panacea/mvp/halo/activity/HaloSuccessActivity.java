package com.panacea.mvp.halo.activity;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.FontHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.utility.SnackNotify;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.halo.model.HaloSuccessModel;
import com.panacea.mvp.halo.presenter.HaloContractor;
import com.panacea.mvp.halo.presenter.HaloSuccessPresenterImpl;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HaloSuccessActivity extends AppCompatActivity implements HaloContractor.HaloSuccessView {

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.imgViewHalo)
    ImageView imgViewHalo;

    @BindView(R.id.txtViewDescription)
    TextView txtViewDescription;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

    HaloSuccessPresenterImpl haloSuccessPresenter;

    DBHelper dbHelper;

    HaloSuccessModel.Data haloSuccessModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halo_success);

        ButterKnife.bind(this);
        dbHelper=new DBHelper(this);
        haloSuccessPresenter = new HaloSuccessPresenterImpl(this, this);
        getHaloSuccessDataApi();
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateCartQty();
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onInternetError() {

        SnackNotify.checkConnection(onRetryGetHaloApi, coordinateLayout);
    }

    OnClickInterface onRetryGetHaloApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getHaloSuccessDataApi();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getHaloSuccess(HaloSuccessModel.Data haloSuccessModel) {
        this.haloSuccessModel = haloSuccessModel;

        setTitle();

        Picasso.with(this).load(haloSuccessModel.getImg()).into(imgViewHalo);

        txtViewDescription.setText(String.valueOf(haloSuccessModel.getDescription()));
    }

    private void setTitle() {
        txtViewTitle.setText(String.valueOf(haloSuccessModel.getHeading()));
    }


    private void getHaloSuccessDataApi() {

        haloSuccessPresenter.getHaloSuccessProduct();
    }

    private void setFont() {

        FontHelper.setFontFace(txtViewDescription, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(btnSubmit, FontHelper.FontType.FONT_SEMIBOLD, this);
    }

    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }


    @OnClick(R.id.imgBackButton)
    public void backPressed() {
        onBackPressed();
    }

    @OnClick(R.id.btnSubmit)
    public void btnSubmitClick() {

        Intent intent = new Intent(HaloSuccessActivity.this, DashboardActivity.class);
        startActivity(intent);

        finish();
    }

    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (dbHelper.getCartCount() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            }
        }
    }
}
