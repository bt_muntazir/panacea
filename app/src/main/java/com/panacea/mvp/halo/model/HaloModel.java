package com.panacea.mvp.halo.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Braintech on 28-12-2017.
 */


public class HaloModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("heading")
        @Expose
        private String heading;
        @SerializedName("img")
        @Expose
        private String img;
        @SerializedName("description")
        @Expose
        private Object description;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("webview_img")
        @Expose
        private String webview_img;

        public String getHeading() {
            return heading;
        }

        public void setHeading(String heading) {
            this.heading = heading;
        }

        public String getImg() {
            return img;
        }

        public void setImg(String img) {
            this.img = img;
        }

        public String getWebViewImg() {
            return webview_img;
        }

        public void setWebViewImg(String webview_img) {
            this.webview_img = webview_img;
        }

        public Object getDescription() {
            return description;
        }

        public void setDescription(Object description) {
            this.description = description;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

    }
}