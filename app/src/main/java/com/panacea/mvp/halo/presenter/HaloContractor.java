package com.panacea.mvp.halo.presenter;

import com.panacea.mvp.commonMvp.BaseView;
import com.panacea.mvp.halo.model.HaloModel;
import com.panacea.mvp.halo.model.HaloSuccessModel;
import com.panacea.mvp.login.model.LoginResponseModel;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Braintech on 28-12-2017.
 */

public class HaloContractor {

    public interface HaloView {

        void getHaloProductSuccess(HaloModel.Data arrayListHalo);

        void getHaloProductUnSucess(String message);

        void haloProductInternetError();

        void haloProductServerError(String message);

        void saveHaloProductSuccess(String message);

        void saveHaloProductUnSucess(String message);

        void saveHaloProductInternetError();

        void saveHaloProductServerError(String message);
    }

    public interface HaloSuccessView extends BaseView {

        void getHaloSuccess(HaloSuccessModel.Data haloSuccessModel);
    }
    public interface Presenter {

        void getHaloProduct();

        void getSaveHaloProduct(JSONObject jsonObject);
    }

    public interface HaloSuccessPresenter {

        void getHaloSuccessProduct();
    }

}


