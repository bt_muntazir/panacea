package com.panacea.mvp.halo.activity;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.GlideDrawableImageViewTarget;
import com.panacea.R;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.FontHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.requestresponse.Const;
import com.panacea.common.utility.SnackNotify;
import com.panacea.common.utility.Utils;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.changepassword.ChangePasswordActivity;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.halo.model.HaloModel;
import com.panacea.mvp.halo.presenter.HaloContractor;
import com.panacea.mvp.halo.presenter.HaloPresenterImpl;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HaloActivity extends AppCompatActivity implements HaloContractor.HaloView {

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.imgViewHalo)
    ImageView imgViewHalo;

    @BindView(R.id.txtViewDescription)
    TextView txtViewDescription;

    @BindView(R.id.txtViewInnerTitle)
    TextView txtViewInnerTitle;


    @BindView(R.id.edtTextCompanyName)
    AppCompatEditText edtTextCompanyName;

    @BindView(R.id.edtTextEmail)
    AppCompatEditText edtTextEmail;

    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    @BindView(R.id.webViewImage)
    WebView webViewImage;



    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;


    HaloModel.Data arrayListHalo;

    HaloPresenterImpl haloPresenter;

    DBHelper dbHelper;
    JSONObject jsonObject;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_halo);
        ButterKnife.bind(this);
        dbHelper=new DBHelper(this);
        setFont();
        haloPresenter = new HaloPresenterImpl(this, this);
        getHaloProductApi();
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateCartQty();
    }

    @Override
    public void getHaloProductSuccess(HaloModel.Data arrayListHalo) {
        this.arrayListHalo = arrayListHalo;

        setTitle();
        setField();
    }

    @Override
    public void getHaloProductUnSucess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void haloProductInternetError() {
        SnackNotify.checkConnection(onRetryGetHaloApi, coordinateLayout);
    }

    OnClickInterface onRetryGetHaloApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getHaloProductApi();
        }
    };

    @Override
    public void haloProductServerError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void saveHaloProductSuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);

        Intent intent = new Intent(HaloActivity.this, HaloSuccessActivity.class);
        startActivity(intent);
    }

    @Override
    public void saveHaloProductUnSucess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void saveHaloProductInternetError() {
        SnackNotify.checkConnection(onretrySaveHeloProApi, coordinateLayout);
    }

    OnClickInterface onretrySaveHeloProApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };

    @Override
    public void saveHaloProductServerError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    private void getHaloProductApi() {

        haloPresenter.getHaloProduct();
    }

    private void setFont() {

        FontHelper.setFontFace(txtViewDescription, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(btnSubmit, FontHelper.FontType.FONT_SEMIBOLD, this);
        FontHelper.setFontFace(txtViewInnerTitle, FontHelper.FontType.FONT_SEMIBOLD, this);
    }


    private void setField() {

        WebSettings settings = webViewImage.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        webViewImage.loadData(arrayListHalo.getWebViewImg(), "text/html; charset=utf-8", null);


       /* Glide
                .with(this) // replace with 'this' if it's in activity
                .load(arrayListHalo.getImg())
                .asGif()
                .error(R.mipmap.ic_appicon) // show error drawable if the image is not a gif
                .into(imgViewHalo);*/
       /* GlideDrawableImageViewTarget imageViewTarget = new GlideDrawableImageViewTarget(imgViewHalo);
        Glide.with(this).load(arrayListHalo.getImg()).into(imageViewTarget);*/
        //Picasso.with(this).load(arrayListHalo.getImg()).into(imgViewHalo);
        txtViewDescription.setText(String.valueOf(arrayListHalo.getDescription()));
        txtViewInnerTitle.setText(String.valueOf(arrayListHalo.getTitle()));
    }

    private void setTitle() {
        txtViewTitle.setText(String.valueOf(arrayListHalo.getHeading()));
    }

    private void getData() {

        String companyName = String.valueOf(edtTextCompanyName.getText());
        String email = String.valueOf(edtTextEmail.getText());

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_COMPANY, companyName);
            jsonObject.put(Const.PARAM_EMAIL, email);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        haloPresenter.getSaveHaloProduct(jsonObject);
    }

    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }

    @OnClick(R.id.imgBackButton)
    public void backPressed() {
        onBackPressed();
    }

    @OnClick(R.id.btnSubmit)
    public void btnSubmitClick() {

        // hide keyboard
        Utils.hideKeyboardIfOpen(this);

        getData();
    }

    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (dbHelper.getCartCount() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            }
        }
    }
}
