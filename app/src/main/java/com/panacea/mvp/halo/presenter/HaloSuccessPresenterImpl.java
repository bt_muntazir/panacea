package com.panacea.mvp.halo.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.halo.model.HaloSuccessModel;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 28-12-2017.
 */

public class HaloSuccessPresenterImpl implements HaloContractor.HaloSuccessPresenter {

    HaloContractor.HaloSuccessView haloSuccessView;
    Activity activity;

    public HaloSuccessPresenterImpl(HaloContractor.HaloSuccessView haloSuccessView, Activity activity) {
        this.haloSuccessView = haloSuccessView;
        this.activity = activity;
    }

    @Override
    public void getHaloSuccessProduct() {
        try {
            ApiAdapter.getInstance(activity);
            getHaloSuccessDetail();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            haloSuccessView.onInternetError();
        }
    }

    private void getHaloSuccessDetail(){

        Progress.start(activity);

        Call<HaloSuccessModel> getHaloOutput = ApiAdapter.getApiService().getHaloSuccess();

        getHaloOutput.enqueue(new Callback<HaloSuccessModel>() {
            @Override
            public void onResponse(Call<HaloSuccessModel> call, Response<HaloSuccessModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    HaloSuccessModel haloSuccessModel = response.body();

                    if (haloSuccessModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        haloSuccessView.getHaloSuccess(haloSuccessModel.getData());
                    } else {
                        haloSuccessView.apiUnsuccess(activity.getString(R.string.error_username_pass));
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    haloSuccessView.serverNotResponding(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<HaloSuccessModel> call, Throwable t) {
                Progress.stop();
                haloSuccessView.serverNotResponding(activity.getString(R.string.error_server));
            }
        });
    }
}
