package com.panacea.mvp.halo.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.commonMvp.model.CommonModel;
import com.panacea.mvp.dashboard.model.CameraModel;
import com.panacea.mvp.halo.model.HaloModel;
import com.panacea.mvp.order_information.model.OrderInformationModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 28-12-2017.
 */

public class HaloPresenterImpl implements HaloContractor.Presenter {
    HaloContractor.HaloView haloView;
    Activity activity;
    JSONObject jsonObject;

    public HaloPresenterImpl(HaloContractor.HaloView haloView, Activity activity) {
        this.haloView = haloView;
        this.activity = activity;
    }

    @Override
    public void getHaloProduct() {
        try {
            ApiAdapter.getInstance(activity);
            getHaloDetail();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            haloView.haloProductInternetError();
        }
    }

    @Override
    public void getSaveHaloProduct(JSONObject jsonObject) {
        try {
            ApiAdapter.getInstance(activity);
            saveHaloProduct(jsonObject);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            haloView.saveHaloProductInternetError();
        }
    }

    private void getHaloDetail() {
        Progress.start(activity);

        Call<HaloModel> getHaloOutput = ApiAdapter.getApiService().getHalo();

        getHaloOutput.enqueue(new Callback<HaloModel>() {
            @Override
            public void onResponse(Call<HaloModel> call, Response<HaloModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    HaloModel haloModel = response.body();

                    if (haloModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        haloView.getHaloProductSuccess(haloModel.getData());
                    } else {
                        haloView.getHaloProductUnSucess(activity.getString(R.string.error_username_pass));
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    haloView.getHaloProductUnSucess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<HaloModel> call, Throwable t) {
                Progress.stop();
                haloView.getHaloProductUnSucess(activity.getString(R.string.error_server));
            }
        });
    }

    private void saveHaloProduct(JSONObject jsonObject) {
        Progress.start(activity);

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CommonModel> getOutput = ApiAdapter.getApiService().saveHaloData("application/json", "no-cache", body);

        getOutput.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    CommonModel commonModel = response.body();

                    if (commonModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        haloView.saveHaloProductSuccess(commonModel.getMessage());
                    } else {
                        haloView.saveHaloProductUnSucess(commonModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    haloView.saveHaloProductServerError(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                Progress.stop();
                haloView.saveHaloProductServerError(activity.getString(R.string.error_server));
            }
        });
    }

}
