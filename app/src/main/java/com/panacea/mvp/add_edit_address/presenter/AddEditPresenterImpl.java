package com.panacea.mvp.add_edit_address.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.add_edit_address.model.AddAddressModel;
import com.panacea.mvp.add_edit_address.model.EditAddressModel;
import com.panacea.mvp.commonMvp.model.CommonModel;
import com.panacea.mvp.dashboard.model.HomeProductsModel;
import com.panacea.mvp.dashboard.model.RadiatorModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 22-12-2017.
 */

public class AddEditPresenterImpl implements  AddEditAddressContractor.Presenter{


    AddEditAddressContractor.AddEditAddressView addEditAddressView;
    Activity activity;
    JSONObject jsonObject;


    public AddEditPresenterImpl(AddEditAddressContractor.AddEditAddressView addEditAddressView, Activity activity) {

        this.addEditAddressView = addEditAddressView;
        this.activity = activity;
    }

    @Override
    public void getAddAddress(JSONObject jsonObject) {
        try {
            ApiAdapter.getInstance(activity);
            addAddress(jsonObject);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            addEditAddressView.addAddressInternetError();
        }
    }

    @Override
    public void getEditAddress(String customerId, String addressId) {
        try {
            ApiAdapter.getInstance(activity);
            editAddress( customerId, addressId);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            addEditAddressView.editAddressInternetError();
        }
    }

    @Override
    public void getSaveEditAddress(JSONObject jsonObject) {
        try {
            ApiAdapter.getInstance(activity);
            saveEditAddress(jsonObject);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            addEditAddressView.saveEditAddressInternetError();
        }
    }

    private void addAddress(JSONObject jsonObject){
        Progress.start(activity);

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<AddAddressModel> getAddAddressOutput = ApiAdapter.getApiService().addAddress("application/json", "no-cache", body);

        getAddAddressOutput.enqueue(new Callback<AddAddressModel>() {
            @Override
            public void onResponse(Call<AddAddressModel> call, Response<AddAddressModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    AddAddressModel addAddressModel = response.body();
                    String message = "Hello";

                    if (addAddressModel.getStatus().equals("1")) {
                        addEditAddressView.getAddAddressSuccess(addAddressModel);
                    } else {
                        addEditAddressView.getAddAddressUnsuccess(addAddressModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    addEditAddressView.getAddAddressUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<AddAddressModel> call, Throwable t) {
                Progress.stop();
                addEditAddressView.getAddAddressUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }

    private void saveEditAddress(JSONObject jsonObject){
        Progress.start(activity);

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CommonModel> getSaveeditAddOutput = ApiAdapter.getApiService().saveEditList("application/json", "no-cache", body);

        getSaveeditAddOutput.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    CommonModel commonModel = response.body();
                    String message = "Hello";

                    if (commonModel.getStatus().equals("1")) {
                        addEditAddressView.getSaveEditAddressSuccess(commonModel.getMessage());
                    } else {
                        addEditAddressView.getSaveEditAddressUnsuccess(commonModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    addEditAddressView.getSaveEditAddressUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                Progress.stop();
                addEditAddressView.getSaveEditAddressUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }

    private void editAddress(String customerId, String addressId){
        Progress.start(activity);

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_CUSTOMER_ID, customerId);
            jsonObject.put(Const.PARAM_ADDRESS_ID, addressId);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }


        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<EditAddressModel> getEditAddressOutput = ApiAdapter.getApiService().getEditAddress("application/json", "no-cache", body);

        getEditAddressOutput.enqueue(new Callback<EditAddressModel>() {
            @Override
            public void onResponse(Call<EditAddressModel> call, Response<EditAddressModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    EditAddressModel editAddressModel = response.body();
                    String message = "Hello";

                    if (editAddressModel.getStatus().equals("1")) {
                        addEditAddressView.getEditAddressSuccess(editAddressModel.getData());
                    } else {
                        addEditAddressView.getEditAddressUnsuccess("");
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    addEditAddressView.getEditAddressUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<EditAddressModel> call, Throwable t) {
                Progress.stop();
                addEditAddressView.getEditAddressUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}
