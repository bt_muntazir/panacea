package com.panacea.mvp.add_edit_address.presenter;

import com.panacea.mvp.add_edit_address.model.AddAddressModel;
import com.panacea.mvp.add_edit_address.model.EditAddressModel;
import com.panacea.mvp.address.model.AddressModel;
import com.panacea.mvp.commonMvp.BaseView;

import org.json.JSONObject;

/**
 * Created by Braintech on 22-12-2017.
 */

public interface AddEditAddressContractor {

    public interface AddEditAddressView {

        void getAddAddressSuccess(AddAddressModel addAddressModel);

        void getAddAddressUnsuccess(String message);

        void addAddressInternetError();

        void addAddressServerError(String message);


        void getEditAddressSuccess(EditAddressModel.Data editAddress);

        void getEditAddressUnsuccess(String message);

        void editAddressInternetError();

        void editAddressServerError(String message);


        void getSaveEditAddressSuccess(String message);

        void getSaveEditAddressUnsuccess(String message);

        void saveEditAddressInternetError();

        void saveEditAddressServerError(String message);
    }

    public interface Presenter {
        void getAddAddress(JSONObject jsonObject);

        void getEditAddress(String customerId, String addressId);

        void getSaveEditAddress(JSONObject jsonObject);
    }
}
