package com.panacea.mvp.add_edit_address;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.adapter.model.CountrySpinnerModel;
import com.panacea.common.adapter.model.StateSpinnerModel;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.FontHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.requestresponse.Const;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.common.utility.SnackNotify;
import com.panacea.common.utility.Utils;
import com.panacea.mvp.add_edit_address.model.AddAddressModel;
import com.panacea.mvp.add_edit_address.model.EditAddressModel;
import com.panacea.mvp.add_edit_address.presenter.AddEditAddressContractor;
import com.panacea.mvp.add_edit_address.presenter.AddEditPresenterImpl;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.checkout.delivery_detail.DeliveryDetailActivity;
import com.panacea.mvp.checkout.delivery_method.DeliveryMethodActivity;
import com.panacea.mvp.commonMvp.adapter.AdapterSpinner;
import com.panacea.mvp.register.presenter.CountyrStatePresenterImpl;
import com.panacea.mvp.register.presenter.RegisterContractor;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnItemSelected;

public class AddEditAddressActivity extends AppCompatActivity implements AddEditAddressContractor.AddEditAddressView, RegisterContractor.CountryStateView {

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.txtViewPersonalDetail)
    AppCompatTextView txtViewPersonalDetail;

    @BindView(R.id.txtViewFirstname)
    AppCompatTextView txtViewFirstname;

    @BindView(R.id.txtViewLastName)
    AppCompatTextView txtViewLastName;

    @BindView(R.id.txtViewCompany)
    AppCompatTextView txtViewCompany;

    @BindView(R.id.txtViewAddress1)
    AppCompatTextView txtViewAddress1;

    @BindView(R.id.txtViewAddress2)
    AppCompatTextView txtViewAddress2;

    @BindView(R.id.txtViewCity)
    AppCompatTextView txtViewCity;

    @BindView(R.id.txtViewPostalCode)
    AppCompatTextView txtViewPostalCode;

    @BindView(R.id.txtViewCountry)
    AppCompatTextView txtViewCountry;

    @BindView(R.id.txtViewState)
    AppCompatTextView txtViewState;

    @BindView(R.id.txtViewDefaultAddress)
    AppCompatTextView txtViewDefaultAddress;

    @BindView(R.id.edtTextFirstName)
    AppCompatEditText edtTextFirstName;

    @BindView(R.id.edtTextLastName)
    AppCompatEditText edtTextLastName;

    @BindView(R.id.edtTextCompany)
    AppCompatEditText edtTextCompany;

    @BindView(R.id.edtTextAddress1)
    AppCompatEditText edtTextAddress1;

    @BindView(R.id.edtTextAddress2)
    AppCompatEditText edtTextAddress2;

    @BindView(R.id.edtTextPostalCode)
    AppCompatEditText edtTextPostalCode;

    @BindView(R.id.edtTextCity)
    AppCompatEditText edtTextCity;

    @BindView(R.id.linLayCountry)
    LinearLayout linLayCountry;

    @BindView(R.id.linLayState)
    LinearLayout linLayState;

    @BindView(R.id.spinnerCountry)
    AppCompatSpinner spinnerCountry;

    @BindView(R.id.spinnerState)
    AppCompatSpinner spinnerState;

    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;

    @BindView(R.id.radioYes)
    RadioButton radioYes;

    @BindView(R.id.radioNo)
    RadioButton radioNo;

    /*toolbar*/
    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.checkboxAddressConfirmation)
    AppCompatCheckBox checkboxAddressConfirmation;

    @BindView(R.id.btnContinue)
    Button btnContinue;

    ArrayList<HashMap<String, String>> arrayListCountry;
    ArrayList<HashMap<String, String>> arrayListState;

    AdapterSpinner adapterSpinnerCountry;
    AdapterSpinner adapterSpinnerState;

    String countryId = "";
    String stateId = "";
    String selectedStateName;
    String selectedCountryName;
    String addressId;
    String customerId;
    int selectedRadioButtonId = 0;
    DBHelper dbHelper;
    boolean isAdd = false;
    int checkboxAddressConfirm = 1;
    int intentdata;
    JSONObject jsonObject;

    CountyrStatePresenterImpl spinnerValuePresenterImpl;
    AddEditPresenterImpl addEditPresenter;

    EditAddressModel.Data editAddressData;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_edit_address);

        ButterKnife.bind(this);

        dbHelper = new DBHelper(this);

        //get intent data
        getIntentData();

        setFont();

        setTitle();

        addEditPresenter = new AddEditPresenterImpl(this, this);
        spinnerValuePresenterImpl = new CountyrStatePresenterImpl(this, this);

        setInitialAdapter();

        customerId = LoginManager.getInstance().getUserData().getCustomerId();

        setLayout();

        if (isAdd) {
            callCountryId();
        } else {
            getEditAddressDataApi();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateCartQty();
    }

    @Override
    public void getAddAddressSuccess(final AddAddressModel addAddressModel) {
        SnackNotify.showMessage(addAddressModel.getMessage(), coordinateLayout);

        Handler handler = new Handler();

        if (intentdata == ConstIntent.KEY_ADD) {
            handler.postDelayed(new Runnable() {
                public void run() {
                    finish();
                }
            }, 1000);
        } else if (intentdata == ConstIntent.KEY_EDIT) {

            handler.postDelayed(new Runnable() {
                public void run() {
                    finish();
                }
            }, 1000);

        } else if (intentdata == ConstIntent.KEY_ADD_CHECKOUT_BILLING) {

            handler.postDelayed(new Runnable() {
                public void run() {
                    if (checkboxAddressConfirm == 1) {

                        //save addressId in session
                        LoginManager.getInstance().saveBillingAddressId(String.valueOf(addAddressModel.getAddressId()));
                        LoginManager.getInstance().saveDeliveryAddressId(String.valueOf(addAddressModel.getAddressId()));

                        Intent intent = new Intent(AddEditAddressActivity.this, DeliveryMethodActivity.class);
                        startActivity(intent);


                    } else {

                        //save addressId in session
                        LoginManager.getInstance().saveBillingAddressId(String.valueOf(addAddressModel.getAddressId()));

                        Intent intent = new Intent(AddEditAddressActivity.this, DeliveryDetailActivity.class);
                        startActivity(intent);
                    }
                }
            }, 1000);

        } else if (intentdata == ConstIntent.KEY_ADD_CHECKOUT_DELIVERY) {

            handler.postDelayed(new Runnable() {
                public void run() {

                    //save addressId in session
                    LoginManager.getInstance().saveDeliveryAddressId(String.valueOf(addAddressModel.getAddressId()));

                    Intent intent = new Intent(AddEditAddressActivity.this, DeliveryMethodActivity.class);
                    startActivity(intent);
                }
            }, 1000);

        }
    }

    @Override
    public void getAddAddressUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void addAddressInternetError() {

    }

    @Override
    public void addAddressServerError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getEditAddressSuccess(EditAddressModel.Data editAddress) {
        editAddressData = editAddress;

        setTheFields();
    }

    @Override
    public void getEditAddressUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void editAddressInternetError() {

    }

    @Override
    public void editAddressServerError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getSaveEditAddressSuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);


        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            public void run() {
                finish();
            }
        }, 1000);
    }

    @Override
    public void getSaveEditAddressUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void saveEditAddressInternetError() {

    }

    @Override
    public void saveEditAddressServerError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getCountrySuccess(ArrayList<CountrySpinnerModel.Datum> arrayListCountryList) {

        int position = 0;
        for (int i = 0; i < arrayListCountryList.size(); i++) {
            CountrySpinnerModel.Datum datum = arrayListCountryList.get(i);
            String countryId = datum.getCountryId();
            String countryName = datum.getName();

            HashMap hashMap = new HashMap();
            hashMap.put(Const.KEY_ID, countryId);
            hashMap.put(Const.KEY_NAME, countryName);
            arrayListCountry.add(hashMap);

            if (!isAdd) {
                try {
                    if (editAddressData.getCountryId().equals(countryId)) {
                        position = i + 1;
                    }
                } catch (ArrayIndexOutOfBoundsException ee) {
                    ee.printStackTrace();
                }
            }
        }

        setCountyAdapter();
        if (!isAdd) {
            spinnerCountry.setSelection(position);
        }
       /* if (!isAdd) {
            for (int i = 0; i < arrayListCountryList.size(); i++) {
                CountrySpinnerModel.Datum datum = arrayListCountryList.get(i);
                if (editAddressData.getCountryId().equals(datum.getCountryId())) ;
                {
                    spinnerCountry.setSelection(i + 1);
                    break;
                }
            }
        }*/
    }

    @Override
    public void getCountryUnSucess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void countryInternetError() {
        SnackNotify.checkConnection(onretryCountryApi, coordinateLayout);
    }

    OnClickInterface onretryCountryApi = new OnClickInterface() {
        @Override
        public void onClick() {
            callCountryId();
        }
    };


    @Override
    public void countryServerError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getStateSuccess(ArrayList<StateSpinnerModel.Datum> arrayListStateList) {

        int position = 0;
        for (int i = 0; i < arrayListStateList.size(); i++) {
            StateSpinnerModel.Datum datum = arrayListStateList.get(i);
            String stateId = datum.getZoneId();
            String stateName = datum.getName();

            HashMap hashMap = new HashMap();
            hashMap.put(Const.KEY_ID, stateId);
            hashMap.put(Const.KEY_NAME, stateName);
            arrayListState.add(hashMap);

            if (!isAdd) {
                try {
                    if (editAddressData.getZoneId().equals(stateId)) {
                        position = i + 1;
                    }
                } catch (ArrayIndexOutOfBoundsException ee) {
                    ee.printStackTrace();
                }
            }
        }

        setStateAdapter();
        if (!isAdd) {
            spinnerState.setSelection(position);
        }

       /* if (!isAdd) {
            for (int i = 0; i < arrayListStateList.size(); i++) {
                StateSpinnerModel.Datum datum = arrayListStateList.get(i);
                if (editAddressData.getZoneId().equals(datum.getZoneId())) ;
                {
                    spinnerState.setSelection(i + 1);
                    break;
                }
            }
        }*/
    }

    @Override
    public void getStateUnSucess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void StateInternetError() {
        SnackNotify.checkConnection(onretryStateApi, coordinateLayout);
    }

    OnClickInterface onretryStateApi = new OnClickInterface() {
        @Override
        public void onClick() {
            callStateId(countryId);
        }
    };


    @Override
    public void StateServerError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    /*---------------------------------private methods--------------------------------------*/

    private void setTitle() {
        if (isAdd) {
            txtViewTitle.setText(getString(R.string.title_add_address));
            txtViewPersonalDetail.setText(getString((R.string.title_add_your_address)));
        } else {
            txtViewTitle.setText(getString(R.string.title_edit_address));
            txtViewPersonalDetail.setText(getString((R.string.title_edit_your_address)));
        }
    }

    private void setFont() {

        FontHelper.setFontFace(txtViewPersonalDetail, FontHelper.FontType.FONT_THIN, this);
        FontHelper.setFontFace(txtViewFirstname, FontHelper.FontType.FONT_SEMIBOLD, this);
        FontHelper.setFontFace(txtViewLastName, FontHelper.FontType.FONT_SEMIBOLD, this);
        FontHelper.setFontFace(txtViewCompany, FontHelper.FontType.FONT_SEMIBOLD, this);
        FontHelper.setFontFace(txtViewAddress1, FontHelper.FontType.FONT_SEMIBOLD, this);
        FontHelper.setFontFace(txtViewAddress2, FontHelper.FontType.FONT_SEMIBOLD, this);
        FontHelper.setFontFace(txtViewCity, FontHelper.FontType.FONT_SEMIBOLD, this);
        FontHelper.setFontFace(txtViewPostalCode, FontHelper.FontType.FONT_SEMIBOLD, this);
        FontHelper.setFontFace(txtViewCountry, FontHelper.FontType.FONT_SEMIBOLD, this);
        FontHelper.setFontFace(txtViewState, FontHelper.FontType.FONT_SEMIBOLD, this);
        FontHelper.setFontFace(txtViewDefaultAddress, FontHelper.FontType.FONT_SEMIBOLD, this);
    }

    private void saveAddressDataApi() {

        getData();
        addEditPresenter.getAddAddress(jsonObject);
    }

    private void saveEditAddressDataApi() {

        getData();
        addEditPresenter.getSaveEditAddress(jsonObject);
    }

    private void getEditAddressDataApi() {


        addEditPresenter.getEditAddress(customerId, addressId);
    }

    private void getData() {

        // hide keyboard
        Utils.hideKeyboardIfOpen(this);

        String firstname = edtTextFirstName.getText().toString();
        String lastName = edtTextLastName.getText().toString();
        String companyName = edtTextCompany.getText().toString();
        String city = edtTextCity.getText().toString();
        String postalcode = edtTextPostalCode.getText().toString();
        String address1 = edtTextAddress1.getText().toString();
        String address2 = edtTextAddress2.getText().toString();

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_CUSTOMER_ID, customerId);

            if (!isAdd) {
                jsonObject.put(Const.PARAM_ADDRESS_ID, addressId);
            }
            jsonObject.put(Const.PARAM_FIRSTNAME, firstname);
            jsonObject.put(Const.PARAM_LASTNAME, lastName);
            jsonObject.put(Const.PARAM_COMPANY, companyName);
            jsonObject.put(Const.PARAM_ZIP, postalcode);
            jsonObject.put(Const.PARAM_ADDRESS_1, address1);
            jsonObject.put(Const.PARAM_ADDRESS_2, address2);
            jsonObject.put(Const.PARAM_POSTCODE, postalcode);
            jsonObject.put(Const.PARAM_CITY, city);
            jsonObject.put(Const.PARAM_COUNTRY_ID, countryId);
            jsonObject.put(Const.PARAM_ZONE_ID, stateId);
            jsonObject.put(Const.PARAM_DEFAULT, selectedRadioButtonId);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

    }

    private void getIntentData() {

        Intent intent = getIntent();

        if (intent.hasExtra(ConstIntent.KEY_ADD_EDIT_ADDRESS)) {
            intentdata = intent.getIntExtra(ConstIntent.KEY_ADD_EDIT_ADDRESS, 0);

            if (intentdata == ConstIntent.KEY_ADD) {
                isAdd = true;
                checkboxAddressConfirmation.setVisibility(View.GONE);
                radioGroup.setVisibility(View.VISIBLE);
                txtViewDefaultAddress.setVisibility(View.VISIBLE);
                frameLayCart.setVisibility(View.VISIBLE);

            } else if (intentdata == ConstIntent.KEY_EDIT) {
                addressId = intent.getStringExtra(ConstIntent.KEY_ADDRESS_ID);
                isAdd = false;
                checkboxAddressConfirmation.setVisibility(View.GONE);
                radioGroup.setVisibility(View.VISIBLE);
                txtViewDefaultAddress.setVisibility(View.VISIBLE);
                frameLayCart.setVisibility(View.VISIBLE);

            } else if (intentdata == ConstIntent.KEY_ADD_CHECKOUT_BILLING) {
                isAdd = true;
                checkboxAddressConfirmation.setVisibility(View.VISIBLE);
                radioGroup.setVisibility(View.GONE);
                txtViewDefaultAddress.setVisibility(View.GONE);
                frameLayCart.setVisibility(View.GONE);
            } else if (intentdata == ConstIntent.KEY_ADD_CHECKOUT_DELIVERY) {
                isAdd = true;
                checkboxAddressConfirmation.setVisibility(View.GONE);
                radioGroup.setVisibility(View.GONE);
                txtViewDefaultAddress.setVisibility(View.GONE);
                frameLayCart.setVisibility(View.GONE);
            }
        }
    }

    private void setLayout() {

        if (isAdd) {


        }
    }

    private void callCountryId() {
        spinnerValuePresenterImpl.getCountry();
    }

    private void setTheFields() {

        edtTextFirstName.setText(editAddressData.getFirstname());
        edtTextLastName.setText(editAddressData.getLastname());
        edtTextCompany.setText(editAddressData.getCompany());
        edtTextAddress1.setText(editAddressData.getAddress1());
        edtTextAddress2.setText(editAddressData.getAddress2());
        edtTextPostalCode.setText(editAddressData.getPostcode());
        edtTextCity.setText(editAddressData.getCity());

        if (editAddressData.getDefault() == 0) {
            selectedRadioButtonId = 0;
            radioNo.setChecked(true);
            radioYes.setChecked(false);

        } else {
            selectedRadioButtonId = 1;
            radioNo.setChecked(false);
            radioYes.setChecked(true);
        }

        callCountryId();
    }

    private void setInitialAdapter() {
        arrayListCountry = new ArrayList<>();
        HashMap hashMapFirstIndexCountry = new HashMap();
        hashMapFirstIndexCountry.put(Const.KEY_ID, 0);
        hashMapFirstIndexCountry.put(Const.KEY_NAME, getString(R.string.hint_country_star));
        arrayListCountry.add(hashMapFirstIndexCountry);

        arrayListState = new ArrayList<>();
        HashMap hashMapFirstIndexState = new HashMap();
        hashMapFirstIndexState.put(Const.KEY_ID, 0);
        hashMapFirstIndexState.put(Const.KEY_NAME, getString(R.string.hint_state_star));
        arrayListState.add(hashMapFirstIndexState);

        setCountyAdapter();
        setStateAdapter();
    }

    private void setCountyAdapter() {
        adapterSpinnerCountry = new AdapterSpinner(this, R.layout.spinner_selected_view, arrayListCountry);
        spinnerCountry.setAdapter(adapterSpinnerCountry);
    }

    private void setStateAdapter() {
        adapterSpinnerState = new AdapterSpinner(this, R.layout.spinner_selected_view, arrayListState);
        spinnerState.setAdapter(adapterSpinnerState);
    }


    /*-----------------------------public method------------------------------------------------*/

    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }

    /*-----------------------------on click method-----------------------------------------------*/


    @OnClick(R.id.imgBackButton)
    public void onBackClick() {

        onBackPressed();
    }

    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (dbHelper.getCartCount() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            }
        }
    }

    private void callStateId(String countryId) {
        spinnerValuePresenterImpl.getState(countryId);
    }


    @OnItemSelected(R.id.spinnerCountry)
    void onItemCountrySelected(int position) {
        if (position != 0) {
            countryId = String.valueOf(arrayListCountry.get(position).get(Const.KEY_ID));
            spinnerCountry.setSelection(arrayListCountry.indexOf(countryId));
            selectedCountryName = String.valueOf(arrayListCountry.indexOf(countryId));
            callStateId(countryId);
        }
    }

    @OnItemSelected(R.id.spinnerState)
    void onItemStateSelected(int position) {
        if (position != 0) {
            stateId = String.valueOf(arrayListState.get(position).get(Const.KEY_ID));
            spinnerCountry.setSelection(arrayListCountry.indexOf(stateId));
            selectedStateName = String.valueOf(arrayListCountry.indexOf(stateId));
        }
    }

    @OnClick(R.id.btnContinue)
    public void btnContinueClick() {

        if (isAdd) {
            saveAddressDataApi();
        } else {

            saveEditAddressDataApi();
        }
    }

    @OnCheckedChanged({R.id.radioYes, R.id.radioNo})
    public void onRadioButtonCheckChanged(CompoundButton button, boolean checked) {
        if (checked) {
            switch (button.getId()) {
                case R.id.radioYes:
                    selectedRadioButtonId = 1;
                    break;
                case R.id.radioNo:
                    selectedRadioButtonId = 0;
                    break;
            }
        }
    }

    @OnCheckedChanged({R.id.checkboxAddressConfirmation})
    public void onCheckBoxCheckChanged(CompoundButton button, boolean checked) {

        switch (button.getId()) {
            case R.id.checkboxAddressConfirmation:
                if (checked) {
                    checkboxAddressConfirm = 1;
                } else {
                    checkboxAddressConfirm = 0;
                }
                break;
        }
    }
}
