package com.panacea.mvp.direction;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.panacea.R;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.mvp.cart.ShoppingCartActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class GetDirectionActivity extends FragmentActivity implements OnMapReadyCallback {

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    private GoogleMap mMap;

    double latitude = 0.0;
    double longitude = 0.0;

    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_direction);
        ButterKnife.bind(this);
        dbHelper = new DBHelper(this);
        //set toolbar
        setToolbar();
        // get intent data coming from previous page
        getIntentData();
        // load map
        SupportMapFragment mapFragment = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        frameLayCart.setVisibility(View.GONE);
        updateCartQty();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        googleMap.getUiSettings().setZoomControlsEnabled(true);

        // Add a marker in Sydney, Australia, and move the camera.
        LatLng myLatLng = new LatLng(latitude, longitude);
        // mMap.addMarker(new MarkerOptions().position(sydney).title("Current Loc"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(myLatLng));

        MarkerOptions markerOptMyLoc = new MarkerOptions().position(myLatLng).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_location));

        Marker markerYLoc = googleMap.addMarker(markerOptMyLoc);

        // bounce marker
        // bounceMarker(markerYLoc);
    }

    private void getIntentData() {

        Intent intent = getIntent();

        if (intent.hasExtra(ConstIntent.KEY_LAT)) {

            latitude = Double.parseDouble(intent.getStringExtra(ConstIntent.KEY_LAT));
            longitude = Double.parseDouble(intent.getStringExtra(ConstIntent.KEY_LON));
        }
    }

    private void setToolbar() {

        txtViewTitle.setText(getString(R.string.title_get_location));
    }

    public void bounceMarker(final Marker marker) {

        if (!(marker == null)) {

            final Handler handler = new Handler();
            final long startTime = SystemClock.uptimeMillis();
            final long duration = 2000;
            final Interpolator interpolator = new BounceInterpolator();
            handler.post(new Runnable() {
                @Override
                public void run() {
                    long elapsed = SystemClock.uptimeMillis() - startTime;
                    float t = Math.max(1 - interpolator.getInterpolation((float) elapsed / duration), 0);
                    marker.setAnchor(0.5f, 1.0f + t);

                    if (t > 0.0) {
                        handler.postDelayed(this, 16);
                    } else {
                        bounceMarker(marker);
                    }
                }
            });
        }
    }

    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }

    @OnClick(R.id.imgBackButton)
    public void onBackClick() {

        onBackPressed();
    }

    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (dbHelper.getCartCount() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            }
        }
    }

}
