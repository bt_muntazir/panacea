package com.panacea.mvp.success_order.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Braintech on 05-01-2018.
 */

public class OrderSuccessModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("heading")
        @Expose
        private String heading;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("m1")
        @Expose
        private String m1;
        @SerializedName("m2")
        @Expose
        private String m2;
        @SerializedName("m3")
        @Expose
        private String m3;
        @SerializedName("m4")
        @Expose
        private String m4;

        public String getHeading() {
            return heading;
        }

        public void setHeading(String heading) {
            this.heading = heading;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getM1() {
            return m1;
        }

        public void setM1(String m1) {
            this.m1 = m1;
        }

        public String getM2() {
            return m2;
        }

        public void setM2(String m2) {
            this.m2 = m2;
        }

        public String getM3() {
            return m3;
        }

        public void setM3(String m3) {
            this.m3 = m3;
        }

        public String getM4() {
            return m4;
        }

        public void setM4(String m4) {
            this.m4 = m4;
        }
    }
}