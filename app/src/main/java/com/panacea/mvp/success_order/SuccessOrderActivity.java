package com.panacea.mvp.success_order;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.CharacterStyle;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.FontHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.utility.SnackNotify;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.dashboard.adapter.SearchPartsAdapter;
import com.panacea.mvp.productdetail.model.ProductDetailModel;
import com.panacea.mvp.success_order.model.OrderSuccessModel;
import com.panacea.mvp.success_order.presenter.SuccessOrderPresenterImpl;
import com.panacea.mvp.success_order.presenter.SuccressOrderContractor;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SuccessOrderActivity extends AppCompatActivity implements SuccressOrderContractor.SuccessOrderView {

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.imgViewSuccess)
    ImageView imgViewSuccess;

    @BindView(R.id.txtViewMessage1)
    TextView txtViewMessage1;

    @BindView(R.id.txtViewMessage2)
    TextView txtViewMessage2;

    @BindView(R.id.txtViewMessage3)
    TextView txtViewMessage3;

    @BindView(R.id.btnGotoDashboard)
    Button btnGotoDashboard;

    SuccessOrderPresenterImpl successOrderPresenter;
    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success_order);
        ButterKnife.bind(this);
        dbHelper = new DBHelper(this);
        successOrderPresenter = new SuccessOrderPresenterImpl(this, this);
        setFont();
        getOrderSuccessDataApi();
    }

    @Override
    protected void onResume() {
        super.onResume();

        frameLayCart.setVisibility(View.GONE);
    }


    @Override
    public void onBackPressed() {
        //   super.onBackPressed();

        startActivity(new Intent(this, DashboardActivity.class));

        finishAffinity();
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onInternetError() {

        SnackNotify.checkConnection(onretryOrderSuccessApi, coordinateLayout);
    }

    OnClickInterface onretryOrderSuccessApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getOrderSuccessDataApi();
        }
    };


    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getSuccessOrderSuccess(OrderSuccessModel.Data proData) {
        //SnackNotify.showMessage(proData.getMessage(), coordinateLayout);

        //clear the checkout session
        LoginManager.getInstance().clearCheckOutSession();

        //empty the cart
        dbHelper.deleteAllCartItem();

        SimpleSpanBuilder ssb = new SimpleSpanBuilder();
        ssb.appendWithSpace(proData.getM1());
        ssb.append(proData.getM2(), new ForegroundColorSpan(Color.RED));
        txtViewMessage1.setText(ssb.build());
        txtViewMessage2.setText(proData.getM3());
        txtViewMessage3.setText(proData.getM4());

        //set the field
        Picasso.with(this).load(proData.getImage()).into(imgViewSuccess);
        // txtViewDescription.setText(proData.getMessage());
        txtViewTitle.setText(proData.getHeading());
    }

    /*-------------------------------------private method------------------------------*/

    private void getOrderSuccessDataApi() {
        String orderId = LoginManager.getInstance().getOrderId();
        if (orderId != null) {
            successOrderPresenter.getSuccessOrder(orderId);
        }
    }

    private void setFont() {

        FontHelper.setFontFace(txtViewMessage1, FontHelper.FontType.FONT_SEMIBOLD, this);
        FontHelper.setFontFace(txtViewMessage2, FontHelper.FontType.FONT_THIN, this);
        FontHelper.setFontFace(txtViewMessage3, FontHelper.FontType.FONT_THIN, this);
        FontHelper.setFontFace(btnGotoDashboard, FontHelper.FontType.FONT_SEMIBOLD, this);
    }

    /*-----------------------------------------click method------------------------------*/

    @OnClick(R.id.imgBackButton)
    public void onBackClick() {

        startActivity(new Intent(this, DashboardActivity.class));

        finishAffinity();
    }


    @OnClick(R.id.btnGotoDashboard)
    public void btnGotoDashboardClicked() {

        startActivity(new Intent(this, DashboardActivity.class));

        finishAffinity();
    }


    public class SimpleSpanBuilder {
        private class SpanSection {
            private final String text;
            private final int startIndex;
            private final CharacterStyle[] styles;

            private SpanSection(String text, int startIndex, CharacterStyle... styles) {
                this.styles = styles;
                this.text = text;
                this.startIndex = startIndex;
            }

            private void apply(SpannableStringBuilder spanStringBuilder) {
                if (spanStringBuilder == null) return;
                for (CharacterStyle style : styles) {
                    spanStringBuilder.setSpan(style, startIndex, startIndex + text.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                }
            }
        }

        private ArrayList<SpanSection> spanSections;
        private StringBuilder stringBuilder;

        public SimpleSpanBuilder() {
            stringBuilder = new StringBuilder();
            spanSections = new ArrayList<>();
        }

        public SimpleSpanBuilder append(String text, CharacterStyle... styles) {
            if (styles != null && styles.length > 0) {
                spanSections.add(new SpanSection(text, stringBuilder.length(), styles));
            }
            stringBuilder.append(text);
            return this;
        }

        public SimpleSpanBuilder appendWithSpace(String text, CharacterStyle... styles) {
            return append(text.concat(" "), styles);
        }

        public SimpleSpanBuilder appendWithLineBreak(String text, CharacterStyle... styles) {
            return append(text.concat("\n"), styles);
        }

        public SpannableStringBuilder build() {
            SpannableStringBuilder ssb = new SpannableStringBuilder(stringBuilder.toString());
            for (SpanSection section : spanSections) {
                section.apply(ssb);
            }
            return ssb;
        }

        @Override
        public String toString() {
            return stringBuilder.toString();
        }
    }
}
