package com.panacea.mvp.success_order.presenter;

import com.panacea.mvp.commonMvp.BaseView;
import com.panacea.mvp.productdetail.model.ProductDetailModel;
import com.panacea.mvp.success_order.model.OrderSuccessModel;

/**
 * Created by Braintech on 05-01-2018.
 */

public class SuccressOrderContractor {
    public interface SuccessOrderView extends BaseView {

        void getSuccessOrderSuccess(OrderSuccessModel.Data proData);
    }

    public interface Presenter {
        void getSuccessOrder(String orderId);
    }
}
