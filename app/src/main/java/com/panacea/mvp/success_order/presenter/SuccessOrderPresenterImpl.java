package com.panacea.mvp.success_order.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.commonMvp.BaseView;
import com.panacea.mvp.productdetail.model.ProductDetailModel;
import com.panacea.mvp.productdetail.presenter.ProductDetailContractor;
import com.panacea.mvp.success_order.model.OrderSuccessModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 05-01-2018.
 */

public class SuccessOrderPresenterImpl implements SuccressOrderContractor.Presenter {

    SuccressOrderContractor.SuccessOrderView successOrderView;
    Activity activity;
    JSONObject jsonObject;

    public SuccessOrderPresenterImpl(SuccressOrderContractor.SuccessOrderView successOrderView, Activity activity) {
        this.successOrderView = successOrderView;
        this.activity = activity;
    }

    @Override
    public void getSuccessOrder(String orderId) {
        try {
            ApiAdapter.getInstance(activity);
            successDetail(orderId);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            successOrderView.onInternetError();
        }
    }

    private void successDetail(String orderId) {

        Progress.start(activity);

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_ORDER_ID, orderId);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<OrderSuccessModel> getProductDetailOutput = ApiAdapter.getApiService().orderSuccess("application/json", "no-cache", body);

        getProductDetailOutput.enqueue(new Callback<OrderSuccessModel>() {
            @Override
            public void onResponse(Call<OrderSuccessModel> call, Response<OrderSuccessModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    OrderSuccessModel orderSuccessModel = response.body();

                    if (orderSuccessModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        successOrderView.getSuccessOrderSuccess(orderSuccessModel.getData());
                    } else {
                        successOrderView.apiUnsuccess("");
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    successOrderView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<OrderSuccessModel> call, Throwable t) {
                Progress.stop();
                successOrderView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}
