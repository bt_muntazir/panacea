package com.panacea.mvp.dashboard.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.renderscript.Sampler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.database.CartItem;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.TextViewMoreLess;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.requestresponse.Const;
import com.panacea.common.utility.SnackNotify;
import com.panacea.common.utility.Utils;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.dashboard.adapter.LedRadiatorAdapter;
import com.panacea.mvp.dashboard.adapter.SearchPartsAdapter;
import com.panacea.mvp.dashboard.model.PartsModel;
import com.panacea.mvp.dashboard.model.RadiatorModel;
import com.panacea.mvp.dashboard.presenter.DashboardContractor;
import com.panacea.mvp.dashboard.presenter.HomeProductImpl;
import com.panacea.mvp.dashboard.presenter.LedProductImpl;
import com.panacea.mvp.dashboard.presenter.PartsProductImpl;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.nfc.tech.MifareUltralight.PAGE_SIZE;


public class PartsFragment extends Fragment implements DashboardContractor.PartsProductView {

    @BindView(R.id.mainContainer)
    FrameLayout mainContainer;

    @BindView(R.id.recyclerViewProducts)
    RecyclerView recyclerViewProducts;

    @BindView(R.id.imgViewNoRecordFound)
    ImageView imgViewNoRecordFound;

    //for paging
    boolean isEndReached;
    boolean mIsLoading;
    int page = 0;
    boolean isSearchClicked = false;
    LinearLayoutManager linearLayoutManager;

    //Fragment
    String search1;
    String search2;
    String search3;
    String search4;
    String search5;
    String search6;
    String search7;
    String search8;

//For dialog
    RelativeLayout relLayoutContainer;

    String qty1;
    String qty2;
    String qty3;
    String qty4;
    String qty5;
    String qty6;
    String qty7;
    String qty8;

    PartsProductImpl partsProduct;
    JSONObject jsonObject;
    DBHelper dbHelper;
    ArrayList<CartItem> getCartItems;


    ArrayList<PartsModel.Data.Product> arrayListPartsProducts;
    SearchPartsAdapter searchPartsAdapter;

    public PartsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_parts, container, false);

        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        arrayListPartsProducts = new ArrayList<>();
        dbHelper = new DBHelper(getActivity());
        getCartItems = dbHelper.getCartItem();
        partsProduct = new PartsProductImpl(this, getActivity());
        searchPartsAdapter = new SearchPartsAdapter(getActivity(), arrayListPartsProducts);
        setLayoutManager();
        getPaginationData(false);
        openDialogForAppointmentDetail();
    }

    @Override
    public void onStart() {
        super.onStart();

        getCartItems = dbHelper.getCartItem();

        if (arrayListPartsProducts.size() != 0) {
            if (getCartItems != null && getCartItems.size() != 0) {
                for (int i = 0; i < arrayListPartsProducts.size(); i++) {

                    for (int j = 0; j < getCartItems.size(); j++) {

                        if (getCartItems.get(j).getProductId().equals(arrayListPartsProducts.get(i).getProductId())) {
                            arrayListPartsProducts.get(i).setAddedToCart(true);
                            break;
                        } else {
                            arrayListPartsProducts.get(i).setAddedToCart(false);
                        }
                    }
                }
            } else {
                for (int i = 0; i < arrayListPartsProducts.size(); i++) {
                    arrayListPartsProducts.get(i).setAddedToCart(false);
                }
            }

            searchPartsAdapter.notifyDataSetChanged();
        }
    }



    @Override
    public void getPartsProductSuccess(PartsModel.Data arrayListPartModel) {
        arrayListPartsProducts = arrayListPartModel.getProducts();
        PartsModel.Data.FilterData filterData = arrayListPartModel.getFilterData();

        for (int i = 0; i < arrayListPartModel.getProducts().size(); i++) {

            for (int j = 0; j < getCartItems.size(); j++) {

                if (getCartItems.get(j).getProductId().equals(arrayListPartsProducts.get(i).getProductId())) {
                    arrayListPartsProducts.get(i).setAddedToCart(true);
                    break;
                } else {
                    arrayListPartsProducts.get(i).setAddedToCart(false);
                }
            }

        }


        int totalRecords = Integer.valueOf(filterData.getLimit());

        //set the page title
        ((DashboardActivity) getActivity()).setDashboardTitle(filterData.getPageTitle());

        if (arrayListPartsProducts != null && arrayListPartsProducts.size()>0) {

            //hide recyclerview and show image no record found
            recyclerViewProducts.setVisibility(View.VISIBLE);
            imgViewNoRecordFound.setVisibility(View.GONE);

            if (arrayListPartsProducts.size() > (totalRecords - 1)) {
                isEndReached = false;
            } else {
                isEndReached = true;
            }
            searchPartsAdapter.updateData(arrayListPartsProducts, isSearchClicked);
            isSearchClicked = false;
        } else {
            //hide recyclerview and show image no record found
            recyclerViewProducts.setVisibility(View.GONE);
            imgViewNoRecordFound.setVisibility(View.VISIBLE);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mIsLoading = false;
            }
        }, 1000);
    }

    @Override
    public void getPartsProductUnSucess(String message) {

        mIsLoading = false;
        isEndReached = false;

        arrayListPartsProducts.clear();
        recyclerViewProducts.setAdapter(null);
        searchPartsAdapter = new SearchPartsAdapter(getActivity(), arrayListPartsProducts);
        recyclerViewProducts.setAdapter(searchPartsAdapter);
    }

    @Override
    public void partsProductInternetError() {
        SnackNotify.checkConnection(onretryMultipleSearchDataApi, mainContainer);
    }

    OnClickInterface onretryMultipleSearchDataApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getMultipleSearchData();
        }
    };
    @Override
    public void partsProductServerError(String message) {
        SnackNotify.showMessage(message, mainContainer);
    }

    private void getPaginationData(boolean isRetry) {

        mIsLoading = true;
        if (isRetry) {
            if (page == -1) {
                page = 0;
            }
            getMultipleSearchData();
        } else {
            page = page + 1;
            getMultipleSearchData();
        }
    }

    private void getMultipleSearchData() {

        // hide keyboard
        Utils.hideKeyboardIfOpen(getActivity());


        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_SEARCH1, search1);
            jsonObject.put(Const.PARAM_SEARCH2, search2);
            jsonObject.put(Const.PARAM_SEARCH3, search3);
            jsonObject.put(Const.PARAM_SEARCH4, search4);
            jsonObject.put(Const.PARAM_SEARCH5, search5);
            jsonObject.put(Const.PARAM_SEARCH6, search6);
            jsonObject.put(Const.PARAM_QTY1, qty1);
            jsonObject.put(Const.PARAM_QTY2, qty2);
            jsonObject.put(Const.PARAM_QTY3, qty3);
            jsonObject.put(Const.PARAM_QTY4, qty4);
            jsonObject.put(Const.PARAM_QTY5, qty5);
            jsonObject.put(Const.PARAM_QTY6, qty6);
            jsonObject.put(Const.PARAM_PAGE, page);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        // call login api
        partsProduct.getPartsProducts(jsonObject);
    }


    private void openDialogForAppointmentDetail() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.layout_parts_search);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.alpha(Color.BLACK)));

        final TextView txtViewTopConst = (TextView) dialog.findViewById(R.id.txtViewProSummary);
       relLayoutContainer = (RelativeLayout) dialog.findViewById(R.id.relLayoutContainer);

        final AppCompatEditText txtViewSearch1 = (AppCompatEditText) dialog.findViewById(R.id.edtTextSearch1);
        final AppCompatEditText txtViewSearch2 = (AppCompatEditText) dialog.findViewById(R.id.edtTextSearch2);
        final AppCompatEditText txtViewSearch3 = (AppCompatEditText) dialog.findViewById(R.id.edtTextSearch3);
            final AppCompatEditText txtViewSearch4 = (AppCompatEditText) dialog.findViewById(R.id.edtTextSearch4);
       final AppCompatEditText txtViewSearch5 = (AppCompatEditText) dialog.findViewById(R.id.edtTextSearch5);
        final AppCompatEditText txtViewSearch6 = (AppCompatEditText) dialog.findViewById(R.id.edtTextSearch6);
/*         final AppCompatEditText txtViewSearch7 = (AppCompatEditText) dialog.findViewById(R.id.edtTextSearch7);
        final AppCompatEditText txtViewSearch8 = (AppCompatEditText) dialog.findViewById(R.id.edtTextSearch8);*/
        final AppCompatEditText txtViewQty1 = (AppCompatEditText) dialog.findViewById(R.id.edtTextQnty1);
        final AppCompatEditText txtViewQty2 = (AppCompatEditText) dialog.findViewById(R.id.edtTextQnty2);
        final AppCompatEditText txtViewQty3 = (AppCompatEditText) dialog.findViewById(R.id.edtTextQnty3);
        final AppCompatEditText txtViewQty4 = (AppCompatEditText) dialog.findViewById(R.id.edtTextQnty4);
        final AppCompatEditText txtViewQty5 = (AppCompatEditText) dialog.findViewById(R.id.edtTextQnty5);
        final AppCompatEditText txtViewQty6 = (AppCompatEditText) dialog.findViewById(R.id.edtTextQnty6);
     /*    final AppCompatEditText txtViewQty7 = (AppCompatEditText) dialog.findViewById(R.id.edtTextQnty7);
        final AppCompatEditText txtViewQty8 = (AppCompatEditText) dialog.findViewById(R.id.edtTextQnty8);*/
        final ImageView imgViewClose = (ImageView) dialog.findViewById(R.id.imgViewClose);
        final Button btnSearch = (Button) dialog.findViewById(R.id.btnSearch);


       String profileSummary=getString(R.string.title_subparts_string);

        if (profileSummary != null && !TextUtils.isEmpty(profileSummary)) {
            txtViewTopConst.setText(profileSummary);
            //Read more
            if (TextViewMoreLess.isViewMoreRequired(txtViewTopConst))
                TextViewMoreLess.doResizeTextView(txtViewTopConst, 2, "View More", true);


        } else {

        }

        imgViewClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DashboardFragment dashboardFragment = new DashboardFragment();
                ((DashboardActivity) getActivity()).replaceFragment(dashboardFragment,(getString(R.string.tag_dashboard)));
                ((DashboardActivity) getActivity()).createTabUnSelctedIcons();
                dialog.dismiss();
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                search1 = String.valueOf(txtViewSearch1.getText());
                search2 = String.valueOf(txtViewSearch2.getText());
                search3 = String.valueOf(txtViewSearch3.getText());
                search4 = String.valueOf(txtViewSearch4.getText());
               search5 = String.valueOf(txtViewSearch5.getText());
                search6 = String.valueOf(txtViewSearch6.getText());
             /*    search7 = String.valueOf(txtViewSearch7.getText());
                search8 = String.valueOf(txtViewSearch8.getText());*/

                qty1 = String.valueOf(txtViewQty1.getText());
                qty2 = String.valueOf(txtViewQty2.getText());
                qty3 = String.valueOf(txtViewQty3.getText());
                qty4 = String.valueOf(txtViewQty4.getText());
                qty5 = String.valueOf(txtViewQty5.getText());
                qty6 = String.valueOf(txtViewQty6.getText());
               /*  qty7 = String.valueOf(txtViewQty7.getText());
                qty8 = String.valueOf(txtViewQty8.getText());*/

                if (validation(search1, search2, search3, search4, search5, search6)) {

                    // hide keyboard
                    //Utils.hideKeyboardIfOpen(getActivity());

                    getMultipleSearchData();
                    ((DashboardActivity) getActivity()).createTabUnSelctedIcons();
                    dialog.dismiss();
                }

            }

        });
        dialog.show();
    }
    private boolean validation(String search1, String search2, String search3, String search4, String search5, String search6) {

        if ((Utils.isEmptyOrNull(search1)) && (Utils.isEmptyOrNull(search2))&& (Utils.isEmptyOrNull(search3))
                && (Utils.isEmptyOrNull(search4)) && (Utils.isEmptyOrNull(search5)) && (Utils.isEmptyOrNull(search6))) {

            SnackNotify.showMessage(getString(R.string.empty_part_no), relLayoutContainer);

            return false;
        }
        return true;
    }



    private void setLayoutManager() {
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewProducts.setLayoutManager(linearLayoutManager);
        recyclerViewProducts.setItemAnimator(new DefaultItemAnimator());
        recyclerViewProducts.setHasFixedSize(true);
        recyclerViewProducts.setNestedScrollingEnabled(false);
        setAdapter();
        recyclerViewProducts.addOnScrollListener(mRecyclerViewOnScrollListener);
    }

    // implement paging
    private RecyclerView.OnScrollListener mRecyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = linearLayoutManager.getChildCount();
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

            if (!mIsLoading && !isEndReached) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
                    getPaginationData(false);
                }
            }
        }
    };

    private void setAdapter() {
        searchPartsAdapter = new SearchPartsAdapter(getActivity(), arrayListPartsProducts);
        recyclerViewProducts.setAdapter(searchPartsAdapter);
    }


}
