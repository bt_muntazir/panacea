package com.panacea.mvp.dashboard.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.adapter.model.CountrySpinnerModel;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.mvp.dashboard.model.HomeProductsModel;
import com.panacea.mvp.register.presenter.RegisterContractor;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 07-12-2017.
 */

public class HomeProductImpl implements DashboardContractor.HomeProduct {

    DashboardContractor.HomeProductView homeProductView;
    Activity activity;


    public HomeProductImpl(DashboardContractor.HomeProductView homeProductView, Activity activity) {

        this.homeProductView = homeProductView;
        this.activity = activity;
    }

    @Override
    public void getHomeProducts() {
        try {
            ApiAdapter.getInstance(activity);
            homeProducts();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            homeProductView.homeProductInternetError();
        }
    }

    private void homeProducts() {
        Progress.start(activity);

        Call<HomeProductsModel> getCountryOutput = ApiAdapter.getApiService().getHomeProducts();

        getCountryOutput.enqueue(new Callback<HomeProductsModel>() {
            @Override
            public void onResponse(Call<HomeProductsModel> call, Response<HomeProductsModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    HomeProductsModel homeProductsModel = response.body();
                    String message = "Hello";

                    if (homeProductsModel.getStatus().equals("1")) {
                        homeProductView.getHomeProductSuccess(homeProductsModel.getData());
                    } else {
                        homeProductView.getHomeProductUnSucess("Login Fail");
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    homeProductView.getHomeProductUnSucess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<HomeProductsModel> call, Throwable t) {
                Progress.stop();
                homeProductView.getHomeProductUnSucess(activity.getString(R.string.error_server));
            }
        });
    }
}