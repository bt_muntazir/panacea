package com.panacea.mvp.dashboard.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.adapter.model.CountrySpinnerModel;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.common.utility.SnackNotify;
import com.panacea.common.utility.Utils;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.dashboard.adapter.HomeProductAdapter;
import com.panacea.mvp.dashboard.model.HomeProductsModel;
import com.panacea.mvp.dashboard.presenter.DashboardContractor;
import com.panacea.mvp.dashboard.presenter.HomeProductImpl;
import com.panacea.mvp.register.presenter.CountyrStatePresenterImpl;
import com.panacea.mvp.register.presenter.RegisterContractor;
import com.panacea.mvp.register.presenter.RegisterPresenterImpl;
import com.panacea.mvp.splash.model.SplashModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DashboardFragment extends Fragment implements DashboardContractor.HomeProductView {

    @BindView(R.id.txtViewConstant)
    TextView txtViewConstant;

    @BindView(R.id.txtViewAddress1)
    TextView txtViewAddress1;

    @BindView(R.id.txtViewAddress2)
    TextView txtViewAddress2;

    @BindView(R.id.imgViewTopConstant)
    ImageView imgViewTopConstant;

    @BindView(R.id.mainContainer)
    FrameLayout mainContainer;

    @BindView(R.id.recyclerViewProducts)
    RecyclerView recyclerViewProducts;

    HomeProductImpl homeProduct;

    ArrayList<HomeProductsModel.Datum> homeProductsModels;

    SplashModel.Data splashModel;

    public DashboardFragment() {
    }

    String thumbImage = LoginManager.getInstance().getConstantData().getVideoThumbUrl();
    String thumbImageVideo = LoginManager.getInstance().getConstantData().getVideoUrl();

    String thumbImageText1 = LoginManager.getInstance().getConstantData().getVideoText1();
    String thumbImageText2 = LoginManager.getInstance().getConstantData().getVideoText2();


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        homeProduct = new HomeProductImpl(this, getActivity());

        setConstantData();

        getHomeProduct();
    }

    @Override
    public void getHomeProductSuccess(ArrayList<HomeProductsModel.Datum> arrayListHomeProducts) {

        homeProductsModels = arrayListHomeProducts;

        if (arrayListHomeProducts.size() > 0) {
            // imgViewNoRecordFound.setVisibility(View.GONE);
            recyclerViewProducts.setVisibility(View.VISIBLE);
            setLayoutManager();
        } else {
            // imgViewNoRecordFound.setVisibility(View.VISIBLE);
            recyclerViewProducts.setVisibility(View.GONE);
        }
    }


    @Override
    public void getHomeProductUnSucess(String message) {
        SnackNotify.showMessage(message, mainContainer);
    }

    @Override
    public void homeProductInternetError() {
        SnackNotify.checkConnection(onretryHomeProductApi, mainContainer);
    }

    OnClickInterface onretryHomeProductApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getHomeProduct();
        }
    };

    @Override
    public void homeProductServerError(String message) {
        SnackNotify.showMessage(message, mainContainer);
    }

    private void getHomeProduct() {
        homeProduct.getHomeProducts();
    }

    private void setConstantData(){

        Picasso.with(getActivity()).load(thumbImage).into(imgViewTopConstant);
        txtViewAddress1.setText(thumbImageText1);
        txtViewAddress2.setText(thumbImageText2);

        ((DashboardActivity)getActivity()).setDashboardImageLogo();
    }



    private void setLayoutManager() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewProducts.setLayoutManager(linearLayoutManager);
        recyclerViewProducts.setItemAnimator(new DefaultItemAnimator());
        recyclerViewProducts.setHasFixedSize(true);
        recyclerViewProducts.setNestedScrollingEnabled(false);
        setAdapter();
    }

    private void setAdapter() {
        HomeProductAdapter homeProductAdapter = new HomeProductAdapter(getActivity(), homeProductsModels);
        recyclerViewProducts.setAdapter(homeProductAdapter);
    }


    @OnClick(R.id.txtViewConstant)
    public void txtViewCameraClick() {

       /* Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(splashModel.getVideoUrl()));
        intent.setDataAndType(Uri.parse(splashModel.getVideoUrl()), "video/mp4");
        startActivity(intent);*/

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(thumbImageVideo));
        intent.setDataAndType(Uri.parse(thumbImageVideo), "video/mp4");
        startActivity(intent);
    }

    @OnClick(R.id.cardViewImage)
    public void cardViewImageClicked(){
        Utils.hideKeyboardIfOpen(getActivity());
    }
}
