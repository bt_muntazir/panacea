package com.panacea.mvp.dashboard.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Braintech on 12-12-2017.
 */
public class PartsModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("filter_data")
        @Expose
        private FilterData filterData;
        @SerializedName("products")
        @Expose
        private ArrayList<Product> products = null;

        public FilterData getFilterData() {
            return filterData;
        }

        public void setFilterData(FilterData filterData) {
            this.filterData = filterData;
        }

        public ArrayList<Product> getProducts() {
            return products;
        }

        public void setProducts(ArrayList<Product> products) {
            this.products = products;
        }


        public class FilterData {

            @SerializedName("page_title")
            @Expose
            private String pageTitle;
            @SerializedName("search")
            @Expose
            private String search;
            @SerializedName("multiple")
            @Expose
            private Multiple multiple;
            @SerializedName("sort")
            @Expose
            private String sort;
            @SerializedName("order")
            @Expose
            private String order;
            @SerializedName("page")
            @Expose
            private Integer page;
            @SerializedName("limit")
            @Expose
            private String limit;
            @SerializedName("total_pagination")
            @Expose
            private Integer totalPagination;

            public String getPageTitle() {
                return pageTitle;
            }

            public void setPageTitle(String pageTitle) {
                this.pageTitle = pageTitle;
            }

            public String getSearch() {
                return search;
            }

            public void setSearch(String search) {
                this.search = search;
            }

            public Multiple getMultiple() {
                return multiple;
            }

            public void setMultiple(Multiple multiple) {
                this.multiple = multiple;
            }

            public String getSort() {
                return sort;
            }

            public void setSort(String sort) {
                this.sort = sort;
            }

            public String getOrder() {
                return order;
            }

            public void setOrder(String order) {
                this.order = order;
            }

            public Integer getPage() {
                return page;
            }

            public void setPage(Integer page) {
                this.page = page;
            }

            public String getLimit() {
                return limit;
            }

            public void setLimit(String limit) {
                this.limit = limit;
            }

            public Integer getTotalPagination() {
                return totalPagination;
            }

            public void setTotalPagination(Integer totalPagination) {
                this.totalPagination = totalPagination;
            }

            public class Multiple {

                @SerializedName("search1")
                @Expose
                private String search1;
                @SerializedName("qty1")
                @Expose
                private String qty1;
                @SerializedName("search2")
                @Expose
                private String search2;
                @SerializedName("qty2")
                @Expose
                private String qty2;
                @SerializedName("search3")
                @Expose
                private String search3;
                @SerializedName("qty3")
                @Expose
                private String qty3;
                @SerializedName("search4")
                @Expose
                private String search4;
                @SerializedName("qty4")
                @Expose
                private String qty4;

                public String getSearch1() {
                    return search1;
                }

                public void setSearch1(String search1) {
                    this.search1 = search1;
                }

                public String getQty1() {
                    return qty1;
                }

                public void setQty1(String qty1) {
                    this.qty1 = qty1;
                }

                public String getSearch2() {
                    return search2;
                }

                public void setSearch2(String search2) {
                    this.search2 = search2;
                }

                public String getQty2() {
                    return qty2;
                }

                public void setQty2(String qty2) {
                    this.qty2 = qty2;
                }

                public String getSearch3() {
                    return search3;
                }

                public void setSearch3(String search3) {
                    this.search3 = search3;
                }

                public String getQty3() {
                    return qty3;
                }

                public void setQty3(String qty3) {
                    this.qty3 = qty3;
                }

                public String getSearch4() {
                    return search4;
                }

                public void setSearch4(String search4) {
                    this.search4 = search4;
                }

                public String getQty4() {
                    return qty4;
                }

                public void setQty4(String qty4) {
                    this.qty4 = qty4;
                }

            }

        }

        public class Product {

            @SerializedName("isAddedToCart")
            @Expose
            private boolean isAddedToCart;

            @SerializedName("product_id")
            @Expose
            private String productId;
            @SerializedName("thumb")
            @Expose
            private String thumb;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("model")
            @Expose
            private String model;
            @SerializedName("description")
            @Expose
            private String description;
            @SerializedName("price")
            @Expose
            private String price;
            @SerializedName("special")
            @Expose
            private Boolean special;
            @SerializedName("tax")
            @Expose
            private String tax;
            @SerializedName("minimum")
            @Expose
            private String minimum;
            @SerializedName("rating")
            @Expose
            private Integer rating;
            @SerializedName("href")
            @Expose
            private String href;

            public boolean getIsAddedToCart() {
                return isAddedToCart;
            }

            public void setAddedToCart(boolean isAddedToCart) {
                this.isAddedToCart = isAddedToCart;
            }

            public String getProductId() {
                return productId;
            }

            public void setProductId(String productId) {
                this.productId = productId;
            }

            public String getThumb() {
                return thumb;
            }

            public void setThumb(String thumb) {
                this.thumb = thumb;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getModel() {
                return model;
            }

            public void setModel(String model) {
                this.model = model;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public Boolean getSpecial() {
                return special;
            }

            public void setSpecial(Boolean special) {
                this.special = special;
            }

            public String getTax() {
                return tax;
            }

            public void setTax(String tax) {
                this.tax = tax;
            }

            public String getMinimum() {
                return minimum;
            }

            public void setMinimum(String minimum) {
                this.minimum = minimum;
            }

            public Integer getRating() {
                return rating;
            }

            public void setRating(Integer rating) {
                this.rating = rating;
            }

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }

        }
    }
}









