package com.panacea.mvp.dashboard.presenter;

import com.panacea.mvp.dashboard.model.CameraModel;
import com.panacea.mvp.dashboard.model.HomeProductsModel;
import com.panacea.mvp.dashboard.model.PartsModel;
import com.panacea.mvp.dashboard.model.RadiatorModel;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Braintech on 07-12-2017.
 */

public class DashboardContractor {

    public interface HomeProduct {

        void getHomeProducts();
    }

    public interface HomeProductView {
        void getHomeProductSuccess(ArrayList<HomeProductsModel.Datum> arrayListCountry);

        void getHomeProductUnSucess(String message);

        void homeProductInternetError();

        void homeProductServerError(String message);
    }

    public  interface LedRadiatorProduct{

        void getLedRadiatorProducts(int page,String filterSelectedName);
    }

    public interface LedRadiatorProductView {
        void getLedRadiatorProductSuccess(RadiatorModel.Data radiatorModels);

        void getLedRadiatorProductUnSucess(String message);

        void ledRadiatorProductInternetError();

        void ledRadiatorProductServerError(String message);
    }

    public  interface PartsProduct{

        void getPartsProducts(JSONObject jsonObject);
        void getSearchProducts(String search,int page);

    }

    public interface PartsProductView {
        void getPartsProductSuccess(PartsModel.Data partModel);

        void getPartsProductUnSucess(String message);

        void partsProductInternetError();

        void partsProductServerError(String message);
    }

    public  interface CameraProduct{

        void getCameraProducts();

    }

    public interface CameraProductView {
        void getCameraProductSuccess(CameraModel.Data partModel);

        void getCameraProductUnSucess(String message);

        void cameraProductInternetError();

        void cameraProductServerError(String message);
    }
}
