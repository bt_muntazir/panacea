package com.panacea.mvp.dashboard.presenter;

import android.app.Activity;
import android.graphics.Camera;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.dashboard.fragment.CameraFragment;
import com.panacea.mvp.dashboard.model.CameraModel;
import com.panacea.mvp.dashboard.model.RadiatorModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 20-12-2017.
 */

public class CameraProductImpl implements DashboardContractor.CameraProduct {

    DashboardContractor.CameraProductView cameraProductView;
    Activity activity;


    public CameraProductImpl (DashboardContractor.CameraProductView cameraProductView, Activity activity) {

        this.cameraProductView = cameraProductView;
        this.activity = activity;
    }

    @Override
    public void getCameraProducts() {
        try {
            ApiAdapter.getInstance(activity);
            cameraData();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            cameraProductView.cameraProductInternetError();
        }
    }

    private void cameraData(){

        Progress.start(activity);

        Call<CameraModel> getCameraOutput = ApiAdapter.getApiService().getCamera();

        getCameraOutput.enqueue(new Callback<CameraModel>() {
            @Override
            public void onResponse(Call<CameraModel> call, Response<CameraModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    CameraModel cameraModel = response.body();
                    String message = "Hello";

                    if (cameraModel.getStatus().equals("1")) {
                        cameraProductView.getCameraProductSuccess(cameraModel.getData());
                    } else {
                        cameraProductView.getCameraProductUnSucess("Login Fail");
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    cameraProductView.getCameraProductUnSucess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CameraModel> call, Throwable t) {
                Progress.stop();
                cameraProductView.getCameraProductUnSucess(activity.getString(R.string.error_server));
            }
        });
    }
}
