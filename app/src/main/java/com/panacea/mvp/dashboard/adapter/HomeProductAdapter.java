package com.panacea.mvp.dashboard.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.dashboard.fragment.CameraFragment;
import com.panacea.mvp.dashboard.fragment.DashboardFragment;
import com.panacea.mvp.dashboard.fragment.LedLightFragment;
import com.panacea.mvp.dashboard.fragment.PartsFragment;
import com.panacea.mvp.dashboard.fragment.RadiatorFragment;
import com.panacea.mvp.dashboard.model.HomeProductsModel;
import com.panacea.mvp.halo.activity.HaloActivity;
import com.panacea.mvp.productdetail.ProductDetailActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 07-12-2017.
 */

public class HomeProductAdapter extends RecyclerView.Adapter {

    Activity activity;
    ArrayList<HomeProductsModel.Datum> homeProductsModels;

    int dataPosition;

    public HomeProductAdapter(Activity activity, ArrayList<HomeProductsModel.Datum> homeProductsModels) {
        this.activity = activity;
        this.homeProductsModels = homeProductsModels;

        Log.e("size", "" + homeProductsModels.size());

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view;
      /*  if (dataPosition < homeProductsModels.size() - 1) {*/

        switch (viewType) {


            case 1:
                view = inflater.inflate(R.layout.item_home_products, null, false);
                return new SingleProductViewHolder(view);
            case 0:
                view = inflater.inflate(R.layout.item_home_two_products, null, false);
                return new DoubleProductViewHolder(view);
        }

        //}
        return null;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        int pos = position % 2;


        if (dataPosition < homeProductsModels.size() - 1) {
            switch (pos) {
                case 0:

                    if (position != 0)
                        dataPosition++;

                    final HomeProductsModel.Datum datum = homeProductsModels.get(dataPosition);
                    String imageUrl = datum.getThumb();
                    String productName = datum.getName();
                    Picasso.with(activity).load(imageUrl).into(((SingleProductViewHolder) holder).imgProduct);
                    ((SingleProductViewHolder) holder).txtViewProduct.setText(productName);

                    ((SingleProductViewHolder) holder).relLayProduct.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String Id = datum.getProductId();
                            if (Id.equals("116")) {
                                if (activity instanceof DashboardActivity) {
                                    CameraFragment cameraFragment = new CameraFragment();
                                    ((DashboardActivity) activity).replaceFragment(cameraFragment, (activity.getString(R.string.tag_camera)));
                                }
                            } else if (Id.equals("117")) {
                                if (activity instanceof DashboardActivity) {

                                    Intent intent = new Intent(activity, ProductDetailActivity.class);
                                    intent.putExtra(ConstIntent.KEY_PRODUCT_ID, Id);
                                    activity.startActivity(intent);
                                }
                            } else if (Id.equals("118")) {
                                if (activity instanceof DashboardActivity) {
                                    LedLightFragment ledLightFragment = new LedLightFragment();
                                    ((DashboardActivity) activity).replaceFragment(ledLightFragment, (activity.getString(R.string.tag_led_light)));
                                }
                            } else if (Id.equals("119")) {
                                if (activity instanceof DashboardActivity) {
                                    PartsFragment partsFragment = new PartsFragment();
                                    ((DashboardActivity) activity).replaceFragment(partsFragment, (activity.getString(R.string.tag_parts)));
                                }
                            } else if (Id.equals("120")) {
                                if (activity instanceof DashboardActivity) {
                                    RadiatorFragment radiatorFragment = new RadiatorFragment();
                                    ((DashboardActivity) activity).replaceFragment(radiatorFragment, (activity.getString(R.string.tag_radiators)));
                                }
                            }else if (Id.equals("121")) {
                                if (activity instanceof DashboardActivity) {

                                    Intent intent = new Intent(activity, HaloActivity.class);
                                    activity.startActivity(intent);
                                }
                            }
                        }
                    });

                    break;
                case 1:

                    if (position != 0)
                        dataPosition++;

                    final HomeProductsModel.Datum datumPos = homeProductsModels.get(dataPosition);
                    Picasso.with(activity).load(datumPos.getThumb()).into(((DoubleProductViewHolder) holder).imgProduct);
                    ((DoubleProductViewHolder) holder).txtViewProduct.setText(datumPos.getName());

                    dataPosition++;

                    final HomeProductsModel.Datum datumPos2 = homeProductsModels.get(dataPosition);
                    Picasso.with(activity).load(datumPos2.getThumb()).into(((DoubleProductViewHolder) holder).imgProduct2);
                    ((DoubleProductViewHolder) holder).txtViewProduct2.setText(datumPos2.getName());

                    ((DoubleProductViewHolder) holder).relLayFirst.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String Id = datumPos.getProductId();
                            if (Id.equals("116")) {
                                if (activity instanceof DashboardActivity) {
                                    CameraFragment cameraFragment = new CameraFragment();
                                    ((DashboardActivity) activity).replaceFragment(cameraFragment, (activity.getString(R.string.tag_camera)));
                                }
                            } else if (Id.equals("117")) {
                                if (activity instanceof DashboardActivity) {

                                    Intent intent = new Intent(activity, ProductDetailActivity.class);
                                    intent.putExtra(ConstIntent.KEY_PRODUCT_ID, Id);
                                    activity.startActivity(intent);
                                }
                            } else if (Id.equals("118")) {
                                if (activity instanceof DashboardActivity) {
                                    LedLightFragment ledLightFragment = new LedLightFragment();
                                    ((DashboardActivity) activity).replaceFragment(ledLightFragment, (activity.getString(R.string.tag_led_light)));
                                }
                            } else if (Id.equals("119")) {
                                if (activity instanceof DashboardActivity) {
                                    PartsFragment partsFragment = new PartsFragment();
                                    ((DashboardActivity) activity).replaceFragment(partsFragment, (activity.getString(R.string.tag_parts)));
                                }
                            } else if (Id.equals("120")) {
                                if (activity instanceof DashboardActivity) {
                                    RadiatorFragment radiatorFragment = new RadiatorFragment();
                                    ((DashboardActivity) activity).replaceFragment(radiatorFragment, (activity.getString(R.string.tag_radiators)));
                                }
                            } else if (Id.equals("121")) {
                                if (activity instanceof DashboardActivity) {

                                    Intent intent = new Intent(activity, HaloActivity.class);
                                    activity.startActivity(intent);
                                }
                            }

                        }
                    });

                    ((DoubleProductViewHolder) holder).relLaySecond.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            String Id = datumPos2.getProductId();
                            if (Id.equals("116")) {
                                if (activity instanceof DashboardActivity) {
                                    CameraFragment cameraFragment = new CameraFragment();
                                    ((DashboardActivity) activity).replaceFragment(cameraFragment, (activity.getString(R.string.tag_camera)));
                                }
                            } else if (Id.equals("117")) {
                                if (activity instanceof DashboardActivity) {

                                    Intent intent = new Intent(activity, ProductDetailActivity.class);
                                    intent.putExtra(ConstIntent.KEY_PRODUCT_ID, Id);
                                    activity.startActivity(intent);
                                }
                            } else if (Id.equals("118")) {
                                if (activity instanceof DashboardActivity) {
                                    LedLightFragment ledLightFragment = new LedLightFragment();
                                    ((DashboardActivity) activity).replaceFragment(ledLightFragment, (activity.getString(R.string.tag_led_light)));
                                }
                            } else if (Id.equals("119")) {
                                if (activity instanceof DashboardActivity) {
                                    PartsFragment partsFragment = new PartsFragment();
                                    ((DashboardActivity) activity).replaceFragment(partsFragment, (activity.getString(R.string.tag_parts)));
                                }
                            } else if (Id.equals("120")) {
                                if (activity instanceof DashboardActivity) {
                                    RadiatorFragment radiatorFragment = new RadiatorFragment();
                                    ((DashboardActivity) activity).replaceFragment(radiatorFragment, (activity.getString(R.string.tag_radiators)));
                                }
                            } else if (Id.equals("121")) {
                                if (activity instanceof DashboardActivity) {

                                    Intent intent = new Intent(activity, HaloActivity.class);
                                    activity.startActivity(intent);
                                }
                            }
                        }
                    });

                    break;
            }
        } else {
            Log.e("else", "else");
            switch (pos) {
                case 0:
                    ((SingleProductViewHolder) holder).relLayProduct.setVisibility(View.GONE);
                    break;
                case 1:
                    ((DoubleProductViewHolder) holder).linLayProd.setVisibility(View.GONE);
                    break;
            }
        }
    }

    @Override
    public int getItemViewType(int position) {

        if (position % 2 == 0) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public int getItemCount() {
        return homeProductsModels.size();
    }

    public class SingleProductViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayProduct)
        RelativeLayout relLayProduct;

        @BindView(R.id.imgProduct)
        ImageView imgProduct;

        @BindView(R.id.txtViewProduct)
        AppCompatTextView txtViewProduct;

        @BindView(R.id.btnLearnMore)
        Button btnLearnMore;

        public SingleProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            if (dataPosition < homeProductsModels.size() - 1) {
                //do nothing
            } else {
                itemView.setVisibility(View.GONE);
                itemView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
            }
        }
    }

    public class DoubleProductViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayFirst)
        RelativeLayout relLayFirst;

        @BindView(R.id.relLaySecond)
        RelativeLayout relLaySecond;


        @BindView(R.id.linLayProd)
        LinearLayout linLayProd;

        @BindView(R.id.imgProduct)
        ImageView imgProduct;

        @BindView(R.id.txtViewProduct)
        AppCompatTextView txtViewProduct;

        @BindView(R.id.imgProduct2)
        ImageView imgProduct2;

        @BindView(R.id.txtViewProduct2)
        AppCompatTextView txtViewProduct2;

        @BindView(R.id.btnLearnMore)
        Button btnLearnMore;


        @BindView(R.id.btnLearnMore2)
        Button btnLearnMore2;

        public DoubleProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            if (dataPosition < homeProductsModels.size() - 1) {
                //do nothing
            } else {
                itemView.setVisibility(View.GONE);
                itemView.setLayoutParams(new RecyclerView.LayoutParams(0, 0));
            }
        }
    }
}
