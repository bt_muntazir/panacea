package com.panacea.mvp.dashboard.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.dashboard.model.RadiatorModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 11-12-2017.
 */

public class LedProductImpl implements DashboardContractor.LedRadiatorProduct {

    DashboardContractor.LedRadiatorProductView ledRadiatorProductView;
    Activity activity;
    JSONObject jsonObject;


    public LedProductImpl(DashboardContractor.LedRadiatorProductView ledRadiatorProductView, Activity activity) {

        this.ledRadiatorProductView = ledRadiatorProductView;
        this.activity = activity;
    }

    @Override
    public void getLedRadiatorProducts(int page,String filterSelectedName) {
        try {
            ApiAdapter.getInstance(activity);
            ledData(page,filterSelectedName);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            ledRadiatorProductView.ledRadiatorProductInternetError();
        }
    }

    private void ledData(int page,String filterSelectedName){
        Progress.start(activity);

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_PAGE, page);
            jsonObject.put(Const.PARAM_FILTER_NAME, filterSelectedName);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<RadiatorModel> getRadiatorList = ApiAdapter.getApiService().getLedLightning("application/json", "no-cache", body);


        getRadiatorList.enqueue(new Callback<RadiatorModel>() {
            @Override
            public void onResponse(Call<RadiatorModel> call, Response<RadiatorModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    RadiatorModel radiatorModel = response.body();
                    String message = "Hello";

                    if (radiatorModel.getStatus().equals("1")) {
                        ledRadiatorProductView.getLedRadiatorProductSuccess(radiatorModel.getData());
                    } else {
                        ledRadiatorProductView.getLedRadiatorProductUnSucess("Login Fail");
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    ledRadiatorProductView.getLedRadiatorProductUnSucess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<RadiatorModel> call, Throwable t) {
                Progress.stop();
                ledRadiatorProductView.getLedRadiatorProductUnSucess(activity.getString(R.string.error_server));
            }
        });
    }
}
