package com.panacea.mvp.dashboard.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.database.CartItem;
import com.panacea.common.database.CartOperations;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.viewpagerindicator.CirclePageIndicator;
import com.panacea.common.interfaces.CartUpdate;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.dashboard.fragment.CameraFragment;
import com.panacea.mvp.dashboard.fragment.LedLightFragment;
import com.panacea.mvp.dashboard.fragment.PartsFragment;
import com.panacea.mvp.dashboard.fragment.RadiatorFragment;
import com.panacea.mvp.dashboard.model.PartsModel;
import com.panacea.mvp.dashboard.model.RadiatorModel;
import com.panacea.mvp.login.LoginActivity;
import com.panacea.mvp.productdetail.ProductDetailActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 12-12-2017.
 */

public class SearchPartsAdapter extends RecyclerView.Adapter {


    Activity activity;
    ArrayList<PartsModel.Data.Product> raDataArrayListResult;

    ArrayList<String> arrayListSlideImage;

    public SearchPartsAdapter(Activity activity, ArrayList<PartsModel.Data.Product> raDataArrayList) {
        this.activity = activity;
        this.raDataArrayListResult = raDataArrayList;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_search_products, parent, false);
        return new SearchPartsAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder,final int position) {

        final PartsModel.Data.Product result = raDataArrayListResult.get(position);

        // get thumb image
        String thumbImage = raDataArrayListResult.get(position).getThumb();

        String price = result.getPrice();

        if (price.equals("$0.00")) {

            Picasso.with(activity).load(thumbImage).into(((SearchPartsAdapter.ViewHolder) holder).imgProduct);
            ((SearchPartsAdapter.ViewHolder) holder).txtViewProduct.setText(result.getName().toString());
            ((SearchPartsAdapter.ViewHolder) holder).txtViewProductId.setText(result.getModel().toString());
            ((ViewHolder) holder).getTxtViewPrice.setVisibility(View.GONE);
            ((ViewHolder) holder).btnAddCart.setVisibility(View.GONE);
        } else {
            Picasso.with(activity).load(thumbImage).into(((SearchPartsAdapter.ViewHolder) holder).imgProduct);
            ((SearchPartsAdapter.ViewHolder) holder).txtViewProduct.setText(result.getName().toString());
            ((SearchPartsAdapter.ViewHolder) holder).txtViewProductId.setText(result.getModel().toString());
            ((SearchPartsAdapter.ViewHolder) holder).getTxtViewPrice.setText(price.toString());
            ((ViewHolder) holder).btnAddCart.setVisibility(View.VISIBLE);
        }

        ((SearchPartsAdapter.ViewHolder) holder).imgProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Context context = parent.getContext();
                Intent intent = new Intent(activity, ProductDetailActivity.class);
                intent.putExtra(ConstIntent.KEY_PRODUCT_ID, result.getProductId());
                activity.startActivity(intent);
            }
        });

        if (result.getIsAddedToCart()) {
            ((SearchPartsAdapter.ViewHolder) holder).btnAddCart.setAlpha(0.5f);
            ((SearchPartsAdapter.ViewHolder) holder).btnAddCart.setEnabled(false);
        } else {
            ((SearchPartsAdapter.ViewHolder) holder).btnAddCart.setAlpha(1);
            ((SearchPartsAdapter.ViewHolder) holder).btnAddCart.setEnabled(true);
        }

        //click event add to cart
        ((SearchPartsAdapter.ViewHolder) holder).btnAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CartUpdate cartUpdate = new CartUpdate() {
                    @Override
                    public void onCartUpdate(String type, int qty) {

                        DBHelper dbHelper = new DBHelper(activity);

                        if (activity instanceof DashboardActivity) {
                            ((DashboardActivity) activity).updateCartQty();
                            result.setAddedToCart(true);

                            notifyDataSetChanged();
                        }
                    }
                };
                CartItem cartItem = getCartItems(raDataArrayListResult.get(position));
                cartItem.setProductprice(result.getPrice());

                CartOperations.updateDataBase(activity, cartItem, true, CartOperations.TYPE_ADD_ITEM, 10, cartUpdate);

                ShowDoalogForSuccess(raDataArrayListResult.get(position).getName());
            }
        });
    }

    private void ShowDoalogForSuccess(String productName) {

        // Create custom dialog object
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.item_success_add_cart);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.alpha(Color.BLACK)));


// set values for custom dialog components - text, image and button
        TextView txtViewDescription = (TextView) dialog.findViewById(R.id.txtViewDescription);
        txtViewDescription.setText("You have added "+ productName +" to your shopping cart.");
        ImageView imgViewCross = (ImageView) dialog.findViewById(R.id.imgViewCross);

        dialog.show();

        Button btnGotoCart = (Button) dialog.findViewById(R.id.btnGotoCart);
        Button btnContinueShopping = (Button) dialog.findViewById(R.id.btnContinueShopping);
// if decline button is clicked, close the custom dialog

        imgViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnContinueShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnGotoCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activity instanceof DashboardActivity) {
                    ((DashboardActivity) activity).goToCartFromDialog();
                }
                dialog.dismiss();
            }
        });


    }


    public CartItem getCartItems(PartsModel.Data.Product products) {


        CartItem cartItems = new CartItem();

        // cartItems.setId(products.getId());
        cartItems.setProductId(products.getProductId());
        cartItems.setProductName(products.getName());
        cartItems.setProduct_quantity(1);

        return cartItems;
    }


    @Override
    public int getItemCount() {
        return raDataArrayListResult.size();
    }

    public void updateData(ArrayList<PartsModel.Data.Product> raDataArrayList, boolean isSearchClick) {

        if (isSearchClick)
            raDataArrayListResult.clear();

        if (raDataArrayListResult != null && raDataArrayList != null) {
            for (int i = 0; i < raDataArrayList.size(); i++) {
                raDataArrayListResult.add(raDataArrayList.get(i));
            }
            notifyDataSetChanged();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayProduct)
        RelativeLayout relLayProduct;

        @BindView(R.id.imgProduct)
        ImageView imgProduct;

        @BindView(R.id.txtViewProduct)
        AppCompatTextView txtViewProduct;

        @BindView(R.id.txtViewProductId)
        AppCompatTextView txtViewProductId;

        @BindView(R.id.txtViewPrice)
        AppCompatTextView getTxtViewPrice;

        @BindView(R.id.btnAddCart)
        Button btnAddCart;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
