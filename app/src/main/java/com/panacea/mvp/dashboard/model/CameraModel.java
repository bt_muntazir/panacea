package com.panacea.mvp.dashboard.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Braintech on 20-12-2017.
 */

public class CameraModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("heading_title")
        @Expose
        private String headingTitle;
        @SerializedName("youtube_video1")
        @Expose
        private String youtubeVideo1;
        @SerializedName("youtube_video2")
        @Expose
        private String youtubeVideo2;
        @SerializedName("image1")
        @Expose
        private String image1;
        @SerializedName("image2")
        @Expose
        private String image2;

        public String getHeadingTitle() {
            return headingTitle;
        }

        public void setHeadingTitle(String headingTitle) {
            this.headingTitle = headingTitle;
        }

        public String getYoutubeVideo1() {
            return youtubeVideo1;
        }

        public void setYoutubeVideo1(String youtubeVideo1) {
            this.youtubeVideo1 = youtubeVideo1;
        }

        public String getYoutubeVideo2() {
            return youtubeVideo2;
        }

        public void setYoutubeVideo2(String youtubeVideo2) {
            this.youtubeVideo2 = youtubeVideo2;
        }

        public String getImage1() {
            return image1;
        }

        public void setImage1(String image1) {
            this.image1 = image1;
        }

        public String getImage2() {
            return image2;
        }

        public void setImage2(String image2) {
            this.image2 = image2;
        }

    }
}