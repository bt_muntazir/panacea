package com.panacea.mvp.dashboard;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.internal.NavigationMenuView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.internal.v;
import com.panacea.R;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.AlertDialogHelper;
import com.panacea.common.helpers.FontHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.common.utility.SnackNotify;
import com.panacea.common.utility.Utils;
import com.panacea.mvp.address.AddressActivity;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.changepassword.ChangePasswordActivity;
import com.panacea.mvp.checkout.confirm_order.ConfirmOrderActivity;
import com.panacea.mvp.contactus.ContactUsActivity;
import com.panacea.mvp.dashboard.fragment.CameraFragment;
import com.panacea.mvp.dashboard.fragment.DashboardFragment;
import com.panacea.mvp.dashboard.fragment.LedLightFragment;
import com.panacea.mvp.dashboard.fragment.PartsFragment;
import com.panacea.mvp.dashboard.fragment.RadiatorFragment;
import com.panacea.mvp.dashboard.fragment.SearchFragment;
import com.panacea.mvp.edit_information.EditInformationActivity;
import com.panacea.mvp.login.LoginActivity;
import com.panacea.mvp.order_history.OrderHistoryActivity;
import com.panacea.mvp.order_information.OrderInformationActivity;
import com.panacea.mvp.productdetail.ProductDetailActivity;
import com.panacea.mvp.splash.SplashActivity;
import com.panacea.mvp.splash.model.SplashModel;
import com.panacea.mvp.success_order.SuccessOrderActivity;
import com.panacea.mvp.success_order.model.OrderSuccessModel;
import com.panacea.mvp.youtube_instagram.YoutubeInstagramActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DashboardActivity extends AppCompatActivity {

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.frameLayout)
    FrameLayout frameLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer_layout;

    @BindView(R.id.txtViewDashboard)
    TextView txtViewDashboard;

    @BindView(R.id.txtViewProducts)
    TextView txtViewProducts;

    @BindView(R.id.txtViewCamera)
    TextView txtViewCamera;

    @BindView(R.id.txtViewLedLighting)
    TextView txtViewLedLighting;

    @BindView(R.id.txtViewParts)
    TextView txtViewParts;

    @BindView(R.id.txtViewRadiator)
    TextView txtViewRadiator;


    @BindView(R.id.txtViewCart)
    TextView txtViewCart;

    @BindView(R.id.txtViewMyAccount)
    TextView txtViewMyAccount;


    @BindView(R.id.txtViewAccountInfo)
    TextView txtViewAccountInfo;

    @BindView(R.id.txtViewOrderHistory)
    TextView txtViewOrderHistory;

    @BindView(R.id.txtViewAddress)
    TextView txtViewAddress;

    @BindView(R.id.txtViewContact)
    TextView txtViewContact;


    @BindView(R.id.txtViewChangePassword)
    TextView txtViewChangePassword;

    @BindView(R.id.txtViewLogin)
    TextView txtViewLogin;

    @BindView(R.id.txtViewDashboardTitle)
    AppCompatTextView txtViewDashboardTitle;

    @BindView(R.id.imgViewlogo)
    ImageView imgViewlogo;

    @BindView(R.id.imgViewInstagram)
    ImageView imgViewInstagram;

    @BindView(R.id.imgViewYoutube)
    ImageView imgViewYoutube;

    @BindView(R.id.txtViewSearch)
    TextView txtViewSearch;

    @BindView(R.id.edtTextSearch)
    AppCompatEditText edtTextSearch;

    TextView tabCamera;
    TextView tabLedLighting;
    TextView tabParts;
    TextView tabRadiators;


    DBHelper dbHelper;

    ArrayList<String> itemList;

    String youtubeUrl = LoginManager.getInstance().getConstantData().getYoutubeUrl();
    String instagramUrl = LoginManager.getInstance().getConstantData().getInstagramUrl();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        ButterKnife.bind(this);

        dbHelper = new DBHelper(this);

        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer_layout.addDrawerListener(toggle);
        toggle.syncState();
        toggle.setDrawerIndicatorEnabled(false);
        setFont();
        setTabLayout();
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateCartQty();
    }



    private void checkIsUserLogin() {
        if (LoginManager.getInstance().isLoggedIn()) {

            txtViewLogin.setText(getString(R.string.title_logout));
            txtViewMyAccount.setVisibility(View.VISIBLE);
            txtViewAccountInfo.setVisibility(View.VISIBLE);
            txtViewOrderHistory.setVisibility(View.VISIBLE);
            txtViewChangePassword.setVisibility(View.VISIBLE);
            txtViewAddress.setVisibility(View.VISIBLE);

        } else {
            txtViewLogin.setText(getString(R.string.title_login));
            txtViewMyAccount.setVisibility(View.GONE);
            txtViewAccountInfo.setVisibility(View.GONE);
            txtViewChangePassword.setVisibility(View.GONE);
            txtViewOrderHistory.setVisibility(View.GONE);
            txtViewAddress.setVisibility(View.GONE);
        }
    }

    private void setFont() {

        FontHelper.setFontFace(txtViewDashboard, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewProducts, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewCamera, FontHelper.FontType.FONT_REGULAR, this);

        FontHelper.setFontFace(txtViewLedLighting, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewParts, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewRadiator, FontHelper.FontType.FONT_REGULAR, this);

        // FontHelper.setFontFace(txtViewSmartStart, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewCart, FontHelper.FontType.FONT_REGULAR, this);

        FontHelper.setFontFace(txtViewMyAccount, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewAccountInfo, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewChangePassword, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewOrderHistory, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewAddress, FontHelper.FontType.FONT_REGULAR, this);

        FontHelper.setFontFace(txtViewContact, FontHelper.FontType.FONT_REGULAR, this);
        FontHelper.setFontFace(txtViewLogin, FontHelper.FontType.FONT_REGULAR, this);
    }

    private void setTabLayout() {
        setTab();

        createTabIcons();
        //createTabIcons();
    }

    private void createTabIcons() {
        tabCamera = (TextView) LayoutInflater.from(this).inflate(R.layout.bottom_menu_layout, null);
        tabCamera.setText(getString(R.string.title_cameras));
        tabCamera.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_inactive_camera, 0, 0);
        tabLayout.getTabAt(0).setCustomView(tabCamera);

        tabLedLighting = (TextView) LayoutInflater.from(this).inflate(R.layout.bottom_menu_layout, null);
        tabLedLighting.setText(getString(R.string.title_led_lighting));
        tabLedLighting.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_inactive_selector_led, 0, 0);
        tabLayout.getTabAt(1).setCustomView(tabLedLighting);

        tabParts = (TextView) LayoutInflater.from(this).inflate(R.layout.bottom_menu_layout, null);
        tabParts.setText(getString(R.string.title_parts));
        tabParts.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_inactive_parts, 0, 0);
        tabLayout.getTabAt(2).setCustomView(tabParts);

        tabRadiators = (TextView) LayoutInflater.from(this).inflate(R.layout.bottom_menu_layout, null);
        tabRadiators.setText(getString(R.string.title_radiators));
        tabRadiators.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_radiator_inactive, 0, 0);
        tabLayout.getTabAt(3).setCustomView(tabRadiators);
    }

    public void createTabUnSelctedIcons() {

        tabCamera.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_inactive_camera, 0, 0);
        tabLedLighting.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_inactive_selector_led, 0, 0);
        tabParts.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_inactive_parts, 0, 0);
        tabRadiators.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_radiator_inactive, 0, 0);

        tabCamera.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
        tabLedLighting.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
        tabParts.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
        tabRadiators.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
    }


    private void setTab() {

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.title_cameras)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.title_led_lighting)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.title_parts)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.title_radiators)));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        //Initialize first fragment
        DashboardFragment dshboardFragment = new DashboardFragment();
        addFragment(dshboardFragment, getString(R.string.tag_dashboard));

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if (tab.getPosition() == 0) {

                    Log.e("Tab ", "0");
                    //blank the searchview
                    edtTextSearch.setText("");
                    CameraFragment cameraFragment = new CameraFragment();
                    replaceFragment(cameraFragment, getString(R.string.tag_camera));

                    tabCamera.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_camera, 0, 0);
                    tabCamera.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_white));
                    tabLedLighting.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                    tabParts.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                    tabRadiators.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                } else if (tab.getPosition() == 1) {
                    //blank the searchview
                    edtTextSearch.setText("");
                    LedLightFragment ledLightFragment = new LedLightFragment();
                    replaceFragment(ledLightFragment, getString(R.string.tag_led_light));

                    tabLedLighting.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_led, 0, 0);

                    tabCamera.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                    tabLedLighting.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_white));
                    tabParts.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                    tabRadiators.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));

                } else if (tab.getPosition() == 2) {

                    //blank the searchview
                    edtTextSearch.setText("");

                    PartsFragment partsFragment = new PartsFragment();
                    replaceFragment(partsFragment, getString(R.string.tag_parts));

                    tabParts.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_parts, 0, 0);

                    tabCamera.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                    tabLedLighting.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                    tabParts.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_white));
                    tabRadiators.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                } else if (tab.getPosition() == 3) {
                    //blank the searchview
                    edtTextSearch.setText("");

                    RadiatorFragment radiatorFragment = new RadiatorFragment();
                    replaceFragment(radiatorFragment, getString(R.string.tag_radiators));

                    tabRadiators.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_radiator, 0, 0);

                    tabCamera.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                    tabLedLighting.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                    tabParts.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                    tabRadiators.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_white));
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {

                    //blank the searchview
                    edtTextSearch.setText("");

                    CameraFragment cameraFragment = new CameraFragment();
                    replaceFragment(cameraFragment, getString(R.string.tag_camera));

                    tabCamera.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_camera, 0, 0);
                    tabCamera.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_white));
                    tabLedLighting.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                    tabParts.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                    tabRadiators.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));


                } else if (tab.getPosition() == 1) {

                    //blank the searchview
                    edtTextSearch.setText("");

                    LedLightFragment ledLightFragment = new LedLightFragment();
                 /*   Bundle bundle = new Bundle();
                    bundle.putSerializable(ConstIntent.KEY_ARRAYLIST, arrayListRefund);
                    ordersFragment.setArguments(bundle);*/
                    replaceFragment(ledLightFragment, getString(R.string.tag_led_light));

                    tabLedLighting.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_led, 0, 0);

                    tabCamera.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                    tabLedLighting.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_white));
                    tabParts.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                    tabRadiators.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));

                } else if (tab.getPosition() == 2) {

                    //blank the searchview
                    edtTextSearch.setText("");
                    PartsFragment partsFragment = new PartsFragment();
                    replaceFragment(partsFragment, getString(R.string.tag_parts));

                    tabParts.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_parts, 0, 0);

                    tabCamera.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                    tabLedLighting.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                    tabParts.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_white));
                    tabRadiators.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                } else if (tab.getPosition() == 3) {

                    //blank the searchview
                    edtTextSearch.setText("");
                    RadiatorFragment radiatorFragment = new RadiatorFragment();
                    replaceFragment(radiatorFragment, getString(R.string.tag_radiators));

                    tabRadiators.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_radiator, 0, 0);

                    tabCamera.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                    tabLedLighting.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                    tabParts.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
                    tabRadiators.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_white));
                }
            }
        });
    }

    private void addFragment(Fragment fragment, String tag) {
        Utils.hideKeyboardIfOpen(this);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.frameLayout, fragment, tag);
        transaction.commitAllowingStateLoss();


    }

    public void replaceFragment(Fragment fragment, String tag) {
        Utils.hideKeyboardIfOpen(this);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frameLayout, fragment, tag);

        transaction.commitAllowingStateLoss();
    }

    private void drawerOpen() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        }
    }

    public void setDashboardTitle(String title) {

        imgViewlogo.setVisibility(View.INVISIBLE);
        txtViewDashboardTitle.setVisibility(View.VISIBLE);
        txtViewDashboardTitle.setText(title);
    }

    public void setDashboardImageLogo() {

        imgViewlogo.setVisibility(View.VISIBLE);
        txtViewDashboardTitle.setVisibility(View.INVISIBLE);
    }

    public void openProductDetailActicity() {

        Intent intent = new Intent(DashboardActivity.this, ProductDetailActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (dbHelper.getCartCount() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            }
        }
    }

    public void goToCartFromDialog() {

        startActivity(new Intent(this, ShoppingCartActivity.class));
    }


    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }

    @OnClick(R.id.imgViewMenu)
    void openMenu() {
        Utils.hideKeyboardIfOpen(this);

        //Check whether is login or not
        checkIsUserLogin();

        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        } else {
            drawer_layout.openDrawer(GravityCompat.START);
        }
    }

    @OnClick(R.id.txtViewDashboard)
    public void txtViewDashboardClick() {
        drawerOpen();

        //blank the searchview
        edtTextSearch.setText("");

        DashboardFragment dashboardFragment = new DashboardFragment();
        replaceFragment(dashboardFragment, getString(R.string.tag_dashboard));
        createTabUnSelctedIcons();
    }

    @OnClick(R.id.txtViewCamera)
    public void txtViewCameraClick() {
        drawerOpen();

        //blank the searchview
        edtTextSearch.setText("");

        CameraFragment cameraFragment = new CameraFragment();
        replaceFragment(cameraFragment, getString(R.string.tag_camera));

        tabCamera.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_camera, 0, 0);

        tabCamera.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_white));
        tabLedLighting.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
        tabParts.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
        tabRadiators.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
    }

    @OnClick(R.id.txtViewLedLighting)
    public void txtViewLedLightingClick() {
        drawerOpen();

        //blank the searchview
        edtTextSearch.setText("");

        LedLightFragment ledLightFragment = new LedLightFragment();
        replaceFragment(ledLightFragment, getString(R.string.tag_led_light));

        tabLedLighting.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_led, 0, 0);

        tabCamera.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
        tabLedLighting.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_white));
        tabParts.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
        tabRadiators.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
    }

    @OnClick(R.id.txtViewParts)
    public void txtViewPartsClick() {
        drawerOpen();

        //blank the searchview
        edtTextSearch.setText("");

        PartsFragment partsFragment = new PartsFragment();
        replaceFragment(partsFragment, getString(R.string.tag_parts));

        tabParts.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_parts, 0, 0);

        tabCamera.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
        tabLedLighting.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
        tabParts.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_white));
        tabRadiators.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
    }

    @OnClick(R.id.txtViewRadiator)
    public void txtViewRadiatorClick() {
        drawerOpen();

        //blank the searchview
        edtTextSearch.setText("");

        RadiatorFragment radiatorFragment = new RadiatorFragment();
        replaceFragment(radiatorFragment, getString(R.string.tag_radiators));

        tabRadiators.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.drawable_selector_radiator, 0, 0);
        tabCamera.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
        tabLedLighting.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
        tabParts.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_gray));
        tabRadiators.setTextColor(ContextCompat.getColor(DashboardActivity.this, R.color.color_white));
    }

    @OnClick(R.id.txtViewCart)
    public void txtViewCartClick() {
        drawerOpen();

        if (dbHelper.getCartCount() > 0) {
            startActivity(new Intent(this, ShoppingCartActivity.class));
        }
    }

    @OnClick(R.id.txtViewContact)
    public void onContactsClick() {

        drawerOpen();
        startActivity(new Intent(this, ContactUsActivity.class));
    }

    @OnClick(R.id.txtViewAccountInfo)
    public void txtViewAccountInfoClick() {
        drawerOpen();
        Intent intent = new Intent(DashboardActivity.this, EditInformationActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.txtViewOrderHistory)
    public void txtViewOrderHistoryClick() {
        drawerOpen();

        Intent intent = new Intent(DashboardActivity.this, OrderHistoryActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.txtViewAddress)
    public void txtViewAddressClick() {
        drawerOpen();

        Intent intent = new Intent(DashboardActivity.this, AddressActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.txtViewChangePassword)
    public void txtViewChangePasswordnClick() {
        drawerOpen();

        Intent intent = new Intent(DashboardActivity.this, ChangePasswordActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.txtViewLogin)
    public void txtViewLoginClick() {
        drawerOpen();

        if (LoginManager.getInstance().isLoggedIn()) {

            AlertDialogHelper.showMessageToLogout(this, dbHelper);
        } else {
            Intent intent = new Intent(DashboardActivity.this, LoginActivity.class);
            startActivity(intent);
        }
    }

    @OnClick(R.id.txtViewSearch)
    public void txtViewSearchClick() {

        // hide keyboard
        Utils.hideKeyboardIfOpen(this);

        String searchText = String.valueOf(edtTextSearch.getText());

        if (!Utils.isEmptyOrNull(searchText)) {

            SearchFragment searchFragment = new SearchFragment();
            Bundle bundle = new Bundle();
            bundle.putString(ConstIntent.KEY_SEARCH, searchText);
            searchFragment.setArguments(bundle);
            replaceFragment(searchFragment, getString(R.string.tag_search));

            createTabUnSelctedIcons();

        } else {
            SnackNotify.showMessage(getString(R.string.empty_search), coordinateLayout);
        }
    }


    @OnClick(R.id.imgViewInstagram)
    public void imgViewInstagramClick() {
        Utils.hideKeyboardIfOpen(this);
        drawerOpen();

        Intent intent = new Intent(DashboardActivity.this, YoutubeInstagramActivity.class);
        intent.putExtra(ConstIntent.KEY_INSTAGRAM_YOUTUBE_URL, instagramUrl);
        intent.putExtra(ConstIntent.KEY_INSTAGRAM_YOUTUBE_CONST, "INSTAGRAM");
        startActivity(intent);
    }

    @OnClick(R.id.imgViewYoutube)
    public void imgViewYouTubeClick() {
        Utils.hideKeyboardIfOpen(this);
        drawerOpen();

      /*  Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(youtubeUrl));
        intent.setDataAndType(Uri.parse(youtubeUrl), "video/mp4");
        startActivity(intent);*/

        Intent intent = new Intent(DashboardActivity.this, YoutubeInstagramActivity.class);
        intent.putExtra(ConstIntent.KEY_INSTAGRAM_YOUTUBE_URL, youtubeUrl);
        intent.putExtra(ConstIntent.KEY_INSTAGRAM_YOUTUBE_CONST, "YOUTUBE");
        startActivity(intent);
    }

    @OnClick(R.id.toolbar)
    void toolbar() {

        Utils.hideKeyboardIfOpen(this);
    }

}
