package com.panacea.mvp.dashboard.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubePlayerView;
import com.panacea.Config;
import com.panacea.R;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.utility.SnackNotify;
import com.panacea.common.utility.Utils;
import com.panacea.mvp.YouTubeActivity;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.dashboard.model.CameraModel;
import com.panacea.mvp.dashboard.presenter.CameraProductImpl;
import com.panacea.mvp.dashboard.presenter.DashboardContractor;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.internal.Util;


public class CameraFragment extends Fragment implements DashboardContractor.CameraProductView {

   /* FragmentTransaction transaction1;
    FragmentTransaction transaction2;
    YouTubePlayerSupportFragment youTubePlayerFragment1;
    YouTubePlayerSupportFragment youTubePlayerFragment2;
    YouTubePlayer youTubePlayer1;
   YouTubePlayer youTubePlayer2;
    List<String> arrayList;*/

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.rel)
    RelativeLayout rel;

    @BindView(R.id.imgView1)
    ImageView imgView1;

    @BindView(R.id.imgView2)
    ImageView imgView2;

    CameraProductImpl cameraProduct;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_camera, container, false);
        //YouTubePlayerSupportFragment youTubePlayerFragment = (YouTubePlayerSupportFragment) getActivity().getSupportFragmentManager().findFragmentById(R.id.youtube_fragment1);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        cameraProduct = new CameraProductImpl(this, getActivity());

        getCameraDataApi();

    }

    @Override
    public void getCameraProductSuccess(CameraModel.Data partModel) {

        //set the page title
        ((DashboardActivity) getActivity()).setDashboardTitle(partModel.getHeadingTitle());

        //set the images
        imgView1.setVisibility(View.VISIBLE);
        imgView2.setVisibility(View.VISIBLE);
        Picasso.with(getActivity()).load(partModel.getImage1()).into(imgView1);
        Picasso.with(getActivity()).load(partModel.getImage2()).into(imgView2);

        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        String video1 = partModel.getYoutubeVideo1();
        String video2 = partModel.getYoutubeVideo2();
        String videos[] = {video1, video2};
        YoutubeAdapter adapter = new YoutubeAdapter(getActivity(), this, videos);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void getCameraProductUnSucess(String message) {
        SnackNotify.showMessage(message, rel);

        //Hide the images
        imgView1.setVisibility(View.GONE);
        imgView2.setVisibility(View.GONE);

    }

    @Override
    public void cameraProductInternetError() {

        SnackNotify.checkConnection(onretryLedLightDataApi, rel);
    }

    OnClickInterface onretryLedLightDataApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getCameraDataApi();
        }
    };

    @Override
    public void cameraProductServerError(String message) {
        SnackNotify.showMessage(message, rel);
    }

    private void getCameraDataApi() {
        cameraProduct.getCameraProducts();
    }

    @OnClick(R.id.imgView1)
    public void imgView1() {
        Utils.hideKeyboardIfOpen(getActivity());
    }

    @OnClick(R.id.imgView2)
    public void imgView2() {
        Utils.hideKeyboardIfOpen(getActivity());
    }
}

/*youTubePlayerFragment1 = YouTubePlayerSupportFragment.newInstance();
        transaction1 = getChildFragmentManager().beginTransaction();
        transaction1.add(R.id.youtube_fragment1, youTubePlayerFragment1).commit();

       youTubePlayerFragment2 = YouTubePlayerSupportFragment.newInstance();
        transaction2 = getChildFragmentManager().beginTransaction();
        transaction2.add(R.id.youtube_fragment2, youTubePlayerFragment2);

        youTubePlayerFragment1.initialize(Config.API_KEY, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider arg0, YouTubePlayer youTubePlayer, boolean b) {
                if (!b) {
                    youTubePlayer1 = youTubePlayer;
                    arrayList=new ArrayList<>();
                    arrayList.add(Config.VIDEO_CODE1);
                    arrayList.add(Config.VIDEO_CODE2);
                    youTubePlayer1.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);

                    youTubePlayer1.cueVideos(arrayList,0,0);

                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider arg0, YouTubeInitializationResult arg1) {
                // TODO Auto-generated method stub

            }
        });

    *//*    youTubePlayerFragment2.initialize(Config.API_KEY, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider arg0, YouTubePlayer youTubePlayer, boolean b) {
                if (!b) {
                    youTubePlayer2 = youTubePlayer;
                    youTubePlayer2.cueVideo(Config.VIDEO_CODE2);
                    youTubePlayer2.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider arg0, YouTubeInitializationResult arg1) {
                // TODO Auto-generated method stub

            }
        });*/
