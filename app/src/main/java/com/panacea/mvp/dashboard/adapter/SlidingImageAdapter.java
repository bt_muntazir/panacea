package com.panacea.mvp.dashboard.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.panacea.R;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.common.utility.AlertDialogManager;
import com.panacea.mvp.dashboard.model.RadiatorModel;
import com.panacea.mvp.productdetail.ProductDetailActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by SONU on 29/08/15.
 */
public class SlidingImageAdapter extends PagerAdapter {


    private ArrayList<String> slidingImageList;
    private LayoutInflater inflater;
    private Context context;
    boolean isComeFromDetail;

    String productId;


    public SlidingImageAdapter(Context context, ArrayList<String> slidingImageList, String productId, boolean isComeFromDetail) {
        this.context = context;
        this.slidingImageList = slidingImageList;
        this.productId = productId;
        this.isComeFromDetail = isComeFromDetail;

        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return slidingImageList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {
        View imageLayout = inflater.inflate(R.layout.item_sliding_images, view, false);

        assert imageLayout != null;
        final FrameLayout frameLayout = (FrameLayout) imageLayout.findViewById(R.id.frameLayout);
        final ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);

        // load image
        Picasso.with(context).load(slidingImageList.get(position)).into(imageView);

        view.addView(imageLayout, 0);

        if (!isComeFromDetail) {

            frameLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    // Context context = parent.getContext();
                    Intent intent = new Intent(context, ProductDetailActivity.class);
                    intent.putExtra(ConstIntent.KEY_PRODUCT_ID, productId);
                    context.startActivity(intent);
                }
            });
        } else {
            frameLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Log.e("Clecked","ddfgd");

                    String imageUrl=slidingImageList.get(position);
                    //AlertDialogManager.alertShowImage(context, slidingImageList.get(position).into(imageView));
                    AlertDialogManager.alertShowImage(context, slidingImageList.get(position));

                }
            });
        }

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }


}
