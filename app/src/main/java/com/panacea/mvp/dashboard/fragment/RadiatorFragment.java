package com.panacea.mvp.dashboard.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.panacea.R;
import com.panacea.common.database.CartItem;
import com.panacea.common.database.DBHelper;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.utility.SnackNotify;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.dashboard.adapter.HomeProductAdapter;
import com.panacea.mvp.dashboard.adapter.LedRadiatorAdapter;
import com.panacea.mvp.dashboard.model.PartsModel;
import com.panacea.mvp.dashboard.model.RadiatorModel;
import com.panacea.mvp.dashboard.presenter.DashboardContractor;
import com.panacea.mvp.dashboard.presenter.RadiatorProductImpl;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.nfc.tech.MifareUltralight.PAGE_SIZE;


public class RadiatorFragment extends Fragment implements DashboardContractor.LedRadiatorProductView {

    @BindView(R.id.mainContainer)
    FrameLayout mainContainer;

    @BindView(R.id.recyclerViewProducts)
    RecyclerView recyclerViewProducts;

    //for pagging
    boolean isEndReached;
    boolean mIsLoading;
    int page = 0;
    boolean isSearchClicked = false;
    LinearLayoutManager linearLayoutManager;

    LedRadiatorAdapter ledRadiatorAdapter;
    RadiatorProductImpl radiatorProduct;
    ArrayList<RadiatorModel.Data.Product> arrayListRadiatorProducts;
    DBHelper dbHelper;
    ArrayList<CartItem> getCartItems;


    public RadiatorFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        arrayListRadiatorProducts = new ArrayList<>();
        dbHelper = new DBHelper(getActivity());
        getCartItems = dbHelper.getCartItem();

        radiatorProduct = new RadiatorProductImpl(this, getActivity());
        ledRadiatorAdapter = new LedRadiatorAdapter(getActivity(), arrayListRadiatorProducts);
        setLayoutManager();
        getPaginationData(false);
    }

    @Override
    public void onStart() {
        super.onStart();

        getCartItems = dbHelper.getCartItem();

        if (arrayListRadiatorProducts.size() != 0) {
            if (getCartItems != null && getCartItems.size() != 0) {
                for (int i = 0; i < arrayListRadiatorProducts.size(); i++) {

                    for (int j = 0; j < getCartItems.size(); j++) {

                        if (getCartItems.get(j).getProductId().equals(arrayListRadiatorProducts.get(i).getProductId())) {
                            arrayListRadiatorProducts.get(i).setAddedToCart(true);
                            break;
                        } else {
                            arrayListRadiatorProducts.get(i).setAddedToCart(false);
                        }
                    }
                }
            } else {
                for (int i = 0; i < arrayListRadiatorProducts.size(); i++) {
                    arrayListRadiatorProducts.get(i).setAddedToCart(false);
                }
            }

            ledRadiatorAdapter.notifyDataSetChanged();
        }
    }

    private void getPaginationData(boolean isRetry) {

        mIsLoading = true;
        if (isRetry) {
            if (page == -1) {
                page = 0;
            }
            getRadiatorData();
        } else {
            page = page + 1;
            getRadiatorData();
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_radiator, container, false);

        ButterKnife.bind(this, view);

        return view;
    }



    @Override
    public void getLedRadiatorProductSuccess(RadiatorModel.Data radiatorDataModels) {

        arrayListRadiatorProducts = radiatorDataModels.getProducts();

        for (int i = 0; i < radiatorDataModels.getProducts().size(); i++) {

            for (int j = 0; j < getCartItems.size(); j++) {

                if (getCartItems.get(j).getProductId().equals(arrayListRadiatorProducts.get(i).getProductId())) {
                    arrayListRadiatorProducts.get(i).setAddedToCart(true);
                    break;
                } else {
                    arrayListRadiatorProducts.get(i).setAddedToCart(false);
                }
            }

        }


        RadiatorModel.Data.FilterData filterData= radiatorDataModels.getFilterData();

        int totalRecords = Integer.valueOf(filterData.getLimit());

        //set the page title
        ((DashboardActivity)getActivity()).setDashboardTitle(filterData.getPageTitle());

        if (arrayListRadiatorProducts != null) {
            if (arrayListRadiatorProducts.size() > (totalRecords - 1)) {
                isEndReached = false;
            } else {
                isEndReached = true;
            }
            ledRadiatorAdapter.updateData(arrayListRadiatorProducts, isSearchClicked);
            isSearchClicked = false;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mIsLoading = false;
            }
        }, 1000);

    }

    @Override
    public void getLedRadiatorProductUnSucess(String message) {
        mIsLoading = false;
        isEndReached = false;

        arrayListRadiatorProducts.clear();
        recyclerViewProducts.setAdapter(null);
        ledRadiatorAdapter = new LedRadiatorAdapter(getActivity(), arrayListRadiatorProducts);
        recyclerViewProducts.setAdapter(ledRadiatorAdapter);

    }

    @Override
    public void ledRadiatorProductInternetError() {
        SnackNotify.checkConnection(onretryRadiaterDataApi, mainContainer);
    }

    OnClickInterface onretryRadiaterDataApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getRadiatorData();
        }
    };

    @Override
    public void ledRadiatorProductServerError(String message) {
        SnackNotify.showMessage(message, mainContainer);
    }

    private void getRadiatorData() {

        radiatorProduct.getLedRadiatorProducts(page,"");
    }

    private void setLayoutManager() {
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewProducts.setLayoutManager(linearLayoutManager);
        recyclerViewProducts.setItemAnimator(new DefaultItemAnimator());
        recyclerViewProducts.setHasFixedSize(true);
        recyclerViewProducts.setNestedScrollingEnabled(false);
        setAdapter();
        recyclerViewProducts.addOnScrollListener(mRecyclerViewOnScrollListener);
    }

    // implement paging
    private RecyclerView.OnScrollListener mRecyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = linearLayoutManager.getChildCount();
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

            if (!mIsLoading && !isEndReached) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
                    getPaginationData(false);
                }
            }
        }
    };

    private void setAdapter() {
        ledRadiatorAdapter = new LedRadiatorAdapter(getActivity(), arrayListRadiatorProducts);
        recyclerViewProducts.setAdapter(ledRadiatorAdapter);
    }

}
