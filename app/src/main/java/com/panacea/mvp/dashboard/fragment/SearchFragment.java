package com.panacea.mvp.dashboard.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.panacea.R;
import com.panacea.common.database.CartItem;
import com.panacea.common.database.DBHelper;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.common.utility.SnackNotify;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.dashboard.adapter.LedRadiatorAdapter;
import com.panacea.mvp.dashboard.adapter.SearchPartsAdapter;
import com.panacea.mvp.dashboard.model.PartsModel;
import com.panacea.mvp.dashboard.presenter.DashboardContractor;
import com.panacea.mvp.dashboard.presenter.PartsProductImpl;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.nfc.tech.MifareUltralight.PAGE_SIZE;

/**
 * Created by Braintech on 13-12-2017.
 */

public class SearchFragment extends Fragment implements DashboardContractor.PartsProductView {


    @BindView(R.id.mainContainer)
    FrameLayout mainContainer;

    @BindView(R.id.recyclerViewProducts)
    RecyclerView recyclerViewProducts;

    @BindView(R.id.imgViewNoRecordFound)
    ImageView imgViewNoRecordFound;

    //for paging
    boolean isEndReached;
    boolean mIsLoading;
    int page = 0;
    boolean isSearchClicked = false;
    LinearLayoutManager linearLayoutManager;

    ArrayList<PartsModel.Data.Product> arrayListPartsProducts;
    SearchPartsAdapter searchPartsAdapter;
    PartsProductImpl partsProduct;
    String searchText;
    DBHelper dbHelper;
    ArrayList<CartItem> getCartItems;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_search, container, false);

        ButterKnife.bind(this, view);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            searchText = bundle.getString(ConstIntent.KEY_SEARCH);
        }
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        arrayListPartsProducts = new ArrayList<>();
        dbHelper = new DBHelper(getActivity());
        getCartItems = dbHelper.getCartItem();
        partsProduct = new PartsProductImpl(this, getActivity());
        searchPartsAdapter = new SearchPartsAdapter(getActivity(), arrayListPartsProducts);
        setLayoutManager();
        getPaginationData(false);
    }
    @Override
    public void onStart() {
        super.onStart();

        getCartItems = dbHelper.getCartItem();

        if (arrayListPartsProducts.size() != 0) {
            if (getCartItems != null && getCartItems.size() != 0) {
                for (int i = 0; i < arrayListPartsProducts.size(); i++) {

                    for (int j = 0; j < getCartItems.size(); j++) {

                        if (getCartItems.get(j).getProductId().equals(arrayListPartsProducts.get(i).getProductId())) {
                            arrayListPartsProducts.get(i).setAddedToCart(true);
                            break;
                        } else {
                            arrayListPartsProducts.get(i).setAddedToCart(false);
                        }
                    }
                }
            } else {
                for (int i = 0; i < arrayListPartsProducts.size(); i++) {
                    arrayListPartsProducts.get(i).setAddedToCart(false);
                }
            }

            searchPartsAdapter.notifyDataSetChanged();
        }
    }



    @Override
    public void getPartsProductSuccess(PartsModel.Data partModel) {

        arrayListPartsProducts = partModel.getProducts();
        PartsModel.Data.FilterData filterData = partModel.getFilterData();

        for (int i = 0; i < partModel.getProducts().size(); i++) {

            for (int j = 0; j < getCartItems.size(); j++) {

                if (getCartItems.get(j).getProductId().equals(arrayListPartsProducts.get(i).getProductId())) {
                    arrayListPartsProducts.get(i).setAddedToCart(true);
                    break;
                } else {
                    arrayListPartsProducts.get(i).setAddedToCart(false);
                }
            }

        }

        int totalRecords = Integer.valueOf(filterData.getLimit());

        //set the page title
        ((DashboardActivity) getActivity()).setDashboardTitle(filterData.getPageTitle());

        if (arrayListPartsProducts != null && arrayListPartsProducts.size()>0) {

            //show recyclerview and hide image no record found
            recyclerViewProducts.setVisibility(View.VISIBLE);
            imgViewNoRecordFound.setVisibility(View.GONE);

            if (arrayListPartsProducts.size() > (totalRecords - 1)) {
                isEndReached = false;
            } else {
                isEndReached = true;
            }
            searchPartsAdapter.updateData(arrayListPartsProducts, isSearchClicked);
            isSearchClicked = false;
        }
        else
        {
            //hide recyclerview and show image no record found
            recyclerViewProducts.setVisibility(View.GONE);
            imgViewNoRecordFound.setVisibility(View.VISIBLE);

           // SnackNotify.showMessage(getString(R.string.title_no_record_found), mainContainer);
            searchPartsAdapter.updateData(arrayListPartsProducts, isSearchClicked);
            isSearchClicked = false;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mIsLoading = false;
            }
        }, 1000);
    }

    @Override
    public void getPartsProductUnSucess(String message) {
        mIsLoading = false;
        isEndReached = false;

        arrayListPartsProducts.clear();
        recyclerViewProducts.setAdapter(null);
        searchPartsAdapter = new SearchPartsAdapter(getActivity(), arrayListPartsProducts);
        recyclerViewProducts.setAdapter(searchPartsAdapter);
    }

    @Override
    public void partsProductInternetError() {

        SnackNotify.checkConnection(onretrySearchDataApi, mainContainer);
    }

    OnClickInterface onretrySearchDataApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getSearchDataApi();
        }
    };

    @Override
    public void partsProductServerError(String message) {
        SnackNotify.showMessage(message, mainContainer);
    }

    private void getPaginationData(boolean isRetry) {

        mIsLoading = true;
        if (isRetry) {
            if (page == -1) {
                page = 0;
            }
            getSearchDataApi();
        } else {
            page = page + 1;
            getSearchDataApi();
        }
    }

    private void getSearchDataApi() {

        // call login api
        partsProduct.getSearchProducts(searchText, page);
    }

    private void setLayoutManager() {
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewProducts.setLayoutManager(linearLayoutManager);
        recyclerViewProducts.setItemAnimator(new DefaultItemAnimator());
        recyclerViewProducts.setHasFixedSize(true);
        recyclerViewProducts.setNestedScrollingEnabled(false);
        setAdapter();
        recyclerViewProducts.addOnScrollListener(mRecyclerViewOnScrollListener);
    }

    private void setAdapter() {
        searchPartsAdapter = new SearchPartsAdapter(getActivity(), arrayListPartsProducts);
        recyclerViewProducts.setAdapter(searchPartsAdapter);
    }

    // implement paging
    private RecyclerView.OnScrollListener mRecyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = linearLayoutManager.getChildCount();
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

            if (!mIsLoading && !isEndReached) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
                    getPaginationData(false);
                }
            }
        }
    };

}
