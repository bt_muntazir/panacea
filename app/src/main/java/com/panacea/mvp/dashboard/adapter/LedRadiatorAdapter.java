package com.panacea.mvp.dashboard.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.database.CartItem;
import com.panacea.common.database.CartOperations;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.viewpagerindicator.CirclePageIndicator;
import com.panacea.common.interfaces.CartUpdate;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.common.utility.Utils;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.dashboard.model.HomeProductsModel;
import com.panacea.mvp.dashboard.model.RadiatorModel;
import com.panacea.mvp.productdetail.ProductDetailActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 11-12-2017.
 */

public class LedRadiatorAdapter extends RecyclerView.Adapter {

    Activity activity;
    ArrayList<RadiatorModel.Data.Product> raDataArrayListResult;

    int dataPosition;
    DBHelper dbHelper;

    ArrayList<String> arrayListSlideImage;


    ArrayList<CartItem> getCartItems;

    public LedRadiatorAdapter(Activity activity, ArrayList<RadiatorModel.Data.Product> raDataArrayList) {
        Log.e("size", "" + raDataArrayList.size());
        this.activity = activity;
        this.raDataArrayListResult = raDataArrayList;
        dbHelper = new DBHelper(activity);
        getCartItems = dbHelper.getCartItem();

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_radiator_led_products, parent, false);
        return new LedRadiatorAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        final RadiatorModel.Data.Product result = raDataArrayListResult.get(position);
        // bind all sliding image in a array
        arrayListSlideImage = new ArrayList<>();
        ArrayList<String> arrayListAdditionalImage = raDataArrayListResult.get(position).getAdditionalImage();

        // get thumb image
        String thumbImage = raDataArrayListResult.get(position).getThumb();
        String price = result.getPrice();

        if (thumbImage != null && thumbImage.length() > 0) {

            arrayListSlideImage.add(thumbImage);
        }

        if (arrayListAdditionalImage != null && arrayListAdditionalImage.size() > 0) {

            for (int i = 0; i < arrayListAdditionalImage.size(); i++) {
                arrayListSlideImage.add(arrayListAdditionalImage.get(i));
            }
        }

        // bind image with sliding viewpager
        if (arrayListSlideImage.size() > 0) {
            SlidingImageAdapter slidingImageAdapter = new SlidingImageAdapter(activity, arrayListSlideImage, result.getProductId(), false);
            ((ViewHolder) holder).imageViewPager.setAdapter(slidingImageAdapter);

        } else {
            SlidingImageAdapter slidingImageAdapter = new SlidingImageAdapter(activity, new ArrayList<String>(), "", false);
            ((ViewHolder) holder).imageViewPager.setAdapter(slidingImageAdapter);
        }

        // set view pager indecator
        ((ViewHolder) holder).indicator.setViewPager(((ViewHolder) holder).imageViewPager);

        // hide/show indecator
        if (arrayListSlideImage.size() == 1) {
            ((ViewHolder) holder).indicator.setVisibility(View.GONE);
        } else {
            ((ViewHolder) holder).indicator.setVisibility(View.VISIBLE);
        }

        ((ViewHolder) holder).txtViewProduct.setText(result.getName().toString());
        ((ViewHolder) holder).txtViewProductId.setText(result.getModel().toString());

        if (price.equals("$0.00")) {

            ((LedRadiatorAdapter.ViewHolder) holder).getTxtViewPrice.setVisibility(View.GONE);
            ((LedRadiatorAdapter.ViewHolder) holder).btnAddCart.setVisibility(View.GONE);
        } else {
            ((ViewHolder) holder).getTxtViewPrice.setText(result.getPrice().toString());
            ((LedRadiatorAdapter.ViewHolder) holder).btnAddCart.setVisibility(View.VISIBLE);
        }

        if (result.getIsAddedToCart()) {
            ((LedRadiatorAdapter.ViewHolder) holder).btnAddCart.setAlpha(0.5f);
            ((LedRadiatorAdapter.ViewHolder) holder).btnAddCart.setEnabled(false);
        } else {
            ((LedRadiatorAdapter.ViewHolder) holder).btnAddCart.setAlpha(1);
            ((LedRadiatorAdapter.ViewHolder) holder).btnAddCart.setEnabled(true);
        }


        //click event add to cart
        ((LedRadiatorAdapter.ViewHolder) holder).relLayProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Utils.hideKeyboardIfOpen(activity);
            }
        });


        //click event add to cart
        ((LedRadiatorAdapter.ViewHolder) holder).btnAddCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CartUpdate cartUpdate = new CartUpdate() {
                    @Override
                    public void onCartUpdate(String type, int qty) {

                        dbHelper = new DBHelper(activity);

                        if (activity instanceof DashboardActivity) {
                            ((DashboardActivity) activity).updateCartQty();
                            result.setAddedToCart(true);

                            notifyDataSetChanged();
                        }
                    }
                };
                CartItem cartItem = getCartItems(raDataArrayListResult.get(position));
                cartItem.setProductprice(result.getPrice());

                CartOperations.updateDataBase(activity, cartItem, true, CartOperations.TYPE_ADD_ITEM, 10, cartUpdate);

                ShowDoalogForSuccess(raDataArrayListResult.get(position).getName());
            }
        });
    }

    private void ShowDoalogForSuccess(String productName) {

        // Create custom dialog object
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.item_success_add_cart);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.alpha(Color.BLACK)));


// set values for custom dialog components - text, image and button
        TextView txtViewDescription = (TextView) dialog.findViewById(R.id.txtViewDescription);
        txtViewDescription.setText("You have added " + productName + " to your shopping cart.");
        ImageView imgViewCross = (ImageView) dialog.findViewById(R.id.imgViewCross);

        dialog.show();

        Button btnGotoCart = (Button) dialog.findViewById(R.id.btnGotoCart);
        Button btnContinueShopping = (Button) dialog.findViewById(R.id.btnContinueShopping);
// if decline button is clicked, close the custom dialog

        imgViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnContinueShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnGotoCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activity instanceof DashboardActivity) {
                    ((DashboardActivity) activity).goToCartFromDialog();
                }
                dialog.dismiss();
            }
        });


    }

    public CartItem getCartItems(RadiatorModel.Data.Product products) {

        CartItem cartItems = new CartItem();
        cartItems.setProductId(products.getProductId());
        cartItems.setProductName(products.getName());
        cartItems.setProduct_quantity(1);
        return cartItems;
    }

    @Override
    public int getItemCount() {
        return raDataArrayListResult.size();
    }

    public void updateData(ArrayList<RadiatorModel.Data.Product> raDataArrayList, boolean isSearchClick) {

        if (isSearchClick)
            raDataArrayListResult.clear();

        if (raDataArrayListResult != null && raDataArrayList != null) {
            for (int i = 0; i < raDataArrayList.size(); i++) {
                raDataArrayListResult.add(raDataArrayList.get(i));

            }
            notifyDataSetChanged();
        }
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayProduct)
        RelativeLayout relLayProduct;

        @BindView(R.id.imageViewPager)
        ViewPager imageViewPager;

        @BindView(R.id.indicator)
        CirclePageIndicator indicator;

        @BindView(R.id.txtViewProduct)
        AppCompatTextView txtViewProduct;

        @BindView(R.id.txtViewProductId)
        AppCompatTextView txtViewProductId;

        @BindView(R.id.txtViewPrice)
        AppCompatTextView getTxtViewPrice;

        @BindView(R.id.btnAddCart)
        Button btnAddCart;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
