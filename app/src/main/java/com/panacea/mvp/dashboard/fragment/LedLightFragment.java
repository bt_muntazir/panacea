package com.panacea.mvp.dashboard.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.panacea.R;
import com.panacea.common.adapter.model.CountrySpinnerModel;
import com.panacea.common.adapter.model.StateSpinnerModel;
import com.panacea.common.database.CartItem;
import com.panacea.common.database.DBHelper;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.requestresponse.Const;
import com.panacea.common.utility.SnackNotify;
import com.panacea.mvp.commonMvp.adapter.AdapterSpinner;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.dashboard.adapter.LedRadiatorAdapter;
import com.panacea.mvp.dashboard.model.RadiatorModel;
import com.panacea.mvp.dashboard.presenter.DashboardContractor;
import com.panacea.mvp.dashboard.presenter.LedProductImpl;
import com.panacea.mvp.dashboard.presenter.RadiatorProductImpl;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemSelected;

import static android.nfc.tech.MifareUltralight.PAGE_SIZE;


public class LedLightFragment extends Fragment implements DashboardContractor.LedRadiatorProductView {

    @BindView(R.id.recyclerViewProducts)
    RecyclerView recyclerViewProducts;

    @BindView(R.id.mainContainer)
    FrameLayout mainContainer;

    @BindView(R.id.spinnerFilter)
    AppCompatSpinner spinnerFilter;


    //for paging
    boolean isEndReached;
    boolean mIsLoading;
    int page = 0;
    boolean isSearchClicked = false;
    LinearLayoutManager linearLayoutManager;

    LedProductImpl ledProduct;
    ArrayList<RadiatorModel.Data.Product> arrayListLedProducts;
    LedRadiatorAdapter ledRadiatorAdapter;

    AdapterSpinner adapterSpinnerFilter;
    String filterId;
    String filterSelectedName = "";
    int position;
    DBHelper dbHelper;
    ArrayList<CartItem> getCartItems;


    ArrayList<HashMap<String, String>> arrayListFilter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_led_light, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        arrayListLedProducts = new ArrayList<>();
        arrayListFilter = new ArrayList<>();
        dbHelper = new DBHelper(getActivity());

        ledProduct = new LedProductImpl(this, getActivity());
        ledRadiatorAdapter = new LedRadiatorAdapter(getActivity(), arrayListLedProducts);

        setLayoutManager();

        getPaginationData(false);
    }

    @Override
    public void onStart() {
        super.onStart();

        getCartItems = dbHelper.getCartItem();

        if (arrayListLedProducts.size() != 0) {
            if (getCartItems != null && getCartItems.size() != 0) {
                for (int i = 0; i < arrayListLedProducts.size(); i++) {

                    for (int j = 0; j < getCartItems.size(); j++) {

                        if (getCartItems.get(j).getProductId().equals(arrayListLedProducts.get(i).getProductId())) {
                            arrayListLedProducts.get(i).setAddedToCart(true);
                            break;
                        } else {
                            arrayListLedProducts.get(i).setAddedToCart(false);
                        }
                    }
                }
            } else {
                for (int i = 0; i < arrayListLedProducts.size(); i++) {
                    arrayListLedProducts.get(i).setAddedToCart(false);
                }
            }

            ledRadiatorAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void getLedRadiatorProductSuccess(RadiatorModel.Data ledModels) {
        arrayListLedProducts = ledModels.getProducts();

        for (int i = 0; i < ledModels.getProducts().size(); i++) {

            for (int j = 0; j < getCartItems.size(); j++) {

                if (getCartItems.get(j).getProductId().equals(arrayListLedProducts.get(i).getProductId())) {
                    arrayListLedProducts.get(i).setAddedToCart(true);
                    break;
                } else {
                    arrayListLedProducts.get(i).setAddedToCart(false);
                }
            }

        }

        RadiatorModel.Data.FilterData filterData = ledModels.getFilterData();

        int totalRecords = Integer.valueOf(filterData.getLimit());

        //set the page title
        ((DashboardActivity) getActivity()).setDashboardTitle(filterData.getPageTitle());

        if (arrayListLedProducts != null) {
            if (arrayListLedProducts.size() > (totalRecords - 1)) {
                isEndReached = false;
            } else {
                isEndReached = true;
            }
            ledRadiatorAdapter.updateData(arrayListLedProducts, isSearchClicked);
            isSearchClicked = false;
        }

        if (arrayListFilter.size() == 0) {

            for (int i = 0; i < ledModels.getFilterData().getFilterKeywords().size(); i++) {

                String abc = (ledModels.getFilterData().getFilterKeywords().get(i));

                HashMap hashMap = new HashMap();
                hashMap.put(Const.KEY_ID, i);
                hashMap.put(Const.KEY_NAME, abc);
                arrayListFilter.add(hashMap);

                setFilterAdapter();
            }
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mIsLoading = false;
            }
        }, 1000);
    }

    @Override
    public void getLedRadiatorProductUnSucess(String message) {
        mIsLoading = false;
        isEndReached = false;
        arrayListLedProducts.clear();
        recyclerViewProducts.setAdapter(null);
        ledRadiatorAdapter = new LedRadiatorAdapter(getActivity(), arrayListLedProducts);
        recyclerViewProducts.setAdapter(ledRadiatorAdapter);
    }

    @Override
    public void ledRadiatorProductInternetError() {

        SnackNotify.checkConnection(onretryLedLightDataApi, mainContainer);
    }

    OnClickInterface onretryLedLightDataApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getLedData();
        }
    };

    @Override
    public void ledRadiatorProductServerError(String message) {
        SnackNotify.showMessage(message, mainContainer);
    }


    private void setFilterAdapter() {
        adapterSpinnerFilter = new AdapterSpinner(getActivity(), R.layout.spinner_selected_view, arrayListFilter);
        spinnerFilter.setAdapter(adapterSpinnerFilter);
    }


    private void getPaginationData(boolean isRetry) {

        mIsLoading = true;
        if (isRetry) {
            if (page == -1) {
                page = 0;
            }
            getLedData();
        } else {
            page = page + 1;
            getLedData();
        }
    }


    private void setLayoutManager() {
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerViewProducts.setLayoutManager(linearLayoutManager);
        recyclerViewProducts.setItemAnimator(new DefaultItemAnimator());
        recyclerViewProducts.setHasFixedSize(true);
        recyclerViewProducts.setNestedScrollingEnabled(false);
        setAdapter();

        recyclerViewProducts.addOnScrollListener(mRecyclerViewOnScrollListener);
    }

    // implement paging
    private RecyclerView.OnScrollListener mRecyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = linearLayoutManager.getChildCount();
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

            if (!mIsLoading && !isEndReached) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
                    getPaginationData(false);
                }
            }
        }
    };

    private void setAdapter() {
        ledRadiatorAdapter = new LedRadiatorAdapter(getActivity(), arrayListLedProducts);
        recyclerViewProducts.setAdapter(ledRadiatorAdapter);
    }


    private void getLedData() {
        ledProduct.getLedRadiatorProducts(page, filterSelectedName);
    }

    @OnItemSelected(R.id.spinnerFilter)
    void onItemCountrySelected(int position) {
        if (position >= 0) {
            filterSelectedName = String.valueOf(arrayListFilter.get(position).get(Const.KEY_NAME));
            page = 0;

            isSearchClicked = true;

            getPaginationData(false);
        }
    }
}
