package com.panacea.mvp.dashboard.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.dashboard.model.HomeProductsModel;
import com.panacea.mvp.dashboard.model.PartsModel;
import com.panacea.mvp.login.model.LoginResponseModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 12-12-2017.
 */

public class PartsProductImpl implements DashboardContractor.PartsProduct{


    DashboardContractor.PartsProductView partsProductView;
    Activity activity;
    JSONObject jsonObject;


    public PartsProductImpl(DashboardContractor.PartsProductView partsProductView, Activity activity) {

        this.partsProductView = partsProductView;
        this.activity = activity;
    }

    @Override
    public void getPartsProducts(JSONObject jsonObject) {

        try {
            ApiAdapter.getInstance(activity);
            partsProduct(jsonObject);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            partsProductView.partsProductInternetError();
        }
    }

    @Override
    public void getSearchProducts(String search,int page) {
        try {
            ApiAdapter.getInstance(activity);
            searchProduct(search,page);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            partsProductView.partsProductInternetError();
        }
    }

    private void partsProduct(JSONObject jsonObject){
        Progress.start(activity);

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<PartsModel> getPartSearchOutput = ApiAdapter.getApiService().getSearch("application/json", "no-cache", body);

        getPartSearchOutput.enqueue(new Callback<PartsModel>() {
            @Override
            public void onResponse(Call<PartsModel> call, Response<PartsModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    PartsModel partsModel = response.body();
                    String message = "Hello";

                    if (partsModel.getStatus().equals("1")) {
                        partsProductView.getPartsProductSuccess(partsModel.getData());
                    } else {
                        partsProductView.getPartsProductUnSucess("Login Fail");
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    partsProductView.getPartsProductUnSucess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<PartsModel> call, Throwable t) {
                Progress.stop();
                partsProductView.getPartsProductUnSucess(activity.getString(R.string.error_server));
            }
        });
    }

    private void searchProduct(String search,int page){
        Progress.start(activity);

        try {
            jsonObject=new JSONObject();
            jsonObject.put(Const.PARAM_PAGE, page);
            jsonObject.put(Const.PARAM_SEARCH, search);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<PartsModel> getPartSearchOutput = ApiAdapter.getApiService().getSearch("application/json", "no-cache", body);

        getPartSearchOutput.enqueue(new Callback<PartsModel>() {
            @Override
            public void onResponse(Call<PartsModel> call, Response<PartsModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    PartsModel partsModel = response.body();
                    String message = "Hello";

                    if (partsModel.getStatus().equals("1")) {
                        partsProductView.getPartsProductSuccess(partsModel.getData());
                    } else {
                        partsProductView.getPartsProductUnSucess("Login Fail");
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    partsProductView.getPartsProductUnSucess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<PartsModel> call, Throwable t) {
                Progress.stop();
                partsProductView.getPartsProductUnSucess(activity.getString(R.string.error_server));
            }
        });

    }
}
