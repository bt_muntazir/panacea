package com.panacea.mvp.splash;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.RelativeLayout;

import com.panacea.R;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.common.utility.SnackNotify;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.direction.GetDirectionActivity;
import com.panacea.mvp.login.LoginActivity;
import com.panacea.mvp.navigation.NavigationActivity;
import com.panacea.mvp.productdetail.ProductDetailActivity;
import com.panacea.mvp.splash.model.SplashModel;
import com.panacea.mvp.splash.presenter.SplashContractor;
import com.panacea.mvp.splash.presenter.SplashPresenterImpl;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity implements SplashContractor.SplashView {

    /*@BindView(R.id.relativeLayout)
    RelativeLayout relativeLayout;*/

    //private int SPLASH_TIME_OUT = 3000;

    SplashPresenterImpl splashPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        splashPresenter = new SplashPresenterImpl(this, this);
        getData();
    }

    private void getData() {
        splashPresenter.getConstantData();
    }

    @Override
    public void onError(String message) {
        // SnackNotify.showMessage(message, relativeLayout);
    }

    @Override
    public void onInternetError() {
        // SnackNotify.checkConnection(onretryConstantApi, relativeLayout);
    }

    OnClickInterface onretryConstantApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };


    @Override
    public void apiUnsuccess(String message) {
        // SnackNotify.showMessage(message, relativeLayout);
    }

    @Override
    public void serverNotResponding(String message) {
        // SnackNotify.showMessage(message, relativeLayout);
    }

    @Override
    public void getConstantApiSuccess(SplashModel.Data splashModel) {

        // save response to session
        LoginManager.getInstance().SaveConstantDetail(splashModel);

        Intent intent = new Intent(SplashActivity.this, DashboardActivity.class);
        //Intent intent = new Intent(SplashActivity.this, ProductDetailActivity.class);
        Bundle bundle = new Bundle();
        bundle.putSerializable(ConstIntent.KEY_CONSTANT_SPLASH, splashModel);
        intent.putExtras(bundle);
        startActivity(intent);

        finish();
    }
}
