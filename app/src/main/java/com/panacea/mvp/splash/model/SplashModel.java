package com.panacea.mvp.splash.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Braintech on 08-12-2017.
 */


public class SplashModel implements Serializable {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data implements Serializable {

        @SerializedName("host")
        @Expose
        private String host;
        @SerializedName("video_url")
        @Expose
        private String videoUrl;
        @SerializedName("video_thumb_url")
        @Expose
        private String videoThumbUrl;
        @SerializedName("video_text1")
        @Expose
        private String videoText1;
        @SerializedName("video_text2")
        @Expose
        private String videoText2;
        @SerializedName("contact_email")
        @Expose
        private String contactEmail;
        @SerializedName("contact_phone")
        @Expose
        private String contactPhone;
        @SerializedName("contact_fax")
        @Expose
        private String contactFax;
        @SerializedName("address1")
        @Expose
        private String address1;
        @SerializedName("lat1")
        @Expose
        private String lat1;
        @SerializedName("lon1")
        @Expose
        private String lon1;
        @SerializedName("address2")
        @Expose
        private String address2;
        @SerializedName("lat2")
        @Expose
        private String lat2;
        @SerializedName("lon2")
        @Expose
        private String lon2;
        @SerializedName("instagram_url")
        @Expose
        private String instagramUrl;
        @SerializedName("youtube_url")
        @Expose
        private String youtubeUrl;

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public String getVideoUrl() {
            return videoUrl;
        }

        public void setVideoUrl(String videoUrl) {
            this.videoUrl = videoUrl;
        }

        public String getVideoThumbUrl() {
            return videoThumbUrl;
        }

        public void setVideoThumbUrl(String videoThumbUrl) {
            this.videoThumbUrl = videoThumbUrl;
        }

        public String getVideoText1() {
            return videoText1;
        }

        public void setVideoText1(String videoText1) {
            this.videoText1 = videoText1;
        }

        public String getVideoText2() {
            return videoText2;
        }

        public void setVideoText2(String videoText2) {
            this.videoText2 = videoText2;
        }

        public String getContactEmail() {
            return contactEmail;
        }

        public void setContactEmail(String contactEmail) {
            this.contactEmail = contactEmail;
        }

        public String getContactPhone() {
            return contactPhone;
        }

        public void setContactPhone(String contactPhone) {
            this.contactPhone = contactPhone;
        }

        public String getContactFax() {
            return contactFax;
        }

        public void setContactFax(String contactFax) {
            this.contactFax = contactFax;
        }

        public String getAddress1() {
            return address1;
        }

        public void setAddress1(String address1) {
            this.address1 = address1;
        }

        public String getLat1() {
            return lat1;
        }

        public void setLat1(String lat1) {
            this.lat1 = lat1;
        }

        public String getLon1() {
            return lon1;
        }

        public void setLon1(String lon1) {
            this.lon1 = lon1;
        }

        public String getAddress2() {
            return address2;
        }

        public void setAddress2(String address2) {
            this.address2 = address2;
        }

        public String getLat2() {
            return lat2;
        }

        public void setLat2(String lat2) {
            this.lat2 = lat2;
        }

        public String getLon2() {
            return lon2;
        }

        public void setLon2(String lon2) {
            this.lon2 = lon2;
        }

        public String getInstagramUrl() {
            return instagramUrl;
        }

        public void setInstagramUrl(String instagramUrl) {
            this.instagramUrl = instagramUrl;
        }

        public String getYoutubeUrl() {
            return youtubeUrl;
        }

        public void setYoutubeUrl(String youtubeUrl) {
            this.youtubeUrl = youtubeUrl;
        }

    }
}