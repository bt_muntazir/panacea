package com.panacea.mvp.splash.presenter;

import com.panacea.mvp.commonMvp.BaseView;
import com.panacea.mvp.splash.model.SplashModel;

/**
 * Created by Braintech on 08-12-2017.
 */

public class SplashContractor {

    public interface SplashView extends BaseView {

        void getConstantApiSuccess(SplashModel.Data splashModel);
    }

    public interface Presenter {
        void getConstantData();
    }
}
