package com.panacea.mvp.splash.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.login.model.LoginResponseModel;
import com.panacea.mvp.splash.model.SplashModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 08-12-2017.
 */

public class SplashPresenterImpl implements SplashContractor.Presenter {

    SplashContractor.SplashView splashView;
    Activity activity;
    JSONObject jsonObject;

    public SplashPresenterImpl(SplashContractor.SplashView splashView, Activity activity) {
        this.splashView = splashView;
        this.activity = activity;
    }

    @Override
    public void getConstantData() {
        try {
            ApiAdapter.getInstance(activity);
            constantData();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            splashView.onInternetError();
        }
    }

    private void constantData() {

        Progress.start(activity);

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.APP_KEY, Const.APP_KEY_VALUE);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<SplashModel> getLoginOutput = ApiAdapter.getApiService().getConstant("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<SplashModel>() {
            @Override
            public void onResponse(Call<SplashModel> call, Response<SplashModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    SplashModel splashModel = response.body();
                    String message = "Hello";

                    if (splashModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        splashView.getConstantApiSuccess(splashModel.getData());
                    } else {
                        splashView.apiUnsuccess("Login Fail");
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    splashView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<SplashModel> call, Throwable t) {
                Progress.stop();
                splashView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}
