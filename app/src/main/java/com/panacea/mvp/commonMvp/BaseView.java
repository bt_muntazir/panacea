package com.panacea.mvp.commonMvp;

/**
 * Created by Braintech on 01-12-2017.
 */

public interface BaseView {
    void onError(String message);
    void onInternetError();
    void apiUnsuccess(String message);
    void serverNotResponding(String message);
}

