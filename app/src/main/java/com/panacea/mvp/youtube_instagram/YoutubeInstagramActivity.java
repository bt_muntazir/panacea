package com.panacea.mvp.youtube_instagram;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.mvp.cart.ShoppingCartActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class YoutubeInstagramActivity extends AppCompatActivity {

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.webView)
    WebView webView;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_instagram);
        ButterKnife.bind(this);
        dbHelper = new DBHelper(this);

        Bundle bundle = getIntent().getExtras();
        String url = bundle.getString(ConstIntent.KEY_INSTAGRAM_YOUTUBE_URL);
        String title = bundle.getString(ConstIntent.KEY_INSTAGRAM_YOUTUBE_CONST);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);

        txtViewTitle.setText(title);
    }

    @Override
    protected void onResume() {
        super.onResume();

        frameLayCart.setVisibility(View.GONE);
        updateCartQty();
    }

    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (dbHelper.getCartCount() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            }
        }
    }


    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }


    @OnClick(R.id.imgBackButton)
    public void backPressed() {
        onBackPressed();
    }
}
