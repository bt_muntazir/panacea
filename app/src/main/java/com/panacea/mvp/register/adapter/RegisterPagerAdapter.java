package com.panacea.mvp.register.adapter;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.panacea.mvp.register.RegisterActivity;
import com.panacea.mvp.register.fragment.AddressFragment;
import com.panacea.mvp.register.fragment.PersonalDetailFragment;

/**
 * Created by Braintech on 05-12-2017.
 */

public class RegisterPagerAdapter extends FragmentStatePagerAdapter {

    int tabCount;
    Activity activity;

    public RegisterPagerAdapter(FragmentManager fragmentManager, int tabCount, Activity activity) {
        super(fragmentManager);
        this.activity = activity;
        this.tabCount = tabCount;
    }

    // Returns total number of pages
    @Override
    public int getCount() {
        return tabCount;
    }

    // Returns the fragment to display for that page
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return PersonalDetailFragment.newInstance(position);
            case 1:
                return AddressFragment.newInstance(position);
            default:
                return null;
        }
    }
}
