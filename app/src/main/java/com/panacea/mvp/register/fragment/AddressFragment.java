package com.panacea.mvp.register.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.panacea.R;
import com.panacea.common.adapter.model.CountrySpinnerModel;
import com.panacea.common.adapter.model.StateSpinnerModel;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.interfaces.Updateable;
import com.panacea.common.requestresponse.Const;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.common.utility.SnackNotify;
import com.panacea.common.utility.Utils;
import com.panacea.mvp.commonMvp.adapter.AdapterSpinner;
import com.panacea.mvp.login.model.LoginResponseModel;
import com.panacea.mvp.register.RegisterActivity;
import com.panacea.mvp.register.presenter.RegisterContractor;
import com.panacea.mvp.register.presenter.RegisterPresenterImpl;
import com.panacea.mvp.register.presenter.CountyrStatePresenterImpl;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;

/**
 * Created by Braintech on 01-12-2017.
 */

public class AddressFragment extends Fragment implements RegisterContractor.CountryStateView, RegisterContractor.RegisterView, Updateable {

    @BindView(R.id.mainContainer)
    FrameLayout mainContainer;

    @BindView(R.id.btnRegister)
    Button btnRegister;

   /* @BindView(R.id.edtTextCompany)
    AppCompatEditText edtTextCompany;*/

    @BindView(R.id.edtTextAddress1)
    AppCompatEditText edtTextAddress1;

    @BindView(R.id.edtTextAddress2)
    AppCompatEditText edtTextAddress2;

    @BindView(R.id.edtTextCity)
    AppCompatEditText edtTextCity;

    @BindView(R.id.edtTextPostCode)
    AppCompatEditText edtTextPostCode;

    @BindView(R.id.linLayCountry)
    LinearLayout linLayCountry;

    @BindView(R.id.linLayState)
    LinearLayout linLayState;

    @BindView(R.id.spinnerCountry)
    AppCompatSpinner spinnerCountry;

    @BindView(R.id.spinnerState)
    AppCompatSpinner spinnerState;

    CountyrStatePresenterImpl spinnerValuePresenterImpl;
    RegisterPresenterImpl registerPresenterImpl;

    ArrayList<HashMap<String, String>> arrayListCountry;
    ArrayList<HashMap<String, String>> arrayListState;

    AdapterSpinner adapterSpinnerCountry;
    AdapterSpinner adapterSpinnerState;

    JSONObject jsonObject;

    String firstName;
    String lastName;
    String email;
    String fax;
    String telephone;
    String password;
    String company;

    String countryId="-1";
    String stateId="-1";
    String selectedStateName;
    String selectedCountryName;

    boolean isComeFromRegister;


    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";

    // TODO: Rename and change types and number of parameters
    public static AddressFragment newInstance(int param1) {
        AddressFragment fragment = new AddressFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_address, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //getIntentData();
        spinnerValuePresenterImpl = new CountyrStatePresenterImpl(this, getActivity());
        registerPresenterImpl = new RegisterPresenterImpl(getActivity(), this);
        callCountryId();
        setInitialAdapter();
    }

    @Override
    public void update(JSONObject jsonObject) {
        getLastFragmentData(jsonObject);
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, mainContainer);
    }

    @Override
    public void onInternetError() {
        SnackNotify.checkConnection(onretryRegisterApi, mainContainer);
    }

    OnClickInterface onretryRegisterApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };


    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, mainContainer);

    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, mainContainer);
    }

    @Override
    public void getRegisterSuccess(LoginResponseModel.Data loginResponseModel) {


        LoginManager.getInstance().createLoginSession(loginResponseModel);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ((RegisterActivity) getActivity()).finishCurrentActivity();
            }
        }, 2000);

    }

    @Override
    public void getCountrySuccess(ArrayList<CountrySpinnerModel.Datum> arrayListCountryList) {
        for (int i = 0; i < arrayListCountryList.size(); i++) {
            CountrySpinnerModel.Datum datum = arrayListCountryList.get(i);
            String countryId = datum.getCountryId();
            String countryName = datum.getName();

            HashMap hashMap = new HashMap();
            hashMap.put(Const.KEY_ID, countryId);
            hashMap.put(Const.KEY_NAME, countryName);
            arrayListCountry.add(hashMap);
            setCountyAdapter();
        }
    }

    @Override
    public void getCountryUnSucess(String message) {
        SnackNotify.showMessage(message, mainContainer);
    }

    @Override
    public void countryInternetError() {
        SnackNotify.checkConnection(onretryCountryApi, mainContainer);
    }

    OnClickInterface onretryCountryApi = new OnClickInterface() {
        @Override
        public void onClick() {
            callCountryId();
        }
    };

    @Override
    public void countryServerError(String message) {
        SnackNotify.showMessage(message, mainContainer);
    }

    @Override
    public void getStateSuccess(ArrayList<StateSpinnerModel.Datum> arrayListStateList) {

        for (int i = 0; i < arrayListStateList.size(); i++) {
            StateSpinnerModel.Datum datum = arrayListStateList.get(i);
            String stateId = datum.getZoneId();
            String stateName = datum.getName();

            HashMap hashMap = new HashMap();
            hashMap.put(Const.KEY_ID, stateId);
            hashMap.put(Const.KEY_NAME, stateName);
            arrayListState.add(hashMap);

            setStateAdapter();
        }
    }

    @Override
    public void getStateUnSucess(String message) {
        SnackNotify.showMessage(message, mainContainer);
    }

    @Override
    public void StateInternetError() {
        SnackNotify.checkConnection(onretryStateApi, mainContainer);
    }

    OnClickInterface onretryStateApi = new OnClickInterface() {
        @Override
        public void onClick() {
            callStateId(countryId);
        }
    };


    @Override
    public void StateServerError(String message) {
        SnackNotify.showMessage(message, mainContainer);
    }

    /*--------------------------------Private Method--------------------------------------*/

    private void callCountryId() {
        spinnerValuePresenterImpl.getCountry();
    }

    private void callStateId(String countryId) {
        spinnerValuePresenterImpl.getState(countryId);
    }

    private void setInitialAdapter() {
        arrayListCountry = new ArrayList<>();
        HashMap hashMapFirstIndexCountry = new HashMap();
        hashMapFirstIndexCountry.put(Const.KEY_ID, 0);
        hashMapFirstIndexCountry.put(Const.KEY_NAME, getString(R.string.select_country));
        arrayListCountry.add(hashMapFirstIndexCountry);

        arrayListState = new ArrayList<>();
        HashMap hashMapFirstIndexState = new HashMap();
        hashMapFirstIndexState.put(Const.KEY_ID, 0);
        hashMapFirstIndexState.put(Const.KEY_NAME, getString(R.string.select_state));
        arrayListState.add(hashMapFirstIndexState);

        setCountyAdapter();
        setStateAdapter();
    }

    private void getData() {
        // hide keyboard
        Utils.hideKeyboardIfOpen(getActivity());

        // String company = edtTextCompany.getText().toString();
        String address1 = edtTextAddress1.getText().toString();
        String address2 = edtTextAddress2.getText().toString();
        String city = edtTextCity.getText().toString();
        String postcode = edtTextPostCode.getText().toString();

        if (getActivity() instanceof RegisterActivity)
            getLastFragmentData(((RegisterActivity) getActivity()).getJsonObjectPersonalDetail());

        if (validation(address1, address2, city, postcode, selectedCountryName, selectedStateName)) {

            try {
                jsonObject = new JSONObject();
                jsonObject.put(Const.PARAM_FIRSTNAME, firstName);
                jsonObject.put(Const.PARAM_LASTNAME, lastName);
                jsonObject.put(Const.PARAM_EMAIL, email);
                jsonObject.put(Const.PARAM_FAX, fax);
                jsonObject.put(Const.PARAM_TELEPHONE, telephone);
                jsonObject.put(Const.PARAM_PASSWORD, password);
                jsonObject.put(Const.PARAM_CONFIRMPASSWORD, password);
                jsonObject.put(Const.PARAM_COMPANY, company);
                jsonObject.put(Const.PARAM_ADDRESS_1, address1);
                jsonObject.put(Const.PARAM_ADDRESS_2, address2);
                jsonObject.put(Const.PARAM_CITY, city);
                jsonObject.put(Const.PARAM_POSTCODE, postcode);
                jsonObject.put(Const.PARAM_COUNTRY_ID, countryId);
                jsonObject.put(Const.PARAM_ZONE_ID, stateId);

            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            // call login api
            registerPresenterImpl.getRegister(jsonObject);
        }
    }

    private boolean validation(String address1, String address2, String city, String postcode, String country, String state) {

        if ((Utils.isEmptyOrNull(company)) && (Utils.isEmptyOrNull(firstName)) && (Utils.isEmptyOrNull(lastName))
                && (Utils.isEmptyOrNull(email)) && (Utils.isEmptyOrNull(telephone)) && (Utils.isEmptyOrNull(fax))
                && (Utils.isEmptyOrNull(password)) && (Utils.isEmptyOrNull(address1)) && (Utils.isEmptyOrNull(address2))
                && (Utils.isEmptyOrNull(city)) && (Utils.isEmptyOrNull(postcode))&& (Utils.isEmptyOrNull(countryId))
                && (Utils.isEmptyOrNull(stateId))) {

            SnackNotify.showMessage(getString(R.string.empty_all_fields), mainContainer);
            return false;
        }
        return true;
    }

 /*   private void getIntentData() {

        Intent intent = getActivity().getIntent();

        if (intent.hasExtra(ConstIntent.KEY_REGISTER)) {
            String data = intent.getStringExtra(ConstIntent.KEY_REGISTER);

            if (data.equals(ConstIntent.KEY_FROM_REGISTER)) {
                isComeFromRegister = true;
            } else if (data.equals(ConstIntent.KEY_FROM_CHECKOUT)) {
                isComeFromRegister = false;
            }

        }
    }*/


  /*  private boolean validation(String address1, String address2, String city, String postcode, String country, String state) {

        if (Utils.isEmptyOrNull(company)) {
            SnackNotify.showMessage(getString(R.string.empty_company), mainContainer);
            return false;
        } else if (Utils.isEmptyOrNull(firstName)) {
            SnackNotify.showMessage(getString(R.string.empty_first_name), mainContainer);
            return false;
        } else if (Utils.isEmptyOrNull(lastName)) {
            SnackNotify.showMessage(getString(R.string.empty_last_name), mainContainer);
            return false;
        } else if (Utils.isEmptyOrNull(email)) {
            SnackNotify.showMessage(getString(R.string.empty_email), mainContainer);
            return false;
        } else if (Utils.isEmptyOrNull(telephone)) {
            SnackNotify.showMessage(getString(R.string.empty_telephone), mainContainer);
            return false;
        } else if (Utils.isEmptyOrNull(fax)) {
            SnackNotify.showMessage(getString(R.string.empty_fax), mainContainer);
            return false;
        } else if (Utils.isEmptyOrNull(password)) {
            SnackNotify.showMessage(getString(R.string.empty_password), mainContainer);
            return false;
        } else if (Utils.isEmptyOrNull(password)) {
            SnackNotify.showMessage(getString(R.string.empty_confirm_pass), mainContainer);
            return false;
        } else if (!password.equals(password)) {
            SnackNotify.showMessage(getString(R.string.error_password), mainContainer);
            return false;
        } else if (Utils.isEmptyOrNull(address1)) {
            SnackNotify.showMessage(getString(R.string.empty_address1), mainContainer);
            return false;
        } else if (Utils.isEmptyOrNull(address2)) {
            SnackNotify.showMessage(getString(R.string.empty_address2), mainContainer);
            return false;
        } else if (Utils.isEmptyOrNull(city)) {
            SnackNotify.showMessage(getString(R.string.empty_city), mainContainer);
            return false;
        } else if (Utils.isEmptyOrNull(postcode)) {
            SnackNotify.showMessage(getString(R.string.empty_postalcode), mainContainer);
            return false;
        } else if (Utils.isEmptyOrNull(country)) {
            SnackNotify.showMessage(getString(R.string.empty_country), mainContainer);
            return false;
        } else if (Utils.isEmptyOrNull(state)) {
            SnackNotify.showMessage(getString(R.string.empty_state), mainContainer);
            return false;
        }
        return true;
    }*/


    private void setCountyAdapter() {
        adapterSpinnerCountry = new AdapterSpinner(getActivity(), R.layout.spinner_selected_view, arrayListCountry);
        spinnerCountry.setAdapter(adapterSpinnerCountry);
    }

    private void setStateAdapter() {
        adapterSpinnerState = new AdapterSpinner(getActivity(), R.layout.spinner_selected_view, arrayListState);
        spinnerState.setAdapter(adapterSpinnerState);
    }

    /*-----------------------------------Public Method---------------------------------------*/

    public void getLastFragmentData(JSONObject obj) {
        if (obj != null) {

            try {
                company = obj.getString(Const.PARAM_COMPANY);
                firstName = obj.getString(Const.PARAM_FIRSTNAME);
                lastName = obj.getString(Const.PARAM_LASTNAME);
                email = obj.getString(Const.PARAM_EMAIL);
                fax = obj.getString(Const.PARAM_FAX);
                telephone = obj.getString(Const.PARAM_TELEPHONE);
                password = obj.getString(Const.PARAM_PASSWORD);

            } catch (JSONException ex) {
                ex.printStackTrace();
            }
        }
    }

    /*-----------------------------------Click-----------------------------------------------*/

    @OnItemSelected(R.id.spinnerCountry)
    void onItemCountrySelected(int position) {
        if (position != 0) {
            countryId = String.valueOf(arrayListCountry.get(position).get(Const.KEY_ID));
            spinnerCountry.setSelection(arrayListCountry.indexOf(countryId));
            selectedCountryName = String.valueOf(arrayListCountry.indexOf(countryId));
            callStateId(countryId);
        }
    }

    @OnItemSelected(R.id.spinnerState)
    void onItemStateSelected(int position) {
        if (position != 0) {
            stateId = String.valueOf(arrayListState.get(position).get(Const.KEY_ID));
            spinnerCountry.setSelection(arrayListCountry.indexOf(stateId));
            selectedStateName = String.valueOf(arrayListCountry.indexOf(stateId));
        }
    }

    @OnClick(R.id.btnRegister)
    public void onRegisterClick() {

        getData();
    }
}
