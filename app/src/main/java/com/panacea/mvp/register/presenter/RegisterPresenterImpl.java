package com.panacea.mvp.register.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.commonMvp.model.CommonModel;
import com.panacea.mvp.login.model.LoginResponseModel;
import com.panacea.mvp.login.presenter.LoginContractor;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 06-12-2017.
 */

public class RegisterPresenterImpl implements RegisterContractor.Presenter {

    RegisterContractor.RegisterView registerView;
    Activity activity;
    JSONObject jsonObject;

    public RegisterPresenterImpl(Activity activity,RegisterContractor.RegisterView registerView) {
        this.registerView = registerView;
        this.activity = activity;
    }

    @Override
    public void getRegister(JSONObject jsonObject) {

        try {
            ApiAdapter.getInstance(activity);
            callRegister(jsonObject);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            registerView.onInternetError();
        }
    }

    private void callRegister(JSONObject jsonObject) {
        Progress.start(activity);


        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<LoginResponseModel> getLoginOutput = ApiAdapter.getApiService().register("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    LoginResponseModel loginResponseModel = response.body();
                    String message = "Hello";

                    if (loginResponseModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        registerView.getRegisterSuccess(loginResponseModel.getData());
                    } else {
                        registerView.apiUnsuccess(loginResponseModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    registerView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                Progress.stop();
                registerView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}
