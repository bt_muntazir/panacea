package com.panacea.mvp.register.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.adapter.model.CountrySpinnerModel;
import com.panacea.common.adapter.model.StateSpinnerModel;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.login.model.LoginResponseModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 07-12-2017.
 */

public class CountyrStatePresenterImpl implements RegisterContractor.CountryState {

    RegisterContractor.CountryStateView countryStateView;
    Activity activity;
    JSONObject jsonObject;


    public CountyrStatePresenterImpl(RegisterContractor.CountryStateView countryStateView, Activity activity) {

        this.countryStateView = countryStateView;
        this.activity = activity;

    }

    @Override
    public void getCountry() {
        try {
            ApiAdapter.getInstance(activity);
            getCountryList();
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            countryStateView.countryInternetError();
        }
    }

    @Override
    public void getState(String countryId) {
        try {
            ApiAdapter.getInstance(activity);
            getStateList(countryId);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            countryStateView.StateInternetError();
        }
    }

    private void getCountryList() {

        Progress.start(activity);

        Call<CountrySpinnerModel> getCountryOutput = ApiAdapter.getApiService().getCountry();


        getCountryOutput.enqueue(new Callback<CountrySpinnerModel>() {
            @Override
            public void onResponse(Call<CountrySpinnerModel> call, Response<CountrySpinnerModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    CountrySpinnerModel countrySpinnerModel = response.body();
                    String message = "Hello";

                    if (countrySpinnerModel.getStatus().equals("1")) {
                        countryStateView.getCountrySuccess(countrySpinnerModel.getData());
                    } else {
                        countryStateView.getCountryUnSucess("Login Fail");
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    countryStateView.getCountryUnSucess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CountrySpinnerModel> call, Throwable t) {
                Progress.stop();
                countryStateView.getCountryUnSucess(activity.getString(R.string.error_server));
            }
        });
    }

    private void getStateList(String countryId) {

        Progress.start(activity);

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_COUNTRY_ID, countryId);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<StateSpinnerModel> getStateOutput = ApiAdapter.getApiService().getState("application/json", "no-cache", body);

        getStateOutput.enqueue(new Callback<StateSpinnerModel>() {
            @Override
            public void onResponse(Call<StateSpinnerModel> call, Response<StateSpinnerModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    StateSpinnerModel countrySpinnerModel = response.body();
                    String message = "Hello";

                    if (countrySpinnerModel.getStatus().equals("1")) {
                        countryStateView.getStateSuccess(countrySpinnerModel.getData());
                    } else {
                        countryStateView.getStateUnSucess("Login Fail");
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    countryStateView.getStateUnSucess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<StateSpinnerModel> call, Throwable t) {
                Progress.stop();
                countryStateView.getStateUnSucess(activity.getString(R.string.error_server));
            }
        });
    }



}
