package com.panacea.mvp.register.fragment;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;

import com.panacea.R;
import com.panacea.common.requestresponse.Const;
import com.panacea.common.utility.SnackNotify;
import com.panacea.common.utility.Utils;
import com.panacea.mvp.register.RegisterActivity;
import com.panacea.mvp.register.presenter.RegisterPresenterImpl;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class PersonalDetailFragment extends Fragment {

    @BindView(R.id.mainContainer)
    FrameLayout mainContainer;

    @BindView(R.id.btnContinue)
    Button btnContinue;

    @BindView(R.id.edtTextCompany)
    AppCompatEditText edtTextCompany;

    @BindView(R.id.edtTextFirstName)
    AppCompatEditText edtTextFirstName;

    @BindView(R.id.edtTextLastName)
    AppCompatEditText edtTextLastName;

    @BindView(R.id.edtTextEmail)
    AppCompatEditText edtTextEmail;

    @BindView(R.id.edtTextTelephone)
    AppCompatEditText edtTextTelephone;

    @BindView(R.id.edtTextFax)
    AppCompatEditText edtTextFax;

    @BindView(R.id.edtTextPassword)
    AppCompatEditText edtTextPassword;

    @BindView(R.id.edtTextConfirmPassword)
    AppCompatEditText edtTextConfirmPassword;


    // TODO: Rename parameter arguments, choose names that match
    private static final String ARG_PARAM1 = "param1";

    public PersonalDetailFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static PersonalDetailFragment newInstance(int param1) {
        PersonalDetailFragment fragment = new PersonalDetailFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_personal_detail, container, false);

        ButterKnife.bind(this, view);

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        //registerPresenterImpl = new RegisterPresenterImpl(getActivity(), this);
    }

    private void getData() {
        // hide keyboard
        Utils.hideKeyboardIfOpen(getActivity());

        String company = edtTextCompany.getText().toString();
        String firstname = edtTextFirstName.getText().toString();
        String lastname = edtTextLastName.getText().toString();
        String email = edtTextEmail.getText().toString();
        String phone = edtTextTelephone.getText().toString();
        String fax = edtTextFax.getText().toString();
        String password = edtTextPassword.getText().toString();
        String confirmpassword = edtTextConfirmPassword.getText().toString();

        if (validation(company, firstname, lastname, email, phone, fax, password, confirmpassword)) {

            ((RegisterActivity) getActivity()).changeViewToRegistration();

            JSONObject obj = new JSONObject();

            try {
                obj = new JSONObject();
                obj.put(Const.PARAM_COMPANY, company);
                obj.put(Const.PARAM_FIRSTNAME, firstname);
                obj.put(Const.PARAM_LASTNAME, lastname);
                obj.put(Const.PARAM_EMAIL, email);
                obj.put(Const.PARAM_FAX, fax);
                obj.put(Const.PARAM_TELEPHONE, phone);
                obj.put(Const.PARAM_PASSWORD, password);

            } catch (JSONException ex) {
                ex.printStackTrace();
            }


            if (getActivity() instanceof RegisterActivity) {
                ((RegisterActivity) getActivity()).setJsonObjectPersonalDetail(obj);
            }
        }
    }

    private boolean validation(String company, String firstname, String lastname, String email, String phone, String fax, String password, String confirmpassword) {

        if ((Utils.isEmptyOrNull(company)) && (Utils.isEmptyOrNull(lastname)) && (Utils.isEmptyOrNull(firstname)) &&
                (Utils.isEmptyOrNull(email)) && (Utils.isEmptyOrNull(phone))
                && (Utils.isEmptyOrNull(fax)) && (Utils.isEmptyOrNull(password)) && (Utils.isEmptyOrNull(confirmpassword))) {

            SnackNotify.showMessage(getString(R.string.empty_all_fields), mainContainer);
            return false;
        }
        return true;
    }

   /* private boolean validation(String company, String firstname, String lastname, String email, String phone, String fax, String password, String confirmpassword) {

        if (Utils.isEmptyOrNull(company)) {
            SnackNotify.showMessage(getString(R.string.empty_company), mainContainer);
            return false;
        } else if (Utils.isEmptyOrNull(firstname)) {
            SnackNotify.showMessage(getString(R.string.empty_first_name), mainContainer);
            return false;
        } else if (Utils.isEmptyOrNull(lastname)) {
            SnackNotify.showMessage(getString(R.string.empty_last_name), mainContainer);
            return false;
        } else if (Utils.isEmptyOrNull(email)) {
            SnackNotify.showMessage(getString(R.string.empty_email), mainContainer);
            return false;
        } else if (Utils.isEmptyOrNull(phone)) {
            SnackNotify.showMessage(getString(R.string.empty_telephone), mainContainer);
            return false;
        } else if (Utils.isEmptyOrNull(fax)) {
            SnackNotify.showMessage(getString(R.string.empty_fax), mainContainer);
            return false;
        } else if (Utils.isEmptyOrNull(password)) {
            SnackNotify.showMessage(getString(R.string.empty_password), mainContainer);
            return false;
        } else if (Utils.isEmptyOrNull(confirmpassword)) {
            SnackNotify.showMessage(getString(R.string.empty_confirm_pass), mainContainer);
            return false;
        } else if (!password.equals(confirmpassword)) {
            SnackNotify.showMessage(getString(R.string.error_password), mainContainer);
            return false;
        }
        return true;
    }*/


    @OnClick(R.id.btnContinue)
    public void onRegisterClick() {

        getData();
    }

}
