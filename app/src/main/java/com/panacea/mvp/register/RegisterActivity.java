package com.panacea.mvp.register;

import android.content.Context;
import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.common.utility.SnackNotify;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.checkout.billing_detail.BillingDetailActivity;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.login.LoginActivity;
import com.panacea.mvp.register.adapter.RegisterPagerAdapter;
import com.panacea.mvp.register.fragment.PersonalDetailFragment;
import com.panacea.mvp.register.fragment.AddressFragment;
import com.panacea.mvp.splash.SplashActivity;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegisterActivity extends AppCompatActivity {

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

    @BindView(R.id.txtViewSignIn)
    TextView txtViewSignIn;

    @BindView(R.id.linLayoutBottom)
    LinearLayout linLayoutBottom;

    JSONObject jsonObjectPersonalDetail;

    DBHelper dbHelper;

    boolean isComeFromRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        ButterKnife.bind(this);
        getIntentData();

        hideShowLayout();

        dbHelper = new DBHelper(this);
        setTitle();

        setTab();
    }

    @Override
    protected void onResume() {
        super.onResume();

        frameLayCart.setVisibility(View.GONE);
        updateCartQty();
    }

    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (dbHelper.getCartCount() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            }
        }
    }

    private void hideShowLayout() {

        if (isComeFromRegister) {
            linLayoutBottom.setVisibility(View.VISIBLE);
        } else {
            linLayoutBottom.setVisibility(View.GONE);
        }
    }

    private void getIntentData() {

        Intent intent = getIntent();

        if (intent.hasExtra(ConstIntent.KEY_REGISTER)) {
            String data = intent.getStringExtra(ConstIntent.KEY_REGISTER);

            if (data.equals(ConstIntent.KEY_FROM_REGISTER)) {
                isComeFromRegister = true;
            } else if (data.equals(ConstIntent.KEY_FROM_CHECKOUT)) {
                isComeFromRegister = false;
            }

        }
    }


    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }


    private void setTab() {

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.title_company)));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.title_address)));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        final RegisterPagerAdapter registerPagerAdapter = new RegisterPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount(), this);
        viewPager.setAdapter(registerPagerAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                // Log.e("pos", "" + tab.getPosition());
                viewPager.setCurrentItem(tab.getPosition());

                if (tab.getPosition() == 0) {
                    tabLayout.setBackgroundResource(R.mipmap.ic_tab1_selected);
                } else if (tab.getPosition() == 1) {
                    tabLayout.setBackgroundResource(R.mipmap.ic_tab2_selected);
                }
                //tabLayout.setBackground();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setTitle() {
        txtViewTitle.setText(getString(R.string.title_register));
    }


    public void changeViewToRegistration() {

        viewPager.setCurrentItem(1);
    }

    public void finishCurrentActivity() {

        if (isComeFromRegister) {
            Intent intent = new Intent(RegisterActivity.this, DashboardActivity.class);
            startActivity(intent);
            finish();
        }
        else{
            Intent intent = new Intent(RegisterActivity.this, BillingDetailActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public JSONObject getJsonObjectPersonalDetail() {
        return jsonObjectPersonalDetail;
    }

    public void setJsonObjectPersonalDetail(JSONObject jsonObjectPersonalDetail) {
        this.jsonObjectPersonalDetail = jsonObjectPersonalDetail;
    }

    @OnClick(R.id.imgBackButton)
    public void backPressed() {
        onBackPressed();
    }

    @OnClick(R.id.txtViewSignIn)
    public void clickSignIn() {
        finish();
    }
}
