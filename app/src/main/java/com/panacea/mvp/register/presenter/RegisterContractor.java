package com.panacea.mvp.register.presenter;

import com.panacea.common.adapter.model.CountrySpinnerModel;
import com.panacea.common.adapter.model.StateSpinnerModel;
import com.panacea.mvp.commonMvp.BaseView;
import com.panacea.mvp.login.model.LoginResponseModel;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Braintech on 06-12-2017.
 */

public class RegisterContractor {

    public interface RegisterView extends BaseView {

        void getRegisterSuccess(LoginResponseModel.Data loginResponseModel);
    }

    public interface CountryState{

        void getCountry();
        void getState(String country);
    }

    public interface Presenter {
        void getRegister(JSONObject jsonObject);
    }

    public interface CountryStateView{
        void getCountrySuccess(ArrayList<CountrySpinnerModel.Datum> arrayListCountry);
        void getCountryUnSucess(String message);
        void countryInternetError();
        void countryServerError(String message);

        void getStateSuccess(ArrayList<StateSpinnerModel.Datum> arrayListState);
        void getStateUnSucess(String message);
        void StateInternetError();
        void StateServerError(String message);
    }
}
