package com.panacea.mvp.contactus_sendmessage.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.commonMvp.model.CommonModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 18-12-2017.
 */

public class ContactUsPresenterImpl implements ContactUsContractor.Presenter {

    ContactUsContractor.ContactUsView contactUsView;
    Activity activity;
    JSONObject jsonObject;

    public ContactUsPresenterImpl(ContactUsContractor.ContactUsView contactUsView, Activity activity) {
        this.contactUsView = contactUsView;
        this.activity = activity;
    }

    @Override
    public void getContactsData(JSONObject jsonObject) {
        try {
            ApiAdapter.getInstance(activity);
            getContacts(jsonObject);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            contactUsView.onInternetError();
        }
    }

    private void getContacts(JSONObject jsonObject) {

        Progress.start(activity);

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CommonModel> getOutput = ApiAdapter.getApiService().getContacts("application/json", "no-cache", body);

        getOutput.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    CommonModel commonModel = response.body();

                    if (commonModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        contactUsView.getContactApiSuccess(commonModel.getMessage());
                    } else {
                        contactUsView.apiUnsuccess(commonModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    contactUsView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                Progress.stop();
                contactUsView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }

}

