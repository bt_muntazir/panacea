package com.panacea.mvp.contactus_sendmessage;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.adapter.model.CountrySpinnerModel;
import com.panacea.common.adapter.model.StateSpinnerModel;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.requestresponse.Const;
import com.panacea.common.utility.SnackNotify;
import com.panacea.common.utility.Utils;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.commonMvp.adapter.AdapterSpinner;
import com.panacea.mvp.contactus_sendmessage.presenter.ContactUsContractor;
import com.panacea.mvp.contactus_sendmessage.presenter.ContactUsPresenterImpl;
import com.panacea.mvp.register.RegisterActivity;
import com.panacea.mvp.register.presenter.CountyrStatePresenterImpl;
import com.panacea.mvp.register.presenter.RegisterContractor;
import com.panacea.mvp.register.presenter.RegisterPresenterImpl;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnItemSelected;

public class ContactUsSendMessageActivity extends AppCompatActivity implements ContactUsContractor.ContactUsView, RegisterContractor.CountryStateView {

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.edtTextYourName)
    AppCompatEditText edtTextYourName;

    @BindView(R.id.edtTextCompanyName)
    AppCompatEditText edtTextCompanyName;

    @BindView(R.id.edtTextCity)
    AppCompatEditText edtTextCity;

    @BindView(R.id.edtTextPostalCode)
    AppCompatEditText edtTextPostalCode;

    @BindView(R.id.edtTextStreetAddress)
    AppCompatEditText edtTextStreetAddress;

    @BindView(R.id.edtTextEmail)
    AppCompatEditText edtTextEmail;

    @BindView(R.id.edtTextPhoneNo)
    AppCompatEditText edtTextPhoneNo;

    @BindView(R.id.edtTextEnquiry)
    AppCompatEditText edtTextEnquiry;

    @BindView(R.id.linLayCountry)
    LinearLayout linLayCountry;

    @BindView(R.id.linLayState)
    LinearLayout linLayState;

    @BindView(R.id.spinnerCountry)
    AppCompatSpinner spinnerCountry;

    @BindView(R.id.spinnerState)
    AppCompatSpinner spinnerState;

    /*toolbar*/
    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.btnContinue)
    Button btnContinue;

    @BindView(R.id.radio0)
    RadioButton radio0;

    @BindView(R.id.radio1)
    RadioButton radio1;

    @BindView(R.id.radio2)
    RadioButton radio2;

    CountyrStatePresenterImpl spinnerValuePresenterImpl;

    ContactUsPresenterImpl contactUsPresenter;

    ArrayList<HashMap<String, String>> arrayListCountry;
    ArrayList<HashMap<String, String>> arrayListState;

    AdapterSpinner adapterSpinnerCountry;
    AdapterSpinner adapterSpinnerState;

    JSONObject jsonObject;

    String countryId = "-1";
    String stateId = "-1";
    String selectedStateName;
    String selectedCountryName;
    String selectedRadioButtonText;

    DBHelper dbHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us_send_message);
        ButterKnife.bind(this);

        dbHelper = new DBHelper(this);

        contactUsPresenter = new ContactUsPresenterImpl(this, this);
        spinnerValuePresenterImpl = new CountyrStatePresenterImpl(this, this);

        setTitle();
        setInitialAdapter();
        callCountryId();
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateCartQty();
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onInternetError() {
        SnackNotify.checkConnection(onretryContactusApi, coordinateLayout);
    }

    OnClickInterface onretryContactusApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getContactApiSuccess(String message) {

        SnackNotify.showMessage(message, coordinateLayout);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 2000);

    }

    @Override
    public void getCountrySuccess(ArrayList<CountrySpinnerModel.Datum> arrayListCountryList) {
        for (int i = 0; i < arrayListCountryList.size(); i++) {
            CountrySpinnerModel.Datum datum = arrayListCountryList.get(i);
            String countryId = datum.getCountryId();
            String countryName = datum.getName();

            HashMap hashMap = new HashMap();
            hashMap.put(Const.KEY_ID, countryId);
            hashMap.put(Const.KEY_NAME, countryName);
            arrayListCountry.add(hashMap);
            setCountyAdapter();
        }
    }

    @Override
    public void getCountryUnSucess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void countryInternetError() {
        SnackNotify.checkConnection(onretryCountryApi, coordinateLayout);
    }

    OnClickInterface onretryCountryApi = new OnClickInterface() {
        @Override
        public void onClick() {
            callCountryId();
        }
    };

    @Override
    public void countryServerError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getStateSuccess(ArrayList<StateSpinnerModel.Datum> arrayListStateList) {

        for (int i = 0; i < arrayListStateList.size(); i++) {
            StateSpinnerModel.Datum datum = arrayListStateList.get(i);
            String stateId = datum.getZoneId();
            String stateName = datum.getName();

            HashMap hashMap = new HashMap();
            hashMap.put(Const.KEY_ID, stateId);
            hashMap.put(Const.KEY_NAME, stateName);
            arrayListState.add(hashMap);

            setStateAdapter();
        }
    }

    @Override
    public void getStateUnSucess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void StateInternetError() {
        SnackNotify.checkConnection(onretryStateApi, coordinateLayout);
    }

    OnClickInterface onretryStateApi = new OnClickInterface() {
        @Override
        public void onClick() {
            callStateId(countryId);
        }
    };


    @Override
    public void StateServerError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

     /*--------------------------------Private Method--------------------------------------*/

    private void setTitle() {
        txtViewTitle.setText(getString(R.string.title_send_us_message));
    }

    private void callCountryId() {
        spinnerValuePresenterImpl.getCountry();
    }

    private void callStateId(String countryId) {
        spinnerValuePresenterImpl.getState(countryId);
    }

    private void setInitialAdapter() {
        arrayListCountry = new ArrayList<>();
        HashMap hashMapFirstIndexCountry = new HashMap();
        hashMapFirstIndexCountry.put(Const.KEY_ID, 0);
        hashMapFirstIndexCountry.put(Const.KEY_NAME, getString(R.string.hint_country));
        arrayListCountry.add(hashMapFirstIndexCountry);

        arrayListState = new ArrayList<>();
        HashMap hashMapFirstIndexState = new HashMap();
        hashMapFirstIndexState.put(Const.KEY_ID, 0);
        hashMapFirstIndexState.put(Const.KEY_NAME, getString(R.string.hint_select_state));
        arrayListState.add(hashMapFirstIndexState);

        setCountyAdapter();
        setStateAdapter();
    }

    private void setCountyAdapter() {
        adapterSpinnerCountry = new AdapterSpinner(this, R.layout.spinner_selected_view, arrayListCountry);
        spinnerCountry.setAdapter(adapterSpinnerCountry);
    }

    private void setStateAdapter() {
        adapterSpinnerState = new AdapterSpinner(this, R.layout.spinner_selected_view, arrayListState);
        spinnerState.setAdapter(adapterSpinnerState);
    }

    private void getData() {
        // hide keyboard
        Utils.hideKeyboardIfOpen(this);

        String yourName = edtTextYourName.getText().toString();
        String companyName = edtTextCompanyName.getText().toString();
        String city = edtTextCity.getText().toString();
        String postalcode = edtTextPostalCode.getText().toString();

        String streetAddress = edtTextStreetAddress.getText().toString();
        String email = edtTextEmail.getText().toString();
        String phoneNo = edtTextPhoneNo.getText().toString();
        String enquiry = edtTextEnquiry.getText().toString();


        if (validation(yourName, companyName, city, postalcode, streetAddress, email, phoneNo, enquiry, countryId, stateId)) {

            try {
                jsonObject = new JSONObject();
                jsonObject.put(Const.PARAM_NAME, yourName);
                jsonObject.put(Const.PARAM_COMPANY_NAME, companyName);
                jsonObject.put(Const.PARAM_CITY, city);
                jsonObject.put(Const.PARAM_ZIP, postalcode);
                jsonObject.put(Const.PARAM_ADDRESS, streetAddress);
                jsonObject.put(Const.PARAM_EMAIL, email);
                jsonObject.put(Const.PARAM_PHONE, phoneNo);
                jsonObject.put(Const.PARAM_ENQUIRY, enquiry);
                jsonObject.put(Const.PARAM_COUNTRY_ID, countryId);
                jsonObject.put(Const.PARAM_ZONE_ID, stateId);
                jsonObject.put(Const.PARAM_CONTACT, selectedRadioButtonText);


            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            // call login api
            contactUsPresenter.getContactsData(jsonObject);
        }

    }

    private boolean validation(String yourName, String companyName, String city, String postalcode, String streetAddress, String email, String phoneNo, String enquiry, String countryId, String stateId) {

        if ((Utils.isEmptyOrNull(yourName)) && (Utils.isEmptyOrNull(companyName)) && (Utils.isEmptyOrNull(postalcode))
                && (Utils.isEmptyOrNull(streetAddress)) && (Utils.isEmptyOrNull(email)) && (Utils.isEmptyOrNull(phoneNo))
                && (Utils.isEmptyOrNull(enquiry)) && (Utils.isEmptyOrNull(selectedCountryName)) && (Utils.isEmptyOrNull(selectedStateName))) {

            SnackNotify.showMessage(getString(R.string.empty_all_fields), coordinateLayout);
            return false;
        }
        return true;
    }

    /*------------------------------------public method--------------------------------------*/
    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }


     /*-----------------------------------Click-----------------------------------------------*/

    @OnClick(R.id.imgBackButton)
    public void onBackClick() {

        onBackPressed();
    }


    @OnItemSelected(R.id.spinnerCountry)
    void onItemCountrySelected(int position) {
        if (position != 0) {
            countryId = String.valueOf(arrayListCountry.get(position).get(Const.KEY_ID));
            spinnerCountry.setSelection(arrayListCountry.indexOf(countryId));
            selectedCountryName = String.valueOf(arrayListCountry.indexOf(countryId));
            callStateId(countryId);
        }
    }

    @OnItemSelected(R.id.spinnerState)
    void onItemStateSelected(int position) {
        if (position != 0) {
            stateId = String.valueOf(arrayListState.get(position).get(Const.KEY_ID));
            spinnerCountry.setSelection(arrayListCountry.indexOf(stateId));
            selectedStateName = String.valueOf(arrayListCountry.indexOf(stateId));
        }
    }

    @OnClick(R.id.btnContinue)
    public void btnContinueClick() {

        getData();
    }

    @OnCheckedChanged({R.id.radio0, R.id.radio1, R.id.radio2})
    public void onRadioButtonCheckChanged(CompoundButton button, boolean checked) {
        if (checked) {
            switch (button.getId()) {
                case R.id.radio0:
                    selectedRadioButtonText = getString(R.string.title_radio0);
                    break;
                case R.id.radio1:
                    selectedRadioButtonText = getString(R.string.title_radio1);
                    break;
                case R.id.radio2:
                    selectedRadioButtonText = getString(R.string.title_radio2);
                    break;
            }
        }
    }

    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (dbHelper.getCartCount() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            }
        }
    }
}
