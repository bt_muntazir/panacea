package com.panacea.mvp.contactus_sendmessage.presenter;

import com.panacea.mvp.commonMvp.BaseView;
import com.panacea.mvp.splash.model.SplashModel;

import org.json.JSONObject;

/**
 * Created by Braintech on 18-12-2017.
 */

public interface ContactUsContractor {

    public interface ContactUsView extends BaseView {

        void getContactApiSuccess(String message);
    }

    public interface Presenter {
        void getContactsData(JSONObject jsonObject);
    }
}

