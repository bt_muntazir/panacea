package com.panacea.mvp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.panacea.Config;
import com.panacea.R;

public class YouTubeActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener{


    YouTubePlayerView playerView1;
    YouTubePlayerView playerView2;
    private static final int RECOVERY_DIALOG_REQUEST = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_you_tube);
        playerView1 = (YouTubePlayerView) findViewById(R.id.player_view1);
        playerView1.initialize(Config.API_KEY, this);

        playerView2 = (YouTubePlayerView) findViewById(R.id.player_view2);
        playerView2.initialize(Config.API_KEY, this);
    }


    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
        if (!wasRestored) {

            youTubePlayer.cueVideo(Config.VIDEO_CODE1);
            youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            // initializes the YouTube player view<br />
            playerView1.initialize(Config.API_KEY, this);

            playerView2.initialize(Config.API_KEY, this);
        }
    }
}
