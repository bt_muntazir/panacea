package com.panacea.mvp.order_information;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.FontHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.requestresponse.Const;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.common.utility.SnackNotify;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.order_history.adapter.OrderHistoryAdapter;
import com.panacea.mvp.order_history.model.OrderHistoryModel;
import com.panacea.mvp.order_information.adapter.OrderInformationAdapter;
import com.panacea.mvp.order_information.model.OrderInformationModel;
import com.panacea.mvp.order_information.presenter.OrderInformationContractor;
import com.panacea.mvp.order_information.presenter.OrderInformationPresenterImpl;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OrderInformationActivity extends AppCompatActivity implements OrderInformationContractor.OrderInformationView {

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;


    @BindView(R.id.txtViewOrderId)
    AppCompatTextView txtViewOrderId;

    @BindView(R.id.txtViewPaymentMethod)
    AppCompatTextView txtViewPaymentMethod;

    @BindView(R.id.txtViewShippingMethod)
    AppCompatTextView txtViewShippingMethod;

    @BindView(R.id.txtViewDateAdded)
    AppCompatTextView txtViewDateAdded;

    @BindView(R.id.txtViewPaymentAddress)
    AppCompatTextView txtViewPaymentAddress;

    @BindView(R.id.txtViewShippingAddress)
    AppCompatTextView txtViewShippingAddress;

    @BindView(R.id.recyclerViewProducts)
    RecyclerView recyclerViewProducts;

    @BindView(R.id.txtViewOrderHistory)
    AppCompatTextView txtViewOrderHistory;

    /*@BindView(R.id.txtViewTitlePrice)
    AppCompatTextView txtViewTitlePrice;

    @BindView(R.id.txtViewEqual)
    AppCompatTextView txtViewEqual;

    @BindView(R.id.txtViewPrice)
    AppCompatTextView txtViewPrice;*/

    @BindView(R.id.tableRow)
    TableRow tableRow;

    /*@BindView(R.id.tableRowHistory)*/
    TableRow tableRowHistory;

    @BindView(R.id.tableLayout)
    TableLayout tableLayout;

    @BindView(R.id.tableLayoutHistory)
    TableLayout tableLayoutHistory;

    @BindView(R.id.linLayOrderHistoryHeading)
    LinearLayout linLayOrderHistoryHeading;

    OrderInformationModel.Data orderInfoData;

    OrderInformationPresenterImpl orderInformationPresenter;
    OrderInformationAdapter orderInformationAdapter;

    LinearLayoutManager linearLayoutManager;

    String orderId;

    String customerId;

    DBHelper dbHelper;

    boolean isFirstItem = true;

    ArrayList<OrderInformationModel.Data.Product> arrayListOrderInformationProducts;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_information);

        ButterKnife.bind(this);
        dbHelper = new DBHelper(this);
        customerId = LoginManager.getInstance().getUserData().getCustomerId();

        arrayListOrderInformationProducts = new ArrayList<>();
        orderInformationPresenter = new OrderInformationPresenterImpl(this, this);
        orderInformationAdapter = new OrderInformationAdapter(this, arrayListOrderInformationProducts);

        getOrderInfoDataApi();

        tableLayout.setColumnStretchable(0, true);
        tableLayout.setColumnStretchable(1, true);

        tableLayoutHistory.setColumnStretchable(0, true);
        tableLayoutHistory.setColumnStretchable(1, true);
        tableLayoutHistory.setColumnStretchable(2, true);
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateCartQty();
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onInternetError() {

        SnackNotify.checkConnection(onretryOrderHistoryApi, coordinateLayout);
    }

    OnClickInterface onretryOrderHistoryApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getOrderInfoDataApi();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getOrderInformationSuccess(OrderInformationModel.Data orderInformation) {

        orderInfoData = orderInformation;

        arrayListOrderInformationProducts = orderInformation.getProducts();

        //set toolbar title
        setTitle();
        //set the data to the view
        setData();
        setLayoutManager();
        tableLayout();
        setTableLayoutHistory();
    }

    /*------------------private method--------------------------------*/

    private void tableLayout() {

        ArrayList<OrderInformationModel.Data.Total> arrayListTotal = orderInfoData.getTotals();

        if (arrayListTotal != null && arrayListTotal.size() > 0) {

            for (int i = 0; i < arrayListTotal.size(); i++) {

                tableRow = new TableRow(this);
                tableRow.setVisibility(View.VISIBLE);

                View view = LayoutInflater.from(this).inflate(R.layout.item_order_info_table_row, null, false);
                AppCompatTextView title = (AppCompatTextView) view.findViewById(R.id.txtViewTitlePrice);
                AppCompatTextView price = (AppCompatTextView) view.findViewById(R.id.txtViewPrice);
                RelativeLayout linLayMain = (RelativeLayout) view.findViewById(R.id.linLayMain);

                if (i + 1 == arrayListTotal.size()) {

                    linLayMain.setBackgroundColor(ContextCompat.getColor(this, R.color.color_gray));
                }

                title.setText(arrayListTotal.get(i).getTitle() + " = ");
                price.setText(arrayListTotal.get(i).getText());

                tableRow.addView(view);

                tableLayout.addView(tableRow);
            }
        }
    }

    private void setTableLayoutHistory() {

        ArrayList<OrderInformationModel.Data.History> arrayListHistory = orderInfoData.getHistories();

        if (arrayListHistory != null && arrayListHistory.size() > 0) {

            tableLayoutHistory.setVisibility(View.VISIBLE);
            txtViewOrderHistory.setVisibility(View.VISIBLE);
            linLayOrderHistoryHeading.setVisibility(View.VISIBLE);

            for (int i = 0; i < arrayListHistory.size(); i++) {

                tableRow = new TableRow(this);

                TableRow.LayoutParams lp = new TableRow.LayoutParams(android.widget.TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);
                tableRow.setLayoutParams(lp);

                View view = LayoutInflater.from(this).inflate(R.layout.item_histoty_order_info_table_row, null, false);

                AppCompatTextView date = (AppCompatTextView) view.findViewById(R.id.txtViewDateAdded);
                AppCompatTextView status = (AppCompatTextView) view.findViewById(R.id.txtViewStatus);
                AppCompatTextView coments = (AppCompatTextView) view.findViewById(R.id.txtViewComents);
                RelativeLayout relLaymain = (RelativeLayout) view.findViewById(R.id.linLayMain);
                View lineView = (View) view.findViewById(R.id.lineView);

                lineView.setVisibility(View.VISIBLE);

                date.setText(arrayListHistory.get(i).getDateAdded());
                status.setText(arrayListHistory.get(i).getStatus());
                coments.setText(arrayListHistory.get(i).getComment());

                if (arrayListHistory.get(i).getStatus().equals(Const.PARAM_CANCELED)) {
                    status.setTextColor(ContextCompat.getColor(this, R.color.color_red));
                } else if (arrayListHistory.get(i).getStatus().equals(Const.PARAM_COMPLETE)) {
                    status.setTextColor(ContextCompat.getColor(this, R.color.color_green));
                } else {
                    status.setTextColor(ContextCompat.getColor(this, R.color.color_black));
                }

                tableRow.addView(view);

                tableLayoutHistory.addView(tableRow);
            }
        } else {
            tableLayoutHistory.setVisibility(View.GONE);
            txtViewOrderHistory.setVisibility(View.GONE);
            linLayOrderHistoryHeading.setVisibility(View.GONE);

        }
    }

    private void setLayoutManager() {
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewProducts.setLayoutManager(linearLayoutManager);
        recyclerViewProducts.setItemAnimator(new DefaultItemAnimator());
        recyclerViewProducts.setHasFixedSize(true);
        recyclerViewProducts.setNestedScrollingEnabled(false);
        setAdapter();
    }

    private void setAdapter() {
        orderInformationAdapter = new OrderInformationAdapter(this, arrayListOrderInformationProducts);
        recyclerViewProducts.setAdapter(orderInformationAdapter);
    }

    private void setTitle() {
        txtViewTitle.setText(orderInfoData.getPageTitle());
    }

    private void getOrderInfoDataApi() {

        Intent intent = getIntent();

        if (intent.hasExtra(ConstIntent.KEY_ORDER_ID)) {
            orderId = intent.getStringExtra(ConstIntent.KEY_ORDER_ID);
        }

        orderInformationPresenter.getOrderInformation(customerId, orderId);
    }

    private void setData() {

        String orderId = "(#" + orderInfoData.getOrderId() + ")";

        txtViewOrderId.setText(orderId);
        txtViewDateAdded.setText(orderInfoData.getDateAdded());
        txtViewPaymentMethod.setText(orderInfoData.getPaymentMethod());
        txtViewShippingMethod.setText(orderInfoData.getShippingMethod());

        txtViewPaymentAddress.setText(orderInfoData.getPaymentAddress());
        txtViewShippingAddress.setText(orderInfoData.getShippingAddress());

    }

    /*------------------public method--------------------------------*/

    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }

    /*------------------on click method--------------------------------*/

    @OnClick(R.id.imgBackButton)
    public void backPressed() {
        onBackPressed();
    }

    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (dbHelper.getCartCount() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            }
        }
    }


}
