package com.panacea.mvp.order_information.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.login.model.LoginResponseModel;
import com.panacea.mvp.order_history.presenter.OrderHistoryContractor;
import com.panacea.mvp.order_information.model.OrderInformationModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 21-12-2017.
 */

public class OrderInformationPresenterImpl implements OrderInformationContractor.Presenter{

    OrderInformationContractor.OrderInformationView orderInformationView;
    Activity activity;
    JSONObject jsonObject;

    public OrderInformationPresenterImpl(OrderInformationContractor.OrderInformationView orderInformationView, Activity activity) {
        this.orderInformationView = orderInformationView;
        this.activity = activity;
    }

    @Override
    public void getOrderInformation(String customerId,String orderId) {
        try {
            ApiAdapter.getInstance(activity);
            gettingOrderInfoData(customerId,orderId);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            orderInformationView.onInternetError();
        }
    }

    private void  gettingOrderInfoData(String customerId,String orderId){
        Progress.start(activity);

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_CUSTOMER_ID, customerId);
            jsonObject.put(Const.PARAM_ORDER_ID, orderId);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<OrderInformationModel> getLoginOutput = ApiAdapter.getApiService().orderInformation("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<OrderInformationModel>() {
            @Override
            public void onResponse(Call<OrderInformationModel> call, Response<OrderInformationModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    OrderInformationModel orderInformationModel = response.body();

                    if (orderInformationModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        orderInformationView.getOrderInformationSuccess(orderInformationModel.getData());
                    } else {
                        orderInformationView.apiUnsuccess(activity.getString(R.string.error_username_pass));
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    orderInformationView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<OrderInformationModel> call, Throwable t) {
                Progress.stop();
                orderInformationView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}
