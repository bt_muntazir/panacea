package com.panacea.mvp.order_information.adapter;

import android.app.Activity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.panacea.R;
import com.panacea.mvp.dashboard.adapter.SearchPartsAdapter;
import com.panacea.mvp.order_history.adapter.OrderHistoryAdapter;
import com.panacea.mvp.order_history.model.OrderHistoryModel;
import com.panacea.mvp.order_information.model.OrderInformationModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 21-12-2017.
 */

public class OrderInformationAdapter extends RecyclerView.Adapter {

    Activity activity;
    ArrayList<OrderInformationModel.Data.Product> raDataArrayListResult;

    public OrderInformationAdapter(Activity activity, ArrayList<OrderInformationModel.Data.Product> raDataArrayList) {
        this.activity = activity;
        this.raDataArrayListResult = raDataArrayList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_order_information, parent, false);
        return new OrderInformationAdapter.OrderInformationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final OrderInformationModel.Data.Product productData = raDataArrayListResult.get(position);

        String price = productData.getPrice() + " X " + productData.getQuantity() + " = ";

        double priceWithoutDollar = Double.valueOf(productData.getPrice().substring(1));



        //Picasso.with(activity).load(productData.).into(((OrderInformationAdapter.OrderInformationViewHolder) holder).imgProduct);
        ((OrderInformationAdapter.OrderInformationViewHolder) holder).txtViewHeadingTitle.setText(productData.getName());
        ((OrderInformationAdapter.OrderInformationViewHolder) holder).txtViewPartNo.setText(productData.getModel().toString());
        ((OrderInformationAdapter.OrderInformationViewHolder) holder).txtViewPrice.setText(price);
        ((OrderInformationAdapter.OrderInformationViewHolder) holder).txtViewTotalPrice.setText(productData.getTotal());


    }

    @Override
    public int getItemCount() {
        return raDataArrayListResult.size();
    }

    public class OrderInformationViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtViewHeadingTitle)
        AppCompatTextView txtViewHeadingTitle;

        @BindView(R.id.txtViewPartNo)
        AppCompatTextView txtViewPartNo;

        @BindView(R.id.txtViewPrice)
        AppCompatTextView txtViewPrice;

        @BindView(R.id.txtViewTotalPrice)
        AppCompatTextView txtViewTotalPrice;


        public OrderInformationViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
