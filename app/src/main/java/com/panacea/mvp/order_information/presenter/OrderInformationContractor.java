package com.panacea.mvp.order_information.presenter;

import com.panacea.mvp.commonMvp.BaseView;
import com.panacea.mvp.order_information.model.OrderInformationModel;

/**
 * Created by Braintech on 21-12-2017.
 */

public interface OrderInformationContractor {
    public interface OrderInformationView extends BaseView {

        void getOrderInformationSuccess(OrderInformationModel.Data orderInformation);
    }

    public interface Presenter {
        void getOrderInformation(String customerId,String orderId);
    }
}
