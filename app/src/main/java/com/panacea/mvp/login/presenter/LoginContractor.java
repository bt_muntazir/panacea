package com.panacea.mvp.login.presenter;

import com.panacea.mvp.commonMvp.BaseView;
import com.panacea.mvp.login.model.LoginResponseModel;

import java.util.ArrayList;

/**
 * Created by Braintech on 01-12-2017.
 */

public interface LoginContractor {
    public interface loginView extends BaseView {

        void getLoginSuccess(LoginResponseModel.Data loginModel);
    }

    public interface presenter {
        void getLogin(String userName,String password);
    }
}
