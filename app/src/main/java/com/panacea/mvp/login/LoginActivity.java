package com.panacea.mvp.login;

import android.content.Intent;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.AlertDialogHelper;
import com.panacea.common.helpers.FontHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.common.utility.SnackNotify;
import com.panacea.common.utility.Utils;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.contactus.ContactUsActivity;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.login.model.LoginResponseModel;
import com.panacea.mvp.login.presenter.LoginContractor;
import com.panacea.mvp.login.presenter.LoginPresenterImpl;

import com.panacea.mvp.pacoCreditApplication.PacoCreditActivity;
import com.panacea.mvp.register.RegisterActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AppCompatActivity implements LoginContractor.loginView {

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.edtTextUserName)
    AppCompatEditText edtTextUserName;

    @BindView(R.id.edtTextPassword)
    AppCompatEditText edtTextPassword;

    @BindView(R.id.btnSignIn)
    Button btnSignIn;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.btnRegister)
    Button btnRegister;

    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

    @BindView(R.id.btnCreditApplication)
            Button btnCreditApplication;

    LoginPresenterImpl loginPresenter;

    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        dbHelper=new DBHelper(this);
        setTitle();

        setFont();

        loginPresenter = new LoginPresenterImpl(this, this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        frameLayCart.setVisibility(View.GONE);
        updateCartQty();
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onInternetError() {
        SnackNotify.checkConnection(onretryLoginApi, coordinateLayout);
    }

    OnClickInterface onretryLoginApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getData();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
        //AlertDialogHelper.showMessage(this, message);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getLoginSuccess(LoginResponseModel.Data loginModel) {

        //SnackNotify.showMessage("Login Success", coordinateLayout);
        //create session
        LoginManager.getInstance().createLoginSession(loginModel);

        Intent intent = new Intent(LoginActivity.this, DashboardActivity.class);
        startActivity(intent);

        finish();
    }

    /*---------------------------------private method-------------------------------------------*/

    private void setFont(){

        FontHelper.setFontFace(txtViewTitle, FontHelper.FontType.FONT_REGULAR, this);

        FontHelper.setFontFace(btnSignIn, FontHelper.FontType.FONT_BOLD, this);
        FontHelper.setFontFace(btnRegister, FontHelper.FontType.FONT_THIN, this);
        FontHelper.setFontFace(btnCreditApplication, FontHelper.FontType.FONT_THIN, this);

    }   

    private void getData() {
        // hide keyboard
        Utils.hideKeyboardIfOpen(this);

        String userName = String.valueOf(edtTextUserName.getText());
        String password = String.valueOf(edtTextPassword.getText());

        if (validation(userName, password)) {

            // call login api
            loginPresenter.getLogin(userName, password);
        }
    }

    private boolean validation(String email, String password) {

        if (Utils.isEmptyOrNull(email)) {
            SnackNotify.showMessage(getString(R.string.empty_username),coordinateLayout);
            //edtTextUserName.setError(getString(R.string.empty_email));
            return false;
//        } else if (!Utils.isValidEmail(email)) {
//            edtTextUserName.setError(getString(R.string.error_email));
//            return false;
        } else if (Utils.isEmptyOrNull(password)) {
            SnackNotify.showMessage(getString(R.string.empty_password),coordinateLayout);
            //edtTextPassword.setError(getString(R.string.empty_password));
            return false;
        }
        return true;
    }

    private void setTitle(){
        txtViewTitle.setText(getString(R.string.title_login));
    }

    /*---------------------------public method------------------------------------------*/

    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }

    /*----------------------------------click method------------------------------------------*/

    @OnClick(R.id.imgBackButton)
    public void backPressed() {
        onBackPressed();
    }

    @OnClick(R.id.btnSignIn)
    public void onSignInClick() {

         getData();
    }

    @OnClick(R.id.btnCreditApplication)
    public void onCreditApplicationClick() {

        Intent intent = new Intent(LoginActivity.this, PacoCreditActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btnRegister)
    public void onClickRegister() {

        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        intent.putExtra(ConstIntent.KEY_REGISTER, ConstIntent.KEY_FROM_REGISTER);
        startActivity(intent);
    }
    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (dbHelper.getCartCount() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            }
        }
    }

}
