package com.panacea.mvp.login.presenter;

import android.app.Activity;
import android.provider.Settings;
import android.util.Log;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.common.utility.SnackNotify;
import com.panacea.mvp.login.model.LoginResponseModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 01-12-2017.
 */

public class LoginPresenterImpl implements LoginContractor.presenter{

    LoginContractor.loginView loginView;
    Activity activity;
    JSONObject jsonObject;

    public LoginPresenterImpl(LoginContractor.loginView loginView, Activity activity) {
        this.loginView = loginView;
        this.activity = activity;
    }

    @Override
    public void getLogin(String username, String password) {
        try {
            ApiAdapter.getInstance(activity);
            gettingResultOfLogin(username, password);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            loginView.onInternetError();
        }
    }

    private void gettingResultOfLogin(String username, String password) {
        Progress.start(activity);

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_EMAIL, username);
            jsonObject.put(Const.PARAM_PASSWORD, password);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<LoginResponseModel> getLoginOutput = ApiAdapter.getApiService().login("application/json", "no-cache", body);

        getLoginOutput.enqueue(new Callback<LoginResponseModel>() {
            @Override
            public void onResponse(Call<LoginResponseModel> call, Response<LoginResponseModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    LoginResponseModel loginModel = response.body();
                    String message = "Hello";

                    if (loginModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        loginView.getLoginSuccess(loginModel.getData());
                    } else {
                        loginView.apiUnsuccess(activity.getString(R.string.error_username_pass));
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    loginView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<LoginResponseModel> call, Throwable t) {
                Progress.stop();
                loginView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}
