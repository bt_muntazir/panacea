package com.panacea.mvp.productdetail.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Braintech on 14-12-2017.
 */

public class ProductDetailModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("heading_title")
        @Expose
        private String headingTitle;
        @SerializedName("product_id")
        @Expose
        private Integer productId;
        @SerializedName("manufacturer")
        @Expose
        private Object manufacturer;
        @SerializedName("manufacturers")
        @Expose
        private String manufacturers;
        @SerializedName("model")
        @Expose
        private String model;
        @SerializedName("reward")
        @Expose
        private Object reward;
        @SerializedName("points")
        @Expose
        private String points;
        @SerializedName("description")
        @Expose
        private String description;
        @SerializedName("stock")
        @Expose
        private String stock;
        @SerializedName("popup")
        @Expose
        private String popup;
        @SerializedName("thumb")
        @Expose
        private String thumb;
        @SerializedName("images")
        @Expose
        private ArrayList<Image> images = null;
        @SerializedName("price")
        @Expose
        private String price;
        @SerializedName("special")
        @Expose
        private Boolean special;
        @SerializedName("tax")
        @Expose
        private String tax;
        @SerializedName("discounts")
        @Expose
        private ArrayList<Object> discounts = null;
        @SerializedName("options")
        @Expose
        private ArrayList<Object> options = null;
        @SerializedName("minimum")
        @Expose
        private String minimum;
        @SerializedName("review_status")
        @Expose
        private String reviewStatus;
        @SerializedName("review_guest")
        @Expose
        private Boolean reviewGuest;
        @SerializedName("reviews")
        @Expose
        private String reviews;
        @SerializedName("rating")
        @Expose
        private Integer rating;
        @SerializedName("attribute_groups")
        @Expose
        private ArrayList<Object> attributeGroups = null;
        @SerializedName("products")
        @Expose
        private ArrayList<Object> products = null;
        @SerializedName("tags")
        @Expose
        private ArrayList<Tag> tags = null;
        @SerializedName("recurrings")
        @Expose
        private ArrayList<Object> recurrings = null;

        public String getHeadingTitle() {
            return headingTitle;
        }

        public void setHeadingTitle(String headingTitle) {
            this.headingTitle = headingTitle;
        }

        public Integer getProductId() {
            return productId;
        }

        public void setProductId(Integer productId) {
            this.productId = productId;
        }

        public Object getManufacturer() {
            return manufacturer;
        }

        public void setManufacturer(Object manufacturer) {
            this.manufacturer = manufacturer;
        }

        public String getManufacturers() {
            return manufacturers;
        }

        public void setManufacturers(String manufacturers) {
            this.manufacturers = manufacturers;
        }

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public Object getReward() {
            return reward;
        }

        public void setReward(Object reward) {
            this.reward = reward;
        }

        public String getPoints() {
            return points;
        }

        public void setPoints(String points) {
            this.points = points;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getStock() {
            return stock;
        }

        public void setStock(String stock) {
            this.stock = stock;
        }

        public String getPopup() {
            return popup;
        }

        public void setPopup(String popup) {
            this.popup = popup;
        }

        public String getThumb() {
            return thumb;
        }

        public void setThumb(String thumb) {
            this.thumb = thumb;
        }

        public ArrayList<Image> getImages() {
            return images;
        }

        public void setImages(ArrayList<Image> images) {
            this.images = images;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public Boolean getSpecial() {
            return special;
        }

        public void setSpecial(Boolean special) {
            this.special = special;
        }

        public String getTax() {
            return tax;
        }

        public void setTax(String tax) {
            this.tax = tax;
        }

        public ArrayList<Object> getDiscounts() {
            return discounts;
        }

        public void setDiscounts(ArrayList<Object> discounts) {
            this.discounts = discounts;
        }

        public ArrayList<Object> getOptions() {
            return options;
        }

        public void setOptions(ArrayList<Object> options) {
            this.options = options;
        }

        public String getMinimum() {
            return minimum;
        }

        public void setMinimum(String minimum) {
            this.minimum = minimum;
        }

        public String getReviewStatus() {
            return reviewStatus;
        }

        public void setReviewStatus(String reviewStatus) {
            this.reviewStatus = reviewStatus;
        }

        public Boolean getReviewGuest() {
            return reviewGuest;
        }

        public void setReviewGuest(Boolean reviewGuest) {
            this.reviewGuest = reviewGuest;
        }

        public String getReviews() {
            return reviews;
        }

        public void setReviews(String reviews) {
            this.reviews = reviews;
        }

        public Integer getRating() {
            return rating;
        }

        public void setRating(Integer rating) {
            this.rating = rating;
        }

        public ArrayList<Object> getAttributeGroups() {
            return attributeGroups;
        }

        public void setAttributeGroups(ArrayList<Object> attributeGroups) {
            this.attributeGroups = attributeGroups;
        }

        public ArrayList<Object> getProducts() {
            return products;
        }

        public void setProducts(ArrayList<Object> products) {
            this.products = products;
        }

        public ArrayList<Tag> getTags() {
            return tags;
        }

        public void setTags(ArrayList<Tag> tags) {
            this.tags = tags;
        }

        public ArrayList<Object> getRecurrings() {
            return recurrings;
        }

        public void setRecurrings(ArrayList<Object> recurrings) {
            this.recurrings = recurrings;
        }

        public class Image {

            @SerializedName("popup")
            @Expose
            private String popup;
            @SerializedName("thumb")
            @Expose
            private String thumb;

            public String getPopup() {
                return popup;
            }

            public void setPopup(String popup) {
                this.popup = popup;
            }

            public String getThumb() {
                return thumb;
            }

            public void setThumb(String thumb) {
                this.thumb = thumb;
            }

        }


        public class Tag {

            @SerializedName("tag")
            @Expose
            private String tag;
            @SerializedName("href")
            @Expose
            private String href;

            public String getTag() {
                return tag;
            }

            public void setTag(String tag) {
                this.tag = tag;
            }

            public String getHref() {
                return href;
            }

            public void setHref(String href) {
                this.href = href;
            }

        }

    }
}


