package com.panacea.mvp.productdetail;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.database.CartItem;
import com.panacea.common.database.CartOperations;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.FontHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.helpers.viewpagerindicator.CirclePageIndicator;
import com.panacea.common.interfaces.CartUpdate;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.common.utility.SnackNotify;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.dashboard.adapter.LedRadiatorAdapter;
import com.panacea.mvp.dashboard.adapter.SearchPartsAdapter;
import com.panacea.mvp.dashboard.adapter.SlidingImageAdapter;
import com.panacea.mvp.dashboard.model.RadiatorModel;
import com.panacea.mvp.edit_information.EditInformationActivity;
import com.panacea.mvp.productdetail.model.ProductDetailModel;
import com.panacea.mvp.productdetail.presenter.ProductDetailContractor;
import com.panacea.mvp.productdetail.presenter.ProductDetailImpl;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ProductDetailActivity extends AppCompatActivity implements ProductDetailContractor.ProductDetailView {


    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.txtViewHeadingTitle)
    AppCompatTextView txtViewHeadingTitle;

    @BindView(R.id.txtViewStock)
    AppCompatTextView txtViewStock;

    @BindView(R.id.txtViewPartNo)
    AppCompatTextView txtViewPartNo;

    @BindView(R.id.txtViewCountItem)
    AppCompatTextView txtViewCountItem;

    @BindView(R.id.txtViewPrice)
    AppCompatTextView txtViewPrice;

    @BindView(R.id.imgProduct)
    ImageView imgProduct;

    @BindView(R.id.webViewDescription)
    WebView webViewDescription;

    @BindView(R.id.imgViewPlus)
    ImageView imgViewPlus;

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.imgViewMinus)
    ImageView imgViewMinus;

    @BindView(R.id.btnAddCart)
    Button btnAddCart;

    @BindView(R.id.imageViewPager)
    ViewPager imageViewPager;

    @BindView(R.id.indicator)
    CirclePageIndicator indicator;

    ProductDetailImpl productDetail;

    ProductDetailModel.Data prDetailData;

    String productId;
    DBHelper dbHelper;
    int itemCount = 1;
    ArrayList<CartItem> getCartItems;

    ArrayList<String> arrayListSlideImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);

        ButterKnife.bind(this);

        dbHelper = new DBHelper(this);

        productDetail = new ProductDetailImpl(this, this);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            productId = extras.getString(ConstIntent.KEY_PRODUCT_ID);
        }

        //set the title
        setTitle();

        //set the font
        setFont();

        //call product detail api
        getProductDetailApi();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //frameLayCart.setVisibility(View.GONE);
        getCartItems = dbHelper.getCartItem();
        updateCartQty();
    }

    @Override
    public void getproductdetailSuccess(ProductDetailModel.Data proData) {

        prDetailData = proData;

        String thumbImage = prDetailData.getThumb();


        for (int i = 0; i < getCartItems.size(); i++) {

            if (getCartItems.get(i).getProductId().equals(String.valueOf(prDetailData.getProductId()))) {
                btnAddCart.setAlpha(0.5f);
                btnAddCart.setEnabled(false);
                itemCount = getCartItems.get(i).getProduct_quantity();
                break;
            } else {
                btnAddCart.setAlpha(1);
                btnAddCart.setEnabled(true);
            }
        }


        ArrayList<ProductDetailModel.Data.Image> arrayListAdditionalImage = proData.getImages();

        //Picasso.with(this).load(thumbImage).into(imgProduct);
        txtViewTitle.setText(prDetailData.getHeadingTitle());
        txtViewHeadingTitle.setText(prDetailData.getHeadingTitle());
        txtViewStock.setText(prDetailData.getStock());
        txtViewPartNo.setText(prDetailData.getModel());
        txtViewPrice.setText(prDetailData.getPrice());
        txtViewCountItem.setText(String.valueOf(itemCount));


        arrayListSlideImage = new ArrayList<>();

        if (thumbImage != null && thumbImage.length() > 0) {

            arrayListSlideImage.add(thumbImage);
        }


        if (arrayListAdditionalImage != null && arrayListAdditionalImage.size() > 0) {

            for (int i = 0; i < arrayListAdditionalImage.size(); i++) {
                arrayListSlideImage.add(arrayListAdditionalImage.get(i).getPopup());
            }
        }

        // bind image with sliding viewpager
        if (arrayListSlideImage.size() > 0) {
            SlidingImageAdapter slidingImageAdapter = new SlidingImageAdapter(this, arrayListSlideImage, "", true);
            imageViewPager.setAdapter(slidingImageAdapter);

        } else {
            SlidingImageAdapter slidingImageAdapter = new SlidingImageAdapter(this, new ArrayList<String>(), "", true);
            imageViewPager.setAdapter(slidingImageAdapter);
        }

        // set view pager indecator
        indicator.setViewPager(imageViewPager);

        // hide/show indecator
        if (arrayListSlideImage.size() == 1) {
            indicator.setVisibility(View.GONE);
        } else {
            indicator.setVisibility(View.VISIBLE);
        }

        WebSettings settings = webViewDescription.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        webViewDescription.loadData(prDetailData.getDescription(), "text/html; charset=utf-8", null);
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onInternetError() {
        SnackNotify.checkConnection(onretryProductDetailApi, coordinateLayout);
    }

    OnClickInterface onretryProductDetailApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getProductDetailApi();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }


/*----------------------------private method----------------------------------------*/

    private void setFont() {

        FontHelper.setFontFace(txtViewTitle, FontHelper.FontType.FONT_REGULAR, this);
    }


    private void getProductDetailApi() {
        productDetail.getProductDetail(productId);
    }

    private void setTitle() {
        txtViewTitle.setText("Product Details");
    }

    private void ShowDoalogForSuccess(String productName) {

        // Create custom dialog object
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.item_success_add_cart);
        dialog.getWindow().setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.alpha(Color.BLACK)));


// set values for custom dialog components - text, image and button
        TextView txtViewDescription = (TextView) dialog.findViewById(R.id.txtViewDescription);
        txtViewDescription.setText("You have added " + productName + " to your shopping cart.");
        ImageView imgViewCross = (ImageView) dialog.findViewById(R.id.imgViewCross);

        dialog.show();

        Button btnGotoCart = (Button) dialog.findViewById(R.id.btnGotoCart);
        Button btnContinueShopping = (Button) dialog.findViewById(R.id.btnContinueShopping);
// if decline button is clicked, close the custom dialog

        imgViewCross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnContinueShopping.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnGotoCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProductDetailActivity.this, ShoppingCartActivity.class);
                startActivity(intent);

                dialog.dismiss();
            }
        });
    }

    /*----------------------------public method----------------------------------------*/
    public CartItem getCartItems(ProductDetailModel.Data products) {
        CartItem cartItems = new CartItem();
        cartItems.setProductId(String.valueOf(products.getProductId()));
        cartItems.setProductName(products.getHeadingTitle());
        cartItems.setProduct_quantity(itemCount);
        return cartItems;
    }

    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }

    /*----------------------------click method----------------------------------------*/

    @OnClick(R.id.imgBackButton)
    public void onBackClick() {

        onBackPressed();
    }

    @OnClick(R.id.btnAddCart)
    public void btnAddCartClick() {

        CartUpdate cartUpdate = new CartUpdate() {
            @Override
            public void onCartUpdate(String type, int qty) {

                updateCartQty();
            }
        };
        CartItem cartItem = getCartItems(prDetailData);
        cartItem.setProductprice(prDetailData.getPrice());
        cartItem.setProduct_quantity(itemCount);

        CartOperations.updateDataBase(this, cartItem, true, CartOperations.TYPE_ADD_MULTIPLE_PROUCT, 10, cartUpdate);

        //Disable the click event
        btnAddCart.setAlpha(0.5f);
        btnAddCart.setEnabled(false);
        ShowDoalogForSuccess(prDetailData.getHeadingTitle());
    }

    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (getCartItems.size() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            }
        }
    }

    @OnClick(R.id.imgViewPlus)
    public void imgViewPlusClick() {
        itemCount += 1;

        txtViewCountItem.setText(String.valueOf(itemCount));

        //update the database
        dbHelper.updateCart(itemCount, String.valueOf(prDetailData.getProductId()));
    }

    @OnClick(R.id.imgViewMinus)
    public void imgViewMinusClick() {

        if (itemCount > 1) {
            itemCount -= 1;

            txtViewCountItem.setText(String.valueOf(itemCount));
            //update the database
            dbHelper.updateCart(itemCount - 1, String.valueOf(prDetailData.getProductId()));
        } else if (itemCount == 1) {

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);

            // Setting Dialog Message
            alertDialog.setMessage(this.getString(R.string.confirmation_remove_cart_item));
            alertDialog.setCancelable(true);

            // Setting Positive "Yes" Button
            alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                    dbHelper.deleteCartItem(String.valueOf(prDetailData.getProductId()));
                    finish();
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();

                }
            });

            // Showing Alert Message
            alertDialog.show();
        }
    }
}
