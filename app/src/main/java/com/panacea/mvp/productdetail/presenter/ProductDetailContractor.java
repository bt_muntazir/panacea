package com.panacea.mvp.productdetail.presenter;

import com.panacea.mvp.commonMvp.BaseView;
import com.panacea.mvp.login.model.LoginResponseModel;
import com.panacea.mvp.productdetail.model.ProductDetailModel;

/**
 * Created by Braintech on 14-12-2017.
 */

public interface ProductDetailContractor {

    public interface ProductDetailView extends BaseView {

        void getproductdetailSuccess(ProductDetailModel.Data proData);
    }

    public interface Presenter {
        void getProductDetail(String productId);
    }
}
