package com.panacea.mvp.productdetail.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.login.model.LoginResponseModel;
import com.panacea.mvp.login.presenter.LoginContractor;
import com.panacea.mvp.productdetail.model.ProductDetailModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 14-12-2017.
 */

public class ProductDetailImpl implements ProductDetailContractor.Presenter {

    ProductDetailContractor.ProductDetailView productDetailView;
    Activity activity;
    JSONObject jsonObject;

    public ProductDetailImpl(ProductDetailContractor.ProductDetailView productDetailView, Activity activity) {
        this.productDetailView = productDetailView;
        this.activity = activity;
    }

    @Override
    public void getProductDetail(String productId) {

        try {
            ApiAdapter.getInstance(activity);
            productDetail(productId);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            productDetailView.onInternetError();
        }
    }

    private void productDetail(String productId) {

        Progress.start(activity);

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_PRODUCT_ID, productId);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<ProductDetailModel> getProductDetailOutput = ApiAdapter.getApiService().productDetail("application/json", "no-cache", body);

        getProductDetailOutput.enqueue(new Callback<ProductDetailModel>() {
            @Override
            public void onResponse(Call<ProductDetailModel> call, Response<ProductDetailModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    ProductDetailModel productDetailModel = response.body();
                    String message = "Hello";

                    if (productDetailModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        productDetailView.getproductdetailSuccess(productDetailModel.getData());
                    } else {
                        productDetailView.apiUnsuccess("Login Fail");
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    productDetailView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<ProductDetailModel> call, Throwable t) {
                Progress.stop();
                productDetailView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}
