package com.panacea.mvp.order_history.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.login.model.LoginResponseModel;
import com.panacea.mvp.order_history.model.OrderHistoryModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 21-12-2017.
 */

public class OrderHistoryPresenterImpl implements OrderHistoryContractor.Presenter {

    OrderHistoryContractor.OrderHistoryView orderHistoryView;
    Activity activity;
    JSONObject jsonObject;

    public OrderHistoryPresenterImpl(OrderHistoryContractor.OrderHistoryView orderHistoryView, Activity activity) {
        this.orderHistoryView = orderHistoryView;
        this.activity = activity;
    }

    @Override
    public void getOrderHistory(String customerId,int page) {
        try {
            ApiAdapter.getInstance(activity);
            gettingOrderHistoryData(customerId,page);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            orderHistoryView.onInternetError();
        }
    }

    private void  gettingOrderHistoryData(String customerId,int page){
        Progress.start(activity);

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_CUSTOMER_ID, customerId);
            jsonObject.put(Const.PARAM_PAGE,page);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<OrderHistoryModel> getOrderHistoryOutput = ApiAdapter.getApiService().orderhistory("application/json", "no-cache", body);

        getOrderHistoryOutput.enqueue(new Callback<OrderHistoryModel>() {
            @Override
            public void onResponse(Call<OrderHistoryModel> call, Response<OrderHistoryModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    OrderHistoryModel orderHistoryModel = response.body();

                    if (orderHistoryModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        orderHistoryView.getOrderHistorySuccess(orderHistoryModel.getData());
                    } else {
                        orderHistoryView.apiUnsuccess(activity.getString(R.string.error_username_pass));
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    orderHistoryView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<OrderHistoryModel> call, Throwable t) {
                Progress.stop();
                orderHistoryView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }
}
