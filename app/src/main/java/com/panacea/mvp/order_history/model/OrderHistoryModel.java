package com.panacea.mvp.order_history.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Braintech on 21-12-2017.
 */


public class OrderHistoryModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("filter_data")
        @Expose
        private FilterData filterData;
        @SerializedName("orders")
        @Expose
        private ArrayList<Order> orders = null;

        public FilterData getFilterData() {
            return filterData;
        }

        public void setFilterData(FilterData filterData) {
            this.filterData = filterData;
        }

        public ArrayList<Order> getOrders() {
            return orders;
        }

        public void setOrders(ArrayList<Order> orders) {
            this.orders = orders;
        }

        public class FilterData {

            @SerializedName("customer_id")
            @Expose
            private String customerId;
            @SerializedName("page_title")
            @Expose
            private String pageTitle;
            @SerializedName("limit")
            @Expose
            private Integer limit;
            @SerializedName("page")
            @Expose
            private Integer page;
            @SerializedName("total_pagination")
            @Expose
            private Integer totalPagination;

            public String getCustomerId() {
                return customerId;
            }

            public void setCustomerId(String customerId) {
                this.customerId = customerId;
            }

            public String getPageTitle() {
                return pageTitle;
            }

            public void setPageTitle(String pageTitle) {
                this.pageTitle = pageTitle;
            }

            public Integer getLimit() {
                return limit;
            }

            public void setLimit(Integer limit) {
                this.limit = limit;
            }

            public Integer getPage() {
                return page;
            }

            public void setPage(Integer page) {
                this.page = page;
            }

            public Integer getTotalPagination() {
                return totalPagination;
            }

            public void setTotalPagination(Integer totalPagination) {
                this.totalPagination = totalPagination;
            }

        }


        public class Order {

            @SerializedName("order_id")
            @Expose
            private String orderId;
            @SerializedName("name")
            @Expose
            private String name;
            @SerializedName("status")
            @Expose
            private String status;
            @SerializedName("date_added")
            @Expose
            private String dateAdded;
            @SerializedName("products")
            @Expose
            private Integer products;
            @SerializedName("total")
            @Expose
            private String total;
            @SerializedName("view")
            @Expose
            private String view;

            public String getOrderId() {
                return orderId;
            }

            public void setOrderId(String orderId) {
                this.orderId = orderId;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public String getDateAdded() {
                return dateAdded;
            }

            public void setDateAdded(String dateAdded) {
                this.dateAdded = dateAdded;
            }

            public Integer getProducts() {
                return products;
            }

            public void setProducts(Integer products) {
                this.products = products;
            }

            public String getTotal() {
                return total;
            }

            public void setTotal(String total) {
                this.total = total;
            }

            public String getView() {
                return view;
            }

            public void setView(String view) {
                this.view = view;
            }

        }

    }
}



