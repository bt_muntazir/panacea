package com.panacea.mvp.order_history;

import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.panacea.R;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.utility.SnackNotify;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.dashboard.adapter.SearchPartsAdapter;
import com.panacea.mvp.dashboard.model.PartsModel;
import com.panacea.mvp.order_history.adapter.OrderHistoryAdapter;
import com.panacea.mvp.order_history.model.OrderHistoryModel;
import com.panacea.mvp.order_history.presenter.OrderHistoryContractor;
import com.panacea.mvp.order_history.presenter.OrderHistoryPresenterImpl;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.nfc.tech.MifareUltralight.PAGE_SIZE;

public class OrderHistoryActivity extends AppCompatActivity implements OrderHistoryContractor.OrderHistoryView {

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.recyclerViewOrderHistory)
    RecyclerView recyclerViewOrderHistory;

    @BindView(R.id.imgViewNoRecordFound)
    ImageView imgViewNoRecordFound;

    ArrayList<OrderHistoryModel.Data.Order> arrayListOrderHistoryProducts;

    OrderHistoryPresenterImpl orderHistoryPresenter;
    OrderHistoryAdapter orderHistoryAdapter;

    //for paging
    boolean isEndReached;
    boolean mIsLoading;
    int page = 0;
    boolean isSearchClicked = false;
    LinearLayoutManager linearLayoutManager;

    String customerId;

    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);

        ButterKnife.bind(this);
        dbHelper=new DBHelper(this);

        customerId = LoginManager.getInstance().getUserData().getCustomerId();
        arrayListOrderHistoryProducts = new ArrayList<>();
        orderHistoryPresenter = new OrderHistoryPresenterImpl(this, this);
        orderHistoryAdapter = new OrderHistoryAdapter(this, arrayListOrderHistoryProducts);
        setLayoutManager();
        getPaginationData(false);
        //getOrderHistoryDataApi();
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateCartQty();
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onInternetError() {

        SnackNotify.checkConnection(onretryOrderHistoryApi, coordinateLayout);
    }

    OnClickInterface onretryOrderHistoryApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getOrderHistoryDataApi();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getOrderHistorySuccess(OrderHistoryModel.Data orderHistoryData) {


        arrayListOrderHistoryProducts = orderHistoryData.getOrders();
        OrderHistoryModel.Data.FilterData filterData = orderHistoryData.getFilterData();

        //set toolbar title
        setTitle(filterData.getPageTitle());

        int totalRecords = Integer.valueOf(filterData.getLimit());

        if (arrayListOrderHistoryProducts != null && arrayListOrderHistoryProducts.size() > 0) {

            //show recyclerview and hide image no record found
            recyclerViewOrderHistory.setVisibility(View.VISIBLE);
            imgViewNoRecordFound.setVisibility(View.GONE);

            if (arrayListOrderHistoryProducts.size() > (totalRecords - 1)) {
                isEndReached = false;
            } else {
                isEndReached = true;
            }
            orderHistoryAdapter.updateData(arrayListOrderHistoryProducts, isSearchClicked);
            isSearchClicked = false;
        } else {
            //hide recyclerview and show image no record found
            recyclerViewOrderHistory.setVisibility(View.GONE);
            imgViewNoRecordFound.setVisibility(View.VISIBLE);

            // SnackNotify.showMessage(getString(R.string.title_no_record_found), mainContainer);
            orderHistoryAdapter.updateData(arrayListOrderHistoryProducts, isSearchClicked);
            isSearchClicked = false;
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mIsLoading = false;
            }
        }, 1000);
    }

      /*------------------private method--------------------------------*/

    private void setTitle(String pageTitle) {
        txtViewTitle.setText(pageTitle);
    }

    private void getOrderHistoryDataApi() {

        orderHistoryPresenter.getOrderHistory(customerId, page);
    }

    private void getPaginationData(boolean isRetry) {

        mIsLoading = true;
        if (isRetry) {
            if (page == -1) {
                page = 0;
            }
            getOrderHistoryDataApi();
        } else {
            page = page + 1;
            getOrderHistoryDataApi();
        }
    }


    private void setLayoutManager() {
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewOrderHistory.setLayoutManager(linearLayoutManager);
        recyclerViewOrderHistory.setItemAnimator(new DefaultItemAnimator());
        recyclerViewOrderHistory.setHasFixedSize(true);
        recyclerViewOrderHistory.setNestedScrollingEnabled(false);
        setAdapter();
        recyclerViewOrderHistory.addOnScrollListener(mRecyclerViewOnScrollListener);
    }

    private void setAdapter() {
        orderHistoryAdapter = new OrderHistoryAdapter(this, arrayListOrderHistoryProducts);
        recyclerViewOrderHistory.setAdapter(orderHistoryAdapter);
    }

    // implement paging
    private RecyclerView.OnScrollListener mRecyclerViewOnScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            int visibleItemCount = linearLayoutManager.getChildCount();
            int totalItemCount = linearLayoutManager.getItemCount();
            int firstVisibleItemPosition = linearLayoutManager.findFirstVisibleItemPosition();

            if (!mIsLoading && !isEndReached) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                        && firstVisibleItemPosition >= 0 && totalItemCount >= PAGE_SIZE) {
                    getPaginationData(false);
                }
            }
        }
    };

    /*---------------------------public method-------------------------------------*/

    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }

      /*------------------on click method--------------------------------*/

    @OnClick(R.id.imgBackButton)
    public void backPressed() {
        onBackPressed();
    }

    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (dbHelper.getCartCount() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            }
        }
    }

}
