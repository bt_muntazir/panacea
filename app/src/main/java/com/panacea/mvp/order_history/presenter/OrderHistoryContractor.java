package com.panacea.mvp.order_history.presenter;

import com.panacea.mvp.commonMvp.BaseView;
import com.panacea.mvp.order_history.model.OrderHistoryModel;

/**
 * Created by Braintech on 21-12-2017.
 */

public interface OrderHistoryContractor {
    public interface OrderHistoryView extends BaseView {

        void getOrderHistorySuccess(OrderHistoryModel.Data orderHistoryData);
    }

    public interface Presenter {
        void getOrderHistory(String customerId,int page);
    }
}
