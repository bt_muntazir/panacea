package com.panacea.mvp.order_history.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.panacea.R;
import com.panacea.common.requestresponse.Const;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.mvp.dashboard.adapter.SearchPartsAdapter;
import com.panacea.mvp.dashboard.model.PartsModel;
import com.panacea.mvp.dashboard.model.RadiatorModel;
import com.panacea.mvp.order_history.model.OrderHistoryModel;
import com.panacea.mvp.order_information.OrderInformationActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 21-12-2017.
 */

public class OrderHistoryAdapter extends RecyclerView.Adapter {

    Activity activity;
    ArrayList<OrderHistoryModel.Data.Order> raDataArrayListResult;

    public OrderHistoryAdapter(Activity activity, ArrayList<OrderHistoryModel.Data.Order> raDataArrayList) {
        this.activity = activity;
        this.raDataArrayListResult = raDataArrayList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_order_history, parent, false);
        return new OrderHistoryAdapter.OrderHistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final OrderHistoryModel.Data.Order orderData = raDataArrayListResult.get(position);

        String orderId="(#"+orderData.getOrderId()+")";

        ((OrderHistoryAdapter.OrderHistoryViewHolder) holder).txtViewOrderId.setText(orderId);

        ((OrderHistoryAdapter.OrderHistoryViewHolder) holder).txtViewName.setText(orderData.getName().toString());
        ((OrderHistoryAdapter.OrderHistoryViewHolder) holder).txtViewNoOfProduct.setText(orderData.getProducts().toString());
        ((OrderHistoryAdapter.OrderHistoryViewHolder) holder).txtViewTotal.setText(orderData.getTotal().toString());
        ((OrderHistoryAdapter.OrderHistoryViewHolder) holder).txtViewDate.setText(orderData.getDateAdded().toString());
        ((OrderHistoryAdapter.OrderHistoryViewHolder) holder).txtViewStatus.setText(orderData.getStatus().toString());
        if (orderData.getStatus().equals(Const.PARAM_CANCELED)) {
            ((OrderHistoryAdapter.OrderHistoryViewHolder) holder).txtViewStatus.setTextColor(ContextCompat.getColor(activity,R.color.color_red));
        } else if(orderData.getStatus().equals(Const.PARAM_COMPLETE)){
            ((OrderHistoryAdapter.OrderHistoryViewHolder) holder).txtViewStatus.setTextColor(ContextCompat.getColor(activity,R.color.color_green));
        } else {
            ((OrderHistoryAdapter.OrderHistoryViewHolder) holder).txtViewStatus.setTextColor(ContextCompat.getColor(activity,R.color.color_darkgray));
        }


        ((OrderHistoryAdapter.OrderHistoryViewHolder) holder).relLayMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, OrderInformationActivity.class);
                intent.putExtra(ConstIntent.KEY_ORDER_ID,orderData.getOrderId() );
                activity.startActivity(intent);

            }
        });
    }

    @Override
    public int getItemCount() {
        return raDataArrayListResult.size();
    }

    public void updateData(ArrayList<OrderHistoryModel.Data.Order> raDataArrayList, boolean isSearchClick) {

        if (isSearchClick)
            raDataArrayListResult.clear();

        if (raDataArrayListResult != null && raDataArrayList != null) {
            for (int i = 0; i < raDataArrayList.size(); i++) {
                raDataArrayListResult.add(raDataArrayList.get(i));
            }
            notifyDataSetChanged();
        }
    }


    public class OrderHistoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.relLayMain)
        RelativeLayout relLayMain;

        @BindView(R.id.txtViewOrderId)
        AppCompatTextView txtViewOrderId;

        @BindView(R.id.txtViewName)
        AppCompatTextView txtViewName;

        @BindView(R.id.txtViewNoOfProduct)
        AppCompatTextView txtViewNoOfProduct;

        @BindView(R.id.txtViewTotal)
        AppCompatTextView txtViewTotal;

        @BindView(R.id.txtViewDate)
        AppCompatTextView txtViewDate;

        @BindView(R.id.txtViewStatus)
        AppCompatTextView txtViewStatus;

        public OrderHistoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
