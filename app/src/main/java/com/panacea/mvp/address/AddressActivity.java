package com.panacea.mvp.address;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.panacea.R;
import com.panacea.common.database.DBHelper;
import com.panacea.common.helpers.LoginManager;
import com.panacea.common.interfaces.OnClickInterface;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.common.utility.SnackNotify;
import com.panacea.mvp.add_edit_address.AddEditAddressActivity;
import com.panacea.mvp.address.adapter.AddressAdapter;
import com.panacea.mvp.address.model.AddressModel;
import com.panacea.mvp.address.presenter.AddressContractor;
import com.panacea.mvp.address.presenter.AddressPresenterImpl;
import com.panacea.mvp.cart.ShoppingCartActivity;
import com.panacea.mvp.dashboard.DashboardActivity;
import com.panacea.mvp.order_information.OrderInformationActivity;
import com.panacea.mvp.order_information.adapter.OrderInformationAdapter;
import com.panacea.mvp.youtube_instagram.YoutubeInstagramActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddressActivity extends AppCompatActivity implements AddressContractor.AddressView {

    @BindView(R.id.txtViewCartCount)
    TextView txtViewCartCount;

    @BindView(R.id.frameLayCart)
    FrameLayout frameLayCart;

    @BindView(R.id.coordinateLayout)
    CoordinatorLayout coordinateLayout;

    @BindView(R.id.imgBackButton)
    ImageView imgBackButton;

    @BindView(R.id.txtViewTitle)
    TextView txtViewTitle;

    @BindView(R.id.btnAddNewAddress)
    Button btnAddNewAddress;

    @BindView(R.id.imgViewNoRecordFound)
    ImageView imgViewNoRecordFound;

    @BindView(R.id.recyclerViewAddress)
    RecyclerView recyclerViewAddress;

    AddressPresenterImpl addressPresenter;
    AddressAdapter addressAdapter;

    DBHelper dbHelper;
    ArrayList<AddressModel.Data.Address> arrayListAddress;
    AddressModel.Data addressModelData;
    LinearLayoutManager linearLayoutManager;
    String customerId;
    String addressId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address);
        ButterKnife.bind(this);
        dbHelper=new DBHelper(this);
        customerId = LoginManager.getInstance().getUserData().getCustomerId();
        arrayListAddress = new ArrayList<>();
        addressPresenter = new AddressPresenterImpl(this, this);
        addressAdapter = new AddressAdapter(this, arrayListAddress);
    }

    @Override
    protected void onResume() {
        super.onResume();

        getAddressDataApi();

        updateCartQty();
    }

    @Override
    public void onError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void onInternetError() {

        SnackNotify.checkConnection(onretryAddressDataApi, coordinateLayout);
    }

    OnClickInterface onretryAddressDataApi = new OnClickInterface() {
        @Override
        public void onClick() {
            getAddressDataApi();
        }
    };

    @Override
    public void apiUnsuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void serverNotResponding(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void getAddressSuccess(AddressModel.Data addressModel) {

        addressModelData = addressModel;
        arrayListAddress = addressModel.getAddresses();
        //set toolbar title
        setTitle();

        if (arrayListAddress != null && arrayListAddress.size() > 0) {
            recyclerViewAddress.setVisibility(View.VISIBLE);
            imgViewNoRecordFound.setVisibility(View.GONE);
            setLayoutManager();
        }else
        {
            imgViewNoRecordFound.setVisibility(View.VISIBLE);
            recyclerViewAddress.setVisibility(View.GONE);
        }
    }

    @Override
    public void getDeleteAddressSuccess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);

        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            public void run() {
                getAddressDataApi();
            }
        }, 1000);

    }

    @Override
    public void getDeleteAddressUnSucess(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    @Override
    public void deleteAddressInternetError() {

        SnackNotify.checkConnection(onretryDeleteAddressApi, coordinateLayout);
    }

    OnClickInterface onretryDeleteAddressApi = new OnClickInterface() {
        @Override
        public void onClick() {
            deleteAddress(addressId);
        }
    };

    @Override
    public void deleteAddressServerError(String message) {
        SnackNotify.showMessage(message, coordinateLayout);
    }

    /*---------------------------------private method----------------------------------------------*/


    private void setLayoutManager() {
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerViewAddress.setLayoutManager(linearLayoutManager);
        recyclerViewAddress.setItemAnimator(new DefaultItemAnimator());
        recyclerViewAddress.setHasFixedSize(true);
        recyclerViewAddress.setNestedScrollingEnabled(false);
        setAdapter();
    }

    private void setAdapter() {
        addressAdapter = new AddressAdapter(this, arrayListAddress);
        recyclerViewAddress.setAdapter(addressAdapter);
    }

    private void setTitle() {
        txtViewTitle.setText(addressModelData.getFilterData().getPageTitle());
    }

    private void getAddressDataApi() {
        addressPresenter.getAddress(customerId);
    }

    private void deleteAddress(String addressId) {
        addressPresenter.getDeleteAddress(customerId, addressId);
    }

/*---------------------------------public method----------------------------------------------*/

    public void showDialogDeleteAddress(final String addressId) {

        this.addressId = addressId;


        AlertDialog.Builder alertDialog = new AlertDialog.Builder(AddressActivity.this);

       /* // Setting Dialog Title
        alertDialog.setTitle("Confirm Delete...");*/

        // Setting Dialog Message
        alertDialog.setMessage("Do you want to delete ?");

        // Setting Positive "Yes" Button
        alertDialog.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                deleteAddress(addressId);
            }
        });

        // Setting Negative "NO" Button
        alertDialog.setNegativeButton("NO", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();

    }

    public void updateCartQty() {

        int qty = dbHelper.getCartCount();
        if (qty == 0) {
            txtViewCartCount.setVisibility(View.GONE);
            txtViewCartCount.setText("" + qty);
        } else {

            txtViewCartCount.setVisibility(View.VISIBLE);
            txtViewCartCount.setText("" + qty);

           /* //shake telephone animation
            Animation vibrateImage = AnimationUtils.loadAnimation(this, R.anim.shake_cart);
            vibrateImage.start();
            frameLayCart.startAnimation(vibrateImage);*/
        }
    }

    /*---------------------------------On Click method----------------------------------------------*/

    @OnClick(R.id.frameLayCart)
    public void goToCart() {
        if (dbHelper.getCartCount() > 0) {

            if (LoginManager.getInstance().isLoggedIn()) {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            } else {
                startActivity(new Intent(this, ShoppingCartActivity.class));
            }
        }
    }

    @OnClick(R.id.imgBackButton)
    public void backPressed() {
        onBackPressed();
    }

    @OnClick(R.id.btnAddNewAddress)
    public void btnAddNewAddressClick() {

        Intent intent = new Intent(AddressActivity.this, AddEditAddressActivity.class);
        intent.putExtra(ConstIntent.KEY_ADD_EDIT_ADDRESS, ConstIntent.KEY_ADD);
        startActivity(intent);
    }
}
