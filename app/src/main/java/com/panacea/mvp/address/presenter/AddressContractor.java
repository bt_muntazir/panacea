package com.panacea.mvp.address.presenter;

import com.panacea.mvp.address.model.AddressModel;
import com.panacea.mvp.commonMvp.BaseView;
import com.panacea.mvp.login.model.LoginResponseModel;

/**
 * Created by Braintech on 21-12-2017.
 */

public interface AddressContractor {

    public interface AddressView extends BaseView {

        void getAddressSuccess(AddressModel.Data addressModel);

        void getDeleteAddressSuccess(String message);

        void getDeleteAddressUnSucess(String message);

        void deleteAddressInternetError();

        void deleteAddressServerError(String message);


    }

    public interface Presenter {
        void getAddress(String customerId);

        void getDeleteAddress(String customerId,String addressId);
    }
}
