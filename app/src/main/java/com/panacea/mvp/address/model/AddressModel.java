package com.panacea.mvp.address.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Braintech on 22-12-2017.
 */


public class AddressModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("data")
    @Expose
    private Data data;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }


    public class Data {

        @SerializedName("filter_data")
        @Expose
        private FilterData filterData;
        @SerializedName("addresses")
        @Expose
        private ArrayList<Address> addresses = null;

        public FilterData getFilterData() {
            return filterData;
        }

        public void setFilterData(FilterData filterData) {
            this.filterData = filterData;
        }

        public ArrayList<Address> getAddresses() {
            return addresses;
        }

        public void setAddresses(ArrayList<Address> addresses) {
            this.addresses = addresses;
        }


        public class Address {

            @SerializedName("address_id")
            @Expose
            private String addressId;
            @SerializedName("company")
            @Expose
            private String company;
            @SerializedName("address")
            @Expose
            private String address;
            @SerializedName("update")
            @Expose
            private String update;
            @SerializedName("delete")
            @Expose
            private String delete;

            public String getAddressId() {
                return addressId;
            }

            public void setAddressId(String addressId) {
                this.addressId = addressId;
            }

            public String getCompany() {
                return company;
            }

            public void setCompany(String company) {
                this.company = company;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getUpdate() {
                return update;
            }

            public void setUpdate(String update) {
                this.update = update;
            }

            public String getDelete() {
                return delete;
            }

            public void setDelete(String delete) {
                this.delete = delete;
            }

        }

        public class FilterData {

            @SerializedName("customer_id")
            @Expose
            private String customerId;
            @SerializedName("page_title")
            @Expose
            private String pageTitle;

            public String getCustomerId() {
                return customerId;
            }

            public void setCustomerId(String customerId) {
                this.customerId = customerId;
            }

            public String getPageTitle() {
                return pageTitle;
            }

            public void setPageTitle(String pageTitle) {
                this.pageTitle = pageTitle;
            }

        }
    }
}