package com.panacea.mvp.address.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.panacea.R;
import com.panacea.common.requestresponse.ConstIntent;
import com.panacea.mvp.add_edit_address.AddEditAddressActivity;
import com.panacea.mvp.address.AddressActivity;
import com.panacea.mvp.address.model.AddressModel;
import com.panacea.mvp.dashboard.adapter.SearchPartsAdapter;
import com.panacea.mvp.order_history.model.OrderHistoryModel;
import com.panacea.mvp.productdetail.ProductDetailActivity;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Braintech on 22-12-2017.
 */

public class AddressAdapter extends RecyclerView.Adapter {

    Activity activity;
    ArrayList<AddressModel.Data.Address> raDataArrayListResult;

    public AddressAdapter(Activity activity, ArrayList<AddressModel.Data.Address> raDataArrayList) {
        this.activity = activity;
        this.raDataArrayListResult = raDataArrayList;
    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(activity).inflate(R.layout.item_address, parent, false);
        return new AddressAdapter.AddressViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final AddressModel.Data.Address addressData = raDataArrayListResult.get(position);

        //Picasso.with(activity).load(productData.).into(((OrderInformationAdapter.OrderInformationViewHolder) holder).imgProduct);
        ((AddressAdapter.AddressViewHolder) holder).txtViewCompany.setText(addressData.getCompany());
        ((AddressAdapter.AddressViewHolder) holder).txtViewAddress.setText(addressData.getAddress());

        ((AddressAdapter.AddressViewHolder) holder).imgViewEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, AddEditAddressActivity.class);
                intent.putExtra(ConstIntent.KEY_ADD_EDIT_ADDRESS, ConstIntent.KEY_EDIT);
                intent.putExtra(ConstIntent.KEY_ADDRESS_ID,addressData.getAddressId() );
                activity.startActivity(intent);
            }
        });

        ((AddressAdapter.AddressViewHolder) holder).imgViewDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((AddressActivity)activity).showDialogDeleteAddress(addressData.getAddressId());
            }
        });

    }

    @Override
    public int getItemCount() {
        return raDataArrayListResult.size();
    }

    public class AddressViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txtViewCompany)
        AppCompatTextView txtViewCompany;

        @BindView(R.id.txtViewAddress)
        AppCompatTextView txtViewAddress;

        @BindView(R.id.imgViewEdit)
        ImageView imgViewEdit;

        @BindView(R.id.imgViewDelete)
        ImageView imgViewDelete;

        public AddressViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
