package com.panacea.mvp.address.presenter;

import android.app.Activity;

import com.panacea.BuildConfig;
import com.panacea.R;
import com.panacea.common.helpers.Progress;
import com.panacea.common.requestresponse.ApiAdapter;
import com.panacea.common.requestresponse.Const;
import com.panacea.mvp.address.model.AddressModel;
import com.panacea.mvp.commonMvp.model.CommonModel;
import com.panacea.mvp.login.model.LoginResponseModel;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Braintech on 21-12-2017.
 */

public class AddressPresenterImpl implements AddressContractor.Presenter{

    AddressContractor.AddressView addressView;
    Activity activity;
    JSONObject jsonObject;

    public AddressPresenterImpl(AddressContractor.AddressView addressView, Activity activity) {
        this.addressView = addressView;
        this.activity = activity;
    }

    @Override
    public void getAddress(String customerId) {

        try {
            ApiAdapter.getInstance(activity);
            gettingAddressData(customerId);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            addressView.onInternetError();
        }
    }

    @Override
    public void getDeleteAddress(String customerId, String addressId) {

        try {
            ApiAdapter.getInstance(activity);
            deleteAddress(customerId,addressId);
        } catch (ApiAdapter.NoInternetException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
            addressView.deleteAddressInternetError();
        }
    }

    private void gettingAddressData(String customerId){

        Progress.start(activity);

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_CUSTOMER_ID, customerId);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<AddressModel> getAddressOutput = ApiAdapter.getApiService().getAddressList("application/json", "no-cache", body);

        getAddressOutput.enqueue(new Callback<AddressModel>() {
            @Override
            public void onResponse(Call<AddressModel> call, Response<AddressModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    AddressModel addressModel = response.body();

                    if (addressModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        addressView.getAddressSuccess(addressModel.getData());
                    } else {
                        addressView.apiUnsuccess(activity.getString(R.string.error_username_pass));
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    addressView.apiUnsuccess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<AddressModel> call, Throwable t) {
                Progress.stop();
                addressView.apiUnsuccess(activity.getString(R.string.error_server));
            }
        });
    }

    private void deleteAddress(String customerId, String addressId){
        Progress.start(activity);

        try {
            jsonObject = new JSONObject();
            jsonObject.put(Const.PARAM_CUSTOMER_ID, customerId);
            jsonObject.put(Const.PARAM_ADDRESS_ID, addressId);

        } catch (JSONException ex) {
            ex.printStackTrace();
        }

        final RequestBody body = RequestBody.create(okhttp3.MediaType.parse("application/json; charset=utf-8"), (jsonObject.toString()));

        Call<CommonModel> getDeleteOutput = ApiAdapter.getApiService().deleteAddress("application/json", "no-cache", body);

        getDeleteOutput.enqueue(new Callback<CommonModel>() {
            @Override
            public void onResponse(Call<CommonModel> call, Response<CommonModel> response) {

                Progress.stop();

                try {
                    //getting whole data from response
                    CommonModel commonModel = response.body();

                    if (commonModel.getStatus().equals(Const.PARAM_SUCCESS)) {
                        addressView.getDeleteAddressSuccess(commonModel.getMessage());
                    } else {
                        addressView.getDeleteAddressUnSucess(commonModel.getMessage());
                    }
                } catch (NullPointerException exp) {
                    if (BuildConfig.DEBUG)
                        exp.printStackTrace();
                    addressView.getDeleteAddressUnSucess(activity.getString(R.string.error_server));
                }
            }

            @Override
            public void onFailure(Call<CommonModel> call, Throwable t) {
                Progress.stop();
                addressView.getDeleteAddressUnSucess(activity.getString(R.string.error_server));
            }
        });
    }
}
